package com.boosed.mm.activity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

import com.boosed.mm.R;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.GalleryAdapter;
import com.boosed.mm.ui.ImageCallback;

public class Approve extends AbstractActivity {// implements
	// OnGlobalLayoutListener {

	/** gallery adapter */
	private/* ArrayAdapter<Entry<String, Tuple<Boolean, String>>> */GalleryAdapter adapter;

	/** intent key for index of the referenced vehicle from rpc service */
	public static final String INDEX = "com.boosed.mm.approve.Index";

	/** intent key for actual car reference */
	public static final String CAR = "com.boosed.mm.approve.Car";

	/** intent key for the calling context's mode (search or inventory) */
	// public static final String MODE = "com.boosed.mm.classified.Mode";

	/** indicates whether search or inventory data is being used */
	// private int mode = MODE_SEARCH;

	/** list of cars */
	private final List<Car> cars;

	/**
	 * index of the currently shown item from service list (i.e., search or
	 * inventory)
	 */
	private int index = -2;

	/**
	 * the index of the selected image (only set if this activity is restored,
	 * e.g., during orientation changes)
	 */
	private int img = -1;

	/** max price for marker */
	// private Marker marker;

	/** key listener for price alert */
	// private NumberRangeKeyListener listener;

	private Map<String, Boolean> markings;

	public Approve() {
		// no-arg constructor
		cars = new ArrayList<Car>();
		markings = new HashMap<String, Boolean>();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the content view
		setContentView(R.layout.act_approve);

		// initialize the gallery
		initGallery();
		// layoutImage();

		if (initialized) {
			// initialize parameters from orientation change
			index = savedInstanceState.getInt(INDEX);
			// mode = savedInstanceState.getInt(MODE);
			img = savedInstanceState.getInt("image");
			ad = (Ad) savedInstanceState.getSerializable("ad");
			account = (Account) savedInstanceState.getSerializable("account");
			markings = (Map<String, Boolean>) savedInstanceState.getSerializable("markings");
			// gallery.setSelection(savedInstanceState.getInt("image"));
		} else {
			// set index & mode from the intent
			Intent i = getIntent();
			index = i.getIntExtra(INDEX, -2);
			// mode = i.getIntExtra(MODE, MODE_SEARCH);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// index
		outState.putInt(INDEX, index);

		// mode
		// outState.putInt(MODE, mode);

		// image
		outState.putInt("image", gallery.getSelectedItemPosition());

		// ad
		outState.putSerializable("ad", ad);

		// account
		outState.putSerializable("account", account);

		// markings
		outState.putSerializable("markings", (Serializable) markings);
	}

	/** suppress alert when user seeks to new price */
	// private Boolean suppressPriceAlert = false;

	// @Override
	// public Object onRetainNonConfigurationInstance() {
	// // capture the current state before orientation change
	// Integer[] data = new Integer[] { index, mode };
	// return data;
	// }

	// @Override
	// public void onConfigurationChanged(Configuration newConfig) {
	// super.onConfigurationChanged(newConfig);
	// //setContentView(R.layout.act_classified);
	// initLayout();
	// }

	// private boolean init = false;

	@Override
	public void onBind() {
		// sync cars
		// switch (mode) {
		// case MODE_INVENTORY:
		// // list from inventory
		// rpc.syncClassified(cars);
		// break;
		// case MODE_SEARCH:
		// default:
		// // listing from a search (by default)
		// rpc.syncSearch(cars);
		// break;
		// }
		rpc.syncSearch(cars);

		// sync bookmarks
		// rpc.syncBookmarks(bookmarks);

//		if (!initialized) {
			// increment index to prepare for showing previous
			index++;

			if (!showCar(false))
				// could not initialize car from service; use intent reference
				// instead
				setCar((Car) getIntent().getSerializableExtra(CAR));

			initialized = true;
//		} else
//			// just use intent data to init view
//			setCar((Car) getIntent().getSerializableExtra(CAR));
	}

	private Gallery gallery;
	// private ProgressBar loading;
	private ImageButton btnPrevious, btnNext/* , btnInquire, btnBookmark; */;
	private ImageView image;
	private TextView txtYear, txtCondition, txtModel, txtDoors, txtPrice, txtMileage, txtLocation, txtTransmission,
			txtInterior, txtExterior, txtEngine, txtDrivetrain, txtStock, txtVin;
	private TextView content, name, phone, email, address, url, status;
	private ImageSwitcher test;
	private RadioGroup pending;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// haptic feedback listener
		View.OnClickListener haptic = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			}
		};

		// pending marking
		pending = find(RadioGroup.class, R.id.pending);

		// init car fields
		txtModel = (TextView) findViewById(R.id.model);
		txtYear = (TextView) findViewById(R.id.year);
		txtCondition = (TextView) findViewById(R.id.condition);
		txtPrice = (TextView) findViewById(R.id.price);
		txtLocation = (TextView) findViewById(R.id.location);
		txtMileage = (TextView) findViewById(R.id.mileage);
		txtDoors = (TextView) findViewById(R.id.doors);
		txtExterior = (TextView) findViewById(R.id.exterior);
		txtInterior = (TextView) findViewById(R.id.interior);
		txtTransmission = (TextView) findViewById(R.id.transmission);
		txtEngine = (TextView) findViewById(R.id.engine);
		txtDrivetrain = (TextView) findViewById(R.id.drivetrain);

		txtStock = (TextView) findViewById(R.id.stock);
		txtVin = (TextView) findViewById(R.id.vin);

		// haptic enabled
		txtPrice.setOnClickListener(haptic);
		txtLocation.setOnClickListener(haptic);

		// init ad fields
		content = find(TextView.class, R.id.content);

		// init contact fields
		name = find(TextView.class, R.id.name);
		email = find(TextView.class, R.id.email);
		phone = find(TextView.class, R.id.phone);
		address = find(TextView.class, R.id.address);
		url = find(TextView.class, R.id.url);

		// haptic enabled
		name.setOnClickListener(haptic);
		email.setOnClickListener(haptic);
		phone.setOnClickListener(haptic);
		address.setOnClickListener(haptic);
		url.setOnClickListener(haptic);

		// init buttons
		btnPrevious = (ImageButton) findViewById(R.id.previous);
		btnNext = (ImageButton) findViewById(R.id.next);

		status = find(TextView.class, R.id.status);
		gallery = (Gallery) findViewById(R.id.gallery);
		image = (ImageView) findViewById(R.id.image);
		// loading = (ProgressBar) findViewById(R.id.loading);

		test = (ImageSwitcher) findViewById(R.id.test);
		test.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
		test.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));

		initLayout();

		test.setFactory(new ViewFactory() {

			@Override
			public View makeView() {
				ImageView image = new ImageView(Approve.this);
				image.setScaleType(ScaleType.MATRIX);
				// ImageSwitcher.LayoutParams params = new
				// ImageSwitcher.LayoutParams((int) scrWidth, (int) scrHeight);
				// params.gravity = Gravity.CENTER;
				image.setLayoutParams(new ImageSwitcher.LayoutParams((int) scrWidth, (int) scrHeight));
				// image.setImageMatrix(matrix);
				// image.setImageResource(R.drawable.icon);
				return image;
			}
		});
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	public void onControl(View view) {
		switch (view.getId()) {
		case R.id.previous:
			showCar(false);
			break;
		case R.id.next:
			showCar(true);
			break;
		case R.id.save:
			new MobTask<Void>(view) {

				@Override
				public Void doit() throws RemoteServiceFailureException {
					Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();

					// initialize ids
					fields.put(ad.id, FieldAd.ID_AD);
					fields.put(car.id, FieldAd.ID_CAR);

					// add all image markings
					for (Entry<String, Boolean> entry : markings.entrySet())
						fields.put(entry.getKey(), entry.getValue() ? FieldAd.IMG_APPROVE : FieldAd.IMG_REJECT);

					// pending text
					int selected = pending.getCheckedRadioButtonId();
					switch (selected) {
					case R.id.approve:
						fields.put(pending.hashCode(), FieldAd.AD_APPROVE);
						break;
					case R.id.none:
					case -1:
						break;
					default:
						// none was not selected, must be reject
						fields.put(pending.hashCode(), FieldAd.AD_REJECT);
						break;
					}

					rpc.service.updateAd(fields);
					return null;
				}

				@Override
				public void onSuccess(Void t) {
					finish();
				}
			}.execute();
			break;
		default:
			break;
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		// choose menu based upon approval status
		Entry<String, Tuple<Boolean, String>> entry = adapter.getItem(((AdapterContextMenuInfo) menuInfo).position);

		// init menu (if approved used image menu)
		getMenuInflater().inflate(entry.getValue().a ? R.menu.ctx_menu_image : R.menu.ctx_menu_approve, menu);

		// hide the primary menu option
		if (entry.getValue().a)
			menu.findItem(R.id.ctx_primary).setVisible(false);

		// set title
		menu.setHeaderTitle(R.string.ctx_hdr_image);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
		final String blobkey = ((Entry<String, Tuple<Boolean, String>>) gallery.getItemAtPosition(position)).getKey();

		switch (item.getItemId()) {
		case R.id.ctx_approve:
			markings.put(blobkey, true);
			return true;
		case R.id.ctx_reject:
			markings.put(blobkey, false);
			return true;
		case R.id.ctx_delete:
			if (confirm(item, R.string.cfm_del_img))
				new MobTask<Void>(R.string.stat_delete, true, item) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						rpc.deleteImage(ad.id, blobkey);
						return null;
					}

					@Override
					public void onSuccess(Void t) {
						ad.removeImage(blobkey);
						setAd(ad);
						// adapter.notifyDataSetChanged();
					}
				}.execute();
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public void onReplay(View replay) {
		// replay a control
		onControl(replay);
	}

	// @Override
	// public void onGlobalLayout() {
	// init();
	// }

	// the constrained viewing rectangle
	// private RectF viewRect = new RectF();
	// the projected new viewing rectangle
	// private RectF prjRect = new RectF();
	// the original image rect that is transformed
	// private RectF imgRect = new RectF();

	// private float maxZoom;
	// private float minZoom;

	/** matrix for manipulating the scale of the image */
	private Matrix matrix = new Matrix();

	// /**
	// * Initialize the size of widgets and the loaded image.
	// *
	// * @deprecated
	// */
	// private void init(int... dims) {
	// // width and height of the image view
	// // scrHeight = image.getHeight();
	// // scrWidth = image.getWidth();
	//
	// // Log.i("chat", "scr height: " + scrHeight + ", scr width: " +
	// // scrWidth);
	//
	// // width and height of the loaded image
	// // imgHeight = view.getDrawable().getIntrinsicHeight();
	// // imgWidth = view.getDrawable().getIntrinsicWidth();
	//
	// // Log.i("chat", "the view is: " + view.toString());
	// // Log.i("chat", "the draw is: " + view.getDrawable());
	// // int iw, ih;
	// float imgWidth, imgHeight;
	//
	// if (dims.length == 0) {
	// Drawable d = image.getDrawable();
	// imgWidth = d.getIntrinsicWidth();
	// imgHeight = d.getIntrinsicHeight();
	// } else {
	// imgWidth = dims[1];
	// imgHeight = dims[0];
	// }
	//
	// // Log.i("chat", "img height: " + imgHeight + ", img width: " +
	// // imgWidth);
	// // if (vheight > height || vwidth > width) {
	//
	// // set the min zoom so that img takes up entire screen
	// float magnify = Math.min(scrHeight / imgHeight, scrWidth / imgWidth);
	// Log.w("chat", "min zoom is: " + magnify + " sh: " + scrHeight +
	// ", sw: " + scrWidth + ", " + imgWidth + ", "
	// + imgHeight);
	// // max zoom is 3x image size
	// // maxZoom = magnify * 3;
	//
	// // establish the valid viewing rectangle
	// float dx = (scrWidth - imgWidth * magnify) / 2f;
	// float dy = (scrHeight - imgHeight * magnify) / 2f;
	// // viewRect.set(dx, dy, dx + imgWidth * minZoom, dy + imgHeight *
	// // minZoom);
	//
	// // zero out our cumulative changes
	// matrix.reset();
	//
	// // set proper size (the transformation matrix incorporates the
	// minimum
	// // zoom to optimize size of the image)
	// matrix.postScale(magnify, magnify, 0, 0);
	//
	// // center in middle of screen
	// matrix.postTranslate(dx, dy);
	//
	// // apply the changes
	// image.setImageMatrix(matrix);
	// }

	private void clear() {
		// clear images
		adapter.clear();
		content.setText("loading...");

		// clear account
		setField(name, "loading...");
		setField(email, null);
		setField(address, null);
		setField(phone, null);
		setField(url, null);
	}

	/**
	 * Initialize the size of widgets and the loaded image.
	 */
	private void format(ImageView image) {
		Drawable d = image.getDrawable();
		// imgRect.set(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
		float imgWidth = d.getIntrinsicWidth(), imgHeight = d.getIntrinsicHeight();

		// Log.i("chat", "img height: " + imgHeight + ", img width: " +
		// imgWidth);
		// if (vheight > height || vwidth > width) {

		// set the min zoom so that img takes up entire screen
		float magnify = Math.min(scrHeight / imgHeight, scrWidth / imgWidth);
		// Log.w("chat", "min zoom is: " + magnify + " sh: " + scrHeight +
		// ", sw: " + scrWidth + ", " + imgWidth + ", "
		// + imgHeight);
		// max zoom is 3x image size
		// maxZoom = magnify * 3;

		// establish the valid viewing rectangle
		float dx = (scrWidth - imgWidth * magnify) / 2f;
		float dy = (scrHeight - imgHeight * magnify) / 2f;
		// viewRect.set(dx, dy, dx + imgWidth * minZoom, dy + imgHeight *
		// minZoom);

		// zero out our cumulative changes
		matrix.reset();

		// set proper size (the transformation matrix incorporates the minimum
		// zoom to optimize size of the image)
		matrix.postScale(magnify, magnify, 0, 0);

		// center in middle of screen
		matrix.postTranslate(dx, dy);

		// apply the changes
		image.setImageMatrix(matrix);
	}

	// private void initContent() {
	// // get starting intent and set the car id
	//
	// if (index == -1) {
	// // set index & mode from the intent
	// Intent i = getIntent();
	// index = i.getIntExtra("car", -1);
	// mode = i.getIntExtra("mode", -1);
	// }
	//
	// if (index != -1 && mode != -1) {
	// // Car car = rpc.search.get(index);
	// // if (!"".equals(car.image))
	// index++;
	// showCar(false);
	// }
	// }

	private void initGallery() {
		// Work around a Cupcake bug
		// matrix.setTranslate(1f, 1f);
		image.setImageMatrix(matrix);
		// image.getViewTreeObserver().addOnGlobalLayoutListener(this);

		gallery.setAdapter(adapter = new GalleryAdapter(this, new ArrayList<Entry<String, Tuple<Boolean, String>>>()));

		// set the empty view
		gallery.setEmptyView(findViewById(android.R.id.empty));

		adapter.notifyDataSetChanged();

		gallery.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				Entry<String, Tuple<Boolean, String>> entry = adapter.getItem(position);

				Tuple<Boolean, String> tuple = entry.getValue();

				// set image
				setImage(tuple.b);

				// set status
				status.setText(tuple.a ? "approved" : (markings.containsKey(entry.getKey()) ? (markings.get(entry
						.getKey()) ? "marked approved" : "marked rejected") : "pending"));
			}
		});

		registerForContextMenu(gallery);
	}

	/** values set from initializing the layout */
	private float scrHeight, scrWidth;

	/** the orientation of the screen */
	private int orientation;

	private void initLayout() {
		// Toast.makeText(this, "width: " + metrics.widthPixels + ", height: " +
		// metrics.heightPixels, Toast.LENGTH_SHORT).show();

		// assign the orientation
		orientation = getResources().getConfiguration().orientation;

		switch (orientation) {
		case Configuration.ORIENTATION_LANDSCAPE:
			// layout the image to 60% of screen width
			int dim = (int) (metrics.widthPixels * 0.5);
			// fill_parent does not seem to work here?
			LayoutParams params = new LayoutParams(dim, metrics.heightPixels, Gravity.CENTER);
			// Toast.makeText(this, "resetting the layout to width: " + dim,
			// Toast.LENGTH_SHORT).show();
			image.setLayoutParams(params);
			scrHeight = metrics.heightPixels;
			scrWidth = dim;
			break;
		case Configuration.ORIENTATION_PORTRAIT:
			// layout the image to 40% of screen height
			dim = (int) (metrics.heightPixels * 0.4);
			// Toast.makeText(this, "resetting the layout to height: " + dim,
			// Toast.LENGTH_SHORT).show();
			params = new LayoutParams(/* LayoutParams.FILL_PARENT */metrics.widthPixels, dim, Gravity.CENTER);
			image.setLayoutParams(params);
			scrHeight = dim;
			scrWidth = metrics.widthPixels;
			break;
		}

		// ((ViewGroup)findViewById(android.R.id.content)).getChildAt(0).invalidate();
	}

	// public android.widget.ImageSwitcher.LayoutParams getLayoutParams() {
	// WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
	// switch (wm.getDefaultDisplay().getOrientation()) {
	// case Surface.ROTATION_90:
	// case Surface.ROTATION_270:
	// // layout the image to 60% of screen width
	// int dim = (int) (metrics.widthPixels * 0.6);
	// return new android.widget.ImageSwitcher.LayoutParams(dim,
	// LayoutParams.FILL_PARENT, Gravity.CENTER);
	// // image.setLayoutParams(params);
	// // break;
	// case Surface.ROTATION_0:
	// case Surface.ROTATION_180:
	// // layout the image to 40% of screen height
	// dim = (int) (metrics.heightPixels * 0.4);
	// return new
	// android.widget.ImageSwitcher.LayoutParams(LayoutParams.FILL_PARENT, dim,
	// Gravity.CENTER);
	// // image.setLayoutParams(params);
	// // break;
	// }
	//
	// return null;
	// }

	// private Long carId = null;
	private Car car = null;

	private Account account = null;

	private Ad ad = null;

	private void setAccount(Account account) {
		this.account = account;

		// mandatory
		SpannableStringBuilder ssb = new SpannableStringBuilder(account.name + (account.dealer ? " (dealer)" : ""));

		// only add clickable if current user is not owner
		final String owner = car.owner.getName();

		if (!owner.equals(rpc.accountId)) {
			ssb.setSpan(new ClickableSpan() {
				@Override
				public void onClick(View widget) {
					// start activity for dealer inventory

					// commit preference
					settings.edit().putInt(RpcService.PREF_INVENTORY, Inventory.MODE_DLR).commit();
					// settings.edit().putString(RpcService.PREF_DEALER,
					// car.owner.getName()).commit();

					// create intent for inventory
					Intent inventory = new Intent(Approve.this, Inventory.class);

					// set the dealer as the car owner
					inventory.putExtra(Inventory.DEALER, owner);

					// start new activity
					startActivity(inventory);
				}
			}, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

			name.setMovementMethod(LinkMovementMethod.getInstance());
		}

		// set name
		name.setText(ssb);

		// optional
		// String address = account.address == null ? "" : account.address;
		StringBuilder sb = new StringBuilder(account.address == null ? "" : (account.address + " "));

		if (account.postal != null)
			sb.append(account.postal);

		setField(this.address, sb.toString());// , Linkify.MAP_ADDRESSES);

		// the email field is already nullified on the server
		setField(email, account.email);// , Linkify.EMAIL_ADDRESSES);
		setField(phone, account.phone);// , Linkify.PHONE_NUMBERS);
		setField(url, account.url);// , Linkify.WEB_URLS);
	}

	/**
	 * Sets the <code>Ad</code>.
	 * 
	 * @param ad
	 * @return whether or not the image was set automatically
	 */
	private boolean setAd(Ad ad) {
		this.ad = ad;

		// clear the image approval markings
		markings.clear();

		// set the content
		String content = ad.getPending();// ad.getContent();

		boolean pendingText = content != null;

		// details content
		this.content.setText(pendingText ? content : "none");

		// radio buttons
		((RadioButton) pending.findViewById(R.id.none)).setChecked(true);
		pending.setVisibility(pendingText ? View.VISIBLE : View.GONE);

		// set the images
		// images.clear();
		// images.addAll(ad.getImages());
		toast("there are " + ad.getImages().size() + " photos in this ad");
		adapter.setImages(ad.getImages());

		// init the selected image
		if (img == -1) {
			// no image selected, queue the first gallery item
			if (!adapter.isEmpty())
				gallery.setSelection(0);

			return false;
		} else {
			// set the selected image (ad is loaded dynamically from
			// selected
			// car)

			// set gallery position to previous position
			gallery.setSelection(img);

			// set the image
			// handler.postDelayed(new Runnable() {
			// @Override
			// public void run() {
			setImage(adapter.getItem(img).getValue().b);

			// reset the shown image
			img = -1;

			return true;
			// }
			// }, 100);

			// // get the url value
			// final String url = adapter.getItem(img).getValue().b;
			// // final String url = ((Entry<String, Tuple<Boolean, String>)
			// // gallery.getSelectedItem()).getValue().b;
			//
			// // render this image for main
			// dm.loadImage(url, image, new ImageCallback() {
			// @Override
			// public void onLoad(int height, int width) {
			// // alert.dismiss();
			// // description.setText(photo.getDescription());
			// // init(height, width);
			// format(image);
			// image.setVisibility(View.VISIBLE);
			//
			// // Toast.makeText(ImageViewer.this,
			// // photo.getDescription(), Toast.LENGTH_LONG).show();
			//
			// // reset the value
			// img = -1;
			// }
			// });
		}

		// adapter.notifyDataSetChanged();
		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// adapter.notifyDataSetChanged();
		// // gallery.refreshDrawableState();
		// }
		// }, 200);

	}

	private void setCar(final Car car) {
		// set car id
		// carId = car.id;
		this.car = car;

		// hide/clear out the (previous) view
		clear();

		// set the image
		// if (!"".equals(car.image))
		// try {
		// final PhotoDto photo = (PhotoDto)
		// rpc.viewedProfile[photoIndex];

		// dm.loadImage(car.image, image, new ImageCallback() {
		// @Override
		// public void onLoad(int height, int width) {
		// // alert.dismiss();
		// // description.setText(photo.getDescription());
		// // init(height, width);
		// format(image);
		// image.setVisibility(View.VISIBLE);
		// // Toast.makeText(ImageViewer.this,
		// // photo.getDescription(), Toast.LENGTH_LONG).show();
		// }
		// });

		// Toast.makeText(this, photo.getUrl(),
		// Toast.LENGTH_LONG).show();
		// } catch (Exception e) {
		// e.printStackTrace();
		// Log.i("chat", "there has been an error");
		// // alert.dismiss();
		// }

		// set the title
		StringBuilder sb = new StringBuilder();
		sb.append(car.year).append(' ').append(car.model.getName());
		// (" [" + (index + 1) + "/" + cars.size() + "]")
		if (index != -1)
			sb.append(" [").append(index + 1).append('/').append(cars.size()).append("]");
		// String count = index == -1 ? "" : (" [" + (index + 1) + "/" +
		// cars.size() + "]");
		setTitle(/* getString(R.string.app_listing) + " - " + */sb);

		// quickly set image set to first car image (retains the size of the
		// gallery)
		// if (images.isEmpty()) {
		// Toast.makeText(this, "the images are empty",
		// Toast.LENGTH_SHORT).show();
		// Map<String, Tuple<Boolean, String>> data = new HashMap<String,
		// Tuple<Boolean, String>>();
		// data.put("", new Tuple<Boolean, String>(true, car.image));
		// data.put("b", new Tuple<Boolean, String>(true, car.image));
		// images.clear();
		// images.addAll(data.entrySet());

		// adapter.notifyDataSetChanged();
		// }

		// SpannableString ss = new SpannableString(car.model.getName());
		// ss.setSpan(new StyleSpan(Typeface.BOLD), 0, 5, 0);
		txtModel.setText(car.model.getName());

		// txtModel.setText(car.model.getName() + (car.trim == null ? "" :
		// car.trim));
		txtCondition.setText(car.conditions.get(0).toString());
		txtDoors.setText(Integer.toString(car.doors));
		txtDrivetrain.setText(car.drivetrain.getName());
		txtEngine.setText(car.engine.getName());
		txtExterior.setText(car.exterior.getName());
		txtInterior.setText(car.interior.getName());

		// SpannableString value = rpc.postal == null ? "" : (" (" +
		// df.format(car.distance) + " miles)");
		SpannableStringBuilder ssb = new SpannableStringBuilder(car.location);

		// // retrieved car from search and postal is not null
		// ssb
		// .append((mode != MODE_SEARCH ||
		// settings.getString(RpcService.PREF_POST, null) == null) ? " "
		// : (" (" + df.format(car.distance) + " miles) "));

		// specify exact or approximate location
		ssb.append(car.explicit ? ", exact" : ", approximate");

		// if (car.longitude != null)
		// ss = new SpannableString(car.location + value);
		ssb.setSpan(new ClickableSpan() {
			@Override
			public void onClick(View widget) {
				// txtLocation.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
				String uri = "http://maps.google.com/maps?q=" + car.latitude + "," + car.longitude + "+("
						+ car.model.getName() + ")";
				// String uri = "geo:0,0?q=" + car.latitude + "," +
				// car.longitude + " (location)";
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(/*
																		 * "geo:"
																		 * + car
																		 * .
																		 * latitude
																		 * + ","
																		 * + car
																		 * .
																		 * longitude
																		 */uri));

				startActivity(intent);
			}
		}, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		// ss.setSpan(us, 0, 2, 0);
		// ss.setSpan(new StyleSpan(Typeface.BOLD), 0, 5, 0);
		txtLocation.setMovementMethod(LinkMovementMethod.getInstance());
		txtLocation.setText(ssb);

		txtMileage.setText(df.format(car.mileage));

		// clickable price to show price alert dialog
		ssb = new SpannableStringBuilder("$" + df.format(car.price));
		txtPrice.setText(ssb);

		// txtStock.setText(car.stock == null ? "n/a" : car.stock);
		setField(txtStock, car.stock);// , 0);

		txtTransmission.setText(car.transmission.getName());
		txtYear.setText(Integer.toString(car.year));

		// set the text according to the orientation
		String novin = orientation == Configuration.ORIENTATION_LANDSCAPE ? "vin not available" : "not available";
		txtVin.setText(car.vin == null ? novin : car.vin);

		if (ad != null && ad.car.equals(car.getKey())) {
			// account is same as car owner; use existing ad and account
			setAccount(account);

			if (!setAd(ad)) {
				// set the image
				// toast("set the image from car!");
				// handler.postDelayed(new Runnable() {
				// @Override
				// public void run() {
				setImage(car.image);
				// }
				// }, 2000);
			}
		} else
			// load ad & account for new car
			new MobTask<Tuple<Ad, Account>>() {
				@Override
				public Tuple<Ad, Account> doit() throws RemoteServiceFailureException {
					// do not sanitize
					return rpc.service.loadAd(car.ad.getId(), false);
				}

				public void onFailure(int errResId) {
					// check to see if ad exists
					switch (errResId) {
					case R.string.exception_not_found:
						// the ad does not exist, finish activity
						// if (mode == MODE_SEARCH) {
						// // TODO: this result should be purged from the
						// // search results
						// }

						finish();
						break;
					default:
						// continue with failure
						super.onFailure(errResId);
					}
				}

				@Override
				public void onSuccess(Tuple<Ad, Account> t) {
					setAccount(t.b);

					if (!setAd(t.a)) {

						// // set the image
						// toast("set the image from car!");
						// handler.postDelayed(new Runnable() {
						// @Override
						// public void run() {
						setImage(car.image);
						// }
						// }, 2000);
					}
				}
			}.execute();
	}

	private void setImage(String url) {
		// initially hide image
		image.setVisibility(View.INVISIBLE);

		// render this image for main
		dm.loadImage(url, image, new ImageCallback() {
			@Override
			public void onLoad(int height, int width) {
				// size the main image appropriately
				format(image);

				// show visibility
				image.setVisibility(View.VISIBLE);
			}
		});
	}

	/** decimal format for numbers */
	private DecimalFormat df = new DecimalFormat("###,###,##0");

	private boolean showCar(boolean next) {

		// switch (mode) {
		// case MODE_INVENTORY:
		// // list from inventory
		// cars = rpc.inventory;
		// break;
		// case MODE_SEARCH:
		// default:
		// // listing from a search (by default)
		// cars = rpc.search;
		// break;
		// }

		// check bounds and adjust if necessary (these checks always return
		// index to its original or correct value)
		if (next && ++index == cars.size())
			index--;
		else if (!next && --index < 0)
			index++;

		// initialize button state; get the upper bound
		int limit = cars.size();

		// index is [1, limit)
		btnPrevious.setEnabled((index > 0) && (index < limit));

		// index is [0, limit - 1)
		btnNext.setEnabled((index > -1) && (index < --limit));

		// set the car
		try {
			// use the service results
			setCar(cars.get(index));
			return true;
		} catch (IndexOutOfBoundsException e) {
			// the service has been reset; no data available
			index = -1;
			return false;
		}
	}

	private void setField(TextView field, String value) {
		// set visibility
		((View) field.getParent()).setVisibility(value == null || value.length() == 0 ? View.GONE : View.VISIBLE);

		// set value
		field.setText(value);

		// linkify is now in the layout xml
		// if (mask != 0)
		// Linkify.addLinks(field, mask);
	}
}