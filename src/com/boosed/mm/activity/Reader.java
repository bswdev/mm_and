package com.boosed.mm.activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.boosed.mm.R;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.ListItemPhoto;

/**
 * Viewer for messages.
 * 
 * @author dsumera
 */
public class Reader extends AbstractActivity implements Controllable {

	/** the date format */
	private static final String FMT_DATE = " @ h:mm aa, M/d/yy";

	/** intent key for message index */
	public static final String INDEX = "com.boosed.mm.viewer.Index";

	/** intent key for actual message */
	public static final String MESSAGE = "com.boosed.mm.viewer.Message";

	/** message index */
	private int index = -1;

	/** car reference */
	private Car reference = null;

	/** message */
	private Message message = null;

	/** messages list */
	private List<Message> messages = new ArrayList<Message>();

	public Reader() {
		super(R.layout.title_viewer);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the layout (also assigns the handles)
		setContentView(R.layout.act_reader);

		if (initialized) {
			// set the index from the icicle
			index = savedInstanceState.getInt(INDEX);

			// set the reference from icicle
			reference = (Car) savedInstanceState.getSerializable("reference");
		} else
			// set the index from the intent
			index = getIntent().getIntExtra(INDEX, -1);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save the current index
		outState.putInt(INDEX, index);

		// save the reference
		outState.putSerializable("reference", reference);
	}

	private ListItemPhoto header;
	private TextView content;
	private ImageButton previous, next, respond;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// header (hide check)
		header = new ListItemPhoto(find(View.class, R.id.header));
		header.getCheck().setVisibility(View.GONE);
		header.setClickable(false);

		// content
		content = find(TextView.class, R.id.content);

		// buttons
		previous = find(ImageButton.class, R.id.previous);
		next = find(ImageButton.class, R.id.next);
		respond = find(ImageButton.class, R.id.respond);
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onReplay(View replay) {
		// replay control
		onControl(replay);
	}

	// /**
	// * Specify for the order of this message wrt total.
	// */
	// @Override
	// public void setTitle(CharSequence title) {
	// super.setTitle(getString(R.string.app_viewer) + " [" + title + "]");
	// }

	@Override
	public void onBind() {
		// sync with rpc
		rpc.syncMessages(messages);

		// show the appropriate message (if index negative, try first message)
		showMessage(index == -1 ? 0 : index);

		if (reference == null)
			// set the reference if not set by icicle
			updateCar();
		else
			// restore car from icicle
			setCar(reference);
	}

	@Override
	public void onControl(View view) {
		switch (view.getId()) {
		case R.id.header:
			// show this car in its own ad
			// respond to the current message

			// make sure that this is only clickable when reference is not null
			// if reference != null

			// don't set index and mode to open activity in stand-alone config
			Intent intent = new Intent(this, Classified.class);
			intent.putExtra(Classified.CAR, reference);
			intent.putExtra(Classified.MODE, Classified.MODE_STANDALONE);
			startActivity(intent);
			break;
		case android.R.id.title:
			// go back to messsages
			finish();
			break;
		case R.id.respond:
			// check if this is an alert
			if (message.getType() == MessageType.ALERTS)
				toast("cannot respond to alerts");
			else {
				// respond to the current message
				intent = new Intent(this, Composer.class);
				intent.putExtra(Composer.REFERENCE, reference);
				intent.putExtra(Composer.MESSAGE, message);
				startActivity(intent);
			}
			break;
		case R.id.delete:
			if (confirm(view, R.string.cfm_del_msg))
				// delete the message
				new MobTask<Void>(R.string.stat_delete, true, view) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						// rpc.service.removeMessages(Arrays.asList(message.id));
						rpc.removeMessages(Arrays.asList(message));
						return null;
					}

					@Override
					public void onSuccess(Void t) {
						// remove message from rpc.messages
						// rpc.messages.remove(message);
						rpc.syncMessages(messages);

						if (index < messages.size()) {
							// attempt to stay in place/move forward
							showMessage(index);
							updateCar();
						} else {
							// try moving back
							--index;
							if (index > -1) {
								// still messages to display
								showMessage(index);
								updateCar();
							} else
								// no more messages, close activity
								finish();
						}
					}
				}.execute();
			break;
		case R.id.previous:
			// show previous message
			if (--index < 0)
				index = 0;
			showMessage(index);
			updateCar();
			break;
		case R.id.next:
			// show next message
			int max = messages.size() - 1;
			if (++index > max)
				index = max;
			showMessage(index);
			updateCar();
			break;
		default:
			break;
		}
	}

	/**
	 * Create the thread of messages from the <code>Message</code> item.
	 * 
	 * @param message
	 * @return
	 */
	private Spanned getMessage(Message message) {
		SpannableStringBuilder ssb = new SpannableStringBuilder();

		for (Entry<Long, String> entry : message.messages.entrySet()) {
			// get content
			String content = entry.getValue();

			// set header according to content
			String header = (content.charAt(0) == Message.PRE_RCP ? message.recipientName : message.senderName)
					+ DateFormat.format(FMT_DATE, new Date(entry.getKey())) + ":\n";

			// set bold span on the header
			int start = ssb.length();
			ssb.append(header);
			ssb.setSpan(new StyleSpan(Typeface.BOLD), start, start + header.length(), 0);

			// add the header + content to builder
			ssb.append(content.substring(1)).append("\n\n");
		}

		return ssb;
	}

	/** decimal format for large numbers */
	private DecimalFormat df = new DecimalFormat("###,###,##0");

	/**
	 * Set the <code>Car</code> which this <code>Message</code> references.
	 * 
	 * @param car
	 */
	private void setCar(Car car) {
		// set reference
		reference = car;

		// set image
		final ImageView image = header.getImage();
		
		dm.display(image, reference.image + "=s160", new ImageCallback() {	
			@Override
			public void onLoad(int height, int width) {
				//image.refreshDrawableState();
				image.setVisibility(View.VISIBLE);
			}
		});
//		dm.loadImage(reference.image + "=s160", image, new ImageCallback() {
//			@Override
//			public void onLoad(int height, int width) {
//				image.refreshDrawableState();
//				image.setVisibility(View.VISIBLE);
//			}
//		});

		// set message "subject"
		String title = message.getReference();
		header.getTitle().setText(title.substring(0, title.indexOf(':') + 2) + /*reference.model.getName()*/reference.toString());

		// set year & colors
		header.getContent1().setText(
				reference.year + ", " + reference.exterior.getName() + "/" + reference.interior.getName());

		// set miles & price
		header.getContent2().setText(df.format(reference.mileage) + " miles, $" + df.format(reference.price));

		// set identifying data
		header.getEnd().setText(message.identifier);
	}

	/**
	 * Set the <code>Message</code> which drives this view. Use the current
	 * <code>index</code>.
	 * 
	 * @param message
	 */
	private void showMessage(int index) {
		int size = messages.size();

		if (index < size)
			// show message at given index
			message = messages.get(index);
		else {
			// invalid index, service likely terminated; use the intent message
			message = (Message) getIntent().getSerializableExtra(MESSAGE);

			// set the index to 0, size to 1
			index = 0;
			size = 1;
		}

		if (!message.getState(MessageState.READ))
			// mark message as read
			new MobTask<Void>() {
				@Override
				public Void doit() throws RemoteServiceFailureException {
					// also ensure that notifications are handled
					rpc.markRead(message.id);
					return null;
				}

				@Override
				public void onFailure(int errResId) {
					// ignore errors for session timeout or unauthorized,
					// message remains unmarked
				}

				@Override
				public void onSuccess(Void t) {
					message.setState(MessageState.READ, true);
				}
			}.execute();

		// set title
		setTitle((index + 1) + " of " + size);

		// update buttons

		// previous item has index > -1
		previous.setEnabled((index - 1) > -1);

		// next item has index < size
		next.setEnabled((index + 1) < size);

		// respond if sender is not null
		respond.setEnabled(/*message.getType() != MessageType.ALERTS*/message.sender != null);

		// set the message content
		content.setText(getMessage(message));
	}

	/**
	 * Set the instance of <code>Car</code> which the <code>Message</code> will
	 * reference.
	 * 
	 * @param reference
	 */
	private void updateCar() {
		// set the reference from the message
		new MobTask<Car>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				// disable header for clicks
				header.setClickable(false);
			}

			@Override
			public Car doit() throws RemoteServiceFailureException {
				return rpc.service.loadCar(message.reference.getId());
			}

			@Override
			public void onSuccess(Car t) {
				// set the car
				setCar(t);

				// enable header for clicks
				header.setClickable(true);
			}
		}.execute();
	}
}