package com.boosed.mm.activity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;

import com.boosed.mm.R;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseArrayAdapter;
import com.boosed.mm.ui.CheckedImage;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.ListItemPhoto;

/**
 * Activity for viewing <code>Message</code> content.
 * 
 * @author dsumera
 */
public class Support extends AbstractListActivity implements Controllable {

	/** intent key for car id (as long) */
	// public static final String REFERENCE =
	// "com.boosed.mm.messages.Reference";

	/** intent key for resource id of desired [reference] message type */
	public static final String TYPE = "com.boosed.mm.messages.Type";

	/** pendings */
	private final List<Car> pending;

	/** message cursor */
	private String cursor = null;

	/** message adapter */
	private ArrayAdapter<Car> adapter;

	/** the id of the car reference */
	// private Long carId;

	// /** the id of the view/button to initialize activity */
	// private int viewId;

	/** set of ads marked for deletion */
	private final Set<Car> deletions;

	public Support() {
		// set the empty string
		super(R.string.warn_messages);

		// init fields
		pending = new ArrayList<Car>();
		deletions = new HashSet<Car>();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (initialized)
			// retrieve deletions
			deletions.addAll((Set<Car>) savedInstanceState.getSerializable("deletions"));

		// add the footer before setting the adapter
		mList.addFooterView(footer);

		// set adapter
		setListAdapter(adapter = new BaseArrayAdapter<Car, ListItemPhoto>(this, pending, R.layout.list_item_photo) {

			/** formatter for decimal values */
			private DecimalFormat format = new DecimalFormat("###,###,##0");

			@Override
			protected ListItemPhoto createView(View view) {
				// create view holder
				return new ListItemPhoto(view);
			}

			@Override
			protected void render(Car item, final ListItemPhoto row) {
				// get image
				final ImageView image = row.getImage();

				// set image
				dm.loadImage(item.image + "=s160", image, new ImageCallback() {
					@Override
					public void onLoad(int height, int width) {
						//image.refreshDrawableState();
						image.setVisibility(View.VISIBLE);
					}
				});

				// set text
				row.getTitle().setText(item.model.getName());

				// set year & colors
				StringBuilder sb = new StringBuilder(Integer.toString(item.year));
				sb.append(", ").append(item.exterior.getName());
				sb.append("/").append(item.interior.getName());
				row.getContent1().setText(sb);

				// set miles & price
				row.getContent2().setText(format.format(item.mileage) + " miles, $" + format.format(item.price));

				sb = new StringBuilder(item.location);
				if (settings.getString(RpcService.PREF_POST, null) != null)
					// only show miles if postal code was provided
					sb.append(" (").append(item.distance).append(" miles)");

				// set location
				row.getEnd().setText(sb);

				// set checked
				CheckedImage check = row.getCheck();
				check.setChecked(deletions.contains(item));
				check.setTag(item);
			}

			@Override
			public void notifyDataSetChanged() {
				super.notifyDataSetChanged();

				// reset visibility of shadow if list is empty
				shadow.setVisibility(pending.isEmpty() ? View.GONE : View.VISIBLE);
			}
		});

		// don't need footer so remove it now
		mList.removeFooterView(footer);

		// set divider
		// mList.setDivider(getResources().getDrawable(android.R.drawable.divider_horizontal_bright));

		// register list for context menus
		// registerForContextMenu(mList);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// show the message
		approveCar(position);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save deletions
		outState.putSerializable("deletions", (Serializable) deletions);
	}

	/** button controls */
	private ImageButton /* btnInquiries, btnResponses, btnAll, */btnDelete;

	/** shadow for empty view */
	private ImageView shadow;

	/** the footer */
	private View footer;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// shadow for empty list
		shadow = (ImageView) findViewById(R.id.list_shadow);

		// need to determine if this is for specific vehicle or general messages
		//Intent intent = getIntent();

		// get intent reference id if it exists
		// carId = intent.getLongExtra(REFERENCE, -1);

		// if (carId == -1) {
		// // initialize for general message viewing
		// carId = null;
		// viewId = settings.getInt(RpcService.PREF_MESSAGE, R.id.all);
		//
		// // set the controls
		// View view = getLayoutInflater().inflate(R.layout.ctl_messages, null);
		// LayoutParams lp = new LayoutParams(0, LayoutParams.FILL_PARENT, 1);
		// controls.addView(view, lp);
		// controls.setVisibility(View.VISIBLE);
		// } else {
		// initialize for referenced messages (inquiries & responses)
		// viewId = intent.getIntExtra(TYPE, R.id.inquiries);

		// set the empty string
		setEmptyString(R.string.warn_inquiries);

		// set the controls
		View view = getLayoutInflater().inflate(R.layout.ctl_reference, null);
		LayoutParams lp = new LayoutParams(0, LayoutParams.FILL_PARENT, 1);
		controls.addView(view, lp);
		controls.setVisibility(View.VISIBLE);
		// }

		// // assign buttons
		// btnAll = find(ImageButton.class, R.id.all);
		// btnInquiries = find(ImageButton.class, R.id.inquiries);
		// btnResponses = find(ImageButton.class, R.id.responses);
		btnDelete = find(ImageButton.class, R.id.delete);

		// create the footer
		footer = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_footer, null);
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		// position of item
		//final int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;

		switch (item.getItemId()) {
		default:
			return super.onContextItemSelected(item);
		}
	}

	// @Override
	// public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo
	// menuInfo) {
	// super.onCreateContextMenu(menu, v, menuInfo);
	//
	// // get message
	// Car msg = adapter.getItem(((AdapterContextMenuInfo) menuInfo).position);
	//
	// // set title
	// menu.setHeaderTitle(msg.model.getName());
	//
	// // inflate
	// getMenuInflater().inflate(R.menu.ctx_menu_message, menu);
	//
	// // init menu; repliable?
	// // menu.findItem(R.id.ctx_reply).setVisible(msg.getType() !=
	// // MessageType.ALERTS);
	// }

	@Override
	public void onReplay(View replay) {
		// replay a control
		onControl(replay);
	}

	@Override
	public void onBind() {
		// always refresh the messages on binding/resume (viewId initially set
		// in assignhandles)... NEVER NEED TO SYNC!

		// if (carId == null)
		// // retrieve view from preferences
		// viewId = settings.getInt(RpcService.PREF_MESSAGE, R.id.all);
		// else
		// // initialize for referenced messages (inquiries & responses)
		// viewId = getIntent().getIntExtra(TYPE, R.id.inquiries);

		// perform load
		onControl(findViewById(R.id.refresh));
	}

	@Override
	public void onControl(View view) {
		final int id = view.getId();

		switch (id) {
		case R.id.refresh:
			// case R.id.inquiries:
			// case R.id.responses:
			// update the controls
			// updateControls(id);

			// load the messages
			new MobTask<String>(view) {
				@Override
				protected void onPreExecute() {
					super.onPreExecute();

					// set the loading string
					setEmptyString(R.string.warn_load);

					// clear the messages
					pending.clear();
					cursor = null;

					// update the view
					adapter.notifyDataSetChanged();
				}

				@Override
				public String doit() throws RemoteServiceFailureException {
					// load messages
					// return rpc.loadMessages(carId, false);
					return rpc.loadPending(false);
				}

				@Override
				public void onFailure(int errResId) {
					// synchronize content
					sync(false);

					// continue with failure
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(String t) {
					// set cursor
					cursor = t;

					// synchronize content
					sync(true);
				}
			}.execute();
			break;
		case R.id.delete:
			if (confirm(view, R.string.cfm_del_msgs) && !deletions.isEmpty())
				// delete all items in deletion set
				new MobTask<Void>(R.string.stat_delete, true, view) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						// execute deletion
						// rpc.removeMessages(deletions);
						return null;
					}

					@Override
					public void onFailure(int errResId) {
						// synchronize content
						sync(false);

						// continue with failure
						super.onFailure(errResId);
					}

					@Override
					public void onSuccess(Void t) {
						// clear deletion data
						deletions.clear();

						// sync content
						sync(true);
					}
				}.execute();
			break;
		// case R.id.refresh:
		// // TODO: use existing view id to refresh the list
		// // onControl(findViewById(viewId));
		// onBind();
		// break;
		case android.R.id.checkbox:
			// handle bookmarking
			CheckedImage check = (CheckedImage) view;
			if (check.isChecked())
				// remove the item from bin
				deletions.remove((Car) check.getTag());
			else
				// add item to bin
				deletions.add((Car) check.getTag());

			// set status for delete button
			btnDelete.setEnabled(!deletions.isEmpty());

			// update view
			adapter.notifyDataSetChanged();
			break;
		default:
			break;
		}
	}

	/** fetching status */
	private boolean fetching = false;

	/** footer status */
	private boolean hasFooter = false;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

		// ensure that:
		// 1) rpc available
		// 2) message cursor is not null
		// 3) not currently fetching
		// 4) list adapter has items
		// 5) visible item is w/in 10 (look ahead) of tail
		if (rpc != null && cursor != null & !fetching && totalItemCount != 0
				&& (firstVisibleItem + visibleItemCount + 10) >= totalItemCount)

			// perform a fetch
			new MobTask<String>() {
				@Override
				protected void onPreExecute() {
					// set fetching status
					fetching = true;

					// show footer
					if (!hasFooter) {
						mList.addFooterView(footer);
						hasFooter = true;
					}

					super.onPreExecute();
				}

				@Override
				public String doit() throws RemoteServiceFailureException {
					// return rpc.loadMessages(carId, true);
					return rpc.loadPending(true);
				}

				@Override
				public void onFailure(int errResId) {
					// sync content
					sync(false);

					// continue with failure
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(String t) {
					// set cursor
					cursor = t;

					// remove footer if at end of content
					if (cursor == null) {
						mList.removeFooterView(footer);
						hasFooter = false;
					}

					// sync content
					sync(true);
				}

				@Override
				public void onPostExecute(String result) {
					// set fetching status
					fetching = false;

					// continue
					super.onPostExecute(result);
				}
			}.execute();
	}

	/**
	 * Show the vehicle at the given position.
	 * 
	 * @param position
	 */
	private void approveCar(int position) {
		Intent intent = new Intent(this, Approve.class);
		intent.putExtra(Approve.CAR, pending.get(position));
		intent.putExtra(Approve.INDEX, position);
		startActivity(intent);
	}

	/**
	 * Sync the rpc data with this activity.
	 */
	private void sync(boolean success) {
		// sync model (not necessary if we failed)
		if (success)
			// rpc.syncMessages(pending);
			rpc.syncSearch(pending);

		// update the delete button (deletions & messages are both not empty)
		boolean pendingEmpty = pending.isEmpty();
		btnDelete.setEnabled(!deletions.isEmpty() && !pendingEmpty);

		// update the empty message
		if (pendingEmpty)
			setEmptyString(R.string.warn_listings);
		// switch (settings.getInt(RpcService.PREF_MESSAGE, R.id.all)) {
		// case R.id.inquiries:
		// setEmptyString(R.string.warn_inquiries);
		// break;
		// case R.id.responses:
		// setEmptyString(R.string.warn_responses);
		// break;
		// case R.id.all:
		// default:
		// setEmptyString(R.string.warn_messages);
		// break;
		// }

		// // if this is a general message view, update controls
		// if (carId == null)
		// switch (viewId) {
		// case R.id.inquiries:
		// btnAll.setEnabled(true);
		// btnInquiries.setEnabled(false);
		// btnResponses.setEnabled(true);
		// break;
		// case R.id.responses:
		// btnAll.setEnabled(true);
		// btnInquiries.setEnabled(true);
		// btnResponses.setEnabled(false);
		// break;
		// case R.id.all:
		// default:
		// btnAll.setEnabled(false);
		// btnInquiries.setEnabled(true);
		// btnResponses.setEnabled(true);
		// break;
		// }

		// update view
		adapter.notifyDataSetChanged();
	}

	// /**
	// * Update control panel
	// */
	// private void updateControls(int viewId) {
	// // update state
	// this.viewId = viewId;
	//
	// // update preferences
	// //settings.edit().putInt(RpcService.PREF_MESSAGE, viewId).commit();
	//
	// // update title
	// // switch (viewId) {
	// // case R.id.all:
	// // setTitle(R.string.app_messages);
	// // break;
	// // case R.id.inquiries:
	// // setTitle(R.string.app_inquiries);
	// // break;
	// // case R.id.responses:
	// // setTitle(R.string.app_responses);
	// // break;
	// // }
	//
	// // disable all controls
	// // btnAll.setEnabled(false);
	// // btnInquiries.setEnabled(false);
	// // btnResponses.setEnabled(false);
	// }
}