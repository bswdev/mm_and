package com.boosed.mm.activity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.boosed.mm.R;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.ListItemPhoto;

public class Details extends AbstractActivity implements Controllable {

	/** the reference vehicle for the message */
	private Car car = null;

	/** the reference for the ad */
	private Ad ad = null;

	/** intent key for ad reference */
	public static final String AD = "com.boosed.mm.details.Ad";

	/** intent key for car reference */
	public static final String CAR = "com.boosed.mm.details.Car";

	// /** the date format */
	// private static final String FMT_DATE = "M/d/yy, h:mm aa";

	public Details() {
		super(R.layout.title_save);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the layout (also assigns the handles)
		setContentView(R.layout.act_details);

		// set title
		setTitle(R.string.app_details);

		// set title icon
		setTitleIcon(R.drawable.ic_menu_edit);
		
		// retrieve the intent
		Intent intent = getIntent();

		if (initialized) {
			// restore from previous activity
			setAd((Ad) savedInstanceState.getSerializable(AD));
			setCar((Car) savedInstanceState.getSerializable(CAR));
			// setCar((Car) intent.getSerializableExtra(CAR));
		} else {
			// initialize from intent
			setAd((Ad) intent.getSerializableExtra(AD));
			setCar((Car) intent.getSerializableExtra(CAR));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save the ad
		outState.putSerializable(AD, ad);

		// save the car
		outState.putSerializable(CAR, car);
	}

	private TextView details, tip;
	private EditText pending;
	private ListItemPhoto header;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// fields
		details = find(TextView.class, R.id.details);
		tip = find(TextView.class, R.id.tip);
		pending = find(EditText.class, R.id.pending);
		
		// header
		header = new ListItemPhoto(find(View.class, R.id.header));
		header.getCheck().setVisibility(View.GONE);
		
		// linkify
		Linkify.addLinks(tip, Linkify.ALL);
		tip.setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void onReplay(View replay) {
		// replay view
		onControl(replay);
	}

//	@Override
//	public void setTitle(CharSequence title) {
//		super.setTitle(getString(R.string.app_details) + " [" + title + "]");
//	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onBind() {
		// what happens when we bind to service?
	}

	@Override
	public void onControl(View view) {
		switch (view.getId()) {
		case R.id.header:
			// don't set index and mode to open activity in stand-alone config
			Intent intent = new Intent(this, Classified.class);
			intent.putExtra(Classified.CAR, car);
			intent.putExtra(Classified.MODE, Classified.MODE_STANDALONE);
			startActivity(intent);
			break;
		case R.id.copy:
			// copy over the details to pending
			pending.setText(ad.getContent());
			break;
		case R.id.cancel:
			if (confirm(view, R.string.cfm_exit))
				// discard details changes
				finish();
			break;
		case R.id.save:
			if (confirm(view, R.string.cfm_det))
				// commit the details
				new MobTask<Void>(R.string.stat_save, true) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
						fields.put(ad.id, FieldAd.ID_AD);
						fields.put(pending.getText().toString(), FieldAd.AD);

						rpc.service.updateAd(fields);
						return null;
					}

					@Override
					public void onSuccess(Void t) {
						// this does not change ad immediately so no need for
						// result
						finish();
					}
				}.execute();
			break;
		default:
			break;
		}
	}

	// /**
	// * Set the original <code>Message</code> if this is a response.
	// *
	// * @param message
	// */
	// private void setMessage(Message message) {
	// this.message = message;
	//
	// if (message == null)
	// original.setVisibility(View.GONE);
	// else {
	// try {
	// // show the message with markup
	// // String intro = "previous message:\n\n";
	// String intro = message.senderName
	// + (message.getType() == MessageType.RESPONSES ? " responded " :
	// " inquired ") + "("
	// + DateFormat.format(FMT_DATE, new Date(message.time)) + "):";
	// SpannableString ss = new SpannableString(intro + "\n" + message.message);
	// // message.senderName + " wrote:\n" + message.message)
	// // bold the header
	// ss.setSpan(new StyleSpan(Typeface.BOLD), 0, intro.length(), 0);
	// // italicize the name
	// // int len = date.length();
	// // ss.setSpan(new StyleSpan(Typeface.ITALIC), len, len +
	// // message.senderName.length(), 0);
	// original.setText(ss);
	// // original.setText(DateFormat.format(FMT_DATE, new
	// // Date(message.time)) + ", " + message.senderName
	// // + " wrote:" + "\n" + message.message);
	// original.setVisibility(View.VISIBLE);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// }

	/**
	 * Set the <code>Ad</code> for this view.
	 * 
	 * @param ad
	 */
	private void setAd(Ad ad) {
		this.ad = ad;

		// set pending
		pending.setText(ad.getPending());

		// set details
		String content = ad.getContent();
		details.setText(content == null || content.length() == 0 ? "none" : content);
		
		// set tip
		tip.setText(Html.fromHtml(MessageFormat.format(getString(R.string.fmt_tip), ad.key)));
	}

	private static final DecimalFormat df = new DecimalFormat("###,###,##0");

	/**
	 * Set the instance of <code>Car</code> which the <code>Message</code> will
	 * reference.
	 * 
	 * @param car
	 */
	private void setCar(Car car) {
		this.car = car;

		// set title
		//setTitle(car.year + " " + car.model.getName());

		// set image
		final ImageView image = header.getImage();
		
		dm.display(image, car.image + "=s160", new ImageCallback() {
			@Override
			public void onLoad(int height, int width) {
				//image.refreshDrawableState();
				image.setVisibility(View.VISIBLE);
			}
		});
		
//		dm.loadImage(car.image + "=s160", image, new ImageCallback() {
//			@Override
//			public void onLoad(int height, int width) {
//				image.refreshDrawableState();
//				image.setVisibility(View.VISIBLE);
//			}
//		});

		// set first row content
		header.getContent1().setText(
				car.year + ", " + car.exterior.getName() + "/" + car.interior.getName());

		// if (message == null) {
		// set items as inquiry
		// set title
		header.getTitle().setText(/*car.model.getName()*/car.toString());

		// set first row content
		header.getContent1().setText(
				car.year + ", " + car.exterior.getName() + "/" + car.interior.getName());

		// set second row content
		header.getContent2().setText(df.format(car.mileage) + " miles, $" + df.format(car.price));

		// stock & vin
		StringBuilder sb = new StringBuilder();
		sb.append("st: ").append(car.stock.equals("") ? "n/a" : car.stock);
		sb.append(", vin: ").append(car.vin == null ? "n/a" : car.vin);
		header.getEnd().setText(sb.toString());
		// } else {
		// // set items as response
		// // set title
		// String title = message.getReference();
		// title = title.substring(0, title.indexOf(" ") + 1);
		// header.getTitle().setText(title + " " + reference.model.getName());
		//
		// // set first row content
		// header.getContent1().setText(
		// reference.year + ", " + reference.exterior.getName() + "/" +
		// reference.interior.getName());
		//
		// // set second row content
		// header.getContent2().setText(message.identifier);
		//
		// // only show miles if postal code was provided
		// header.getEnd().setText(message.senderName + " @ " +
		// DateFormat.format(FMT_DATE, new Date(message.time)));
		// }
	}
}