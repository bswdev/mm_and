package com.boosed.mm.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;

import com.boosed.mm.R;

public abstract class AbstractListActivity extends AbstractActivity implements OnScrollListener {

	/* This field should be made private, so it is hidden from the SDK. {@hide} */
	protected ListAdapter mAdapter;

	/* This field should be made private, so it is hidden from the SDK. {@hide} */
	protected ListView mList;

	// private Handler mHandler = new Handler();
	private boolean mFinishedStart = false;

	/* the resource id for the text to show when empty */
	private int emptyResId;

	/* the resource id for the text for the function button */
	// private int funcResId;

	/* whether to enable the display of the function button */
	// private boolean showFunction, funcTrans;

	/** the empty text */
	private TextView text;

	private Runnable mRequestFocus = new Runnable() {
		public void run() {
			mList.focusableViewAvailable(mList);
		}
	};

	/**
	 * Initialize the list activity with the given empty string resource id.
	 * 
	 * @param emptyResId
	 */
	public AbstractListActivity(int emptyResId, int titleLayoutResId) {
		super(titleLayoutResId);
		this.emptyResId = emptyResId;
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the standard content view
		setContentView(R.layout.act_base_list);

		// set custom title
		// getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
		// R.layout.title_crowds);

		// set the scroll listener
		mList.setOnScrollListener(this);
	}

	/**
	 * This method will be called when an item in the list is selected.
	 * Subclasses should override. Subclasses can call
	 * getListView().getItemAtPosition(position) if they need to access the data
	 * associated with the selected item.
	 * 
	 * @param l
	 *            The ListView where the click happened
	 * @param v
	 *            The view that was clicked within the ListView
	 * @param position
	 *            The position of the view in the list
	 * @param id
	 *            The row id of the item that was clicked
	 */
	protected void onListItemClick(ListView l, View v, int position, long id) {
	}

	/**
	 * Ensures the list view has been created before Activity restores all of
	 * the view states.
	 * 
	 * @see Activity#onRestoreInstanceState(Bundle)
	 */
	@Override
	protected void onRestoreInstanceState(Bundle state) {
		ensureList();
		super.onRestoreInstanceState(state);
	}

	/**
	 * Updates the screen state (current list and other views) when the content
	 * changes.
	 * 
	 * @see Activity#onContentChanged()
	 */
	@Override
	public void onContentChanged() {
		super.onContentChanged();
		// TextView emptyView = (TextView) findViewById(android.R.id.empty);
		View emptyView = findViewById(android.R.id.empty);
		mList = (ListView) findViewById(android.R.id.list);
		if (mList == null) {
			throw new RuntimeException("Your content must have a ListView whose id attribute is "
					+ "'android.R.id.list'");
		}
		if (emptyView != null) {
			text = (TextView) findViewById(R.id.empty);
			text.setText(emptyResId);
			mList.setEmptyView(emptyView);
		}
		mList.setOnItemClickListener(mOnClickListener);

		if (mFinishedStart) {
			setListAdapter(mAdapter);
		}
		handler.post(mRequestFocus);
		mFinishedStart = true;
	}

	/**
	 * Provide the cursor for the list view.
	 */
	public void setListAdapter(ListAdapter adapter) {
		synchronized (this) {
			ensureList();
			mAdapter = adapter;
			mList.setAdapter(adapter);
		}
	}

	/**
	 * Set the currently selected list item to the specified position with the
	 * adapter's data
	 * 
	 * @param position
	 */
	public void setSelection(int position) {
		mList.setSelection(position);
	}

	/**
	 * Get the position of the currently selected list item.
	 */
	public int getSelectedItemPosition() {
		return mList.getSelectedItemPosition();
	}

	/**
	 * Get the cursor row ID of the currently selected list item.
	 */
	public long getSelectedItemId() {
		return mList.getSelectedItemId();
	}

	/**
	 * Get the activity's list view widget.
	 */
	public ListView getListView() {
		ensureList();
		return mList;
	}

	/**
	 * Get the ListAdapter associated with this activity's ListView.
	 */
	public ListAdapter getListAdapter() {
		return mAdapter;
	}

	private void ensureList() {
		if (mList != null) {
			return;
		}
		// setContentView(com.android.internal.R.layout.list_content);

	}

	private AdapterView.OnItemClickListener mOnClickListener = new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			onListItemClick((ListView) parent, v, position, id);
		}
	};
	
	/**
	 * Set the empty string to show when list is unpopulated.
	 * 
	 * @param emptyResId
	 */
	public void setEmptyString(int emptyResId, boolean progress) {
		// hide the progress
		this.progress.setVisibility(progress ? View.VISIBLE : View.GONE);
		
		this.emptyResId = emptyResId;
		text.setText(emptyResId);
	}

	// public void onClick(View view) {
	// switch (view.getId()) {
	// case R.id.function:
	// // process back click
	// onFunctionClick();
	// break;
	// case android.R.id.empty:
	// // process empty click
	// onEmptyClick();
	// break;
	// case R.id.logout:
	// onLogout();
	// break;
	// default:
	// break;
	// }
	// }

	//protected LinearLayout controls;

	// private Animation fadein, fadeout;

	private ProgressBar progress;
	
	public void assignHandles() {
		super.assignHandles();
		
		// progress
		progress = find(ProgressBar.class, R.id.progress);
		
		//
		// // set the name of the button
		// ((Button) findViewById(R.id.function)).setText(funcResId);
		//
		// fadein = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
		// fadeout = AnimationUtils.loadAnimation(this,
		// android.R.anim.fade_out);

		// set the visibility of the back layout
		//controls = (LinearLayout) findViewById(R.id.controls);
		//
		// // set not of initial value to force initial change
		// showFunction = !showFunction;
		// // function transitions if showFunction is NOT permanent
		// funcTrans = showFunction;
		// showFunction(!showFunction);
		//
		// // need to explicitly set the empty box to clickable
		// findViewById(android.R.id.empty).setClickable(true);
	}

	// /* whether to show the back button on the screen */
	// public void showFunction(boolean showFunction) {
	// // no changes (the values are not different)
	// if (!(this.showFunction ^ showFunction))
	// return;
	//
	// // assign the new value for function
	// this.showFunction = showFunction;
	//
	// // only animate layout if the function is NOT permanent
	// if (funcTrans)
	// backLayout.startAnimation(showFunction ? fadein : fadeout);
	//
	// backLayout.setVisibility(showFunction ? View.VISIBLE : View.GONE);
	// }
	//
	// abstract public void onFunctionClick();
	//
	// abstract public void onEmptyClick();
	//
	// abstract public void onLogout();

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// not implemented
	}
}