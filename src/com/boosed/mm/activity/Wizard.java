package com.boosed.mm.activity;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.boosed.mm.R;
import com.boosed.mm.activity.task.RpcTask;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.util.DataUtil;
import com.boosed.mm.ui.AdapterFactory;
import com.boosed.mm.ui.BaseArrayAdapter;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.AdapterFactory.AdapterLabel;
import com.boosed.mm.ui.crop.CropImage;
import com.boosed.mm.ui.image.FileCache;
import com.googlecode.objectify.Key;

public class Wizard extends AbstractActivity implements Controllable, OnGlobalLayoutListener {

	private Map<FieldCar, Serializable> fields;

	private ArrayAdapter<Make> makeAdapter;

	private ArrayAdapter<Model> modelAdapter;

	private ArrayAdapter<Integer> yearAdapter;

	private ArrayAdapter<String> doorAdapter;

	private ArrayAdapter<ConditionType> conditionAdapter;

	private ArrayAdapter<Key<Color>> exteriorAdapter, interiorAdapter;

	private ArrayAdapter<Key<Drivetrain>> drivetrainAdapter;

	private ArrayAdapter<Key<Engine>> engineAdapter;

	private ArrayAdapter<Key<Transmission>> transmissionAdapter;

	private List<Make> makes = new ArrayList<Make>();

	private List<Model> models = new ArrayList<Model>();

	private List<Key<Drivetrain>> drivetrains = new ArrayList<Key<Drivetrain>>();

	private List<Key<Engine>> engines = new ArrayList<Key<Engine>>();

	private List<Key<Transmission>> transmissions = new ArrayList<Key<Transmission>>();

	private List<Integer> years = new ArrayList<Integer>();

	private List<Key<Color>> colors = new ArrayList<Key<Color>>();

	private List<ConditionType> conditions = Arrays.asList(ConditionType.NEW,
			ConditionType.CERTIFIED, ConditionType.USED);

	private List<Entry<String, Tuple<Boolean, String>>> images1 = new ArrayList<Entry<String, Tuple<Boolean, String>>>();

	private ArrayAdapter<Entry<String, Tuple<Boolean, String>>> photos;

	// private boolean suppressBind = false;

	private Criteria locationCritera;

	private Car car;

	private Ad ad;

	private String imageUrl;

	/** has the wizard form been initialized? */
	private boolean initd = false;

	private static final int DLG_GEO = 1;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_wizard);

		// initialize the galleries
		initGalleries();

		// check if this is an edit or add
		//Model model = (Model) getIntent().getSerializableExtra("model");
		car = (Car) getIntent().getSerializableExtra("car");
		
		if (car != null) {
			// init ad images
			setAd((Ad) getIntent().getSerializableExtra("ad"));

			// init car
			car = (Car) getIntent().getSerializableExtra("car");

			// set the image
			imageUrl = car.image;

			// set the title
			setTitle(getString(R.string.app_edit) + " - " + car.model.getName());
		} else
			// set the title
			setTitle(R.string.app_create);

		// initialize all the adapters
		initAdapters();

		// restore the saved state
		if (savedInstanceState != null) {

			// fields
			fields = (Map<FieldCar, Serializable>) savedInstanceState.getSerializable("car");

			// ad
			setAd((Ad) savedInstanceState.getSerializable("ad"));

			// makes
			makes.clear();
			makes.addAll((List<Make>) savedInstanceState.getSerializable("makes"));
			makeAdapter.notifyDataSetChanged();

			// models
			models.clear();
			models.addAll((List<Model>) savedInstanceState.getSerializable("models"));
			modelAdapter.notifyDataSetChanged();

			// years
			years.clear();
			years.addAll((List<Integer>) savedInstanceState.getSerializable("years"));
			yearAdapter.notifyDataSetChanged();

			// colors
			colors.clear();
			colors.addAll((List<Key<Color>>) savedInstanceState.getSerializable("colors"));
			exteriorAdapter.notifyDataSetChanged();
			interiorAdapter.notifyDataSetChanged();

			// drivetrains
			drivetrains.clear();
			drivetrains.addAll((List<Key<Drivetrain>>) savedInstanceState
					.getSerializable("drivetrains"));
			drivetrainAdapter.notifyDataSetChanged();

			// engines
			engines.clear();
			engines.addAll((List<Key<Engine>>) savedInstanceState.getSerializable("engines"));
			engineAdapter.notifyDataSetChanged();

			// transmissions
			transmissions.clear();
			transmissions.addAll((List<Key<Transmission>>) savedInstanceState
					.getSerializable("transmissions"));
			transmissionAdapter.notifyDataSetChanged();

			// initialization state
			// init = savedInstanceState.getBoolean("init");

			// suppress the binding lookup
			initd = true;

			// update view to current view
			updateView(savedInstanceState.getInt("view"));

			// set the image
			imageUrl = savedInstanceState.getString("image");
		} else {
			fields = new HashMap<FieldCar, Serializable>();

			// update the view to attributes initially
			updateView(R.id.attributes);
		}

		// initialize the car image
		dm.loadImage(imageUrl, image, new ImageCallback() {
			@Override
			public void onLoad(int height, int width) {
				init(height, width);
				image.setVisibility(View.VISIBLE);
			}
		});

		// for geocoding
		locationCritera = new Criteria();
		locationCritera.setAccuracy(Criteria.ACCURACY_FINE);
		locationCritera.setAltitudeRequired(false);
		locationCritera.setBearingRequired(false);
		locationCritera.setCostAllowed(true);
		locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// need to add delay for orientation changes or galleries will not
		// redraw properly

		// may need to adjust this time to work properly
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// refresh the adapters
				photos.notifyDataSetChanged();
			}
		}, 2000);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// for all cases
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		switch (id) {
		case DLG_GEO:
			// title
			builder.setTitle("geolocation");
			builder.setMessage("what do you do?");

			// set the click listener
			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						getLocation();
						break;
					case Dialog.BUTTON_NEGATIVE:
					default:
						break;
					}
				}
			};
			builder.setPositiveButton("ok", ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		default:
			return super.onCreateDialog(id);
		}
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onBind() {
		if (!initd) {
			// initialize the form
			new MobTask<Map<FieldCar, List<?>>>() {

				@Override
				public Map<FieldCar, List<?>> doit() throws RemoteServiceFailureException {
					return rpc.service.loadEdits();
				}

				@SuppressWarnings("unchecked")
				@Override
				public void onSuccess(Map<FieldCar, List<?>> t) {

					for (Entry<FieldCar, List<?>> entry : t.entrySet())
						switch (entry.getKey()) {
						case DRIVETRAIN:
							// set drivetrains
							drivetrains.clear();
							drivetrains.addAll((List<Key<Drivetrain>>) entry.getValue());
							drivetrainAdapter.notifyDataSetChanged();
							break;
						case ENGINE:
							// set engines
							engines.clear();
							engines.addAll((List<Key<Engine>>) entry.getValue());
							engineAdapter.notifyDataSetChanged();
							break;
						case EXTERIOR:
							// set colors
							colors.clear();
							colors.addAll((List<Key<Color>>) entry.getValue());
							exteriorAdapter.notifyDataSetChanged();
							interiorAdapter.notifyDataSetChanged();
							break;
						case TRANSMISSION:
							// set transmissions
							transmissions.clear();
							transmissions.addAll((List<Key<Transmission>>) entry.getValue());
							transmissionAdapter.notifyDataSetChanged();
							break;
						default:
							break;
						}
				}
			}.execute();

			// initialize the make list
			new MobTask<List<Make>>() {

				@Override
				public List<Make> doit() throws RemoteServiceFailureException {
					return rpc.service.loadMakes(false);
				}

				@Override
				public void onSuccess(List<Make> t) {
					// update makes
					makes.clear();
					makes.addAll(t);

					// select the correct make
					if (!initd && car != null) {
						Log.w("chat", "setting up the makes");
						spMake.setSelection(DataUtil.getKeys(makes).indexOf(car.make));
					}

					makeAdapter.notifyDataSetChanged();
				}
			}.execute();
		}
	}

	private ImageView image;
	private Gallery gallery;
	private ImageButton attributes, content, images;
	private ViewFlipper flipper;
	private EditText trim, mileage, price, vin, postal;
	private Spinner spMake, spModel, spYear, spDoor, spCondition, spExterior, spInterior,
			spDrivetrain, spEngine, spTransmission;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// view flipper
		flipper = (ViewFlipper) findViewById(R.id.flipper);

		// galleries
		image = (ImageView) findViewById(R.id.image);
		Log.w("chat", "assigning the image: " + image);
		gallery = (Gallery) findViewById(R.id.gallery);
		// gallery2 = (Gallery) findViewById(R.id.gallery2);

		// buttons
		View controls = findViewById(R.id.controls);
		attributes = (ImageButton) controls.findViewById(R.id.attributes);
		content = (ImageButton) controls.findViewById(R.id.content);
		images = (ImageButton) controls.findViewById(R.id.images);

		// fields
		trim = (EditText) findViewById(R.id.trim);
		mileage = (EditText) findViewById(R.id.mileage);
		price = (EditText) findViewById(R.id.price);
		postal = (EditText) findViewById(R.id.postal);
		vin = (EditText) findViewById(R.id.vin);

		// spinners
		spMake = (Spinner) findViewById(R.id.make);
		spModel = (Spinner) findViewById(R.id.model);
		spYear = (Spinner) findViewById(R.id.year);
		spDoor = (Spinner) findViewById(R.id.door);
		spCondition = (Spinner) findViewById(R.id.condition);
		spExterior = (Spinner) findViewById(R.id.exterior);
		spInterior = (Spinner) findViewById(R.id.interior);
		spDrivetrain = (Spinner) findViewById(R.id.drivetrain);
		spEngine = (Spinner) findViewById(R.id.engine);
		spTransmission = (Spinner) findViewById(R.id.transmission);

		// // set the controls
		// View view = getLayoutInflater().inflate(R.layout.search_ctrl, null);
		//
		// LayoutParams lp = new LayoutParams(0, LayoutParams.FILL_PARENT, 1);
		// LinearLayout controls = (LinearLayout)
		// findViewById(R.id.list_control);
		// controls.addView(view, lp);
		// controls.setVisibility(View.VISIBLE);
		//
		// btnTypes = (ImageButton) findViewById(R.id.types);
		// btnTypes.setEnabled(false);
		// btnMakes = (ImageButton) findViewById(R.id.makes);
		// btnMakes.setEnabled(false);
		// btnModels = (ImageButton) findViewById(R.id.models);
		// btnModels.setEnabled(false);
		// btnFilters = (ImageButton) findViewById(R.id.filters);
		// btnFilters.setEnabled(false);
	}

	@Override
	public void onControl(View view) {
		int viewId = view.getId();
		switch (viewId) {
		case R.id.attributes:
		case R.id.content:
		case R.id.images:
			// flip the view
			updateView(viewId);
			break;
		case R.id.save:
			saveCar();
			break;
		case R.id.location:
			// clicked on target button
			// reference the location of the vehicle
			showDialog(DLG_GEO);
			// getLocation();

			break;
		//case R.id.upload:
		//	doTakePhotoAction();
		//	break;
		default:
			break;
		}
	}

	private Uri mUri;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case 10:
				// returned from logging in, reattempt to save the car
				saveCar();
				break;
			case CROP_RESULT:
				submitImage(mUri, car.ad.getId());
				break;
			case CAMERA_RESULT:
				Intent intent = new Intent(this, CropImage.class);
				// here you have to pass absolute path to your file
				intent.putExtra("image-path", mUri.getPath());
				intent.putExtra("outputX", 400);
				intent.putExtra("ouputY", 240);
				intent.putExtra("aspectX", 5);
				intent.putExtra("aspectY", 3);
				intent.putExtra("scale", true);
				startActivityForResult(intent, CROP_RESULT);
				break;
			default:
				break;
			}
		}
	}

	private void doTakePhotoAction() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		File file = new File(Environment.getExternalStorageDirectory(), FileCache.DIRECTORY);
		if (!file.exists()) {
			file.mkdir();
			Log.w("chat", "does not exist");
		}
		file = new File(file, "pic_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
		mUri = Uri.fromFile(file/*
								 * new
								 * File(Environment.getExternalStorageDirectory
								 * () + "/com.boosed", "pic_" +
								 * String.valueOf(System.currentTimeMillis()) +
								 * ".jpg")
								 */);
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mUri);

		try {
			intent.putExtra("return-data", false);
			startActivityForResult(intent, CAMERA_RESULT);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static final int CAMERA_RESULT = 1;
	private static final int CROP_RESULT = 2;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// car
		outState.putSerializable("car", (Serializable) fields);

		// ad
		outState.putSerializable("ad", (Serializable) ad);

		// make
		// outState.putInt("make", spMake.getSelectedItemPosition());
		outState.putSerializable("makes", (Serializable) makes);

		// model
		// outState.putInt("model", spModel.getSelectedItemPosition());
		outState.putSerializable("models", (Serializable) models);

		// years
		// outState.putInt("year", spYear.getSelectedItemPosition());
		outState.putSerializable("years", (Serializable) years);

		// exterior
		outState.putSerializable("colors", (Serializable) colors);

		// engine
		outState.putSerializable("engines", (Serializable) engines);

		// transmission
		outState.putSerializable("transmissions", (Serializable) transmissions);

		// drivetrain
		outState.putSerializable("drivetrains", (Serializable) drivetrains);

		// initialization state
		// outState.putBoolean("init", init);

		// view
		outState.putInt("view", flipper.getCurrentView().getId());

		// image
		outState.putString("image", imageUrl);
	}

	private void getLocation() {
		// LocationManager.NETWORK_PROVIDER;
		final String providerName = lm.getBestProvider(locationCritera, true);

		// there is a provider & it is enabled
		if (providerName != null && lm.isProviderEnabled(providerName))
			new MobTask<Address>("geocoding") {

				@Override
				public Address doit() throws RemoteServiceFailureException {
					// provider is enabled
					Location loc = lm.getLastKnownLocation(providerName);

					if (loc == null)
						throw new RemoteServiceFailureException();

					// Log.i("chat", providerName + ", " +
					// loc.getLatitude());
					Geocoder geo = new Geocoder(Wizard.this);
					try {
						List<Address> addresses;
						while ((addresses = geo.getFromLocation(loc.getLatitude(), loc
								.getLongitude(), 1)).size() == 0)
							// wait until we can get an address
							Thread.sleep(2000);

						return addresses.get(0);
					} catch (Exception e) {
						throw new RemoteServiceFailureException();
					}
				}

				// @Override
				// public void onFailure(int errResId) {
				// Toast.makeText(Edit.this, "lookup failed",
				// Toast.LENGTH_SHORT).show();
				// endStatus();
				// // // set postal
				// // suppressLocationLookup = true;
				// // postal.setText("");
				// // postal.append("00000");
				// //
				// // // set location
				// // location.setText("unknown");
				// }

				@Override
				public void onSuccess(Address t) {
					// update values on car field map
					fields.put(FieldCar.LATITUDE, t.getLatitude());
					fields.put(FieldCar.LONGITUDE, t.getLongitude());

					String postalCode = t.getPostalCode();
					fields.put(FieldCar.LOCATION, postalCode);
					postal.setText(postalCode);
					// // set the postal code
					// suppressLocationLookup = true;
					// postal.setText("");
					// postal.append(t.getPostalCode());
					//
					// // set the location dto
					// tempLocation = new LocationDto();
					// tempLocation.setCity(t.getLocality());
					// tempLocation.setStatoid(t.getAdminArea());
					//
					// // set the location
					// location.setText(tempLocation.getLocation());
				}
			}.execute();
		else {
			// provider not enabled, prompt user to enable it
			Toast.makeText(this, R.string.warn_location, Toast.LENGTH_LONG).show();
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
	}

	@SuppressWarnings("unchecked")
	private void initAdapters() {

		// labels
		AdapterLabel<Integer> labelInteger = new AdapterFactory.AdapterLabel<Integer>() {
			@Override
			public CharSequence getName(Integer item) {
				return item.toString();
			}
		};

		AdapterLabel<? extends Key> labelKey = new AdapterFactory.AdapterLabel<Key>() {
			@Override
			public CharSequence getName(Key item) {
				return item.getName();
			}
		};

		// make adapter
		makeAdapter = new ArrayAdapter<Make>(this, R.layout.spinner_item, makes);
		makeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spMake.setAdapter(makeAdapter);
		OnItemSelectedListener oisl = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {

				// retrieve the models from the database
				new MobTask<List<Model>>() {
					@Override
					public List<Model> doit() throws RemoteServiceFailureException {
						Make make = makes.get(position);
						//Key<Make> key = make.getKey();
						return rpc.service.loadModels(make.desc);
					}

					@Override
					public void onSuccess(List<Model> t) {
						// adjust shown models
						models.clear();
						models.addAll(t);

						// select the correct model
						Log.w("chat", "make listener: " + initd);
						if (!initd && car != null) {
							Log.i("chat", "initializing the model!");
							spModel.setSelection(DataUtil.getKeys(models).indexOf(car.model));
						}

						modelAdapter.notifyDataSetChanged();
					}
				}.execute();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// not implemented
			}
		};
		spMake.setOnItemSelectedListener(oisl);

		// model adapter
		modelAdapter = new ArrayAdapter<Model>(this, R.layout.spinner_item, models);
		modelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spModel.setAdapter(modelAdapter);
		oisl = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {

				Model model = (Model) spModel.getSelectedItem();// models.get(position);

				// set the model on car
				// Log.w("chat", "adding this to car " + model);
				// car.put(FieldCar.MODEL, model.desc);

				// set years
				years.clear();
				years.addAll(model.years);

				// set these items according to the model
				// set doors
				// set drivetrains
				// set engines
				// set transmissions

				// select the correct year
				Log.w("chat", "model listener: " + initd);
				if (!initd && car != null) {
					spYear.setSelection(model.years.indexOf(car.year));

					Log.i("chat", "initializing the entire form");
					initForm();
				}

				yearAdapter.notifyDataSetChanged();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// not implemented
			}
		};
		spModel.setOnItemSelectedListener(oisl);

		// year adapter
		yearAdapter = AdapterFactory.getAdapter(this, years, labelInteger);
		spYear.setAdapter(yearAdapter);

		// doors adapter
		doorAdapter = AdapterFactory.getAdapter(this, getResources().getStringArray(R.array.doors));
		spDoor.setSelection(0);
		spDoor.setAdapter(doorAdapter);

		// drivetrain adapter
		drivetrainAdapter = AdapterFactory.getAdapter(this, drivetrains,
				(AdapterLabel<Key<Drivetrain>>) labelKey);
		spDrivetrain.setAdapter(drivetrainAdapter);

		// engine adapter
		engineAdapter = AdapterFactory.getAdapter(this, engines,
				(AdapterLabel<Key<Engine>>) labelKey);
		spEngine.setAdapter(engineAdapter);

		// transmission adapter
		transmissionAdapter = AdapterFactory.getAdapter(this, transmissions,
				(AdapterLabel<Key<Transmission>>) labelKey);
		spTransmission.setAdapter(transmissionAdapter);

		// condition adapter
		conditionAdapter = new ArrayAdapter<ConditionType>(this, R.layout.spinner_item, conditions);
		conditionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spCondition.setAdapter(conditionAdapter);

		// exterior adapter
		exteriorAdapter = AdapterFactory.getAdapter(this, colors,
				(AdapterLabel<Key<Color>>) labelKey);
		spExterior.setAdapter(exteriorAdapter);

		// interior adapter
		interiorAdapter = AdapterFactory.getAdapter(this, colors,
				(AdapterLabel<Key<Color>>) labelKey);
		spInterior.setAdapter(interiorAdapter);
	}

	/**
	 * Initialize the <code>Ad</code> and set the appropriate images, etc.
	 * 
	 * @param ad
	 */
	private void setAd(Ad ad) {
		this.ad = ad;

		images1.clear();
		images1.addAll(/* ad.getImages(true).values() */ad.getImages());
		photos.notifyDataSetChanged();

		// images2.clear();
		// images2.addAll(ad.getImages(false).values());
		// photos2.notifyDataSetChanged();

		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// // refresh the adapters
		// photos1.notifyDataSetChanged();
		// photos2.notifyDataSetChanged();
		// }
		// }, 500);
	}

	private void initForm() {
		spDoor.setSelection(car.doors - 2);
		spDrivetrain.setSelection(drivetrains.indexOf(car.drivetrain));
		spEngine.setSelection(engines.indexOf(car.engine));
		spTransmission.setSelection(transmissions.indexOf(car.transmission));

		// initialize the "universal" fields
		spCondition.setSelection(conditions.indexOf(car.conditions.get(0)));
		spExterior.setSelection(colors.indexOf(car.exterior));
		spInterior.setSelection(colors.indexOf(car.interior));

		// initialize the text fields
		trim.setText(car.trim);
		price.setText(Integer.toString(car.price));
		mileage.setText(Integer.toString(car.mileage));
		vin.setText(car.vin);

		// set the coordinates on map
		postal.setText(car.postal);
		fields.put(FieldCar.LATITUDE, car.latitude);
		fields.put(FieldCar.LONGITUDE, car.longitude);

		// set initialized to true
		initd = true;
	}

	private void initGalleries() {
		image.setImageMatrix(matrix);
		image.getViewTreeObserver().addOnGlobalLayoutListener(this);

		// prepare galleries
		photos = new BaseArrayAdapter<Entry<String, Tuple<Boolean, String>>, ImageView>(this,
				images1, R.layout.list_item_img) {

			@Override
			protected ImageView createView(View view) {
				return (ImageView) view;
			}

			@Override
			protected void render(Entry<String, Tuple<Boolean, String>> item, final ImageView row) {
				String url = item.getValue().b;
				if (!"".equals(url))
					dm.loadImage(url + "=s160", row, new ImageCallback() {
						@Override
						public void onLoad(int height, int width) {
							// notifyDataSetChanged();
							// row.refreshDrawableState();
							// image.setVisibility(View.VISIBLE);
							// Log.w("chat", "refreshing an images");
						}
					});
			}
		};

		gallery.setAdapter(photos);
		gallery.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				final String url = photos.getItem(position).getValue().b;

				// hide image to show progress bar
				image.setVisibility(View.INVISIBLE);

				// load the selected image
				dm.loadImage(url, image, new ImageCallback() {
					@Override
					public void onLoad(int height, int width) {
						// row.refreshDrawableState();
						init(height, width);
						image.setVisibility(View.VISIBLE);
						image.invalidate();
						// image.refreshDrawableState();

						// ensures that the borders redraw properly
						photos.notifyDataSetChanged();
						// Log.w("chat", "refreshing an images");

						// save selected image
						imageUrl = url;
					}
				});
			}
		});

		// photos2 = new BaseArrayAdapter<String, ImageView>(this, images2,
		// R.layout.list_item_img) {
		//
		// @Override
		// protected ImageView createView(View view) {
		// return (ImageView) view;
		// }
		//
		// @Override
		// protected void render(String item, final ImageView row) {
		// if (!"".equals(item))
		// dm.loadImage(item + "=s150", row, new ImageCallback() {
		// @Override
		// public void onLoad(int height, int width) {
		// //notifyDataSetChanged();
		// //row.refreshDrawableState();
		// // image.setVisibility(View.VISIBLE);
		// // Log.w("chat", "refreshing an images");
		// }
		// });
		// }
		// };

		// gallery2.setAdapter(photos2);
		// gallery2.setOnItemClickListener(new OnItemClickListener() {
		// public void onItemClick(AdapterView<?> parent, View v, int position,
		// long id) {
		// Toast.makeText(Wizard.this, photos2.getItem(position),
		// Toast.LENGTH_SHORT).show();
		// }
		// });
	}

	@Override
	public void onGlobalLayout() {
		init();
	}

	@SuppressWarnings("unchecked")
	private void saveCar() {
		// create the map
		fields.put(FieldCar.ID, car == null ? null : car.id);
		fields.put(FieldCar.CONDITION, (Integer) spCondition.getSelectedItemPosition());
		fields.put(FieldCar.DOORS, (Integer) spDoor.getSelectedItemPosition() + 2);
		fields.put(FieldCar.DRIVETRAIN, ((Key) spDrivetrain.getSelectedItem()).getName());
		fields.put(FieldCar.ENGINE, ((Key) spEngine.getSelectedItem()).getName());
		fields.put(FieldCar.EXTERIOR, ((Key) spExterior.getSelectedItem()).getName());
		fields.put(FieldCar.INTERIOR, ((Key) spInterior.getSelectedItem()).getName());
		fields.put(FieldCar.LOCATION, postal.getText().toString());
		fields.put(FieldCar.MAKE, ((Make) spMake.getSelectedItem()).desc);
		fields.put(FieldCar.MODEL, ((Model) spModel.getSelectedItem()).desc);
		fields.put(FieldCar.MILEAGE, Integer.parseInt(mileage.getText().toString()));
		fields.put(FieldCar.PRICE, Integer.parseInt(price.getText().toString()));
		fields.put(FieldCar.TRANSMISSION, ((Key) spTransmission.getSelectedItem()).getName());
		fields.put(FieldCar.TRIM, trim.getText().toString());
		fields.put(FieldCar.VIN, vin.getText().toString());
		fields.put(FieldCar.YEAR, (Integer) spYear.getSelectedItem());

		new MobTask<Void>(R.string.stat_save, true) {

			@Override
			public Void doit() throws RemoteServiceFailureException {
				rpc.service.updateCar(fields);
				return null;
			}

			// @Override
			// public void onFailure(int errResId) {
			// endStatus();
			// if (errResId == R.string.exception_session_timeout)
			// //Toast.makeText(Edit.this, "you are not logged in",
			// Toast.LENGTH_SHORT).show();
			// startActivityForResult(new Intent(Edit.this, Accounts.class),
			// 10);
			// }

			@Override
			public void onSuccess(Void t) {
				setResult(RESULT_OK);
				finish();

				// endStatus();
				// Toast.makeText(Edit.this, "saved a car, are you happy?",
				// Toast.LENGTH_SHORT).show();
			}
		}.execute();
	}

	private void updateView(int viewId) {
		switch (viewId) {
		case R.id.attributes:
			attributes.setEnabled(false);

			// navigate to attributes
			switch (flipper.getCurrentView().getId()) {
			case R.id.images:
				// flip forward
				flipper.showNext();
				images.setEnabled(true);
				break;
			case R.id.content:
				// flip backward
				flipper.showPrevious();
				content.setEnabled(true);
				break;
			default:
				// do nothing
				break;
			}
			break;
		case R.id.content:
			content.setEnabled(false);

			// navigate to content
			switch (flipper.getCurrentView().getId()) {
			case R.id.attributes:
				// flip forward
				flipper.showNext();
				attributes.setEnabled(true);
				break;
			case R.id.images:
				// flip backward
				flipper.showPrevious();
				images.setEnabled(true);
				break;
			default:
				// do nothing
				break;
			}
			break;
		case R.id.images:
			images.setEnabled(false);

			// navigate to images
			switch (flipper.getCurrentView().getId()) {
			case R.id.content:
				// flip forward
				flipper.showNext();
				content.setEnabled(true);
				break;
			case R.id.attributes:
				// flip backward
				flipper.showPrevious();
				attributes.setEnabled(true);
				break;
			default:
				// do nothing
				break;
			}

			// update the galleries
			photos.notifyDataSetChanged();
			// photos2.notifyDataSetChanged();

			break;
		}
	}

	// image stuff
	float scrHeight, scrWidth;

	// the constrained viewing rectangle
	private RectF viewRect = new RectF();
	// the projected new viewing rectangle
	private RectF prjRect = new RectF();
	// the original image rect that is transformed
	private RectF imgRect = new RectF();

	private float maxZoom;
	private float minZoom;

	Matrix matrix = new Matrix();

	/**
	 * Initialize the size of widgets and the loaded image.
	 */
	private void init(int... dims) {
		// width and height of the image view
		scrHeight = image.getHeight();
		scrWidth = image.getWidth();

		// Log.i("chat", "scr height: " + scrHeight + ", scr width: " +
		// scrWidth);

		// width and height of the loaded image
		// imgHeight = view.getDrawable().getIntrinsicHeight();
		// imgWidth = view.getDrawable().getIntrinsicWidth();

		// Log.i("chat", "the view is: " + view.toString());
		// Log.i("chat", "the draw is: " + view.getDrawable());
		int iw, ih;
		if (dims.length == 0) {
			iw = image.getDrawable().getIntrinsicWidth();
			ih = image.getDrawable().getIntrinsicHeight();
		} else {
			ih = dims[0];
			iw = dims[1];
		}

		imgRect.set(0, 0, iw, ih);
		float imgWidth = imgRect.width(), imgHeight = imgRect.height();

		// Log.i("chat", "img height: " + imgHeight + ", img width: " +
		// imgWidth);
		// if (vheight > height || vwidth > width) {

		// set the min zoom so that img takes up entire screen
		minZoom = Math.min(scrHeight / imgHeight, scrWidth / imgWidth);
		// Log.w("chat", "min zoom is: " + minZoom + " sh: " + scrHeight +
		// ", sw: " + scrWidth + ", " + imgWidth + ", " + imgHeight);
		// max zoom is 3x image size
		maxZoom = minZoom * 3;

		// establish the valid viewing rectangle
		float dx = (scrWidth - imgWidth * minZoom) / 2f;
		float dy = (scrHeight - imgHeight * minZoom) / 2f;
		viewRect.set(dx, dy, dx + imgWidth * minZoom, dy + imgHeight * minZoom);

		// zero out our cumulative changes
		matrix.reset();

		// set proper size (the transformation matrix incorporates the minimum
		// zoom to optimize size of the image)
		matrix.postScale(minZoom, minZoom, 0, 0);

		// center in middle of screen
		matrix.postTranslate(dx, dy);

		// apply the changes
		image.setImageMatrix(matrix);
	}

	public void submitImage(final Uri uri, final long ad) {

		new RpcTask<String>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			public void onFailure(int errResId) {
				Toast.makeText(Wizard.this, "failed to send image", Toast.LENGTH_SHORT).show();
			}

			@Override
			public String doit() throws RemoteServiceFailureException {
				return rpc.service.loadAction();
			}

			@Override
			public void onSuccess(String t) {
//				new AsyncTask<String, Integer, Tuple<Ad, Account>>() {
//					protected void onPreExecute() {
//						setStatus("uploading");
//					}
//
//					@Override
//					protected Tuple<Ad, Account> doInBackground(String... params) {
//						return rpc.uploadFile(params[0], ad, uri);
//					}
//
//					protected void onPostExecute(Tuple<Ad, Account> result) {
//						endStatus();
//
//						// set the ad on the wizard
//						setAd(result.a);
//					}
//				}.execute(t);
			}
		}.execute();
	}
}