package com.boosed.mm.activity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemSelectedListener;

import com.boosed.mm.R;
import com.boosed.mm.activity.task.GeoTask;
import com.boosed.mm.activity.task.LocationTask;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.enums.AdType;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.exception.IncompleteException;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.util.DataUtil;
import com.boosed.mm.ui.AdapterFactory;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.AdapterFactory.AdapterLabel;
import com.boosed.mm.ui.barcode.Intents;
import com.googlecode.objectify.Key;

/**
 * Activity for adding a new <code>Ad</code> or editing an existing one.
 * 
 * @author dsumera
 */
public class Edit extends AbstractActivity implements Controllable {

	/** intent key for car reference */
	public static final String CAR = "com.boosed.mm.edit.Car";

	/** request code for scan */
	private static final int SCAN = 0;

	private Map<FieldCar, Serializable> fields = new HashMap<FieldCar, Serializable>();

	private ArrayAdapter<AdType> adAdapter;

	private ArrayAdapter<Make> makeAdapter;

	private ArrayAdapter<Model> modelAdapter;

	private ArrayAdapter<Integer> doorAdapter, yearAdapter;

	private ArrayAdapter<ConditionType> conditionAdapter;

	private ArrayAdapter<Key<Color>> exteriorAdapter, interiorAdapter;

	private ArrayAdapter<Key<Drivetrain>> drivetrainAdapter;

	private ArrayAdapter<Key<Engine>> engineAdapter;

	private ArrayAdapter<Key<Transmission>> transmissionAdapter;

	private final List<Make> makes = new ArrayList<Make>();

	private final List<Model> models = new ArrayList<Model>();

	private final List<Key<Drivetrain>> drivetrains = new ArrayList<Key<Drivetrain>>();

	private List<Key<Drivetrain>> masterDrivetrains = new ArrayList<Key<Drivetrain>>();

	private final List<Key<Engine>> engines = new ArrayList<Key<Engine>>();

	private List<Key<Engine>> masterEngines = new ArrayList<Key<Engine>>();

	private final List<Key<Transmission>> transmissions = new ArrayList<Key<Transmission>>();

	private List<Key<Transmission>> masterTransmissions = new ArrayList<Key<Transmission>>();

	private final List<Integer> doors = new ArrayList<Integer>();

	private final List<Integer> years = new ArrayList<Integer>();

	private final List<Key<Color>> colors = new ArrayList<Key<Color>>();

	private final List<Integer> masterDoors = new ArrayList<Integer>();

	private final List<ConditionType> conditions = Arrays.asList(ConditionType.NEW,
			ConditionType.CERTIFIED, ConditionType.USED);

	/** criteria for use in location services */
	private final Criteria locationCritera;

	private Car car;

	/** whether edit for creating new or editing existing */
	private boolean isCreate = false;

	/**
	 * private check for whether the ad is past activation deadline and cannot
	 * be modified
	 */
	private boolean isLocked;

	// /** has the wizard form been initialized? */
	// private boolean initd = false;

	// private static final int DLG_GEO = 1;

	public Edit() {
		// for geocoding
		super(R.layout.title_edit);
		locationCritera = new Criteria();
		locationCritera.setAccuracy(Criteria.ACCURACY_FINE);
		locationCritera.setAltitudeRequired(false);
		locationCritera.setBearingRequired(false);
		locationCritera.setCostAllowed(true);
		locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_edit);
		
		// check if this is an edit or add
		car = (Car) getIntent().getSerializableExtra(CAR);
		isCreate = car == null;

		if (isCreate)
			// performing an add; set title to add
			setTitle(R.string.app_create);
		else {
			// performing an edit

			// remove these fields if activated
			if (car.isActive()) {
				// hideField(spAd);
				// hideField(spMake);
				// hideField(spModel);
				spAd.setEnabled(false);
				spMake.setEnabled(false);
				spModel.setEnabled(false);
			}

			// TODO: set the exceeded condition (don't hardcode the hours)
			isLocked = DataUtil.exceededTime(car, 48);

			// remove disallowed fields
			if (isLocked) {
				// make, model, year
				// hideField(spMake);
				// hideField(spModel);
				// hideField(spYear);
				// spMake.setEnabled(false);
				// spModel.setEnabled(false);
				spYear.setEnabled(false);

				// hide vin if already set
				if (car.vin != null) {
					// hideField(vin);
					vin.setEnabled(false);
					barcode.setEnabled(false);
				}
			}

			// set the title to car title
			//setTitle(car.year + " " + car.model.getName());
			setTitle(R.string.app_edit);
		}

		// init doors
		for (int door : getResources().getIntArray(R.array.doors))
			masterDoors.add(door);

		// restrict fields according to vehicle

		// initialize all the adapters
		initAdapters();

		// restore the saved state
		if (initialized) {

			// fields
			fields = (Map<FieldCar, Serializable>) savedInstanceState.getSerializable("car");

			// makes
			makes.clear();
			makes.addAll((List<Make>) savedInstanceState.getSerializable("makes"));
			makeAdapter.notifyDataSetChanged();

			// models
			models.clear();
			models.addAll((List<Model>) savedInstanceState.getSerializable("models"));
			modelAdapter.notifyDataSetChanged();

			// doors
			// doors.clear();
			// doors.addAll((List<Integer>)
			// savedInstanceState.getSerializable("doors"));
			// doorAdapter.notifyDataSetChanged();

			// years
			years.clear();
			years.addAll((List<Integer>) savedInstanceState.getSerializable("years"));
			yearAdapter.notifyDataSetChanged();

			// colors
			colors.clear();
			colors.addAll((List<Key<Color>>) savedInstanceState.getSerializable("colors"));
			exteriorAdapter.notifyDataSetChanged();
			interiorAdapter.notifyDataSetChanged();

			// drivetrains
			// drivetrains.clear();
			// drivetrains.addAll((List<Key<Drivetrain>>)
			// savedInstanceState.getSerializable("drivetrains"));
			masterDrivetrains = (List<Key<Drivetrain>>) savedInstanceState
					.getSerializable("drivetrains");
			drivetrains.clear();
			drivetrains.addAll(masterDrivetrains);
			drivetrainAdapter.notifyDataSetChanged();

			// engines
			// engines.clear();
			// engines.addAll((List<Key<Engine>>)
			// savedInstanceState.getSerializable("engines"));
			masterEngines = (List<Key<Engine>>) savedInstanceState.getSerializable("engines");
			engines.clear();
			engines.addAll(masterEngines);
			engineAdapter.notifyDataSetChanged();

			// transmissions
			// transmissions.clear();
			// transmissions.addAll((List<Key<Transmission>>)
			// savedInstanceState.getSerializable("transmissions"));
			masterTransmissions = (List<Key<Transmission>>) savedInstanceState
					.getSerializable("transmissions");
			transmissions.clear();
			transmissions.addAll(masterTransmissions);
			transmissionAdapter.notifyDataSetChanged();

			// barcode state
			//barcode = savedInstanceState.getBoolean("barcode");
			// initialization state
			// init = savedInstanceState.getBoolean("init");

			// suppress the binding lookup
			// initd = true;
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// car
		outState.putSerializable("car", (Serializable) fields);

		// make
		// outState.putInt("make", spMake.getSelectedItemPosition());
		outState.putSerializable("makes", (Serializable) makes);

		// model
		outState.putSerializable("models", (Serializable) models);

		// doors
		// outState.putSerializable("doors", (Serializable) doors);

		// years
		outState.putSerializable("years", (Serializable) years);

		// exterior
		outState.putSerializable("colors", (Serializable) colors);

		// engine
		outState.putSerializable("engines", (Serializable) masterEngines);

		// transmission
		outState.putSerializable("transmissions", (Serializable) masterTransmissions);

		// drivetrain
		outState.putSerializable("drivetrains", (Serializable) masterDrivetrains);

		// barcode dialog
		//outState.putBoolean("barcode", barcode);
		// initialization state
		// outState.putBoolean("init", init);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// for all cases
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		switch (id) {
		case R.id.location:
			// title
			builder.setTitle(R.string.dlg_title_geocode);
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage(getString(R.string.warn_geo));

			// set the click listener
			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// set the location
						setLocation();
						break;
					case Dialog.BUTTON_NEGATIVE:
						// remove all geolocation values
						removeLocation();
						break;
					default:
						break;
					}
				}
			};
			builder.setPositiveButton(R.string.dlg_btn_ok, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
			// case R.id.barcode:
			// // title
			// builder.setTitle(R.string.dlg_title_confirm).setCancelable(false);
			//
			// ocl = new OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// // do not allow barcode again until focus is lost
			// //barcode = false;
			//
			// switch (which) {
			// case Dialog.BUTTON_POSITIVE:
			// // user has confirmed, show create account dialog
			// startActivityForResult(new Intent(Intents.Scan.ACTION), SCAN);
			// break;
			// case DialogInterface.BUTTON_NEGATIVE:
			// // user cancelled, do nothing
			// break;
			// }
			// }
			// };
			//
			// // ok & cancel buttons
			// builder.setPositiveButton(R.string.dlg_btn_ok, ocl);
			// builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);
			//
			// // set message and create
			// return builder.setMessage(R.string.cfm_scan).create();
		default:
			return super.onCreateDialog(id);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK)
			if (requestCode == 0) {
				// get the vin data
				String value = data.getStringExtra(Intents.Scan.RESULT);

				// trim the import prefix
				if (value.charAt(0) == 'I')
					value = value.substring(1);

				// set the text
				vin.setText(value);
				vin.setSelection(value.length());
				// dismiss the dialog
				// dismissDialog(R.id.vin);
			}
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onBind() {
		if (!initialized)
			// initialize the form
			new MobTask<Map<FieldCar, List<?>>>(R.string.stat_load, true) {
				@Override
				public Map<FieldCar, List<?>> doit() throws RemoteServiceFailureException {
					return rpc.service.loadEdits();
				}

				@SuppressWarnings("unchecked")
				@Override
				public void onSuccess(Map<FieldCar, List<?>> t) {
					// init editable fields
					for (Entry<FieldCar, List<?>> entry : t.entrySet())
						switch (entry.getKey()) {
						case DRIVETRAIN:
							// set drivetrains
							masterDrivetrains = (List<Key<Drivetrain>>) entry.getValue();
							drivetrains.clear();
							drivetrains.addAll(masterDrivetrains);
							drivetrainAdapter.notifyDataSetChanged();
							break;
						case ENGINE:
							// set engines
							masterEngines = (List<Key<Engine>>) entry.getValue();
							engines.clear();
							engines.addAll(masterEngines);
							engineAdapter.notifyDataSetChanged();
							break;
						case EXTERIOR:
							// set colors
							colors.clear();
							colors.addAll((List<Key<Color>>) entry.getValue());
							exteriorAdapter.notifyDataSetChanged();
							interiorAdapter.notifyDataSetChanged();
							break;
						case TRANSMISSION:
							// set transmissions
							masterTransmissions = (List<Key<Transmission>>) entry.getValue();
							transmissions.clear();
							transmissions.addAll(masterTransmissions);
							transmissionAdapter.notifyDataSetChanged();
							break;
						default:
							break;
						}

					// load makes after edits have been loaded
					new MobTask<List<Make>>(R.string.stat_load, true) {
						@Override
						protected void onPreExecute() {
							super.onPreExecute();

							// disable makes spinner
							spMake.setEnabled(false);
						}

						@Override
						public List<Make> doit() throws RemoteServiceFailureException {
							return rpc.service.loadMakes(false);
						}

						@Override
						public void onSuccess(List<Make> t) {
							// update makes
							makes.clear();
							makes.addAll(t);

							if (!initialized && !isCreate)
								// initialize with the make of the given car
								spMake.setSelection(DataUtil.getKeys(makes).indexOf(car.make));
							else if (isCreate)
								// set initialized since this is for creation
								initialized = true;

							makeAdapter.notifyDataSetChanged();
						}

						@Override
						protected void onPostExecute(java.util.List<Make> result) {
							// enable makes spinner
							if (car == null || !car.isActive())
								spMake.setEnabled(true);

							// continue task
							super.onPostExecute(result);
						}
					}.execute();
				}
			}.execute();
	}

	@Override
	public void onControl(View view) {
		switch (view.getId()) {
		case R.id.location:
			if (location.isChecked())
				showDialog(R.id.location);
			else
				removeLocation();
			break;
		case R.id.save:
			if (confirm(view, isCreate ? R.string.cfm_create : R.string.cfm_save))
				saveCar(view);

			break;
		case R.id.cancel:
			if (confirm(view, R.string.cfm_exit)) {
				// user canceled this edit
				setResult(RESULT_CANCELED);
				finish();
			}
			break;
		case R.id.barcode:
			startActivityForResult(new Intent(Intents.Scan.ACTION), SCAN);
			//showDialog(R.id.barcode);
			break;
		default:
			break;
		}
	}

	private ImageButton barcode;
	private ToggleButton location;
	private EditText trim, mileage, price, stock, vin, postal;
	private Spinner spAd, spMake, spModel, spYear, spDoor, spCondition, spExterior, spInterior,
			spDrivetrain, spEngine, spTransmission;

	///** if we should show the barcode dialog */
	//private boolean barcode = true;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// location
		location = (ToggleButton) findViewById(R.id.location);

		// fields
		trim = (EditText) findViewById(R.id.trim);
		mileage = (EditText) findViewById(R.id.mileage);
		price = (EditText) findViewById(R.id.price);
		postal = (EditText) findViewById(R.id.postal);
		stock = (EditText) findViewById(R.id.stock);
		vin = (EditText) findViewById(R.id.vin);
		barcode = find(ImageButton.class, R.id.barcode);	
//		// add listener to use barcode scanner
//		vin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//			@Override
//			public void onFocusChange(View v, boolean hasFocus) {
//				if (hasFocus && vin.isEnabled() && barcode)
//					showDialog(R.id.vin);
//
//				if (!hasFocus) {
//					// handler lost focus, ok to show dialog next time
//					barcode = true;
//
//					// trim & uppercase
//					vin.setText(vin.getText().toString().trim().toUpperCase());
//				}
//			}
//		});

		// spinners
		spAd = (Spinner) findViewById(R.id.ad_type);
		spMake = (Spinner) findViewById(R.id.make);
		spModel = (Spinner) findViewById(R.id.model);
		spYear = (Spinner) findViewById(R.id.year);
		spDoor = (Spinner) findViewById(R.id.door);
		spCondition = (Spinner) findViewById(R.id.condition);
		spExterior = (Spinner) findViewById(R.id.exterior);
		spInterior = (Spinner) findViewById(R.id.interior);
		spDrivetrain = (Spinner) findViewById(R.id.drivetrain);
		spEngine = (Spinner) findViewById(R.id.engine);
		spTransmission = (Spinner) findViewById(R.id.transmission);
	}

	@Override
	public void onReplay(View replay) {
		// if (replay instanceof MenuItem)
		// // replay a context menu item
		// onContextItemSelected((MenuItem) replay);
		// else
		// replay a control
		onControl(replay);

		// // reset to null
		// super.replay(replay);
	}

	// @Override
	// public void setTitle(CharSequence title) {
	// super.setTitle(getString(R.string.app_edit) + " - " + title);
	// }

	// /**
	// * Hide a field from view by hiding its parent layout.
	// *
	// * @param view
	// */
	// private void hideField(View view) {
	// view.setEnabled(false);
	// }

	@SuppressWarnings("unchecked")
	private void initAdapters() {

		// labels
		AdapterLabel<Integer> labelInteger = new AdapterFactory.AdapterLabel<Integer>() {
			@Override
			public CharSequence getName(Integer item) {
				return item.toString();
			}
		};

		AdapterLabel<? extends Key<?>> labelKey = new AdapterFactory.AdapterLabel<Key<?>>() {
			@Override
			public CharSequence getName(Key<?> item) {
				return item.getName();
			}
		};

		// ad adapter
		adAdapter = new ArrayAdapter<AdType>(this, R.layout.spinner_item, AdType.values());
		adAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spAd.setAdapter(adAdapter);
		spAd.setSelection(1);

		// make adapter
		makeAdapter = new ArrayAdapter<Make>(this, R.layout.spinner_item, makes);
		makeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spMake.setAdapter(makeAdapter);
		OnItemSelectedListener oisl = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {

				// retrieve the models from the datastore
				new MobTask<List<Model>>() {
					@Override
					protected void onPreExecute() {
						super.onPreExecute();

						// disable models spinner
						spModel.setEnabled(false);
					}

					@Override
					public List<Model> doit() throws RemoteServiceFailureException {
						return rpc.service.loadModels(makes.get(position).desc);
					}

					@Override
					public void onSuccess(List<Model> t) {
						// adjust shown models
						models.clear();
						models.addAll(t);

						// Log.w("chat", "make listener: " + initd);
						if (!initialized && !isCreate)
							// Log.i("chat", "initializing the model!");
							// initialize with the model of the given car
							spModel.setSelection(DataUtil.getKeys(models).indexOf(car.model));

						modelAdapter.notifyDataSetChanged();
					}

					@Override
					protected void onPostExecute(java.util.List<Model> result) {
						// enable models spinner
						if (car == null || !car.isActive())
							spModel.setEnabled(true);

						// continue task
						super.onPostExecute(result);
					}
				}.execute();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// not implemented
			}
		};
		spMake.setOnItemSelectedListener(oisl);

		// model adapter
		modelAdapter = new ArrayAdapter<Model>(this, R.layout.spinner_item, models);
		modelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spModel.setAdapter(modelAdapter);
		oisl = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {

				// set the model on the form
				initModel((Model) spModel.getSelectedItem());// models.get(position);

				// set the model on car
				// Log.w("chat", "adding this to car " + model);
				// car.put(FieldCar.MODEL, model.desc);

				// Log.w("chat", "model listener: " + initd);
				if (!initialized && !isCreate)
					// init fields according to the given car
					initForm();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// not implemented
			}
		};
		spModel.setOnItemSelectedListener(oisl);

		// year adapter
		yearAdapter = AdapterFactory.getAdapter(this, years, labelInteger);
		spYear.setAdapter(yearAdapter);

		// doors adapter
		doorAdapter = AdapterFactory.getAdapter(this, doors, labelInteger);
		spDoor.setAdapter(doorAdapter);

		// drivetrain adapter
		drivetrainAdapter = AdapterFactory.getAdapter(this, drivetrains,
				(AdapterLabel<Key<Drivetrain>>) labelKey);
		spDrivetrain.setAdapter(drivetrainAdapter);

		// engine adapter
		engineAdapter = AdapterFactory.getAdapter(this, engines,
				(AdapterLabel<Key<Engine>>) labelKey);
		spEngine.setAdapter(engineAdapter);

		// transmission adapter
		transmissionAdapter = AdapterFactory.getAdapter(this, transmissions,
				(AdapterLabel<Key<Transmission>>) labelKey);
		spTransmission.setAdapter(transmissionAdapter);

		// condition adapter
		conditionAdapter = new ArrayAdapter<ConditionType>(this, R.layout.spinner_item, conditions);
		conditionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spCondition.setAdapter(conditionAdapter);

		// exterior adapter
		exteriorAdapter = AdapterFactory.getAdapter(this, colors,
				(AdapterLabel<Key<Color>>) labelKey);
		spExterior.setAdapter(exteriorAdapter);

		// interior adapter
		interiorAdapter = AdapterFactory.getAdapter(this, colors,
				(AdapterLabel<Key<Color>>) labelKey);
		spInterior.setAdapter(interiorAdapter);
	}

	/**
	 * Initialize form according to the provided intent car object.
	 */
	private void initForm() {
		spAd.setSelection(car.adType.ordinal());
		spYear.setSelection(years.indexOf(car.year));
		spDoor.setSelection(doors.indexOf(car.doors));
		spDrivetrain.setSelection(drivetrains.indexOf(car.drivetrain));
		spEngine.setSelection(engines.indexOf(car.engine));
		spTransmission.setSelection(transmissions.indexOf(car.transmission));

		// initialize the "universal" fields
		spCondition.setSelection(conditions.indexOf(car.conditions.get(0)));
		spExterior.setSelection(colors.indexOf(car.exterior));
		spInterior.setSelection(colors.indexOf(car.interior));

		// initialize the text fields
		setField(trim, car.trim);
		setField(price, Integer.toString(car.price));
		setField(mileage, Integer.toString(car.mileage));
		setField(stock, car.stock);
		setField(vin, car.vin);

		// set the coordinates on map (and button state)
		setField(postal, car.postal);

		// is exact location enabled?
		if (car.explicit) {
			// yes
			postal.setEnabled(false);
			location.setChecked(true);

			// set location fields
			fields.put(FieldCar.LATITUDE, car.latitude);
			fields.put(FieldCar.LONGITUDE, car.longitude);
		} else {
			// no
			postal.setEnabled(true);
			location.setChecked(false);
		}

		// set initialized to true
		initialized = true;
	}

	/**
	 * Initialize all field adapters to the provided <code>Model</code>.
	 * 
	 * @param model
	 */
	private void initModel(Model model) {
		// set years
		years.clear();
		years.addAll(model.years);
		yearAdapter.notifyDataSetChanged();

		// set doors
		doors.clear();
		doors.addAll(model.doors.isEmpty() ? masterDoors : model.doors);
		doorAdapter.notifyDataSetChanged();

		// set drivetrains
		drivetrains.clear();
		drivetrains.addAll(model.drivetrains.isEmpty() ? masterDrivetrains : model.drivetrains);
		drivetrainAdapter.notifyDataSetChanged();

		// set engines
		engines.clear();
		engines.addAll(model.engines.isEmpty() ? masterEngines : model.engines);
		engineAdapter.notifyDataSetChanged();

		// set transmissions
		transmissions.clear();
		transmissions.addAll(model.transmissions.isEmpty() ? masterTransmissions
				: model.transmissions);
		transmissionAdapter.notifyDataSetChanged();
	}

	/**
	 * Retrieve selected item from <code>Spinner</code>.
	 * 
	 * @param <T>
	 * @param clazz
	 * @param spinner
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T> T getItem(Class<T> clazz, Spinner spinner) {
		return (T) spinner.getSelectedItem();
	}

	/**
	 * Set an <code>EditText</code> and move the cursor accordingly.
	 * 
	 * @param field
	 * @param value
	 */
	private void setField(EditText field, String value) {
		field.setText(value);
		if (value != null)
			field.setSelection(value.length());
	}

	/**
	 * Resets all location controls to general.
	 */
	private void removeLocation() {
		// enable postal field
		// postal.setFocusable(true);
		postal.setEnabled(true);

		// set checked state of toggle
		location.setChecked(false);

		// remove lat/long arguments
		fields.remove(FieldCar.LATITUDE);
		fields.remove(FieldCar.LONGITUDE);
	}

	private void setLocation() {
		// disable the postal field
		// postal.setEnabled(false);

		// LocationManager.NETWORK_PROVIDER;
		final String provider = lm.getBestProvider(locationCritera, true);
		// final List<String> providers = lm.getProviders(locationCritera,
		// true);

		// there is a provider & it is enabled
		if (/* providers != null && !providers.isEmpty() */provider != null
				&& lm.isProviderEnabled(provider)) {
			// request updates
			// lm.requestLocationUpdates(provider, 5000, 1.0F, Search.this);
			// name.setText("reverse geocoding...");

			new MobTask<Address>(R.string.stat_coord, true) {

				private LocationTask lt;

				private GeoTask gt;

				@Override
				protected void onPreExecute() {
					super.onPreExecute();

					// disable the location toggle
					location.setEnabled(false);
				}

				@Override
				public Address doit() throws RemoteServiceFailureException {
					lt = new LocationTask();
					lm.requestLocationUpdates(provider, 60000, 1, lt, Edit.this.getMainLooper());
					lt.await(15);

					// Location location = task.getLocation();
					gt = new GeoTask(Edit.this, lt.getLocation());
					handler.post(new Runnable() {
						public void run() {
							// setMessage("reverse geocoding...");
							setMessage("reverse geocoding");
						}
					});

					gt.await(15);
					return gt.getAddress();
				}

				public void onFailure(int errResId) {
					// re-enable postal field and toggle
					postal.setEnabled(true);
					location.setChecked(false);

					// perform rest of failure routine
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(final Address t) {
					// update values on car field map
					fields.put(FieldCar.LATITUDE, t.getLatitude());
					fields.put(FieldCar.LONGITUDE, t.getLongitude());

					String postalCode = t.getPostalCode();
					fields.put(FieldCar.LOCATION, postalCode);
					postal.setText(postalCode);
					postal.setError(null);
					postal.setSelection(postalCode.length());

					// disable postal
					postal.setEnabled(false);

					// set checked state of toggle
					location.setChecked(true);

					// suggest this address to database if it's not present
					new MobTask<Void>(null) {
						@Override
						public Void doit() throws RemoteServiceFailureException {
							// submit these parameters to the server
							Location location = gt.getLocation();
							rpc.service.suggest(t.getPostalCode(), t.getLocality(), t
									.getAdminArea(), location.getLatitude(), location
									.getLongitude());
							return null;
						}

						@Override
						public void onSuccess(Void t) {
							// ignored
						}
					}.execute();
				}

				protected void onPostExecute(Address result) {
					// remove the listener for location updates
					lm.removeUpdates(lt);

					// enable the button
					location.setEnabled(true);

					// perform rest of post execute routine
					super.onPostExecute(result);
				}
			}.execute();
		} else {
			// provider not enabled, prompt user to enable it
			Toast.makeText(this, R.string.warn_location, Toast.LENGTH_LONG).show();
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
	}

	// private void stetLocation() {
	// // disable the postal field
	// // postal.setEnabled(false);
	//
	// // LocationManager.NETWORK_PROVIDER;
	// final String name = lm.getBestProvider(locationCritera, true);
	// // final List<String> providers = lm.getProviders(locationCritera,
	// // true);
	//
	// // there is a provider & it is enabled
	// if (/* providers != null && !providers.isEmpty() */name != null &&
	// lm.isProviderEnabled(name))
	// new MobTask<Address>("geocoding") {
	//
	// @Override
	// public Address doit() throws RemoteServiceFailureException {
	// // provider is enabled
	// Location loc = null;
	//
	// // for (String name : providers) {
	//
	// loc = lm.getLastKnownLocation(name);
	// Log.w("chat", "provider: " + name + ", location: " + loc);
	//
	// if (loc == null)
	// throw new InvalidLocationException();
	//
	// // Log.i("chat", providerName + ", " +
	// // loc.getLatitude());
	// Geocoder geo = new Geocoder(Edit.this);
	// try {
	// List<Address> addresses;
	// while ((addresses = geo.getFromLocation(loc.getLatitude(),
	// loc.getLongitude(), 1)).size() == 0)
	// // wait until we can get an address
	// Thread.sleep(2000);
	//
	// return addresses.get(0);
	// } catch (Exception e) {
	// throw new RemoteServiceFailureException();
	// }
	//
	// // }
	//
	// // // no locations found
	// // if (loc == null)
	// // throw new RemoteServiceFailureException();
	// //
	// // // should never be reached?
	// // return null;
	// }
	//
	// // @Override
	// // public void onFailure(int errResId) {
	// // Toast.makeText(Edit.this, "lookup failed",
	// // Toast.LENGTH_SHORT).show();
	// // endStatus();
	// // // // set postal
	// // // suppressLocationLookup = true;
	// // // postal.setText("");
	// // // postal.append("00000");
	// // //
	// // // // set location
	// // // location.setText("unknown");
	// // }
	//
	// public void onFailure(int errResId) {
	// postal.setEnabled(true);
	// location.setChecked(false);
	//
	// super.onFailure(errResId);
	// }
	//
	// @Override
	// public void onSuccess(Address t) {
	// // update values on car field map
	// fields.put(FieldCar.LATITUDE, t.getLatitude());
	// fields.put(FieldCar.LONGITUDE, t.getLongitude());
	//
	// String postalCode = t.getPostalCode();
	// fields.put(FieldCar.LOCATION, postalCode);
	// postal.setText(postalCode);
	//
	// // disable postal
	// postal.setEnabled(false);
	//
	// // set checked state of toggle
	// location.setChecked(true);
	//
	// // // set the postal code
	// // suppressLocationLookup = true;
	// // postal.setText("");
	// // postal.append(t.getPostalCode());
	// //
	// // // set the location dto
	// // tempLocation = new LocationDto();
	// // tempLocation.setCity(t.getLocality());
	// // tempLocation.setStatoid(t.getAdminArea());
	// //
	// // // set the location
	// // location.setText(tempLocation.getLocation());
	// }
	// }.execute();
	// else {
	// // provider not enabled, prompt user to enable it
	// Toast.makeText(this, R.string.warn_location, Toast.LENGTH_LONG).show();
	// startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	// }
	// }

	@SuppressWarnings("unchecked")
	private void saveCar(View view) {
		new MobTask<Void>(R.string.stat_save, true, view) {
			@Override
			public Void doit() throws RemoteServiceFailureException {
				// validate all fields
				validate();

				// create the map
				fields.put(FieldCar.ID, isCreate ? null : car.id);
				// offset past any condition
				fields.put(FieldCar.CONDITION, spCondition.getSelectedItemPosition() + 1);
				fields.put(FieldCar.DOORS, getItem(Integer.class, spDoor));
				fields.put(FieldCar.DRIVETRAIN, getItem(Key.class, spDrivetrain).getName());
				fields.put(FieldCar.ENGINE, ((Key<Engine>) spEngine.getSelectedItem()).getName());
				fields
						.put(FieldCar.EXTERIOR, ((Key<Color>) spExterior.getSelectedItem())
								.getName());
				fields
						.put(FieldCar.INTERIOR, ((Key<Color>) spInterior.getSelectedItem())
								.getName());
				fields.put(FieldCar.LOCATION, postal.getText().toString());

				// check if car is locked
				if (!isLocked) {
					// car is not locked
					fields.put(FieldCar.MAKE, ((Make) spMake.getSelectedItem()).desc);
					fields.put(FieldCar.MODEL, ((Model) spModel.getSelectedItem()).desc);
					fields.put(FieldCar.YEAR, (Integer) spYear.getSelectedItem());
					fields.put(FieldCar.VIN, vin.getText().toString());
				} else if (car.vin == null)
					// car is locked BUT vin is still null
					fields.put(FieldCar.VIN, vin.getText().toString());

				fields.put(FieldCar.MILEAGE, Integer.parseInt(mileage.getText().toString()));
				fields.put(FieldCar.PRICE, Integer.parseInt(price.getText().toString()));
				fields.put(FieldCar.TRANSMISSION, ((Key<Transmission>) spTransmission
						.getSelectedItem()).getName());
				fields.put(FieldCar.TRIM, trim.getText().toString());
				fields.put(FieldCar.STOCK, stock.getText().toString());

				// check if car has been activated
				if (isCreate || /*car.time == null*/!car.isActive())
					// car is not activated, allow ad type change
					fields.put(FieldCar.AD_TYPE, spAd.getSelectedItemPosition());

				rpc.service.updateCar(fields);
				return null;
			}

			public void onFailure(int errResId) {
				switch (errResId) {
				case R.string.exception_invalid_location:
					// set error if location is unknown
					postal.setError(getString(R.string.exception_invalid_location));
					break;
				default:
					// continue with failure
					super.onFailure(errResId);
				}
			}

			@Override
			public void onSuccess(Void t) {
				// set result to ok
				setResult(RESULT_OK);

				// finish activity
				finish();
			}
		}.execute();
	}

	/**
	 * Validate the supplied information before submission.
	 * 
	 * @throws IncompleteException
	 */
	private void validate() throws IncompleteException {
		// ignore trim, stock, vin
		boolean error = false;

		// mileage
		if (DataUtil.getNullString(mileage.getText().toString()) == null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					mileage.setError(getString(R.string.err_mileage));
				}
			});
			error = true;
		}

		// price
		if (DataUtil.getNullString(price.getText().toString()) == null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					price.setError(getString(R.string.err_price));
				}
			});
			error = true;
		}

		// postal
		if (DataUtil.getNullString(postal.getText().toString()) == null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					postal.setError(getString(R.string.err_location));
				}
			});
			error = true;
		}

		// vin
		String value = DataUtil.getNullString(vin.getText().toString());

		if (value != null) {
			final String upper = value.toUpperCase();

			// trim & uppercase
			handler.post(new Runnable() {
				@Override
				public void run() {
					vin.setText(upper);
				}
			});

			// check value
			if (!DataUtil.vinCheck(upper)) {
				handler.post(new Runnable() {
					@Override
					public void run() {
						vin.setError(getString(R.string.err_vin));
					}
				});
				error = true;
			}
		}

		if (error)
			throw new IncompleteException();
	}
}