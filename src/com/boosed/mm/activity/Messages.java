package com.boosed.mm.activity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.StyleSpan;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.boosed.mm.R;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseArrayAdapter;
import com.boosed.mm.ui.CheckedImage;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ListItemMessage;

/**
 * Activity for viewing <code>Message</code> content.
 * 
 * @author dsumera
 */
public class Messages extends AbstractListActivity implements Controllable {

	/** intent key for car id (as long) */
	public static final String REFERENCE = "com.boosed.mm.messages.Reference";

	/** intent key for resource id of desired [reference] message type */
	public static final String TYPE = "com.boosed.mm.messages.Type";
	
	/** layout value for title if different than default */
	public static final String TITLE = "com.boosed.mm.messages.Title";

	/** messages */
	private final List<Message> messages;

	/** message cursor */
	private String cursor = null;

	/** message adapter */
	private ArrayAdapter<Message> adapter;

	/** the id of the car reference */
	private Long carId;

	/** the id of the view/button to initialize activity */
	private int mode;

	/** set of messages marked for deletion */
	private Set<Message> deletions = new HashSet<Message>();

	public Messages() {
		// set the empty string
		super(R.string.warn_messages, R.layout.title_messages);

		// init fields
		messages = new ArrayList<Message>();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// set different title if available
		titleLayoutResId = getIntent().getIntExtra(TITLE, R.layout.title_messages);
		
		super.onCreate(savedInstanceState);
		
		if (initialized) {
			// retrieve deletions
			deletions.addAll((Set<Message>) savedInstanceState.getSerializable("deletions"));
			
			// car id
			carId = savedInstanceState.getLong("id", -1L);
			
			// mode
			mode = savedInstanceState.getInt("mode");
		} else {
			// need to determine if this is for specific vehicle or general messages

			// get intent reference id if it exists
			carId = getIntent().getLongExtra(REFERENCE, -1);
			
			if (carId == -1)
				// retrieve view from preferences
				mode = settings.getInt(RpcService.PREF_MESSAGE, 3);
			else
				// initialize for referenced messages (inquiries & responses)
				mode = getIntent().getIntExtra(TYPE, 0);
		}
		
		// set car id to null if -1
		if (carId == -1L)
			carId = null;

//		if (carId == -1) {
//			// initialize for general message viewing
//			carId = null;
//			mode = settings.getInt(RpcService.PREF_MESSAGE, 3);
//		} else {
//			// initialize for referenced messages (inquiries & responses)
//			mode = intent.getIntExtra(TYPE, 0);
//
//			// set the empty string
//			setEmptyString(R.string.warn_inquiries, false);
//		}
			
		// add the footer before setting the adapter
		mList.addFooterView(footer);

		// set the list adapter
		setListAdapter(adapter = new BaseArrayAdapter<Message, ListItemMessage>(this, messages, R.layout.list_item_msg) {

			private final String dateFormat = "h:mm aa, M/d/yy";

			// private DecimalFormat df = new DecimalFormat("###,###,##0");

			@Override
			protected ListItemMessage createView(View view) {
				return new ListItemMessage(view);
			}

			@Override
			protected void render(Message item, final ListItemMessage row) {

				// set subject
				SpannableStringBuilder ssb = new SpannableStringBuilder(item.getReference());

				if (!item.getState(MessageState.READ))
					// mark bold if not read
					ssb.setSpan(new StyleSpan(Typeface.BOLD), 0, ssb.length(), 0);

				row.getSubject().setText(ssb);

				// set identifier
				row.getReference().setText(item.identifier);

				// set sender
				row.getSender().setText((item.getState(MessageState.REPLIED) ? ">> " : "") + item.senderName);

				// set date
				row.getDate().setText(DateFormat.format(dateFormat, new Date(item.time)));

				// set checked
				CheckedImage check = row.getCheck();
				check.setChecked(deletions.contains(item));
				check.setTag(item);
			}

			// @Override
			// public void notifyDataSetChanged() {
			// super.notifyDataSetChanged();
			//
			// // reset visibility of shadow if list is empty
			// shadow.setVisibility(messages.isEmpty() ? View.GONE :
			// View.VISIBLE);
			// }
		});

		// don't need footer so remove it now
		mList.removeFooterView(footer);

		// set divider
		mList.setDivider(getResources().getDrawable(android.R.drawable.divider_horizontal_bright));

		// register list for context menus
		registerForContextMenu(mList);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// for all cases
		Builder builder = new Builder(this);

		switch (id) {
		case R.id.messages:
			// title
			builder.setTitle(R.string.dlg_title_messages);

			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// if (which == Dialog.BUTTON_NEGATIVE)
					// return;

					// set the message type (make selection of 3rd item the 4th in MessageType[ALL])
					mode = which == 2 ? 3 : which;
					settings.edit().putInt(RpcService.PREF_MESSAGE, mode).commit();

					// perform an update
					load();

					// dismiss
					dialog.dismiss();
				}
			};

			// add negative button to cancel out dialog
			// builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			// if selected mode is 3 (all), set the actual selection to index 2
			return builder.setSingleChoiceItems(R.array.messages, mode == 3 ? 2 : mode, ocl).create();
		default:
			return super.onCreateDialog(id);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// show the message
		showMessage(position);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// deletions
		outState.putSerializable("deletions", (Serializable) deletions);
		
		// car id
		outState.putLong("id", carId == null ? -1 : carId);
		
		// mode
		outState.putInt("mode", mode);
	}

	/** button controls */
	private ImageButton /*btnInquiries, btnResponses, btnAll,*/ btnDelete;

	/** clickable layout */
	private RelativeLayout selector;
	
	// /** shadow for empty view */
	// private ImageView shadow;

	/** the footer */
	private View footer;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// shadow for empty list
		// shadow = (ImageView) findViewById(R.id.list_shadow);

		// selector
		selector = find(RelativeLayout.class, R.id.messages);

		// assign buttons
//		btnAll = find(ImageButton.class, R.id.all);
//		btnInquiries = find(ImageButton.class, R.id.inquiries);
//		btnResponses = find(ImageButton.class, R.id.responses);
		btnDelete = find(ImageButton.class, R.id.delete);

		// create the footer
		footer = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_footer, null);
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case R.id.ctx_view:
			// position of item
			int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
			
			// show message
			showMessage(position);
			return true;
		case R.id.ctx_reply:
			// position of item
			position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
			
			// respond to the current message
			final Message ms = messages.get(position);

			// check if this is an alert
			if (ms.getType() == MessageType.ALERTS)
				toast("cannot respond to alerts");
			else
				// start task to load car appearing in response/composer
				new MobTask<Car>() {
					@Override
					public Car doit() throws RemoteServiceFailureException {
						return rpc.service.loadCar(ms.reference.getId());
					}

					public void onSuccess(Car t) {
						// start composer for replying
						Intent intent = new Intent(Messages.this, Composer.class);
						intent.putExtra(Composer.REFERENCE, t);
						intent.putExtra(Composer.MESSAGE, ms);
						startActivity(intent);
					}
				}.execute();
			return true;
		case R.id.ctx_delete:
			// position of item
			position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
			
			// delete selected item
			final Message msg = messages.get(position);
			new MobTask<Void>(R.string.stat_delete, true, item) {
				@Override
				public Void doit() throws RemoteServiceFailureException {
					// execute deletion
					rpc.removeMessages(Arrays.asList(msg));
					return null;
				}

				@Override
				public void onFailure(int errResId) {
					sync(false);
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(Void t) {
					// remove this item from the marked deletions
					// only retain matching loaded messages
					deletions.remove(msg);

					sync(true);
				}
			}.execute();
			return true;
			// case R.id.ctx_cancel:
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		// get message
		Message msg = adapter.getItem(((AdapterContextMenuInfo) menuInfo).position);

		// set title
		menu.setHeaderTitle(msg.getReference());

		// inflate
		getMenuInflater().inflate(R.menu.ctx_menu_message, menu);

		// init menu; repliable?
		menu.findItem(R.id.ctx_reply).setVisible(msg.getType() != MessageType.ALERTS);
	}

	@Override
	public void onReplay(View replay) {
		// replay a control
		onControl(replay);
	}

	@Override
	public void onBind() {
		// always refresh the messages on binding/resume (viewId initially set
		// in assignhandles)... NEVER NEED TO SYNC!

		// perform load
		//onControl(findViewById(viewId));
		load();
	}

	@Override
	public void onControl(View view) {
		final int id = view.getId();

		switch (id) {
//		case R.id.all:
//		case R.id.inquiries:
//		case R.id.responses:
//			// update the controls
//			updateControls(id);
//
//			// load the messages
//			new MobTask<String>(view) {
//				@Override
//				protected void onPreExecute() {
//					super.onPreExecute();
//
//					// set the loading string
//					setEmptyString(R.string.warn_load);
//
//					// clear the messages
//					messages.clear();
//					cursor = null;
//
//					// update the view
//					adapter.notifyDataSetChanged();
//				}
//
//				@Override
//				public String doit() throws RemoteServiceFailureException {
//					// load messages
//					return rpc.loadMessages(carId, false);
//				}
//
//				@Override
//				public void onFailure(int errResId) {
//					// synchronize content
//					sync(false);
//
//					// continue with failure
//					super.onFailure(errResId);
//				}
//
//				@Override
//				public void onSuccess(String t) {
//					// set cursor
//					cursor = t;
//
//					// synchronize content
//					sync(true);
//				}
//			}.execute();
//			break;
		case R.id.messages:
			if (initialized)
				// show dialog for selecting inventory
				showDialog(id);
			else
				// initialize for first time (no dialog)
				load();
			break;
		case R.id.delete:
			if (confirm(view, R.string.cfm_del_msgs) && !deletions.isEmpty())
				// delete all items in deletion set
				new MobTask<Void>(R.string.stat_delete, true, view) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						// execute deletion
						rpc.removeMessages(deletions);
						return null;
					}

					@Override
					public void onFailure(int errResId) {
						// synchronize content
						sync(false);

						// continue with failure
						super.onFailure(errResId);
					}

					@Override
					public void onSuccess(Void t) {
						// clear deletion data
						deletions.clear();

						// sync content
						sync(true);
					}
				}.execute();
			break;
		case R.id.refresh:
			// TODO: use existing view id to refresh the list
			// onControl(findViewById(viewId));
			onBind();
			break;
		case android.R.id.checkbox:
			// handle bookmarking
			CheckedImage check = (CheckedImage) view;
			if (check.isChecked())
				// remove the item from bin
				deletions.remove((Message) check.getTag());
			else
				// add item to bin
				deletions.add((Message) check.getTag());

			// set status for delete button
			btnDelete.setEnabled(!deletions.isEmpty());

			// update view
			adapter.notifyDataSetChanged();
			break;
		default:
			break;
		}
	}

	/** fetching status */
	private boolean fetching = false;

	/** footer status */
	private boolean hasFooter = false;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

		// ensure that:
		// 1) rpc available
		// 2) message cursor is not null
		// 3) not currently fetching
		// 4) list adapter has items
		// 5) visible item is w/in 10 (look ahead) of tail
		if (rpc != null && cursor != null & !fetching && totalItemCount != 0
				&& (firstVisibleItem + visibleItemCount + 10) >= totalItemCount)

			// perform a fetch
			new MobTask<String>() {
				@Override
				protected void onPreExecute() {
					// set fetching status
					fetching = true;

					// show footer
					if (!hasFooter) {
						mList.addFooterView(footer);
						hasFooter = true;
					}

					super.onPreExecute();
				}

				@Override
				public String doit() throws RemoteServiceFailureException {
					return rpc.loadMessages(carId, true);
				}

				@Override
				public void onFailure(int errResId) {
					// sync content
					sync(false);

					// continue with failure
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(String t) {
					// set cursor
					cursor = t;

					// remove footer if at end of content
					if (cursor == null) {
						mList.removeFooterView(footer);
						hasFooter = false;
					}

					// sync content
					sync(true);
				}

				@Override
				public void onPostExecute(String result) {
					// set fetching status
					fetching = false;

					// continue
					super.onPostExecute(result);
				}
			}.execute();
	}

	private void load() {
		// update the controls
		// TODO update the title
		//updateControls(/*id*/);

		// load the messages
		new MobTask<String>(selector) {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				// set the loading string
				setEmptyString(R.string.warn_load, true);

				// clear the messages
				messages.clear();
				cursor = null;

				// update title
				setTitle(MessageType.values()[mode].toString().toLowerCase());

				// update the view
				adapter.notifyDataSetChanged();
			}

			@Override
			public String doit() throws RemoteServiceFailureException {
				// load messages
				return rpc.loadMessages(carId, false);
			}

			@Override
			public void onFailure(int errResId) {
				// synchronize content
				sync(false);

				// continue with failure
				super.onFailure(errResId);
			}

			@Override
			public void onSuccess(String t) {
				// set to initialized
				initialized = true;
				
				// set cursor
				cursor = t;

				// synchronize content
				sync(true);
			}
		}.execute();	
	}
	
	/**
	 * Show the vehicle at the given position.
	 * 
	 * @param position
	 */
	private void showMessage(int position) {
		Intent intent = new Intent(this, Reader.class);
		intent.putExtra(Reader.MESSAGE, messages.get(position));
		intent.putExtra(Reader.INDEX, position);
		startActivity(intent);
	}

	/**
	 * Sync the rpc data with this activity.
	 */
	private void sync(boolean success) {
		// sync model (not necessary if we failed)
		if (success)
			rpc.syncMessages(messages);

		// update the delete button (deletions & messages are both not empty)
		boolean msgEmpty = messages.isEmpty();
		btnDelete.setEnabled(!deletions.isEmpty() && !msgEmpty);

		// update the empty message
		if (msgEmpty)
			switch (MessageType.values()[/*settings.getInt(RpcService.PREF_MESSAGE, 3)*/mode]) {
			case INQUIRIES:
				setEmptyString(R.string.warn_inquiries, false);
				break;
			case RESPONSES:
				setEmptyString(R.string.warn_responses, false);
				break;
			case ALL:
			default:
				setEmptyString(R.string.warn_messages, false);
				break;
			}

		// if this is a general message view, update controls
//		if (carId == null)
//			switch (mode) {
//			case R.id.inquiries:
//				btnAll.setEnabled(true);
//				btnInquiries.setEnabled(false);
//				btnResponses.setEnabled(true);
//				break;
//			case R.id.responses:
//				btnAll.setEnabled(true);
//				btnInquiries.setEnabled(true);
//				btnResponses.setEnabled(false);
//				break;
//			case R.id.all:
//			default:
//				btnAll.setEnabled(false);
//				btnInquiries.setEnabled(true);
//				btnResponses.setEnabled(true);
//				break;
//			}

		// update view
		adapter.notifyDataSetChanged();
	}

//	/**
//	 * Update control panel
//	 */
//	private void updateControls(/*int viewId*/) {
//		// update state
//		//this.mode = viewId;
//
//		// update preferences
//		//settings.edit().putInt(RpcService.PREF_MESSAGE, viewId).commit();
//
////		// update title
////		switch (viewId) {
////		case R.id.all:
////			setTitle(R.string.app_messages);
////			break;
////		case R.id.inquiries:
////			setTitle(R.string.app_inquiries);
////			break;
////		case R.id.responses:
////			setTitle(R.string.app_responses);
////			break;
////		}
//
////		// disable all controls
////		btnAll.setEnabled(false);
////		btnInquiries.setEnabled(false);
////		btnResponses.setEnabled(false);
//	}
}