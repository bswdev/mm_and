package com.boosed.mm.activity;

import java.util.HashSet;
import java.util.Set;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boosed.mm.R;
import com.boosed.mm.activity.task.RpcTask;
import com.boosed.mm.anim.SpinnerInterpolator;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.rpc.RpcService.LocalBinder;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.image.ImageLoader;

/**
 * All activities for this app will share the same options menus, etc.
 * 
 * @author dsumera
 */
public abstract class AbstractActivity extends Activity {

	// private Dialog dialog;

	protected static ImageLoader dm;

	protected static LocationManager lm;

	protected static DisplayMetrics metrics = null;

	/** credentials dialog id */
	// public static final int DIALOG_CREDS_ID = 20;
	public static final int DLG_ADD_ACCT = 20;
	public static final int DLG_LOGIN_FAILED_ID = 21;
	private static final int DLG_ACCTS = 22;
	private static final int DLG_CONFIRM = 23;
	private static final int DLG_UPDATE = 24;

	// public static final int DIALOG_SENDING_ID = 2;

	/** preferences */
	public static SharedPreferences settings;

	/** remote bounded status */
	private boolean bounded = false;

	/** the handler for this UI */
	public Handler handler;

	/** messenger instances */
	private Messenger localMessenger, remoteMessenger;

	/** raw service handle */
	protected static RpcService rpc;

	/** the interest ops based from ImType */
	private int ops;

	/** rpc service intent */
	private Intent rpcIntent;

	/** the wakelock */
	private static WakeLock wl;

	/**
	 * Whether this activity has already been initialized. If an icicle is
	 * present on <code>onCreate</code>, this is automatically true (activity
	 * was killed and restored). This will be false otherwise since it is never
	 * explicitly set, so be sure to set it to <code>true</code> whenever you
	 * want to use it to discriminate whether an activity has been initialized
	 * or not.
	 */
	protected boolean initialized = false;

	/** "authenticating" dialog */
	private ProgressDialog loginDialog = null;

	// REMOTE SERVICE CONNECTION
	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection connection = new ServiceConnection() {
		public void onServiceConnected(ComponentName name, IBinder service) {

			LocalBinder binder = (LocalBinder) service;

			// get the actual service
			if (AbstractActivity.rpc == null)
				AbstractActivity.rpc = binder.getService();

			// set service on RpcTask
			// RpcTask.setRpc(BaseActivity.this.rpc);

			// get the rpc service's messenger object for listening
			remoteMessenger = binder.getMessenger();

			// We want to monitor the service for as long as we are connected to
			// it.
			try {
				Message msg = Message.obtain(null, RpcService.MSG_REGISTER_CLIENT);

				// set the reply to activity's messenger
				msg.replyTo = localMessenger;

				// setting the interest ops
				msg.arg1 = ops;

				// register this activity as a listener to rpc
				remoteMessenger.send(msg);

				// // initial check for determining connectivity
				// if (BaseActivity.this.rpc.connected)
				// // everything is running
				// onConnect();
				// else {
				// if (BaseActivity.this.rpc.online)
				// // online but not connected (could be disconnected but
				// // don't invoke this on initial bind)
				// onOnline();
				// else
				// // service is offline for the moment
				// onOffline();
				// }

				// reset notifications
				AbstractActivity.rpc.resetNotes(ops);

				// reset the polling for online
				if (!rpc.online)
					rpc.resetPoll();

				// bounded callback
				// Log.i("basic binding");
				onBind();

				// force start this service without binding context (so it won't
				// be terminated along with activity)
				startService(rpcIntent);
			} catch (Exception e) {
				// In this case the service has crashed before we could even
				// do anything with it; we can count on soon being
				// disconnected (and then reconnected if it can be restarted)
				// so there is no need to do anything here.
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			remoteMessenger = null;

			// callback for disconnection
			onDisconnect();
		}
	};

	private void doBindService() {
		// Establish a connection with the service. We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
		bindService(rpcIntent, connection, Context.BIND_AUTO_CREATE);
		bounded = true;
	}

	private void doUnbindService() {
		if (bounded) {
			// If we have received the service, and hence registered with
			// it, then now is the time to unregister.
			if (remoteMessenger != null) {
				try {
					Message msg = Message.obtain(null, RpcService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = localMessenger;
					msg.arg1 = ops;
					remoteMessenger.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service
					// has crashed.
				}
			}

			// callback for disconnection
			// onDisconnect();

			// Detach our existing connection.
			unbindService(connection);
			bounded = false;

			// stop the service if no one else is using it; next activity that
			// binds service will cause rpc.onCreate to be invoked again
			// TODO: when do we stop this service
			// if (rpc != null && !rpc.connected)
			// stopService(rpcIntent);
		}
	}

	// END REMOTE

	// LIFECYCLE METHODS

	protected int titleLayoutResId;

	/**
	 * Constructor to provide the title layout.
	 */
	public AbstractActivity(int titleLayoutResId) {
		this.titleLayoutResId = titleLayoutResId;
	}

	private boolean customTitleSupported;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// change the theme from no title bar to mobimotos (this is done to
		// prevent a title from being shown while activity is still rendering)
		setTheme(R.style.theme_mobimotos);
		// setTheme(android.R.style.Theme);

		// request before setting content
		customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		// request for indeterminate progress bar
		// requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		super.onCreate(savedInstanceState);

		// init intent
		rpcIntent = new Intent(this, RpcService.class);

		// init local handler
		localMessenger = new Messenger(handler = createHandler());

		// initialization state (icicle is not null when activity is being
		// _RE_initialized)
		initialized = savedInstanceState != null;

		// get confirm view/menu item
		confirm = (Tuple<Integer, Object>) getLastNonConfigurationInstance();
		if (confirm == null)
			confirm = new Tuple<Integer, Object>(-1, null);

		if (wl == null) {
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		}

		// init preferences
		if (settings == null)
			settings = PreferenceManager.getDefaultSharedPreferences(this);

		// init dm
		if (dm == null)
			dm = ImageLoader.getInstance(this); //DrawableListManager.getInstance(/*this*/);

		// init lm
		if (lm == null)
			lm = (LocationManager) getSystemService(LOCATION_SERVICE);

		// init metrics
		if (metrics == null)
			metrics = new DisplayMetrics();

		// set metrics
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// bind service
		doBindService();

		if (settings.getBoolean("wakelock", false))
			wl.acquire();
		// // set the title
		// getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
		// R.layout.title);
		// setTitle(R.string.acct_google);

		// change the theme from no title bar to mobimotos (this is done to
		// prevent a title from being shown while activity is still rendering)
		// setTheme(R.style.theme_mobimotos);

		// request before setting content
		// customTitleSupported =
		// requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		// getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
		// R.layout.title);

		// close "authenticating" dialog
		if (loginDialog != null) {
			loginDialog.dismiss();
			loginDialog = null;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		// unbind service
		doUnbindService();

		if (wl.isHeld())
			wl.release();

		// kill all task dialogs
		for (MobTask<?> task : tasks)
			task.dismissDialog();

		// close "authenticating" dialog
		if (loginDialog != null) {
			loginDialog.dismiss();
			loginDialog = null;
		}
	}

	/** labels for the accounts, will be same for all activities */
	private static CharSequence[] labels;

	@Override
	protected Dialog onCreateDialog(final int id) {
		// dialog builder
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		switch (id) {
		case DLG_ADD_ACCT:
			builder.setTitle(R.string.dlg_title_confirm).setCancelable(false);

			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// user has confirmed, show create account dialog
						AccountManager.get(AbstractActivity.this).addAccount(
								getString(R.string.acct_google), getString(R.string.token_gae),
								null, new Bundle(), AbstractActivity.this, null, null);
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						// user cancelled, do nothing
						break;
					}
				}
			};

			// ok & cancel buttons
			builder.setPositiveButton(R.string.dlg_btn_ok, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			// set message and create
			return builder.setMessage(R.string.cfm_add_acct).create();
		case DLG_CONFIRM:
			// provide a confirm dialog for executing any task

			// do not allow the user to cancel this dialog
			builder.setTitle(R.string.dlg_title_confirm).setCancelable(false);

			ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// user has confirmed, run confirm controls
						if (confirm.b != null) {
							if (confirm.b instanceof MenuItem)
								// replay a context menu item
								onContextItemSelected((MenuItem) confirm.b);
							else
								// not menu item, invoke View replay
								onReplay((View) confirm.b);
						}
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						// null out the confirm identifier since user pressed
						// canceled
						confirm.b = null;

						break;
					}

					// remove the dialog
					removeDialog(DLG_CONFIRM);
				}
			};

			// ok & cancel buttons
			builder.setPositiveButton(R.string.dlg_btn_ok, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			// set message and create
			return builder.setMessage(confirm.a).create();
		case DLG_ACCTS:
			builder.setTitle(R.string.dlg_title_login).setCancelable(false);
			ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, final int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// user has confirmed, show create account dialog
						AccountManager.get(AbstractActivity.this).addAccount(
								getString(R.string.acct_google), getString(R.string.token_gae),
								null, new Bundle(), AbstractActivity.this, null, null);
						break;
					case DialogInterface.BUTTON_NEUTRAL:
						startActivity(new Intent(AbstractActivity.this, Notice.class));
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						// null out the replay since user pressed canceled
						replay = null;
						break;
					default:
						// capture the account selection in preferences
						settings.edit().putInt(RpcService.PREF_ACCT, which).commit();

						// perform login with the given account
						new MobTask<Void>() {
							@Override
							public Void doit() throws RemoteServiceFailureException {
								rpc.login(/* rpc.accounts[which] */which);
								return null;
							}

							@Override
							public void onSuccess(Void t) {
								// show "authenticating" dialog
								loginDialog = ProgressDialog.show(AbstractActivity.this, "",
										"authenticating");
								
								// do nothing, the handler will be notified when
								// connected
							}
						}.execute();

//						// show "authenticating" dialog
//						loginDialog = ProgressDialog.show(AbstractActivity.this, "",
//								"authenticating");
						
						// dialog.dismiss();
						// setStatus("authenticating");
						// loginDialog =
						// ProgressDialog.show(AbstractActivity.this, "",
						// "authenticating");
						break;
					}

					// remove this dialog so it is created every time
					removeDialog(id);
				}
			};

			// set the eula here
			builder.setNeutralButton(R.string.dlg_btn_tos, ocl);

			// allow user to cancel
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			// allow user to add account
			builder.setPositiveButton(R.string.dlg_btn_add, ocl);

			// initialize with the last used account OR the first available (if
			// not used before)
			return builder.setSingleChoiceItems(/* rpc.accountLabels */AbstractActivity.labels,
					settings.getInt(RpcService.PREF_ACCT, -1), ocl).create();
		case DLG_UPDATE:
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setTitle(R.string.dlg_title_update).setCancelable(false);

			ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// user has confirmed, visit the market
						Intent intent = new Intent();
						intent.setAction(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(getString(R.string.url_market)));
						startActivity(intent);
						finish();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						// user cancelled, close app
						finish();
						break;
					}
				}
			};

			// update & cancel buttons
			builder.setPositiveButton(R.string.dlg_btn_update, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			// set message and create
			return builder.setMessage(R.string.warn_update).create();
		case DLG_LOGIN_FAILED_ID:
			// alert message for failed login
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			// TODO: do we need a button on it?
			// builder.setPositiveButton(R.string.label_ok, null);
			builder.setTitle(R.string.dlg_title_login);
			builder.setMessage(R.string.warn_creds);

			return builder.create();
			// case DIALOG_SENDING_ID:
			// // sending message
			// return ProgressDialog.show(this, "",
			// getText(R.string.info_sending));
		default:
			return super.onCreateDialog(id);
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		// case DLG_CONFIRM:
		// // get dialog and set the proper message
		// ((AlertDialog) dialog).setMessage(getString(confirm.a));
		// break;
		case DLG_ACCTS:
			// select the previously selected account
			((AlertDialog) dialog).getListView().setItemChecked(
					settings.getInt(RpcService.PREF_ACCT, -1), true);
			break;
		default:
			super.onPrepareDialog(id, dialog);
			break;
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// set metrics
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// this path may be exercised by a replay of options item
		switch (item.getItemId()) {
		case R.id.opt_settings:
			return onOptionsItemSelected(item);
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_options, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		// adjust the logout menu item
		MenuItem logout = menu.findItem(R.id.opt_logout);
		boolean enabled = rpc != null && rpc.accountId != null;
		logout.setEnabled(enabled);
		logout.setVisible(enabled);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.opt_logout:
			new MobTask<Boolean>(item) {

				@Override
				public Boolean doit() throws RemoteServiceFailureException {
					return rpc.logout();
				}

				@Override
				public void onSuccess(Boolean t) {
					if (t)
						toast("log off successful");

				}
			}.execute();
			return true;
		case R.id.opt_search:
			startActivity(new Intent(this, Search.class));
			return true;
		case R.id.opt_dealer:
			startActivity(new Intent(this, Dealer.class));
			return true;
		case R.id.opt_inventory:
			startActivity(new Intent(this, Inventory.class));
			return true;
		case R.id.opt_messages:
			startActivity(new Intent(this, Messages.class));
			return true;
		case R.id.opt_settings:
			new MobTask<Account>(item) {

				@Override
				public Account doit() throws RemoteServiceFailureException {
					return rpc.service.loadAccount();
				}

				@Override
				public void onSuccess(Account t) {
					// create intent and set account
					Intent intent = new Intent(AbstractActivity.this, Manage.class);
					intent.putExtra(Manage.ACCOUNT, t);

					// start activity
					startActivity(intent);
				}
			}.execute();

			// // create intent and set account
			// Intent intent = new Intent(AbstractActivity.this,
			// Accounts.class);
			// Account account = new Account("1", "test", "test@test.com");
			// intent.putExtra("account", account);
			//
			// // start activity
			// startActivity(intent);

			return true;
			// case R.id.opt_crowds:
			// startActivity(new Intent(this, Crowds.class));
			// finish();
			// return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Used to manage the confirmation idiom.
	 */
	@Override
	public Object onRetainNonConfigurationInstance() {
		// return the confirm object that executes on user confirmation
		return confirm;
	}

	// END LIFECYCLE

	// CALLBACK METHODS

	// END CALLBACK

	// PUBLIC METHODS

	// /* track logging in */
	// private boolean login = false;

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);

		// set custom title if it is supported
		if (customTitleSupported)
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, titleLayoutResId);

		// assign all the UI element handles
		assignHandles();
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(view);

		// set custom title if it is supported
		if (customTitleSupported)
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, titleLayoutResId);
	}

	// @Override
	// public void finish() {
	// if (dialog != null)
	// dialog.dismiss();
	//
	// super.finish();
	// }

	// public class LoginTask extends AsyncTask<String, Void, Void> {
	//
	// //private Dialog dialog;
	//
	// @Override
	// protected void onPreExecute() {
	// super.onPreExecute();
	// // login = true;
	// dialog = ProgressDialog.show(BaseActivity.this, "",
	// getText(R.string.info_login));
	// dialog.setCancelable(false);
	// }
	//
	// @Override
	// protected Void doInBackground(String... params) {
	// // setting the username & password prefs triggers the rpc service to
	// // login
	// setPrefs(params);
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// super.onPostExecute(result);
	// // login = false;
	// dialog.dismiss();
	// }
	// }
	//
	// private class LogoutTask extends AsyncTask<Void, Void, Void> {
	//
	// //private Dialog dialog;
	//
	// @Override
	// protected void onPreExecute() {
	// super.onPreExecute();
	// dialog = ProgressDialog.show(BaseActivity.this, "",
	// getText(R.string.info_logout));
	// dialog.setCancelable(false);
	// }
	//
	// @Override
	// protected Void doInBackground(Void... params) {
	// rpc.logout();
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// super.onPostExecute(result);
	//
	// try {
	// dialog.dismiss();
	// } catch (Exception e) {
	// // catch leaked window, ignored
	// }
	// }
	// }

	// public void onConnect() {
	// presence.setImageResource(android.R.drawable.presence_online);
	// }
	//

	/**
	 * Create a new instance of BaseHandler. Override handleMessage in
	 * BaseHandler if additional message handling is needed.
	 */
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	/**
	 * Ends any current statuses, closes login dialog and runs any replays. This
	 * method is invoked immediately if already connected, or potentially
	 * following <code>onOnline</code>.
	 */
	public void onConnect() {
		// connected
		// presence.setImageResource(android.R.drawable.presence_online);

		// suppress login dialog if showing
		if (loginDialog != null) {
			loginDialog.dismiss();
			loginDialog = null;
		}
		// endStatus();

		// user has authenticated, perform any replays
		if (replay != null) {
			if (replay instanceof MenuItem)
				// replay a context menu item
				onContextItemSelected((MenuItem) replay);
			else
				// not menu item, invoke View replay
				onReplay((View) replay);

			// prevent future replays (when connected, execute once only)
			this.replay = null;
		}
	}

	public void onDisconnect() {
		// close "authenticating" dialog
		if (loginDialog != null) {
			loginDialog.dismiss();
			loginDialog = null;
		}
		
		// if (login)
		// user was attempting a login
		// showDialog(DIALOG_LOGIN_FAILED_ID);

		// getWindow().setFeatureInt(Window.FEATURE_INDETERMINATE_PROGRESS, 0);
		// progress.setVisibility(View.INVISIBLE);

		// presence.setImageResource(android.R.drawable.presence_invisible);
	}

	/**
	 * Callback whenever the activity has established internet connectivity. The
	 * user should consider that this callback will not be invoked if the user
	 * is already "connected."
	 */
	public void onOnline() {
		// presence.setImageResource(android.R.drawable.presence_invisible);

		// the default image
		dm.setBlank(new Car().image);

		// the blank image
		// dm.loadImage(RpcService.URL + "/photo/blank.jpg", null);
		// new ImageTask(null, RpcService.URL + "/photo/blank.jpg").execute();

		// generic chatroom image
		// dm.loadImage(RpcService.URL + "/images/room2.png", null);
		// new ImageTask(null, RpcService.URL + "/images/room2.png").execute();
	}

	public void onOffline() {
		// presence.setImageResource(android.R.drawable.presence_offline);
	}

	// /**
	// * Set the <code>ImType</code>s this activity is interested in receiving
	// * callbacks for.
	// *
	// * @param types
	// */
	// public void setOps(ImType... types) {
	// for (ImType type : types)
	// ops |= (1 << type.ordinal());
	// }

	// progress icon
	// protected ProgressBar progress;

	// progress status and title
	// protected TextView status, title;

	private TextView title;

	private ImageView icon;

	private Animation spin;

	public void assignHandles() {
		title = (TextView) findViewById(android.R.id.title);
		icon = (ImageView) findViewById(R.id.title_icon);
		// progress = (ProgressBar) findViewById(R.id.progress);
		// status = (TextView) findViewById(R.id.status);
		// title = (TextView) findViewById(R.id.title);

		// ImageView icon = find(ImageView.class, R.id.title_icon);
		spin = AnimationUtils.loadAnimation(this, R.anim.rotate);
		spin.setInterpolator(new SpinnerInterpolator());
		spin.setRepeatCount(Animation.INFINITE);
		// anim.setRepeatMode(Animation.REVERSE);
		// icon.startAnimation(anim);
		// icon.setAnimation(spin);
	}

	/**
	 * Set a <code>View</code> or <code>MenuItem</code> that will be actuated
	 * upon successful user confirmation.
	 * 
	 * @param confirm
	 */
	public boolean confirm(Object confirm, int stringResId) {
		if (confirm.equals(this.confirm.b)) {
			// this object has already been confirmed through dialog, set to
			// null and return true
			this.confirm.a = -1;
			this.confirm.b = null;
			return true;
		}

		// set the confirm control
		this.confirm.a = stringResId;
		this.confirm.b = confirm;

		// show confirmation dialog (this will set above null if negated)
		// log("showing the confirmation dialog " + stringResId);
		showDialog(DLG_CONFIRM);

		return false;
	}

	public void log(String message) {
		Log.i("mm", message);
	}

	/**
	 * Start dialog for handling logins.
	 * 
	 * @param labels
	 */
	public void login(CharSequence[] labels) {
		// initialize the dialog labels
		AbstractActivity.labels = labels;

		// show the dialog
		showDialog(DLG_ACCTS);
	}

	public void setStatus(String status) {
		// spin.start();
		// getWindow().setFeatureInt(Window.FEATURE_INDETERMINATE_PROGRESS, 1);
		// progress.setVisibility(View.VISIBLE);
		// this.status.setVisibility(View.VISIBLE);
		// this.status.setText(status);
	}

	public void endStatus() {
		// spin.cancel();
		// getWindow().setFeatureInt(Window.FEATURE_INDETERMINATE_PROGRESS,
		// 10000);
		// progress.setVisibility(View.INVISIBLE);
		// status.setVisibility(View.INVISIBLE);
	}

	@Override
	public void setTitle(int titleId) {
		// getWindow().setTitle(getString(titleId));
		title.setText(titleId);
	}

	@Override
	public void setTitle(CharSequence title) {
		// getWindow().setTitle(title);
		this.title.setText(title);
	}

	/**
	 * Set the title icon.
	 * 
	 * @param titleIconResId
	 */
	public void setTitleIcon(int titleIconResId) {
		icon.setImageResource(titleIconResId);
	}

	// protected static Account[] accounts;
	// protected static CharSequence[] accountLabels;

	// public void showAccounts(Account[] accounts) {
	// BaseActivity.accounts = accounts;
	//
	// // create the showable list
	// accountLabels = new CharSequence[accounts.length];
	// int i = 0;
	// String name = "";
	// for (Account account : accounts) {
	// name = account.name;
	// int at = name.indexOf('@');
	// accountLabels[i++] = at == -1 ? name : name.substring(0, at);
	// }
	//
	// showDialog(DLG_ACCOUNTS);
	// }

	// END PUBLIC

	// ABSTRACT METHODS

	/**
	 * Hook for when the service is bound (i.e., rpc object is available;
	 * binding is initiated from onResume).
	 */
	abstract public void onBind();

	// END ABSTRACT

	// class ImageTask extends AsyncTask<String, Void, Bitmap> {
	//
	// private ImageView view;
	//
	// public ImageTask(ImageView view) {
	// this.view = view;
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// view.setImageResource(android.R.drawable.progress_indeterminate_horizontal);
	// }
	//
	// @Override
	// protected Bitmap doInBackground(String... params) {
	// return dm.getDrawable(AbstractActivity.this, params[0]);
	// }
	//
	// @Override
	// protected void onPostExecute(Bitmap result) {
	// // set the image
	// view.setImageBitmap(result);
	// }
	// }

	/** keep track of all tasks executed by this activity */
	private Set<MobTask<?>> tasks = new HashSet<MobTask<?>>();

	/** task which will be executed upon confirmation */
	// private MobTask<?> confirmTask;

	abstract class MobTask<T> extends RpcTask<T> {

		// private String status = null;

		private Object replay = null;

		public MobTask() {
			// default no-arg constructor
			tasks.add(this);
			// this.status = "";
		}

		public MobTask(Object replay) {
			this();
			this.replay = replay;
		}

		// /**
		// * Run an <code>AsyncTask</code> with status in the title bar.
		// *
		// * @param status
		// */
		// private MobTask(String status) {
		// tasks.add(this);
		// this.status = status;
		// }

		// public MobTask(int status) {
		// this(getString(status));
		// }

		// private MobTask(String status, Object replay) {
		// this(status);
		// this.replay = replay;
		// }

		// public MobTask(int status, Object replay) {
		// this(getString(status), replay);
		// }

		/**
		 * Run an <code>AsyncTask</code> with the alert dialog showing.
		 * 
		 * @param status
		 * @param modal
		 */
		private MobTask(String status, boolean modal) {
			super(AbstractActivity.this, status);
			tasks.add(this);
		}

		public MobTask(int status, boolean modal) {
			this(getString(status), modal);
		}

		private MobTask(String status, boolean modal, Object replay) {
			this(status, modal);
			this.replay = replay;
		}

		public MobTask(int status, boolean modal, Object replay) {
			this(getString(status), modal, replay);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// if (status != null)
			setStatus(/* status */null);
		}

		/**
		 * Failure routine automatically captures session timeouts and replays
		 * to notify and process user logins.
		 */
		@Override
		public void onFailure(int errResId) {
			switch (errResId) {
			case R.string.exception_rpc_incompatible:
				// old client, suggest updating
				showDialog(DLG_UPDATE);
				break;
			case R.string.exception_session_timeout:
				// start a new task to retrieve accounts for login
				new MobTask<Void>(null) {
					@Override
					protected void onPreExecute() {
						// set replay to local instance
						AbstractActivity.this.replay = MobTask.this.replay;
					}

					@Override
					public Void doit() throws RemoteServiceFailureException {
						// start a login dialog for this activity
						rpc.showLogin(handler);
						return null;
					}

					@Override
					public void onSuccess(Void t) {
						// not implemented
					}
				}.execute();
				break;
			default:
				// toast the error
				toast(getString(errResId));
				break;
			}
		}

		/**
		 * Ends any outstanding title statuses and removes task from observed
		 * list.
		 */
		protected void onPostExecute(T result) {
			// end the progress status in the title bar
			// if (status != null)
			endStatus();

			// execution complete, remove this task from the list
			tasks.remove(this);

			super.onPostExecute(result);
		}
	}

	public void toast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	/** the view that will be "replayed" when user authenticates */
	private Object replay = null;

	/** the view/menu item that will be executed when user confirms action */
	private Tuple<Integer, Object> confirm;

	/**
	 * Replay a <code>View</code> which has invoked an action. The base class
	 * automatically handles menu item replays. Override this to replay the
	 * <code>view</code> against custom callbacks.
	 * 
	 * @param view
	 */
	public void onReplay(View replay) {

	}

	/**
	 * Convenience method to retrieve widgets by their android ids.
	 * 
	 * @param <T>
	 * @param clazz
	 * @param resId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends View> T find(Class<T> clazz, int resId) {
		return (T) findViewById(resId);
	}

	// @Override
	// protected void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// //super.onActivityResult(requestCode, resultCode, data);
	//
	// if (resultCode == RESULT_OK)
	// if (requestCode == 0) {
	// // get the result
	// String result = data.getStringExtra(Intents.Scan.RESULT);
	// toast("you got some data! " + result);
	// //setTheme(R.style.theme_mobimotos);
	// }
	// }

	// /**
	// * Set <code>View</code> to invoke when rpc service has connected.
	// *
	// * @param replay
	// */
	// protected void setReplay(Object replay) {
	// this.replay = replay;
	// }

	// /**
	// * Set a <code>View</code> that has invoked an action hook.
	// *
	// * @param view
	// */
	// public void setReplay(View view) {
	// view = replay;
	// }
}