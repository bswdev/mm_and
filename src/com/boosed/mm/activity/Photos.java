package com.boosed.mm.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map.Entry;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;

import com.boosed.mm.R;
import com.boosed.mm.activity.task.RpcTask;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.GalleryAdapter;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.crop.CropImage;
import com.boosed.mm.ui.image.FileCache;

public class Photos extends AbstractActivity implements Controllable {

	/** intent key for ad reference */
	public static final String AD = "com.boosed.mm.photos.Ad";

	/** intent key for model id/name */
	public static final String MODEL = "com.boosed.mm.photos.Model";

	/** intent key for ad position */
	public static final String POSITION = "com.boosed.mm.photos.Position";

	/** intent key for ad main image url */
	public static final String URL = "com.boosed.mm.photos.Url";

	// private List<Entry<String, Tuple<Boolean, String>>> images = new
	// ArrayList<Entry<String, Tuple<Boolean, String>>>();

	private/* ArrayAdapter<Entry<String, Tuple<Boolean, String>>> */GalleryAdapter adapter;

	/** the ad */
	private Ad ad;

	/** uri for the saved image resource */
	private Uri uri;

	/** the image index for the gallery */
	private int index = -1;

	/** the extras */
	// private Bundle data;

	public Photos() {
		super(R.layout.title_photos);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_photos);

		Intent intent = getIntent();

		// initialize the galleries
		initGallery();

		if (initialized) {
			// restore from previous activity (sometimes this is called twice
			// from emulator?)

			// set the uri
			uri = savedInstanceState.getParcelable("uri");

			// set the index
			index = savedInstanceState.getInt("index");

			// set the ad
			setAd((Ad) savedInstanceState.getSerializable(AD));

			// set the result (if we already made an edit)
			// Intent data = savedInstanceState.getParcelable("result");
			if (intent.getStringExtra(Photos.URL) != null)
				setResult(RESULT_OK, intent);
			// data = savedInstanceState.getParcelable("data");
		} else {
			// initialize from intent
			setAd((Ad) intent.getSerializableExtra(AD));

			// get the data
			// data = intent.getExtras();
		}

		// set the title
		// setTitle(intent.getStringExtra(MODEL));
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// ad
		outState.putSerializable(AD, (Serializable) ad);

		// image resource
		outState.putParcelable("uri", uri);

		// gallery index
		outState.putInt("index", gallery.getSelectedItemPosition());

		// result
		// outState.putParcelable("result", getIntent());
		// outState.putParcelable("data", data);
	}

	// @Override
	// protected void onResume() {
	// super.onResume();
	//
	// // need to add delay for orientation changes or galleries will not
	// // redraw properly
	//
	// // may need to adjust this time to work properly
	// handler.postDelayed(new Runnable() {
	// @Override
	// public void run() {
	// // refresh the adapters
	// adapter.notifyDataSetChanged();
	// }
	// }, 2000);
	// }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		// init menu
		getMenuInflater().inflate(R.menu.ctx_menu_image, menu);

		// set title
		menu.setHeaderTitle(R.string.ctx_hdr_image);

		// set primary photo option to true/false based upon photo approval
		// make sure the current photo is also not the primary
		Entry<String, Tuple<Boolean, String>> entry = adapter
				.getItem(((AdapterContextMenuInfo) menuInfo).position);
		menu.findItem(R.id.ctx_primary).setVisible(
				entry.getValue().a && !entry.getKey().equals(ad.imageKey));
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case R.id.ctx_primary:
			new MobTask<Void>(item) {

				private int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
				private final String blobkey = ((Entry<String, Tuple<Boolean, String>>) gallery
						.getItemAtPosition(position)).getKey();

				@Override
				public Void doit() throws RemoteServiceFailureException {
					rpc.setPrimary(ad.car.getId(), ad.id, blobkey);
					return null;
				}

				@Override
				public void onSuccess(Void t) {
					ad.imageKey = blobkey;
					// url = ad.getUrl(blobkey);

					// set the new url
					// data.putString(Photos.URL, ad.getUrl(blobkey));

					// set result iff we change the primary image
					// just reuse the invoking intent
					setResult(RESULT_OK, getIntent().putExtra(Photos.URL, ad.getUrl(blobkey)));// new
					// Intent().putExtras(data));

					// change the display for current image entry only if
					// approved
					if (entry.getValue().a) {
						if (entry.getKey().equals(blobkey)) {
							// image is primary
							approval.setImageResource(R.drawable.ic_menu_gallery);
							status.setText(R.string.stat_primary);
						} else {
							// image is approved
							approval.setImageResource(R.drawable.ic_menu_tick);
							status.setText(R.string.stat_approved);
						}
					}

					// imageUrl = entry.getValue().b;
					// adapter.notifyDataSetChanged();
				}
			}.execute();
			return true;
		case R.id.ctx_delete:
			int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
			final String blobkey = ((Entry<String, Tuple<Boolean, String>>) gallery
					.getItemAtPosition(position)).getKey();

			if (confirm(item, R.string.cfm_del_img))
				new MobTask<Void>(R.string.stat_delete, true, item) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						rpc.deleteImage(ad.id, blobkey);
						return null;
					}

					@Override
					public void onSuccess(Void t) {
						ad.removeImage(blobkey);
						setAd(ad);
						// adapter.notifyDataSetChanged();
					}
				}.execute();
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	// @Override
	// protected Dialog onCreateDialog(int id) {
	// // for all cases
	// // AlertDialog.Builder builder = new AlertDialog.Builder(this);
	//
	// switch (id) {
	// default:
	// return super.onCreateDialog(id);
	// }
	// }

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onBind() {
		// redraw the gallery to reset the borders
		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// adapter.notifyDataSetChanged();
		// }
		// }, 500);
		// image.invalidate();
	}

	private TextView status;
	private ImageView image, approval;
	private Gallery gallery;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// galleries
		status = find(TextView.class, R.id.status);
		approval = find(ImageView.class, R.id.approval);
		image = (ImageView) findViewById(R.id.image);
		gallery = (Gallery) findViewById(R.id.gallery);
		// gallery2 = (Gallery) findViewById(R.id.gallery2);

		// // set the controls
		// View view = getLayoutInflater().inflate(R.layout.search_ctrl, null);
		//
		// LayoutParams lp = new LayoutParams(0, LayoutParams.FILL_PARENT, 1);
		// LinearLayout controls = (LinearLayout)
		// findViewById(R.id.list_control);
		// controls.addView(view, lp);
		// controls.setVisibility(View.VISIBLE);
		//
		// btnTypes = (ImageButton) findViewById(R.id.types);
		// btnTypes.setEnabled(false);
		// btnMakes = (ImageButton) findViewById(R.id.makes);
		// btnMakes.setEnabled(false);
		// btnModels = (ImageButton) findViewById(R.id.models);
		// btnModels.setEnabled(false);
		// btnFilters = (ImageButton) findViewById(R.id.filters);
		// btnFilters.setEnabled(false);
	}

	// @Override
	// public void setTitle(CharSequence title) {
	// super.setTitle(getString(R.string.app_photos) + " [" + title + "]");
	// }

	@Override
	public void onControl(View view) {
		int viewId = view.getId();
		switch (viewId) {
		case R.id.photo:
			doTakePhotoAction();
			break;
		case R.id.select:
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent,
					getString(R.string.dlg_title_picture)), SELECT_RESULT);
			break;
		case R.id.cancel:
			// set result ok and return
			// setResult(RESULT_OK, new Intent().putExtra("url", url));
			finish();
			break;
		default:
			break;
		}
	}

	public String getPath(Uri uri) {
		String[] projection = { /* MediaStore.Images.Media.DATA, Media.TITLE, */Media.DISPLAY_NAME };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(Media.DISPLAY_NAME);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case RESULT_OK:
			switch (requestCode) {
			case 10:
				break;
			case SELECT_RESULT:
				doCopyPhoto(data.getData());
				// follow through to camera_result
			case CAMERA_RESULT:
				Intent intent = new Intent(this, CropImage.class);
				// here you have to pass absolute path to your file
				intent.putExtra("image-path", uri.getPath());
				// Log.w("chat", uri.getPath());
				intent.putExtra("outputX", 400);
				intent.putExtra("ouputY", 240);
				intent.putExtra("aspectX", 5);
				intent.putExtra("aspectY", 3);
				intent.putExtra("scale", true);
				startActivityForResult(intent, CROP_RESULT);
				break;
			// case SELECT_RESULT:
			// // picked photo, need to copy to uri
			// doCopyPhoto(data.getData());
			// intent = new Intent(this, CropImage.class);
			// intent.putExtra("image-path", uri.getPath());
			//
			// intent.putExtra("outputX", 400);
			// intent.putExtra("ouputY", 240);
			// intent.putExtra("aspectX", 5);
			// intent.putExtra("aspectY", 3);
			// intent.putExtra("scale", true);
			// startActivityForResult(intent, CROP_RESULT);
			// break;
			case CROP_RESULT:
				submitImage(uri, /* car. */ad.id);
				break;
			default:
				break;
			}
			break;
		case RESULT_CANCELED:
			// reset the ad
			setAd(ad);
			break;
		}
	}

	// public void copyFile(Uri sourceFile, File destFile) throws IOException {
	// if (!destFile.exists())
	// destFile.createNewFile();
	//
	// //FileChannel source = null;
	// ReadableByteChannel c = null;
	// FileChannel destination = null;
	// try {
	// c =
	// Channels.newChannel(getContentResolver().openInputStream(sourceFile));
	// //source = new FileInputStream(sourceFile).getChannel();
	// destination = new FileOutputStream(destFile).getChannel();
	// destination.transferFrom(c, 0, Long.MAX_VALUE);
	// } finally {
	// if (c != null)
	// c.close();
	//
	// if (destination != null)
	// destination.close();
	// }
	// }

	/**
	 * Copy the photo to images directory for cropping.
	 * 
	 * @param photo
	 */
	private void doCopyPhoto(Uri photo) {
		// output
		File file = new File(Environment.getExternalStorageDirectory(), FileCache.DIRECTORY);

		if (!file.exists()) {
			file.mkdir();
			log("creating images directory");
		}

		// create a new file under the directory
		file = new File(file, "pic_" + String.valueOf(System.currentTimeMillis()) + ".jpg");

		try {
			// open new file for output
			OutputStream fos = new FileOutputStream(file);

			// input stream from passed in uri
			InputStream fis = getContentResolver().openInputStream(photo);
			int bytesAvailable = fis.available();
			int bufferSize = Math.min(bytesAvailable, 1024);
			byte[] buffer = new byte[bufferSize];
			int bytesRead = fis.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				fos.write(buffer, 0, bufferSize);
				bytesAvailable = fis.available();
				bufferSize = Math.min(bytesAvailable, 1024);
				bytesRead = fis.read(buffer, 0, bufferSize);
			}

			// close input
			fis.close();

			// flush the output & close
			fos.flush();
			fos.close();

			// copyFile(photo, file);

			// set the uri field for this activity
			uri = Uri.fromFile(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void doTakePhotoAction() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		File file = new File(Environment.getExternalStorageDirectory(), FileCache.DIRECTORY);
		if (!file.exists()) {
			file.mkdir();
			log("dir /com.boosed does not exist");
		}
		file = new File(file, "pic_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
		uri = Uri.fromFile(file/*
								 * new
								 * File(Environment.getExternalStorageDirectory
								 * () + "/com.boosed", "pic_" +
								 * String.valueOf(System.currentTimeMillis()) +
								 * ".jpg")
								 */);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

		try {
			intent.putExtra("return-data", false);
			startActivityForResult(intent, CAMERA_RESULT);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static final int CAMERA_RESULT = 1;
	private static final int CROP_RESULT = 2;
	private static final int SELECT_RESULT = 3;

	/**
	 * Initialize the <code>Ad</code> and set the appropriate images, etc.
	 * 
	 * @param ad
	 */
	private void setAd(Ad ad) {
		this.ad = ad;

		// set the images
		adapter.setImages(ad.getImages());

		// images.clear();
		// images.addAll(/* ad.getImages(true).values() */ad.getImages());
		// adapter.notifyDataSetChanged();

		// redraw the gallery to reset the borders

		// toast("the index value is: " + img);

		// init gallery
		if (index == -1) {
			// // initialize shown image to default
			// Tuple<Boolean, String> entry = new Tuple<Boolean, String>(false,
			// new Car().image);
			//
			// // check if images exist; if so, set entry to first value
			// // Set<Entry<String, Tuple<Boolean, String>>> data =
			// ad.getImages();
			// if (!images.isEmpty())
			// entry = images.iterator().next().getValue();

			// set image to default car or first image of images
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					// toast("the adapter is empty? " + adapter.isEmpty());

					setImage(/* images.isEmpty() */adapter.isEmpty() ? null : /*
																			 * images
																			 * .
																			 * iterator
																			 * (
																			 * )
																			 * .
																			 * next
																			 * (
																			 * )
																			 * .
																			 * getValue
																			 * (
																			 * )
																			 */adapter.getItem(0));
				}
			}, 100);

			// set the approval status
			// approval.setImageResource(approved ? R.drawable.ic_menu_car :
			// R.drawable.ic_menu_brush);

			// image.setVisibility(View.INVISIBLE);

			// if (!"".equals(url))
			// dm.loadImage(url, image, new ImageCallback() {
			// @Override
			// public void onLoad(int height, int width) {
			// init(height, width);
			// // format(image);
			// image.setVisibility(View.VISIBLE);
			// }
			// });
		} else {
			// set the selected image (ad is loaded dynamically from selected
			// car)
			gallery.setSelection(index, true);

			// set the image
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (index != -1)
						// only set the image if index is valid
						setImage(adapter.getItem(index));

					// reset the shown image
					index = -1;
				}
			}, 100);
		}

		// adapter.notifyDataSetChanged();
		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// adapter.notifyDataSetChanged();
		// }
		// }, 200);

		// images2.clear();
		// images2.addAll(ad.getImages(false).values());
		// photos2.notifyDataSetChanged();

		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// // refresh the adapters
		// photos1.notifyDataSetChanged();
		// photos2.notifyDataSetChanged();
		// }
		// }, 500);
	}

	// private void initForm() {
	// // set initialized to true
	// initd = true;
	// }

	/** currently selected image entry */
	private Entry<String, Tuple<Boolean, String>> entry;

	private void setImage(
	/* final Tuple<Boolean, String> entry */final Entry<String, Tuple<Boolean, String>> entry) {
		// hide the status
		approval.setVisibility(View.INVISIBLE);

		// url of image to load
		String url = "";

		// use the blank image
		if (entry == null)
			url = new Car().image;
		else {
			// set entry
			this.entry = entry;

			// set url
			url = entry.getValue().b;

			if (entry.getValue().a) {
				// approved images, set whether main entry or just approved
				if (entry.getKey().equals(ad.imageKey)) {
					// image is primary
					approval.setImageResource(R.drawable.ic_menu_gallery);
					status.setText(R.string.stat_primary);
				} else {
					// image is approved
					approval.setImageResource(R.drawable.ic_menu_tick);
					status.setText(R.string.stat_approved);
				}
			} else {
				// image is pending
				approval.setImageResource(R.drawable.ic_menu_clock);
				status.setText(R.string.stat_pending);
			}
		}

		// initially hide image
		image.setVisibility(View.INVISIBLE);

		dm.display(image, url, new ImageCallback() {	
			@Override
			public void onLoad(int height, int width) {
				format(image);
				
				// set image to visible
				image.setVisibility(View.VISIBLE);
				
				// set the approval image
				approval.setVisibility(View.VISIBLE);

				// draw correct border around image when clicked
				adapter.notifyDataSetChanged();
			}
		});
		
//		// render this image for main
//		dm.loadImage(url, image, new ImageCallback() {
//			@Override
//			public void onLoad(int height, int width) {
//				// description.setText(photo.getDescription());
//				// init(height, width);
//				// size the main image appropriately
//				format(image);
//
//				// set image to visible
//				image.setVisibility(View.VISIBLE);
//
//				// handler.post(new Runnable() {
//				// public void run() {
//				// format(image);
//				// image.setVisibility(View.VISIBLE);
//				// }
//				// });
//
//				// set the approval image
//				approval.setVisibility(View.VISIBLE);
//
//				// draw correct border around image when clicked
//				adapter.notifyDataSetChanged();
//			}
//		});
	}

	private void initGallery() {
		image.setImageMatrix(matrix);

		// listen to changes to properly init layout
		// image.getViewTreeObserver().addOnGlobalLayoutListener(this);

		// init images
		// Map<String, Tuple<Boolean, String>> data = new HashMap<String,
		// Tuple<Boolean, String>>();
		// data.put("", new Tuple<Boolean, String>(true, car.image));
		// images.clear();
		// images.addAll(data.entrySet());
		// adapter.notifyDataSetChanged();

		// prepare galleries
		adapter = new GalleryAdapter(this, new ArrayList<Entry<String, Tuple<Boolean, String>>>());

		// new BaseArrayAdapter<Entry<String, Tuple<Boolean, String>>,
		// ImageView>(this, images,
		// R.layout.list_item_img) {
		//
		// /** image count */
		// private int count = 0;
		//
		// @Override
		// protected ImageView createView(View view) {
		// return (ImageView) view;
		// }
		//
		// @Override
		// protected void render(Entry<String, Tuple<Boolean, String>> item,
		// final ImageView row) {
		// // set the loading image
		// row.setImageResource(R.drawable.w100);
		//
		// // load image
		// dm.loadImage(item.getValue().b + "=s150", row, new ImageCallback() {
		// @Override
		// public void onLoad(int height, int width) {
		// // check when images count has been reached
		// //if (++count == images.size())
		// // perform last update to redraw gallery borders
		// // notifyDataSetChanged();
		// }
		// });
		//
		// // String url = item.getValue().b;
		// // //if (!"".equals(url))
		// // dm.loadImage(url + "=s150", row, new ImageCallback() {
		// // @Override
		// // public void onLoad(int height, int width) {
		// // //notifyDataSetChanged();
		// // //row.refreshDrawableState();
		// // //row.invalidate();
		// // // image.setVisibility(View.VISIBLE);
		// // // Log.w("chat", "refreshing an images");
		// // }
		// // });
		// }
		// };

		gallery.setAdapter(adapter);
		adapter.notifyDataSetChanged();

		gallery.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				setImage(adapter.getItem(position));
				// // set the approval status of the item
				// approval.setImageResource(entry.a ? R.drawable.ic_menu_car :
				// R.drawable.ic_menu_brush);
				//
				// // hide image to show progress bar
				// image.setVisibility(View.INVISIBLE);
				//
				// // load the selected image
				// dm.loadImage(entry.b, image, new ImageCallback() {
				// @Override
				// public void onLoad(int height, int width) {
				// // row.refreshDrawableState();
				// init(height, width);
				// //format(image);
				// image.setVisibility(View.VISIBLE);
				// // image.invalidate();
				// // image.refreshDrawableState();
				//
				// // ensures that the borders redraw properly
				// // adapter.notifyDataSetChanged();
				// // Log.w("chat", "refreshing an images");
				//
				// // save selected image
				// // imageUrl = url;
				// }
				// });
			}
		});

		registerForContextMenu(gallery);

		gallery.setEmptyView(findViewById(android.R.id.empty));

		// photos2 = new BaseArrayAdapter<String, ImageView>(this, images2,
		// R.layout.list_item_img) {
		//
		// @Override
		// protected ImageView createView(View view) {
		// return (ImageView) view;
		// }
		//
		// @Override
		// protected void render(String item, final ImageView row) {
		// if (!"".equals(item))
		// dm.loadImage(item + "=s150", row, new ImageCallback() {
		// @Override
		// public void onLoad(int height, int width) {
		// //notifyDataSetChanged();
		// //row.refreshDrawableState();
		// // image.setVisibility(View.VISIBLE);
		// // Log.w("chat", "refreshing an images");
		// }
		// });
		// }
		// };

		// gallery2.setAdapter(photos2);
		// gallery2.setOnItemClickListener(new OnItemClickListener() {
		// public void onItemClick(AdapterView<?> parent, View v, int position,
		// long id) {
		// Toast.makeText(Wizard.this, photos2.getItem(position),
		// Toast.LENGTH_SHORT).show();
		// }
		// });
	}

	// @Override
	// public void onGlobalLayout() {
	// //init();
	// format(image);
	//
	// toast("the layout is changing");
	// }

	// image stuff
	// float scrHeight, scrWidth;

	// the constrained viewing rectangle
	// private RectF viewRect = new RectF();
	// the projected new viewing rectangle
	// private RectF prjRect = new RectF();
	// the original image rect that is transformed
	// private RectF imgRect = new RectF();

	// private float maxZoom;
	// private float minZoom;

	private Matrix matrix = new Matrix();

	/**
	 * Initialize the size of widgets and the loaded image.
	 */
	private void format(ImageView image) {
		Drawable d = image.getDrawable();
		// imgRect.set(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
		float imgWidth = d.getIntrinsicWidth(), imgHeight = d.getIntrinsicHeight();

		// Log.i("chat", "img height: " + imgHeight + ", img width: " +
		// imgWidth);
		// if (vheight > height || vwidth > width) {

		int scrHeight = image.getHeight();
		int scrWidth = image.getWidth();
		// set the min zoom so that img takes up entire screen
		float magnify = Math.min(scrHeight / imgHeight, scrWidth / imgWidth);
		// Log.w("chat", "min zoom is: " + magnify + " sh: " + scrHeight +
		// ", sw: " + scrWidth + ", " + imgWidth + ", "
		// + imgHeight);
		// max zoom is 3x image size
		// maxZoom = magnify * 3;

		// establish the valid viewing rectangle
		float dx = (scrWidth - imgWidth * magnify) / 2f;
		float dy = (scrHeight - imgHeight * magnify) / 2f;
		// viewRect.set(dx, dy, dx + imgWidth * minZoom, dy + imgHeight *
		// minZoom);

		// zero out our cumulative changes
		matrix.reset();

		// set proper size (the transformation matrix incorporates the minimum
		// zoom to optimize size of the image)
		matrix.postScale(magnify, magnify, 0, 0);

		// center in middle of screen
		matrix.postTranslate(dx, dy);

		// apply the changes
		image.setImageMatrix(matrix);
	}

	/**
	 * Submit the image/uri to the <code>Ad</code> identified by <code>id</code>
	 * .
	 * 
	 * @param uri
	 * @param adId
	 */
	private void submitImage(final Uri uri, final long adId) {

		// new MobTask<String>() {
		// };
		new RpcTask<String>() {
			// @Override
			// protected void onPreExecute() {
			// super.onPreExecute();
			// }

			@Override
			public void onFailure(int errResId) {
				toast("failed to send image");
			}

			@Override
			public String doit() throws RemoteServiceFailureException {
				return rpc.service.loadAction();
			}

			@Override
			public void onSuccess(final String t) {
				new MobTask<Ad>(R.string.stat_upload, true) {
					public Ad doit() throws RemoteServiceFailureException {
						return rpc.uploadFile(t, adId, uri);
					}

					public void onFailure(int errResId) {
						switch (errResId) {
						case R.string.exception_resource_limit:
							toast("the photo submission limit for this ad type has been reached");
							break;
						default:
							// continue with failure
							super.onFailure(errResId);
						}
					}

					@Override
					public void onSuccess(Ad t) {
						// check that the upload didn't fail!
						if (t == null)
							// failed
							onFailure(R.string.exception_rpc_invocation);
						else
							// set the ad on the wizard
							setAd(t);
					}
				}.execute();
			}
		}.execute();
	}

	// /**
	// * Initialize the size of widgets and the loaded image.
	// */
	// private void init(int... dims) {
	// // width and height of the image view
	// int scrHeight = image.getHeight();
	// int scrWidth = image.getWidth();
	//
	// // Log.i("chat", "scr height: " + scrHeight + ", scr width: " +
	// // scrWidth);
	//
	// // width and height of the loaded image
	// // imgHeight = view.getDrawable().getIntrinsicHeight();
	// // imgWidth = view.getDrawable().getIntrinsicWidth();
	//
	// // Log.i("chat", "the view is: " + view.toString());
	// // Log.i("chat", "the draw is: " + view.getDrawable());
	// // int iw, ih;
	// float imgWidth, imgHeight;
	//
	// if (dims.length == 0) {
	// Drawable d = image.getDrawable();
	// imgWidth = d.getIntrinsicWidth();
	// imgHeight = d.getIntrinsicHeight();
	// } else {
	// imgWidth = dims[1];
	// imgHeight = dims[0];
	// }
	//
	// // Log.i("chat", "img height: " + imgHeight + ", img width: " +
	// // imgWidth);
	// // if (vheight > height || vwidth > width) {
	//
	// // set the min zoom so that img takes up entire screen
	// float magnify = Math.min(scrHeight / imgHeight, scrWidth / imgWidth);
	// // Log.w("chat", "min zoom is: " + magnify + " sh: " + scrHeight +
	// // ", sw: " + scrWidth + ", " + imgWidth + ", "
	// // + imgHeight);
	// // max zoom is 3x image size
	// // maxZoom = magnify * 3;
	//
	// // establish the valid viewing rectangle
	// float dx = (scrWidth - imgWidth * magnify) / 2f;
	// float dy = (scrHeight - imgHeight * magnify) / 2f;
	// // viewRect.set(dx, dy, dx + imgWidth * minZoom, dy + imgHeight *
	// // minZoom);
	//
	// // zero out our cumulative changes
	// matrix.reset();
	//
	// // set proper size (the transformation matrix incorporates the minimum
	// // zoom to optimize size of the image)
	// matrix.postScale(magnify, magnify, 0, 0);
	//
	// // center in middle of screen
	// matrix.postTranslate(dx, dy);
	//
	// // apply the changes
	// image.setImageMatrix(matrix);
	// }
}