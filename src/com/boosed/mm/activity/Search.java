package com.boosed.mm.activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.boosed.mm.R;
import com.boosed.mm.activity.task.GeoTask;
import com.boosed.mm.activity.task.LocationTask;
import com.boosed.mm.activity.task.RpcTask;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.AdType;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.util.DataUtil;
import com.boosed.mm.ui.BaseArrayAdapter;
import com.boosed.mm.ui.CheckedImage;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.ListItemPhoto;
import com.boosed.mm.ui.NumberRangeKeyListener;
import com.googlecode.objectify.Key;

/**
 * Search interface, extends from <code>AbstractListActivity</code>.
 * 
 * @author dsumera
 */
public class Search extends AbstractListActivity implements Controllable {

	/** titles for dialogs */
	private static final Map<Integer, Integer> dlgTitles = new HashMap<Integer, Integer>();

	/** max (cardinality) selection limits for criteria */
	private static final Map<Integer, Integer> critLimits = new HashMap<Integer, Integer>();

	/** threshold criteria */
	private static final Map<Integer, Integer> critThresholds = new HashMap<Integer, Integer>();

	/** criteria for using location service */
	private final Criteria locationCritera;

	/** bookmarks */
	private final Set<Key<Car>> bookmarks;

	/** results of search */
	private final List<Car> results;

	/** result offset */
	private int offset = 0;

	/** car adapter */
	private ArrayAdapter<Car> adapter;

	/** corrects the threshholds only once associated dialogs are displayed */
	private final Map<Integer, Boolean> initThreshhold;

	/** max price for marker */
	private Marker marker;

	/** key listener for price alert */
	private NumberRangeKeyListener listener;

	/**
	 * Default no-arg constructor
	 */
	public Search() {
		// set the empty string for the list
		super(R.string.warn_search, R.layout.title_search);

		// init collections
		bookmarks = new HashSet<Key<Car>>();
		results = new ArrayList<Car>();

		// for geocoding
		locationCritera = new Criteria();
		locationCritera.setAccuracy(Criteria.ACCURACY_FINE);
		locationCritera.setAltitudeRequired(false);
		locationCritera.setBearingRequired(false);
		locationCritera.setCostAllowed(true);
		locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);

		// initialize dialog titles
		if (dlgTitles.isEmpty()) {
			dlgTitles.put(R.id.types, R.string.dlg_title_types);
			dlgTitles.put(R.id.makes, R.string.dlg_title_makes);
			dlgTitles.put(R.id.models, R.string.dlg_title_models);
			dlgTitles.put(R.id.filters, R.string.dlg_title_filter);
			dlgTitles.put(R.id.condition, R.string.dlg_title_condition);
			dlgTitles.put(R.id.mileage, R.string.dlg_title_mileage);
			dlgTitles.put(R.id.price, R.string.dlg_title_price);

			dlgTitles.put(R.id.geocode, R.string.dlg_title_geocode);
			dlgTitles.put(R.id.location, R.string.dlg_title_location);
		}

		// initialize criteria limits
		if (critLimits.isEmpty()) {
			critLimits.put(R.id.types, 1);
			critLimits.put(R.id.makes, 3);
			critLimits.put(R.id.models, 3);
		}

		// initialize criteria thresholds
		if (critThresholds.isEmpty()) {
			critThresholds.put(R.id.condition, R.array.condition);
			critThresholds.put(R.id.mileage, R.array.mileage);
			critThresholds.put(R.id.price, R.array.price);
		}

		// init the threshholds map
		initThreshhold = new HashMap<Integer, Boolean>();
		initThreshhold.put(R.id.condition, true);
		initThreshhold.put(R.id.mileage, true);
		initThreshhold.put(R.id.price, true);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set activity title
		// setTitle(R.string.app_search);

		// add the footer before setting the adapter
		mList.addFooterView(footer);
		
		//footer.setLayoutParams(new LayoutParams(0, 0));

		// set adapter
		setListAdapter(adapter = new BaseArrayAdapter<Car, ListItemPhoto>(this, results, R.layout.list_item_photo) {

			/** formatter for decimal values */
			private DecimalFormat format = new DecimalFormat("###,###,##0");

			@Override
			protected ListItemPhoto createView(View view) {
				// create view holder
				return new ListItemPhoto(view);
			}

			@Override
			protected void render(Car item, final ListItemPhoto row) {
				// get image
				final ImageView image = row.getImage();
				
				// set image
				dm.display(image, item.image + "=s160", new ImageCallback() {
					@Override
					public void onLoad(int height, int width) {
						//image.refreshDrawableState();
						image.setVisibility(View.VISIBLE);
						
					}
				});

				// set model w/ trim
				row.getTitle().setText(item.toString());

				// set year & colors
				StringBuilder sb = new StringBuilder(Integer.toString(item.year));
				sb.append(", ").append(item.exterior.getName());
				sb.append("/").append(item.interior.getName());
				row.getContent1().setText(sb);

				// set miles & price
				row.getContent2().setText(format.format(item.mileage) + " miles, $" + format.format(item.price));

				sb = new StringBuilder(item.location);
				if (settings.getString(RpcService.PREF_POST_SRCH, null) != null)
					// only show miles if postal code was provided
					sb.append(" (").append(item.distance).append(" miles)");

				// set location
				row.getEnd().setText(sb);

				// set bookmark
				CheckedImage check = row.getCheck();

				if (item.owner.getName().equals(rpc.accountId)) {
					// user is owner
					// check.setEnabled(false);
					check.setClickable(false);
					check.setChecked(false);
					check.setTag(null);
				} else {
					// user is not owner
					// check.setEnabled(true);
					check.setClickable(true);
					check.setChecked(bookmarks.contains(item.getKey()));
					check.setTag(item);
				}
			}

			// @Override
			// public void notifyDataSetChanged() {
			// super.notifyDataSetChanged();
			//
			// // reset visibility of shadow if list is empty
			// shadow.setVisibility(results.isEmpty() ? View.GONE :
			// View.VISIBLE);
			// }
		});
		
		// don't need footer so remove it now
		mList.removeFooterView(footer);

		// register list for context menu
		registerForContextMenu(mList);

		if (initialized) {
			// init marker
			marker = (Marker) savedInstanceState.get("marker");
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save marker
		outState.putSerializable("marker", marker);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// show car when item is clicked
		showCar(position);
	}

	/** listviews backing dialogs (condition, mileage, price) */
	private Map<Integer, ListView> lists = new HashMap<Integer, ListView>();

	/** editor */
	private Editor editor;

	/** choice items mapped to sort type indexes */
	private static final int[] filters = new int[] { SortType.NONE.ordinal(), SortType.LOCATION.ordinal(),
			SortType.MILEAGE.ordinal(), SortType.MILEAGE_DESC.ordinal(), SortType.PRICE.ordinal(),
			SortType.PRICE_DESC.ordinal(), SortType.YEAR_DESC.ordinal(), SortType.YEAR.ordinal() };

	/** suppress a location lookup when text changes in postal edit text */
	private boolean suppressLocationLookup = false;

	/** suppress alert when user seeks to new price */
	private Boolean suppressPriceAlert = false;

	@Override
	protected Dialog onCreateDialog(final int id) {

		// for all cases
		Builder builder = new Builder(this);

		switch (id) {
		case R.id.types:
			// title
			builder.setTitle(R.string.dlg_title_types);

			// set not cancelable to force dialog click
			builder.setCancelable(false);

			// types
			final boolean[] types = rpc.getChecks(id);

			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (which != Dialog.BUTTON_NEGATIVE) {
						// zero out the checks array
						Arrays.fill(types, false);

						// check item (account for any option @ index 0)
						if (which != 0)
							types[--which] = true;

						// set the checked item against the rpc
						rpc.setChecks(id);

						// search
						search();
					}
					
					// remove dialog to reuse values
					removeDialog(id);
				}
			};
			// get the first selected item
			int selected = 0;
			for (int i = types.length; --i > -1;)
				if (types[i]) {
					// account for "any" option
					selected = i + 1;
					break;
				}
			builder.setSingleChoiceItems(rpc.getChoices(id), selected, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.makes:
		case R.id.models:
			// title
			builder.setTitle(dlgTitles.get(id));

			// set not cancelable to force dialog click
			builder.setCancelable(false);

			// get checks from rpc
			final boolean[] checks = rpc.getChecks(id);

			// set the multichoice listener
			OnMultiChoiceClickListener omccl = new OnMultiChoiceClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which, boolean isChecked) {
					if (isChecked) {
						ListView list = ((AlertDialog) dialog).getListView();// lists.get(id);
						int total = list.getCheckItemIds().length;

						if (total > critLimits.get(id)) {
							// criteria limit exceeded, set check to false
							checks[which] = false;
							list.setItemChecked(which, false);
						}
					}
				}
			};
			builder.setMultiChoiceItems(rpc.getChoices(id), checks, omccl);

			// set the click listener
			ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_NEUTRAL:
						// zero out the checks array
						Arrays.fill(checks, false);

						// uncheck the items in the UI
						ListView list = ((AlertDialog) dialog).getListView();
						for (int i = checks.length; --i > -1;)
							list.setItemChecked(i, false);
					case Dialog.BUTTON_POSITIVE:
						// set the checks against the rpc
						rpc.setChecks(id);

						// perform the query
						search();
					case Dialog.BUTTON_NEGATIVE:
					default:
						// remove the dialog to reuse values from rpc
						removeDialog(id);
						break;
					}
				}
			};
			builder.setPositiveButton(R.string.dlg_btn_set, ocl);
			builder.setNeutralButton(R.string.dlg_btn_clear, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.filters:
			// title
			builder.setTitle(dlgTitles.get(id));

			// not cancelable
			// builder.setCancelable(false);

			// choice items mapped to dialog ids
			final int[] dialogs = new int[] { -1, R.id.location, R.id.mileage, R.id.mileage, R.id.price, R.id.price,
					R.id.condition, R.id.condition };

			// set the single choice listener
			ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					// init editor
					if (editor == null)
						// create a new editor
						editor = settings.edit();

					// display the secondary dialogs
					switch (which) {
					case Dialog.BUTTON_NEUTRAL:
						// reset all fields

						// set current sort to none
						editor.putInt(RpcService.PREF_FILTER, SortType.NONE.ordinal());
						((AlertDialog) dialog).getListView()/* ;lists.get(id) */.setItemChecked(0, true);

						// remove location
						editor.putString(RpcService.PREF_POST_SRCH, null);
						editor.putString(RpcService.PREF_NAME_SRCH, null);

						// set condition to any
						editor.putInt(RpcService.PREF_COND, ConditionType.ANY.ordinal());
						ListView list = lists.get(R.id.condition);
						if (list != null)
							list.setItemChecked(0, true);

						// set mileage to any
						editor.putInt(RpcService.PREF_MILE, MileageType.ANY.ordinal());
						list = lists.get(R.id.mileage);
						if (list != null)
							list.setItemChecked(0, true);

						// set price to any
						editor.putInt(RpcService.PREF_PRICE, PriceType.ANY.ordinal());
						list = lists.get(R.id.price);
						if (list != null)
							list.setItemChecked(0, true);
					case Dialog.BUTTON_POSITIVE:
						// commit edits
						editor.commit();

						// perform the query/search
						search();
					case Dialog.BUTTON_NEGATIVE:
						// discard the editor
						editor = null;
						break;
					case 0:
						// most recent (no sort)
						editor.putInt(RpcService.PREF_FILTER, filters[which]);
						break;
					default:
						// 1 - 7 (set filter and show associated dialog)
						editor.putInt(RpcService.PREF_FILTER, filters[which]);
						showDialog(dialogs[which]);
						break;
					}
				}
			};

			// selected by onPrepareDialog
			builder.setSingleChoiceItems(R.array.filters, -1, ocl);
			builder.setPositiveButton(R.string.dlg_btn_set, ocl);
			builder.setNeutralButton(R.string.dlg_btn_clear, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.condition:
		case R.id.mileage:
		case R.id.price:
			// title
			builder.setTitle(dlgTitles.get(id));

			ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// queue change onto editor
					switch (id) {
					case R.id.condition:
						editor.putInt(RpcService.PREF_COND, ConditionType.values()[which].ordinal());
						break;
					case R.id.mileage:
						editor.putInt(RpcService.PREF_MILE, MileageType.values()[which].ordinal());
						break;
					case R.id.price:
						editor.putInt(RpcService.PREF_PRICE, PriceType.values()[which].ordinal());
						break;
					default:
						break;
					}

					// dismiss the dialog once selected
					dialog.dismiss();
				}
			};

			// // attempt to find selected threshhold in settings
			// int selected;
			//
			// switch (id) {
			// case R.id.condition:
			// selected = settings.getInt(RpcService.PREF_COND, 0);
			// break;
			// case R.id.mileage:
			// selected = settings.getInt(RpcService.PREF_MILE, 0);
			// break;
			// case R.id.price:
			// selected = settings.getInt(RpcService.PREF_PRICE, 0);
			// break;
			// default:
			// // choose the any option
			// selected = 0;
			// break;
			// }

			// selected by onPrepare dialog
			AlertDialog rv = builder.setSingleChoiceItems(critThresholds.get(id), -1, ocl).create();
			lists.put(id, rv.getListView());
			return rv;
		case R.id.geocode:
			// location/geocode warning
			builder.setTitle(dlgTitles.get(id));
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage(getString(R.string.warn_rev_geo));

			// set the click listener
			ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// set the location
						setLocation();
					case Dialog.BUTTON_NEGATIVE:
						// do nothing
					default:
						break;
					}
				}
			};
			builder.setPositiveButton(R.string.dlg_btn_ok, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.location:
			// postal dialog
			// set the view
			builder.setTitle(dlgTitles.get(id));
			View view = getLayoutInflater().inflate(R.layout.dlg_crit_geo, (ViewGroup) findViewById(R.id.dlg_crit_geo));
			builder.setView(view);

			// the distance
			distance = (Spinner) view.findViewById(R.id.distance);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources()
					.getStringArray(R.array.distances));
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			distance.setAdapter(adapter);
			// distance.setOnItemSelectedListener(new OnItemSelectedListener() {
			// @Override
			// public void onItemSelected(AdapterView<?> arg0, View arg1, int
			// position, long id) {
			// // set the distance
			// settings.edit().putInt(RpcService.PREF_DIST, position).commit();
			// // rpc.distance = position;
			// }
			//
			// @Override
			// public void onNothingSelected(AdapterView<?> arg0) {
			// // not implemented
			// }
			// });

			// the name of the city
			name = (TextView) view.findViewById(R.id.name);

			// the geocode button
			geocode = (ImageButton) view.findViewById(R.id.geocode);

			// listen to changes on textview
			postal = (EditText) view.findViewById(R.id.postal);

			postal.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(final CharSequence s, int start, int before, int count) {
					final String zip = s.toString();

					// length is 5 and not using same value
					if (zip.length() == 5) {
						// performed population of postal textview

						// suppress lookup for this instance
						if (suppressLocationLookup)
							suppressLocationLookup = false;
						else
							// perform a lookup
							new MobTask<com.boosed.mm.shared.db.Location>() {
								@Override
								protected void onPreExecute() {
									super.onPreExecute();
									name.setText(R.string.dlg_resolve);
								}

								@Override
								public com.boosed.mm.shared.db.Location doit() throws RemoteServiceFailureException {
									return rpc.service.loadLocation(zip);
									// try {
									// List<Address> addresses;
									// while ((addresses =
									// geo.getFromLocationName(zip, 1)).size()
									// == 0)
									// // wait until we can get an address
									// Thread.sleep(2000);
									//
									// return addresses.get(0);
									// } catch (Exception e) {
									// throw new
									// RemoteServiceFailureException();
									// }
								}

								public void onFailure(int errResId) {
									// mark unsuccessful
									postal.setTag(false);

									// set error on the name field
									name.setText(errResId);
								}

								@Override
								public void onSuccess(com.boosed.mm.shared.db.Location t) {
									// mark successful
									postal.setTag(true);

									// set name to location
									name.setText(t.toString());
								}
							}.execute();
					} else {
						// length is not 5

						// mark unsuccessful
						postal.setTag(false);

						name.setText(R.string.dlg_anywhere);
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not implemented
				}

				@Override
				public void afterTextChanged(final Editable s) {
					// not implemented
				}
			});

			ocl = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// set the postal field only if valid & user confirms it
						if ((Boolean) postal.getTag()) {
							// successful lookup, save postal parameters to
							// preferences
							settings.edit().putString(RpcService.PREF_POST_SRCH, postal.getText().toString()).commit();
							settings.edit().putString(RpcService.PREF_NAME_SRCH, name.getText().toString()).commit();
							settings.edit().putInt(RpcService.PREF_DIST_SRCH, distance.getSelectedItemPosition()).commit();

							// break out of switch
							break;
						} // else
							// positive falls through if lookup unsuccessful
					case Dialog.BUTTON_NEUTRAL:
						// clear postal parameters
						settings.edit().putString(RpcService.PREF_POST_SRCH, null).commit();
						settings.edit().putString(RpcService.PREF_NAME_SRCH, null).commit();
						settings.edit().putInt(RpcService.PREF_DIST_SRCH, 0).commit();
						break;
					case Dialog.BUTTON_NEGATIVE:
					default:
						// close dialog
						break;
					}
				}
			};
			builder.setPositiveButton(R.string.dlg_btn_set, ocl);
			builder.setNeutralButton(R.string.dlg_btn_clear, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.alert:
			// set title
			builder.setTitle(R.string.dlg_title_alert);

			view = getLayoutInflater().inflate(R.layout.dlg_price_alt, (ViewGroup) findViewById(R.id.dlg_price_alt));
			alert = (SeekBar) view.findViewById(R.id.alert);
			hi = (TextView) view.findViewById(R.id.hi);
			target = (EditText) view.findViewById(R.id.target);
			builder.setView(view);

			// set text watcher
			TextWatcher tw = new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					synchronized (suppressPriceAlert) {
						// suppress changes on seekbar
						suppressPriceAlert = true;

						// if text is blank, set progress to zero
						alert.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not implemented
				}

				@Override
				public void afterTextChanged(Editable s) {
					// not implemented
				}
			};
			target.addTextChangedListener(tw);

			// set key listener
			target.setFilters(new InputFilter[] { listener = new NumberRangeKeyListener(marker.price) });

			// mText.setRawInputType(InputType.TYPE_CLASS_NUMBER);

			OnSeekBarChangeListener osbcl = new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// not implemented
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// not implemented
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					// target.setText(Integer.toString(progress));
					synchronized (suppressPriceAlert) {

						if (suppressPriceAlert)
							suppressPriceAlert = false;
						else {
							// int value = Integer.parseInt(s.toString());
							// alert.setProgress(value);
							String value = Integer.toString(progress);
							target.setText(value);
							target.setSelection(value.length());
						}

						// //suppressPriceAlert = true;
						// String value = Integer.toString(progress);
						// target.setText(value);
						// target.setSelection(value.length());
					}
				}
			};
			alert.setOnSeekBarChangeListener(osbcl);

			ocl = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						new MobTask<Void>(R.string.stat_price, true) {
							@Override
							public Void doit() throws RemoteServiceFailureException {
								// set the price alert value
								rpc.service.updateAlert(marker.car.getId(), alert.getProgress());
								return null;
							}

							@Override
							public void onSuccess(Void t) {
								// not implemented
							}
						}.execute();
						break;
					case Dialog.BUTTON_NEGATIVE:
					default:
						// close dialog
						break;
					}
				}
			};

			// set buttons
			builder.setPositiveButton(R.string.dlg_btn_set, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		default:
			return super.onCreateDialog(id);
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);

		switch (id) {
		// case R.id.types:
		// // types selection
		// ListView list = ((AlertDialog) dialog).getListView();
		//
		// // check any by default
		// list.setItemChecked(0, true);
		//
		// int i = 0;
		// for (boolean check : rpc.getChecks(id)) {
		// i++;
		// if (check) {
		// // item selected, check it
		// list.setItemChecked(i, true);
		// break;
		// }
		// }
		// break;
		case R.id.location:
			// postal dialog

			// code
			String value = settings.getString(RpcService.PREF_POST_SRCH, null);
			if (/* rpc.postal */value == null) {
				// no location
				postal.setText(/* rpc.postal */value);
				postal.setTag(false);
			} else {
				// suppress lookup since setting text triggers the textwatcher
				suppressLocationLookup = true;
				postal.setText(/* rpc.postal */value);
				postal.setTag(true);
				postal.setSelection(/* rpc.postal */value.length());
			}

			// name
			value = settings.getString(RpcService.PREF_NAME_SRCH, null);
			if (value == null)
				name.setText(R.string.dlg_anywhere);
			else
				name.setText(value);

			// set distance
			distance.setSelection(/* rpc.distance */settings.getInt(RpcService.PREF_DIST_SRCH, 0));

			// move to front?
			// dialog.getWindow().makeActive();
			break;
		case R.id.filters:
			// filter/sort dialog

			// init the threshholds map
			initThreshhold.put(R.id.condition, true);
			initThreshhold.put(R.id.mileage, true);
			initThreshhold.put(R.id.price, true);

			// // attempt to find selected filter in settings
			// int selected = settings.getInt(RpcService.PREF_FILTER, 0);
			// for (int i = filters.length; --i > -1;)
			// if (filters[i] == selected) {
			// // sort at index i matches selected value, set the index
			// selected = i;
			// break;
			// }

			// set selected filter
			((AlertDialog) dialog).getListView().setItemChecked(
					DataUtil.getIndex(filters, settings.getInt(RpcService.PREF_FILTER, 0)), true);
			// lists.get(id).setItemChecked(selected, true);
			break;
		case R.id.condition:
		case R.id.mileage:
		case R.id.price:
			// threshhold dialogs

			// set only once AFTER filters dialog is shown
			if (!initThreshhold.get(id))
				// already corrected once
				return;

			// attempt to find selected sort in settings
			int selected;

			switch (id) {
			case R.id.condition:
				selected = settings.getInt(RpcService.PREF_COND, 0);
				break;
			case R.id.mileage:
				selected = settings.getInt(RpcService.PREF_MILE, 0);
				break;
			case R.id.price:
				selected = settings.getInt(RpcService.PREF_PRICE, 0);
				break;
			default:
				// choose the any option
				selected = 0;
				break;
			}

			// set the selected threshhold
			((AlertDialog) dialog).getListView().setItemChecked(selected, true);
			// lists.get(id).setItemChecked(selected, true);

			// threshhold has been prepared
			initThreshhold.put(id, false);
			break;
		case R.id.alert:
			// set the markers
			alert.setMax(marker.price);
			alert.setProgress(marker.point);

			//
			hi.setText("$" + Integer.toString(marker.price));
			value = Integer.toString(marker.point);
			target.setText(value);
			target.setSelection(value.length());
			listener.setMax(marker.price);
			// target.setFilters(new InputFilter[] { new
			// NumberRangeKeyListener(marker.price) });
			break;
		default:
			break;
		}
	}

	/** buttons for controls */
	private ImageButton btnMakes, btnModels, btnFilters;
	private RelativeLayout btnTypes;

	/** shadow to hide when list is empty */
	// private ImageView shadow;

	/** textfields */
	private TextView name, hi;
	private EditText postal, target;
	private Spinner distance;
	private ImageButton geocode;

	/** footer */
	private View footer;

	/** alert seekbar */
	private SeekBar alert;
	
	@Override
	public void assignHandles() {
		super.assignHandles();
		
		// set the controls
		// View view = getLayoutInflater().inflate(R.layout.ctl_search, null);
		// shadow = (ImageView) findViewById(R.id.list_shadow);
		//
		// LayoutParams lp = new LayoutParams(0, LayoutParams.FILL_PARENT, 1);
		// controls.addView(view, lp);
		// controls.setVisibility(View.VISIBLE);

		btnMakes = (ImageButton) findViewById(R.id.makes);
		btnMakes.setEnabled(false);
		btnModels = (ImageButton) findViewById(R.id.models);
		btnModels.setEnabled(false);
		btnFilters = (ImageButton) findViewById(R.id.filters);
		btnFilters.setEnabled(false);
		btnTypes = find(RelativeLayout.class, R.id.types);
		btnTypes.setEnabled(false);
		
//		ImageView icon = find(ImageView.class, R.id.title_icon);
//		Animation anim = AnimationUtils.loadAnimation(this, R.anim.rotate);
//		anim.setInterpolator(new SpinnerInterpolator());
//		anim.setRepeatCount(Animation.INFINITE);
//		//anim.setRepeatMode(Animation.REVERSE);
//		icon.startAnimation(anim);
		
		// create the footer
		footer = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_footer, null);
	}

	@Override
	public void onConnect() {
		// load the user's bookmarks once connected
		new MobTask<Void>() {
			@Override
			public Void doit() throws RemoteServiceFailureException {
				rpc.loadBookmarks();
				return null;
			}

			@Override
			public void onSuccess(Void t) {
				// update bookmarks
				rpc.syncBookmarks(bookmarks);

				// update the adapter
				adapter.notifyDataSetChanged();
			}

			protected void onPostExecute(Void result) {
				// finish this task
				super.onPostExecute(result);

				// perform onOnline steps (since we are connected now)
				onOnline();
			}
		}.execute();

		// end authentication dialog and run replays
		super.onConnect();
	}

	@Override
	public void onOnline() {
		super.onOnline();

		// initialize the query values
		new MobTask<Void>() {
			protected void onPreExecute() {
				super.onPreExecute();

				// update message to initializing
				setEmptyString(R.string.warn_init, true);
			}

			@Override
			public Void doit() throws RemoteServiceFailureException {
				rpc.initQuery();
				return null;
			}

			protected void onPostExecute(Void result) {
				// reset empty message to default
				setEmptyString(R.string.warn_search, false);
				
				// proceed
				super.onPostExecute(result);
			}
			
			public void onSuccess(Void t) {
				// update button state
				updateControls();
				
				// perform initial search when started
				if (!rpc.init)
					search();
			}
		}.execute();
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		final int id = item.getItemId();
		switch (id) {
		case R.id.ctx_bookmark:
		case R.id.ctx_remove:
			final boolean marked = id == R.id.ctx_remove;
			// handle bookmarks
			new MobTask<Void>(marked ? R.string.stat_remove : R.string.stat_bookmark, true, item) {
				@Override
				public Void doit() throws RemoteServiceFailureException {
					rpc.bookmark(adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position).id, !marked);
					return null;
				}

				@Override
				public void onSuccess(Void t) {
					// update bookmarks
					rpc.syncBookmarks(bookmarks);

					// update the adapter
					adapter.notifyDataSetChanged();
				}
			}.execute();
			return true;
		case R.id.ctx_inquire:
			// start activty for making an inquiry
			Car car = adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position);
			if (car.owner.getName().equals(rpc.accountId))
				toast(getString(R.string.exception_owner));
			else {
				Intent intent = new Intent(this, Composer.class);
				intent.putExtra(Composer.REFERENCE, car);
				startActivity(intent);
			}
			return true;
		case R.id.ctx_view:
			// show car
			showCar(((AdapterContextMenuInfo) item.getMenuInfo()).position);
			return true;
		case R.id.ctx_price:
			// load the marker
			new MobTask<Marker>(item) {

				/** selected car */
				final Car car = adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position);

				@Override
				public Marker doit() throws RemoteServiceFailureException {
					return rpc.service.loadPrice(car.id);
				}

				@Override
				public void onSuccess(Marker t) {
					if (t == null) {
						marker = new Marker();
						marker.price = car.price;
						marker.point = 0;
						marker.car = car.getKey();
					} else {
						marker = t;

						// adjust for inactive marker
						if (!marker.active) {
							// treat like new marker
							marker.price = car.price;
							marker.point = 0;
							marker.car = car.getKey();
						}
					}

					// decrement price by 1
					marker.price--;

					// show the alert dialog
					showDialog(R.id.alert);
				}
			}.execute();
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		// retrieve selected item
		Car car = adapter.getItem(((AdapterContextMenuInfo) menuInfo).position);

		// set menu title
		menu.setHeaderTitle(/*car.model.getName()*/car.toString());

		// initialize menu
		getMenuInflater().inflate(R.menu.ctx_menu_buying, menu);

		// init menu items
		boolean isBookmark = bookmarks.contains(car.getKey());
		boolean isOwner = car.owner.getName().equals(rpc.accountId);

		// bookmarks does not contain key
		menu.findItem(R.id.ctx_bookmark).setVisible(!isOwner && !isBookmark);

		// user is connected and bookmarks contains key
		menu.findItem(R.id.ctx_remove).setVisible(!isOwner && isBookmark);

		// user is owner, cannot set price alerts
		menu.findItem(R.id.ctx_price).setVisible(!isOwner);

		// user is owner, cannot inquire
		menu.findItem(R.id.ctx_inquire).setVisible(!isOwner && car.adType != AdType.BASIC);
	}

	@Override
	public void onReplay(View replay) {
		// replay a control
		onControl(replay);
	}

	@Override
	public void onBind() {
		// sync the results
		sync(true);
	}

	@Override
	public void onControl(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.types:
		case R.id.makes:
		case R.id.models:
		case R.id.filters:
		case R.id.geocode:
			// show appropriate dialog
			showDialog(id);
			break;
		case android.R.id.checkbox:
			final CheckedImage bookmark = (CheckedImage) view;
			final boolean marked = bookmark.isChecked();
			new MobTask<Void>(marked ? R.string.stat_remove : R.string.stat_bookmark, true, view) {
				@Override
				public Void doit() throws RemoteServiceFailureException {
					rpc.bookmark(((Car) bookmark.getTag()).id, !marked);
					return null;
				}

				@Override
				public void onSuccess(Void t) {
					// update bookmarks
					rpc.syncBookmarks(bookmarks);

					// update the adapter
					adapter.notifyDataSetChanged();
				}
			}.execute();
			break;
		default:
			break;
		}
	}

	/** track whether the scrollview is in a fetching state */
	private boolean fetching = false;

	/** footer status */
	//private boolean hasFooter = false;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (rpc != null && offset != 0 & !fetching && totalItemCount != 0
				&& (firstVisibleItem + visibleItemCount) == totalItemCount)

			// scroll results
			new MobTask<Integer>() {
				@Override
				protected void onPreExecute() {
					super.onPreExecute();

					// set fetching on
					fetching = true;

					// show footer
					if (mList.getFooterViewsCount() == 0) {
						//footer.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
						//footer.setVisibility(View.VISIBLE);
						mList.addFooterView(footer);
						//hasFooter = true;
					}
				}

				@Override
				public Integer doit() throws RemoteServiceFailureException {
					return rpc.loadCars(true);
				}

				@Override
				protected void onPostExecute(Integer result) {
					// set fetching variables and end status
					fetching = false;

					// proceed with failure/success
					super.onPostExecute(result);
				}
				
				@Override
				public void onFailure(int errResId) {
					// sync content
					sync(false);

					// continue failure
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(Integer t) {
					// set offset
					offset = t;

					// remove footer if at end of content
					if (offset == 0 && mList.getFooterViewsCount() > 0) {
						mList.removeFooterView(footer);
						//hasFooter = false;
						//footer.setVisibility(View.GONE);
						//footer.setLayoutParams(new LayoutParams(0, 0));
					}

					// sync content
					sync(true);
				}
			}.execute();
	}

	/**
	 * Set the location using services.
	 */
	private void setLocation() {
		// disable the postal field
		// postal.setEnabled(false);

		// LocationManager.NETWORK_PROVIDER;
		final String provider = lm.getBestProvider(locationCritera, true);
		// final List<String> providers = lm.getProviders(locationCritera,
		// true);

		// there is a provider & it is enabled
		if (/* providers != null && !providers.isEmpty() */provider != null && lm.isProviderEnabled(provider)) {
			// request updates
			// lm.requestLocationUpdates(provider, 5000, 1.0F, Search.this);
			// name.setText("reverse geocoding...");

			new MobTask<Address>(/* "finding coordinates...", true */) {

				private LocationTask lt;

				private GeoTask gt;

				protected void onPreExecute() {
					super.onPreExecute();

					// set geocode button to disabled
					geocode.setEnabled(false);

					// set current location status to false in case user oks
					// dialog while task executes
					postal.setTag(false);

					name.setText(provider + getString(R.string.dlg_coord));
				}

				@Override
				public Address doit() throws RemoteServiceFailureException {
					lt = new LocationTask();
					lm.requestLocationUpdates(provider, 60000, 1, lt, Search.this.getMainLooper());

					// wait 15 seconds for coordinates to be found
					lt.await(15);

					// for (String name : providers)
					// loc = lm.getLastKnownLocation(provider);

					gt = new GeoTask(Search.this, lt.getLocation());
					handler.post(new Runnable() {
						public void run() {
							// set geocoding message
							name.setText(R.string.dlg_geocode);
						}
					});

					// wait 15 seconds for address to be found
					gt.await(15);
					return gt.getAddress();
				}

				public void onFailure(int errResId) {
					// remove zip
					postal.setText("");

					// mark unsuccessful
					postal.setTag(false);

					// set name to error
					name.setText(errResId);
				}

				@Override
				public void onSuccess(final Address t) {
					// mark successful
					postal.setTag(true);

					// suppress lookup when populating the postal field
					suppressLocationLookup = true;
					postal.setText(t.getPostalCode());
					postal.setSelection(5);
					name.setText(t.getLocality() + ", " + t.getAdminArea());

					// suggest this address to database if it's not present
					new RpcTask<Void>() {
						@Override
						public Void doit() throws RemoteServiceFailureException {
							// submit these parameters to the server
							Location location = gt.getLocation();
							rpc.service.suggest(t.getPostalCode(), t.getLocality(), t.getAdminArea(),
									location.getLatitude(), location.getLongitude());
							return null;
						}

						@Override
						public void onSuccess(Void t) {
							// ignored
						}

						@Override
						public void onFailure(int errResId) {
							// ignored
						}
					}.execute();
				}

				protected void onPostExecute(Address result) {
					// remove the listener for location updates
					lm.removeUpdates(lt);

					// enable the button
					geocode.setEnabled(true);

					// continue with task
					super.onPostExecute(result);
				}
			}.execute();
		} else {
			// provider not enabled, prompt user to enable it
			Toast.makeText(this, R.string.warn_location, Toast.LENGTH_LONG).show();
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
	}

	/**
	 * Perform search.
	 */
	private void search() {
		new MobTask<Integer>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				// loading
				setEmptyString(R.string.warn_load, true);

				// clear results
				results.clear();
				offset = 0;

				// update title view
				Key<Type> type = rpc.getType();
				setTitle(type == null ? "any" : type.getName());

				// update view
				adapter.notifyDataSetChanged();
			}

			@Override
			public Integer doit() throws RemoteServiceFailureException {
				return rpc.loadCars(false);
			}

			@Override
			public void onFailure(int errResId) {
				// set to init'd
				initialized = true;
				
				// sync content
				sync(false);

				// continue failure
				super.onFailure(errResId);
			}

			public void onSuccess(Integer t) {
				// set to init'd
				initialized = true;
				
				// set the offset
				offset = t;

				// sync content
				sync(true);
			}
		}.execute();
	}

	/**
	 * Show the vehicle at the given position.
	 * 
	 * @param position
	 */
	private void showCar(int position) {
		Intent intent = new Intent(this, Classified.class);
		intent.putExtra(Classified.CAR, results.get(position));
		intent.putExtra(Classified.INDEX, position);
		intent.putExtra(Classified.MODE, Classified.MODE_SEARCH);
		startActivity(intent);
	}

	/**
	 * Sync the rpc data with this activity.
	 */
	private void sync(boolean success) {
		if (success)
			// succeeded, sync results
			offset = rpc.syncSearch(results);

		// update the empty message
		if (results.isEmpty())
			setEmptyString(R.string.warn_search, false);

		// update controls
		updateControls();

		// update view
		adapter.notifyDataSetChanged();
	}

	private void updateControls() {
		// update title view
		Key<Type> type = rpc.getType();
		setTitle(type == null ? "any" : type.getName());

		// types are available iff cars are available (account for "any" option)
		boolean hasCars = rpc.getChoices(R.id.types).length > 1;

		btnTypes.setEnabled(hasCars);
		btnMakes.setEnabled(hasCars);
		btnFilters.setEnabled(hasCars);

		// check if models has already been set
		btnModels.setEnabled(rpc.getChoices(R.id.models).length != 0);
	}
}