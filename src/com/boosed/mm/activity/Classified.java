package com.boosed.mm.activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.boosed.mm.R;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.enums.AdType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.GalleryAdapter;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.NumberRangeKeyListener;
import com.googlecode.objectify.Key;

public class Classified extends AbstractActivity {// implements
	// OnGlobalLayoutListener {

	/** gallery adapter */
	private/* ArrayAdapter<Entry<String, Tuple<Boolean, String>>> */GalleryAdapter adapter;

	/** intent key for index of the referenced vehicle from rpc service */
	public static final String INDEX = "com.boosed.mm.classified.Index";

	/** intent key for actual car reference */
	public static final String CAR = "com.boosed.mm.classified.Car";

	/** intent key for the calling context's mode (search or inventory) */
	public static final String MODE = "com.boosed.mm.classified.Mode";

	/** mode to reference search list */
	public static final int MODE_SEARCH = 0;

	/** mode to reference inventory list */
	public static final int MODE_INVENTORY = 1;

	/** mode for stand alone */
	public static final int MODE_STANDALONE = 2;

	/** indicates whether search or inventory data is being used */
	private int mode = MODE_SEARCH;

	/** list of cars */
	private final List<Car> cars;

	/** bookmarks */
	private final Set<Key<Car>> bookmarks;

	/**
	 * index of the currently shown item from service list (i.e., search or
	 * inventory)
	 */
	private int index = -2;

	/**
	 * the index of the selected image (only set if this activity is restored,
	 * e.g., during orientation changes)
	 */
	private int img = -1;

	/** max price for marker */
	private Marker marker;

	/** key listener for price alert */
	private NumberRangeKeyListener listener;

	public Classified() {
		// no-arg constructor
		super(R.layout.title_classified);
		cars = new ArrayList<Car>();
		bookmarks = new HashSet<Key<Car>>();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the content view
		setContentView(R.layout.act_classified);

		// initialize the gallery
		initGallery();
		// layoutImage();

		if (initialized) {
			// initialize parameters from orientation change
			index = savedInstanceState.getInt(INDEX);
			mode = savedInstanceState.getInt(MODE);
			img = savedInstanceState.getInt("image");
			ad = (Ad) savedInstanceState.getSerializable("ad");
			account = (Account) savedInstanceState.getSerializable("account");
			marker = (Marker) savedInstanceState.getSerializable("marker");
			// gallery.setSelection(savedInstanceState.getInt("image"));
		} else {
			// set index & mode from the intent
			Intent i = getIntent();
			index = i.getIntExtra(INDEX, -2);
			mode = i.getIntExtra(MODE, MODE_SEARCH);
		}
		//
		// if (index == -1) {
		// // set index & mode from the intent
		// Intent i = getIntent();
		// index = i.getIntExtra("car", -1);
		// mode = i.getIntExtra("mode", -1);
		// }
		//
		// if (index != -1 && mode != -1) {
		// // Car car = rpc.search.get(index);
		// // if (!"".equals(car.image))
		// index++;
		// showCar(false);
		// }
		// Integer[] data = (Integer[]) getLastNonConfigurationInstance();
		// if (data != null) {
		// index = data[0];
		// mode = data[1];
		// }
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// index
		outState.putInt(INDEX, index);

		// mode
		outState.putInt(MODE, mode);

		// image
		outState.putInt("image", gallery.getSelectedItemPosition());

		// ad
		outState.putSerializable("ad", ad);

		// account
		outState.putSerializable("account", account);

		// marker
		outState.putSerializable("marker", marker);
	}

	/** suppress alert when user seeks to new price */
	private Boolean suppressPriceAlert = false;

	@Override
	protected Dialog onCreateDialog(final int id) {

		// for all cases
		Builder builder = new Builder(this);

		switch (id) {
		case R.id.alert:
			// set title
			builder.setTitle(R.string.dlg_title_alert);

			View view = getLayoutInflater().inflate(R.layout.dlg_price_alt,
					(ViewGroup) findViewById(R.id.dlg_price_alt));
			alert = (SeekBar) view.findViewById(R.id.alert);
			hi = (TextView) view.findViewById(R.id.hi);
			target = (EditText) view.findViewById(R.id.target);
			builder.setView(view);

			// set text watcher
			TextWatcher tw = new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					synchronized (suppressPriceAlert) {
						// suppress changes on seekbar
						suppressPriceAlert = true;

						// if text is blank, set progress to zero
						alert.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not implemented
				}

				@Override
				public void afterTextChanged(Editable s) {
					// not implemented
				}
			};
			target.addTextChangedListener(tw);

			// set key listener
			target.setFilters(new InputFilter[] { listener = new NumberRangeKeyListener(
					marker.price) });
			// mText.setRawInputType(InputType.TYPE_CLASS_NUMBER);

			// set seek listener
			OnSeekBarChangeListener osbcl = new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// not implemented
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// not implemented
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					// target.setText(Integer.toString(progress));
					synchronized (suppressPriceAlert) {
						if (suppressPriceAlert)
							suppressPriceAlert = false;
						else {
							String value = Integer.toString(progress);
							target.setText(value);
							target.setSelection(value.length());
						}
					}
				}
			};
			alert.setOnSeekBarChangeListener(osbcl);

			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						new MobTask<Void>(R.string.stat_price, true) {
							@Override
							public Void doit() throws RemoteServiceFailureException {
								// set the price alert value
								rpc.service.updateAlert(marker.car.getId(), alert.getProgress());
								return null;
							}

							@Override
							public void onSuccess(Void t) {
								// not implemented
							}
						}.execute();
						break;
					case Dialog.BUTTON_NEGATIVE:
					default:
						// close dialog
						break;
					}
				}
			};

			// set buttons
			builder.setPositiveButton(R.string.dlg_btn_set, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		default:
			return super.onCreateDialog(id);
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case R.id.alert:
			// set the markers
			alert.setMax(marker.price);
			alert.setProgress(marker.point);

			// set text values
			hi.setText("$" + Integer.toString(marker.price));
			String value = Integer.toString(marker.point);
			target.setText(value);
			target.setSelection(value.length());
			listener.setMax(marker.price);
			break;
		default:
			super.onPrepareDialog(id, dialog);
			break;
		}
	}

	// @Override
	// public Object onRetainNonConfigurationInstance() {
	// // capture the current state before orientation change
	// Integer[] data = new Integer[] { index, mode };
	// return data;
	// }

	// @Override
	// public void onConfigurationChanged(Configuration newConfig) {
	// super.onConfigurationChanged(newConfig);
	// //setContentView(R.layout.act_classified);
	// initLayout();
	// }

	// private boolean init = false;

	@Override
	public void onBind() {
		// sync cars
		switch (mode) {
		case MODE_INVENTORY:
			// list from inventory
			rpc.syncClassified(cars);
			break;
		case MODE_SEARCH:
		default:
			// listing from a search (by default)
			rpc.syncSearch(cars);
			break;
		}

		// sync bookmarks
		rpc.syncBookmarks(bookmarks);

		// if (!initialized) {
		// increment index to prepare for showing previous
		index++;

		if (!showCar(false))
			// could not initialize car from service; use intent reference
			// instead
			setCar((Car) getIntent().getSerializableExtra(CAR));

		initialized = true;
		// } else
		// // just use intent data to init view
		// setCar((Car) getIntent().getSerializableExtra(CAR));
	}

	private Gallery gallery;
	// private ProgressBar loading;
	private ImageButton btnPrevious, btnNext, btnBookmark, btnInventory;
	private Button btnAlert;
	private ImageView image;
	private TextView txtYear, txtCondition, txtModel, txtDoors, txtPrice, txtMileage, txtLocation,
			txtTransmission, txtInterior, txtExterior, txtEngine, txtDrivetrain, txtStock, txtVin;
	private TextView content, name, phone, email, address, url;
	// private ImageSwitcher test;
	private TextView hi;
	private EditText target;

	/** price alert seekbar */
	private SeekBar alert;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// haptic feedback listener
		View.OnClickListener haptic = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			}
		};

		// init car fields
		txtModel = (TextView) findViewById(R.id.model);
		txtYear = (TextView) findViewById(R.id.year);
		txtCondition = (TextView) findViewById(R.id.condition);
		txtPrice = (TextView) findViewById(R.id.price);
		txtLocation = (TextView) findViewById(R.id.location);
		txtMileage = (TextView) findViewById(R.id.mileage);
		txtDoors = (TextView) findViewById(R.id.doors);
		txtExterior = (TextView) findViewById(R.id.exterior);
		txtInterior = (TextView) findViewById(R.id.interior);
		txtTransmission = (TextView) findViewById(R.id.transmission);
		txtEngine = (TextView) findViewById(R.id.engine);
		txtDrivetrain = (TextView) findViewById(R.id.drivetrain);

		txtStock = (TextView) findViewById(R.id.stock);
		txtVin = (TextView) findViewById(R.id.vin);

		// haptic enabled
		txtPrice.setOnClickListener(haptic);
		txtLocation.setOnClickListener(haptic);

		// init ad fields
		content = find(TextView.class, R.id.content);

		// init contact fields
		name = find(TextView.class, R.id.name);
		email = find(TextView.class, R.id.email);
		phone = find(TextView.class, R.id.phone);
		address = find(TextView.class, R.id.address);
		url = find(TextView.class, R.id.url);

		// haptic enabled
		name.setOnClickListener(haptic);
		email.setOnClickListener(haptic);
		phone.setOnClickListener(haptic);
		address.setOnClickListener(haptic);
		url.setOnClickListener(haptic);

		// init buttons
		btnPrevious = (ImageButton) findViewById(R.id.previous);
		btnNext = (ImageButton) findViewById(R.id.next);
		btnInventory = find(ImageButton.class, R.id.inventory);
		btnBookmark = (ImageButton) findViewById(R.id.bookmark);
		btnAlert = find(Button.class, R.id.alert);

		gallery = (Gallery) findViewById(R.id.gallery);
		image = (ImageView) findViewById(R.id.image);
		// loading = (ProgressBar) findViewById(R.id.loading);

		// test = (ImageSwitcher) findViewById(R.id.test);
		// test.setInAnimation(AnimationUtils.loadAnimation(this,
		// android.R.anim.fade_in));
		// test.setOutAnimation(AnimationUtils.loadAnimation(this,
		// android.R.anim.fade_out));

		initLayout();

		// test.setFactory(new ViewFactory() {
		//
		// @Override
		// public View makeView() {
		// ImageView image = new ImageView(Classified.this);
		// image.setScaleType(ScaleType.MATRIX);
		// // ImageSwitcher.LayoutParams params = new
		// // ImageSwitcher.LayoutParams((int) scrWidth, (int) scrHeight);
		// // params.gravity = Gravity.CENTER;
		// image.setLayoutParams(new ImageSwitcher.LayoutParams((int) scrWidth,
		// (int) scrHeight));
		// // image.setImageMatrix(matrix);
		// // image.setImageResource(R.drawable.icon);
		// return image;
		// }
		// });
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	public void onControl(View view) {
		switch (view.getId()) {
		case R.id.previous:
			showCar(false);
			break;
		case R.id.next:
			showCar(true);
			break;
		case android.R.id.title:
			// go back to results
			finish();
			break;
		case R.id.inventory:
			// commit preference
			settings.edit().putInt(RpcService.PREF_INVENTORY, Inventory.MODE_DLR).commit();
			// settings.edit().putString(RpcService.PREF_DEALER,
			// car.owner.getName()).commit();

			// create intent for inventory
			Intent inventory = new Intent(Classified.this, Inventory.class);

			// set the dealer as the car owner
			inventory.putExtra(Inventory.DEALER, car.owner.getName());

			// start new activity
			startActivity(inventory);
			break;
		// case R.id.inquire:
		// // new MobTask<Void>() {
		// // @Override
		// // public Void doit() throws RemoteServiceFailureException {
		// // rpc.service.checkCreds();
		// // return null;
		// // }
		// //
		// // @Override
		// // public void onSuccess(Void t) {
		// // }
		// // }.execute();
		// // check that the current user is not also owner
		// if (car.owner.getName().equals(rpc.accountId))
		// toast(getString(R.string.exception_owner));
		// else {
		// Intent intent = new Intent(this, Composer.class);
		// intent.putExtra(Composer.REFERENCE, car);
		// startActivity(intent);
		// }
		// break;
		case R.id.bookmark:
			// set bookmark
			final boolean marked = bookmarks.contains(car.getKey());
			new MobTask<Void>(marked ? R.string.stat_remove : R.string.stat_bookmark, true, view) {
				@Override
				public Void doit() throws RemoteServiceFailureException {
					rpc.bookmark(car.id, !marked);
					// if (marked)
					// rpc.removeBookmark(car.id);
					// else
					// rpc.addBookmark(car.id);
					return null;
				}

				// @Override
				// public void onFailure(int errResId) {
				// switch (errResId) {
				// case R.string.exception_resource_limit:
				// toast("your limit of allotted bookmarks has been reached");
				// break;
				// default:
				// // continue with failure
				// super.onFailure(errResId);
				// }
				// }

				@Override
				public void onSuccess(Void t) {
					// update bookmarks
					rpc.syncBookmarks(bookmarks);

					// update bookmark
					updateBookmark();
				}
			}.execute();
			break;
		case R.id.alert:
			// load the marker
			new MobTask<Marker>(view) {
				@Override
				public Marker doit() throws RemoteServiceFailureException {
					return rpc.service.loadPrice(car.id);
				}

				@Override
				public void onSuccess(Marker t) {
					if (t == null) {
						marker = new Marker();
						marker.price = car.price;
						marker.point = 0;
						marker.car = car.getKey();
					} else {
						marker = t;

						// adjust for inactive marker
						if (!marker.active) {
							// treat like new marker
							marker.price = car.price;
							marker.point = 0;
							marker.car = car.getKey();
						}
					}

					// decrement price by 1
					marker.price--;

					// show the alert dialog
					showDialog(R.id.alert);
				}
			}.execute();
			break;
		default:
			break;
		}
	}

	@Override
	public void onReplay(View replay) {
		// replay a control
		onControl(replay);
	}

	// @Override
	// public void onGlobalLayout() {
	// init();
	// }

	// the constrained viewing rectangle
	// private RectF viewRect = new RectF();
	// the projected new viewing rectangle
	// private RectF prjRect = new RectF();
	// the original image rect that is transformed
	// private RectF imgRect = new RectF();

	// private float maxZoom;
	// private float minZoom;

	/** matrix for manipulating the scale of the image */
	private Matrix matrix = new Matrix();

	// /**
	// * Initialize the size of widgets and the loaded image.
	// *
	// * @deprecated
	// */
	// private void init(int... dims) {
	// // width and height of the image view
	// // scrHeight = image.getHeight();
	// // scrWidth = image.getWidth();
	//
	// // Log.i("chat", "scr height: " + scrHeight + ", scr width: " +
	// // scrWidth);
	//
	// // width and height of the loaded image
	// // imgHeight = view.getDrawable().getIntrinsicHeight();
	// // imgWidth = view.getDrawable().getIntrinsicWidth();
	//
	// // Log.i("chat", "the view is: " + view.toString());
	// // Log.i("chat", "the draw is: " + view.getDrawable());
	// // int iw, ih;
	// float imgWidth, imgHeight;
	//
	// if (dims.length == 0) {
	// Drawable d = image.getDrawable();
	// imgWidth = d.getIntrinsicWidth();
	// imgHeight = d.getIntrinsicHeight();
	// } else {
	// imgWidth = dims[1];
	// imgHeight = dims[0];
	// }
	//
	// // Log.i("chat", "img height: " + imgHeight + ", img width: " +
	// // imgWidth);
	// // if (vheight > height || vwidth > width) {
	//
	// // set the min zoom so that img takes up entire screen
	// float magnify = Math.min(scrHeight / imgHeight, scrWidth / imgWidth);
	// Log.w("chat", "min zoom is: " + magnify + " sh: " + scrHeight +
	// ", sw: " + scrWidth + ", " + imgWidth + ", "
	// + imgHeight);
	// // max zoom is 3x image size
	// // maxZoom = magnify * 3;
	//
	// // establish the valid viewing rectangle
	// float dx = (scrWidth - imgWidth * magnify) / 2f;
	// float dy = (scrHeight - imgHeight * magnify) / 2f;
	// // viewRect.set(dx, dy, dx + imgWidth * minZoom, dy + imgHeight *
	// // minZoom);
	//
	// // zero out our cumulative changes
	// matrix.reset();
	//
	// // set proper size (the transformation matrix incorporates the
	// minimum
	// // zoom to optimize size of the image)
	// matrix.postScale(magnify, magnify, 0, 0);
	//
	// // center in middle of screen
	// matrix.postTranslate(dx, dy);
	//
	// // apply the changes
	// image.setImageMatrix(matrix);
	// }

	private void clear() {
		// clear images
		image.setVisibility(View.INVISIBLE);
		adapter.clear();
		content.setText("loading...");

		// clear account
		setField(name, "loading...");
		setField(email, null);
		setField(address, null);
		setField(phone, null);
		setField(url, null);
	}

	/**
	 * Initialize the size of widgets and the loaded image.
	 */
	private void format(ImageView image) {
		Drawable d = image.getDrawable();
		// imgRect.set(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
		float imgWidth = d.getIntrinsicWidth(), imgHeight = d.getIntrinsicHeight();

		// Log.i("chat", "img height: " + imgHeight + ", img width: " +
		// imgWidth);
		// if (vheight > height || vwidth > width) {

		// set the min zoom so that img takes up entire screen
		float magnify = Math.min(scrHeight / imgHeight, scrWidth / imgWidth);
		// Log.w("chat", "min zoom is: " + magnify + " sh: " + scrHeight +
		// ", sw: " + scrWidth + ", " + imgWidth + ", "
		// + imgHeight);
		// max zoom is 3x image size
		// maxZoom = magnify * 3;

		// establish the valid viewing rectangle
		float dx = (scrWidth - imgWidth * magnify) / 2f;
		float dy = (scrHeight - imgHeight * magnify) / 2f;
		// viewRect.set(dx, dy, dx + imgWidth * minZoom, dy + imgHeight *
		// minZoom);

		// zero out our cumulative changes
		matrix.reset();

		// set proper size (the transformation matrix incorporates the minimum
		// zoom to optimize size of the image)
		matrix.postScale(magnify, magnify, 0, 0);

		// center in middle of screen
		matrix.postTranslate(dx, dy);

		// apply the changes
		image.setImageMatrix(matrix);
	}

	// private void initContent() {
	// // get starting intent and set the car id
	//
	// if (index == -1) {
	// // set index & mode from the intent
	// Intent i = getIntent();
	// index = i.getIntExtra("car", -1);
	// mode = i.getIntExtra("mode", -1);
	// }
	//
	// if (index != -1 && mode != -1) {
	// // Car car = rpc.search.get(index);
	// // if (!"".equals(car.image))
	// index++;
	// showCar(false);
	// }
	// }

	private void initGallery() {
		// Work around a Cupcake bug
		// matrix.setTranslate(1f, 1f);
		image.setImageMatrix(matrix);
		// image.getViewTreeObserver().addOnGlobalLayoutListener(this);

		adapter = new GalleryAdapter(this, new ArrayList<Entry<String, Tuple<Boolean, String>>>());

		// // prepare galleries
		// adapter = new BaseArrayAdapter<Entry<String, Tuple<Boolean, String>>,
		// ImageView>(this,
		// images, R.layout.list_item_img) {
		//
		// private int count = 0;
		//
		// @Override
		// protected ImageView createView(View view) {
		// return (ImageView) view;
		// }
		//
		// @Override
		// protected void render(final Entry<String, Tuple<Boolean, String>>
		// item,
		// final ImageView row) {
		// // set the loading image
		// row.setImageResource(R.drawable.w100);
		//
		// // String url = item.getValue().b;
		// // if (!"".equals(url)) {
		// // dm.loadImage(url + "=s150", row, new ImageCallback() {
		// // @Override
		// // public void onLoad(int height, int width) {
		// // //notifyDataSetChanged();
		// // //row.refreshDrawableState();
		// // // adapter.notifyDataSetInvalidated();
		// // //gallery.invalidate();
		// // //row.postInvalidate();
		// // // image.setVisibility(View.VISIBLE);
		// // // Log.w("chat", "refreshing an images");
		// // }
		// // });
		// dm.loadImage(item.getValue().b + "=s150", row, new ImageCallback() {
		//
		// @Override
		// public void onLoad(int height, int width) {
		// if (++count == images.size()) {
		// toast("updating the adapter!");
		// notifyDataSetChanged();
		// }
		// }
		// });
		// // row.setImageResource(Integer.parseInt(url));
		// // }
		// // row.setAnimation(AnimationUtils.loadAnimation(Classified.this,
		// // R.anim.rotate));
		// // row.
		//
		// // new ImageTask(row).execute(url);
		// }
		// };

		gallery.setAdapter(adapter);

		// set the empty view
		gallery.setEmptyView(findViewById(android.R.id.empty));

		adapter.notifyDataSetChanged();

		gallery.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				setImage(adapter.getItem(position).getValue().b);
				// final String url = adapter.getItem(position).getValue().b;
				//
				// // hide image to show progress bar
				// image.setVisibility(View.INVISIBLE);
				// // image.setImageResource(Integer.parseInt(url));
				// // image.setVisibility(View.VISIBLE);
				//
				// // test.setImageResource(Integer.parseInt(url));
				// // format((ImageView) test.getCurrentView());
				//
				// // Log.w("chat", test.getCurrentView().hashCode() + "");
				//
				// // load the selected image
				// dm.loadImage(url, image, new ImageCallback() {
				// @Override
				// public void onLoad(int height, int width) {
				// // row.refreshDrawableState();
				// // init(height, width);
				// format(image);
				// image.setVisibility(View.VISIBLE);
				// // image.refreshDrawableState();
				//
				// // ensures that the borders redraw properly
				// // adapter.notifyDataSetChanged();
				// // Log.w("chat", "refreshing an images");
				//
				// // save selected image
				// // imageUrl = url;
				// }
				// });
			}
		});

		// gallery.setOnItemSelectedListener(new OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
		// long arg3) {
		// Toast.makeText(Classified.this, "something has been selected",
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		//
		// }
		//
		// });
	}

	/** values set from initializing the layout */
	private float scrHeight, scrWidth;

	/** the orientation of the screen */
	private int orientation;

	private void initLayout() {
		// Toast.makeText(this, "width: " + metrics.widthPixels + ", height: " +
		// metrics.heightPixels, Toast.LENGTH_SHORT).show();

		// assign the orientation
		orientation = getResources().getConfiguration().orientation;

		switch (orientation) {
		case Configuration.ORIENTATION_LANDSCAPE:
			// layout the image to 50% of screen width
			int dim = (int) (metrics.widthPixels * 0.5);
			// fill_parent does not seem to work here?
			LayoutParams params = new LayoutParams(dim, metrics.heightPixels, Gravity.CENTER);
			// Toast.makeText(this, "resetting the layout to width: " + dim,
			// Toast.LENGTH_SHORT).show();
			image.setLayoutParams(params);
			scrHeight = metrics.heightPixels;
			scrWidth = dim;
			break;
		case Configuration.ORIENTATION_PORTRAIT:
			// layout the image to 40% of screen height
			dim = (int) (metrics.heightPixels * 0.4);
			// Toast.makeText(this, "resetting the layout to height: " + dim,
			// Toast.LENGTH_SHORT).show();
			params = new LayoutParams(/* LayoutParams.FILL_PARENT */metrics.widthPixels, dim,
					Gravity.CENTER);
			image.setLayoutParams(params);
			scrHeight = dim;
			scrWidth = metrics.widthPixels;
			break;
		}

		// ((ViewGroup)findViewById(android.R.id.content)).getChildAt(0).invalidate();
	}

	// public android.widget.ImageSwitcher.LayoutParams getLayoutParams() {
	// WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
	// switch (wm.getDefaultDisplay().getOrientation()) {
	// case Surface.ROTATION_90:
	// case Surface.ROTATION_270:
	// // layout the image to 60% of screen width
	// int dim = (int) (metrics.widthPixels * 0.6);
	// return new android.widget.ImageSwitcher.LayoutParams(dim,
	// LayoutParams.FILL_PARENT, Gravity.CENTER);
	// // image.setLayoutParams(params);
	// // break;
	// case Surface.ROTATION_0:
	// case Surface.ROTATION_180:
	// // layout the image to 40% of screen height
	// dim = (int) (metrics.heightPixels * 0.4);
	// return new
	// android.widget.ImageSwitcher.LayoutParams(LayoutParams.FILL_PARENT, dim,
	// Gravity.CENTER);
	// // image.setLayoutParams(params);
	// // break;
	// }
	//
	// return null;
	// }

	// private Long carId = null;
	private Car car = null;

	private Account account = null;

	private Ad ad = null;

	private void setAccount(Account account) {
		this.account = account;

		// mandatory
		SpannableStringBuilder ssb = new SpannableStringBuilder(account.name
				+ (account.dealer ? " (dealer)" : ""));

		final boolean isOwner = car.owner.getName().equals(rpc.accountId);

		// direct messaging
		if (!isOwner && car.adType != AdType.BASIC) {
			// user is not owner and ad type is not free
			ssb.setSpan(new ClickableSpan() {
				@Override
				public void onClick(View widget) {
					// check again since might be auth'd now
					if (car.owner.getName().equals(rpc.accountId))
						toast(getString(R.string.exception_owner));
					else {
						Intent intent = new Intent(Classified.this, Composer.class);
						intent.putExtra(Composer.REFERENCE, car);
						startActivity(intent);
					}
				}
			}, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

			name.setMovementMethod(LinkMovementMethod.getInstance());
		}

		// set name
		name.setText(ssb);

		// only add inventory button if current user is not owner
		// final String owner = car.owner.getName();
		btnInventory.setEnabled(!isOwner);

		// optional
		// String address = account.address == null ? "" : account.address;
		StringBuilder sb = new StringBuilder(account.address == null ? "" : (account.address + " "));

		if (account.postal != null)
			sb.append(account.postal);

		setField(this.address, sb.toString());// , Linkify.MAP_ADDRESSES);

		// the email field is already nullified on the server
		setField(email, account.email);// , Linkify.EMAIL_ADDRESSES);
		setField(phone, account.phone);// , Linkify.PHONE_NUMBERS);
		setField(url, account.url);// , Linkify.WEB_URLS);
	}

	/**
	 * Sets the <code>Ad</code>.
	 * 
	 * @param ad
	 * @return whether or not the image was set automatically
	 */
	private boolean setAd(Ad ad) {
		this.ad = ad;

		// set the content
		String content = ad.getContent();
		this.content.setText(content == null ? "none" : content);

		// set the images
		// images.clear();
		// images.addAll(ad.getImages());
		adapter.setImages(ad.getImages());

		// init the selected image
		if (img == -1) {
			// no image selected, queue the first gallery item
			if (!adapter.isEmpty())
				gallery.setSelection(0);

			return false;
		} else {
			// set the selected image (ad is loaded dynamically from
			// selected
			// car)

			// set gallery position to previous position
			gallery.setSelection(img);

			// set the image
			// handler.postDelayed(new Runnable() {
			// @Override
			// public void run() {
			setImage(adapter.getItem(img).getValue().b);

			// reset the shown image
			img = -1;

			return true;
			// }
			// }, 100);

			// // get the url value
			// final String url = adapter.getItem(img).getValue().b;
			// // final String url = ((Entry<String, Tuple<Boolean, String>)
			// // gallery.getSelectedItem()).getValue().b;
			//
			// // render this image for main
			// dm.loadImage(url, image, new ImageCallback() {
			// @Override
			// public void onLoad(int height, int width) {
			// // alert.dismiss();
			// // description.setText(photo.getDescription());
			// // init(height, width);
			// format(image);
			// image.setVisibility(View.VISIBLE);
			//
			// // Toast.makeText(ImageViewer.this,
			// // photo.getDescription(), Toast.LENGTH_LONG).show();
			//
			// // reset the value
			// img = -1;
			// }
			// });
		}

		// adapter.notifyDataSetChanged();
		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// adapter.notifyDataSetChanged();
		// // gallery.refreshDrawableState();
		// }
		// }, 200);

	}

	private void setCar(final Car car) {
		// set car id
		// carId = car.id;
		this.car = car;

		// hide/clear out the (previous) view
		clear();

		// set the image
		// if (!"".equals(car.image))
		// try {
		// final PhotoDto photo = (PhotoDto)
		// rpc.viewedProfile[photoIndex];

		// dm.loadImage(car.image, image, new ImageCallback() {
		// @Override
		// public void onLoad(int height, int width) {
		// // alert.dismiss();
		// // description.setText(photo.getDescription());
		// // init(height, width);
		// format(image);
		// image.setVisibility(View.VISIBLE);
		// // Toast.makeText(ImageViewer.this,
		// // photo.getDescription(), Toast.LENGTH_LONG).show();
		// }
		// });

		// Toast.makeText(this, photo.getUrl(),
		// Toast.LENGTH_LONG).show();
		// } catch (Exception e) {
		// e.printStackTrace();
		// Log.i("chat", "there has been an error");
		// // alert.dismiss();
		// }

		// init buttons
		int limit = cars.size();

		// set the title
		StringBuilder sb = new StringBuilder();
		// sb.append(car.year).append(' ').append(car.model.getName());
		// (" [" + (index + 1) + "/" + cars.size() + "]")
		if (index == -1)
			// standalong mode
			sb.append("return");
		else
			sb.append(index + 1).append(" of ").append(limit);

		// String count = index == -1 ? "" : (" [" + (index + 1) + "/" +
		// cars.size() + "]");
		setTitle(/* getString(R.string.app_listing) + " - " + */sb);

		// initialize button state; get the upper bound

		// index is [1, limit)
		btnPrevious.setEnabled((index > 0) && (index < limit));

		// index is [0, limit - 1)
		btnNext.setEnabled((index > -1) && (index < --limit));

		// quickly set image set to first car image (retains the size of the
		// gallery)
		// if (images.isEmpty()) {
		// Toast.makeText(this, "the images are empty",
		// Toast.LENGTH_SHORT).show();
		// Map<String, Tuple<Boolean, String>> data = new HashMap<String,
		// Tuple<Boolean, String>>();
		// data.put("", new Tuple<Boolean, String>(true, car.image));
		// data.put("b", new Tuple<Boolean, String>(true, car.image));
		// images.clear();
		// images.addAll(data.entrySet());

		// adapter.notifyDataSetChanged();
		// }

		// SpannableString ss = new SpannableString(car.model.getName());
		// ss.setSpan(new StyleSpan(Typeface.BOLD), 0, 5, 0);
//		sb = new StringBuilder(car.model.getName());
//
//		// add trim
//		if (car.trim != null)
//			sb.append(" ").append(car.trim);
		txtModel.setText(car.toString());

		// txtModel.setText(car.model.getName() + (car.trim == null ? "" :
		// car.trim));
		txtCondition.setText(car.conditions.get(0).toString());
		txtDoors.setText(Integer.toString(car.doors));
		txtDrivetrain.setText(car.drivetrain.getName());
		txtEngine.setText(car.engine.getName());
		txtExterior.setText(car.exterior.getName());
		txtInterior.setText(car.interior.getName());

		// SpannableString value = rpc.postal == null ? "" : (" (" +
		// df.format(car.distance) + " miles)");
		SpannableStringBuilder ssb = new SpannableStringBuilder(car.location);

		// ~(retrieved car from search and postal is not null)
		ssb
				.append((mode != MODE_SEARCH || settings.getString(RpcService.PREF_POST_SRCH, null) == null) ? " "
						: (" (" + df.format(car.distance) + " miles) "));

		// specify exact or approximate location
		ssb.append(car.explicit ? ", exact" : ", approximate");

		// if (car.longitude != null)
		// ss = new SpannableString(car.location + value);
		ssb.setSpan(new ClickableSpan() {
			@Override
			public void onClick(View widget) {
				// txtLocation.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
				String uri = "http://maps.google.com/maps?q=" + car.latitude + "," + car.longitude
						+ "+(" + /*car.model.getName()*/car.toString() + ")";
				// String uri = "geo:0,0?q=" + car.latitude + "," +
				// car.longitude + " (location)";
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(/*
																		 * "geo:"
																		 * + car
																		 * .
																		 * latitude
																		 * + ","
																		 * + car
																		 * .
																		 * longitude
																		 */uri));

				startActivity(intent);
			}
		}, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		// ss.setSpan(us, 0, 2, 0);
		// ss.setSpan(new StyleSpan(Typeface.BOLD), 0, 5, 0);
		txtLocation.setMovementMethod(LinkMovementMethod.getInstance());
		txtLocation.setText(ssb);

		txtMileage.setText(df.format(car.mileage));

		// clickable price to show price alert dialog
		ssb = new SpannableStringBuilder("$" + df.format(car.price));

		// only add clickable if current user is not owner
		if (!car.owner.getName().equals(rpc.accountId)) {
			ssb.setSpan(new ClickableSpan() {
				@Override
				public void onClick(View widget) {
					// check if the current user is owner
					if (car.owner.getName().equals(rpc.accountId))
						toast(getString(R.string.exception_owner));
					else
						// fire off alert control
						onControl(btnAlert);
					// txtPrice.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
				}
			}, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			txtPrice.setMovementMethod(LinkMovementMethod.getInstance());
		}
		txtPrice.setText(ssb);

		// txtStock.setText(car.stock == null ? "n/a" : car.stock);
		setField(txtStock, car.stock);// , 0);

		txtTransmission.setText(car.transmission.getName());
		txtYear.setText(Integer.toString(car.year));

		// set the text according to the orientation
		String novin = orientation == Configuration.ORIENTATION_LANDSCAPE ? "vin not available"
				: "not available";
		txtVin.setText(car.vin == null ? novin : car.vin);

		// enable the bookmark button
		boolean isOwner = car.owner.getName().equals(rpc.accountId);
		btnBookmark.setEnabled(!isOwner);

		// update bookmark button
		updateBookmark();
		// btnBookmark.setEnabled();

		if (ad != null && ad.car.equals(car.getKey())) {
			// account is same as car owner; use existing ad and account
			setAccount(account);

			if (!setAd(ad)) {
				// set the image
				// toast("set the image from car!");
				// handler.postDelayed(new Runnable() {
				// @Override
				// public void run() {
				setImage(car.image);
				// }
				// }, 2000);
			}
		} else
			// load ad & account for new car
			new MobTask<Tuple<Ad, Account>>() {
				@Override
				public Tuple<Ad, Account> doit() throws RemoteServiceFailureException {
					return rpc.service.loadAd(car.ad.getId(), true);
				}

				public void onFailure(int errResId) {
					// check to see if ad exists
					switch (errResId) {
					case R.string.exception_not_found:
						// the ad does not exist, finish activity
						if (mode == MODE_SEARCH) {
							// TODO: this result should be purged from the
							// search results
						}

						finish();
						break;
					default:
						// continue with failure
						super.onFailure(errResId);
					}
				}

				@Override
				public void onSuccess(Tuple<Ad, Account> t) {
					setAccount(t.b);

					if (!setAd(t.a)) {

						// // set the image
						// toast("set the image from car!");
						// handler.postDelayed(new Runnable() {
						// @Override
						// public void run() {
						setImage(car.image);
						// }
						// }, 2000);
					}
				}
			}.execute();
	}

	private void setImage(String url) {
		// initially hide image
		image.setVisibility(View.INVISIBLE);

		// render this image for main
		dm.display(image, url, new ImageCallback() {
			@Override
			public void onLoad(int height, int width) {
				format(image);
				image.setVisibility(View.VISIBLE);
			}
		});

		// dm.loadImage(url, image, new ImageCallback() {
		// @Override
		// public void onLoad(int height, int width) {
		// // description.setText(photo.getDescription());
		// // init(height, width);
		// // size the main image appropriately
		// format(image);
		//
		// // set image to visible
		// // image.setVisibility(View.VISIBLE);
		//
		// // handler.post(new Runnable() {
		// // public void run() {
		// // format(image);
		// image.setVisibility(View.VISIBLE);
		// // }
		// // });
		//
		// // draw correct border around gallery items when image displayed
		// // handler.postDelayed(new Runnable() {
		// // @Override
		// // public void run() {
		// // adapter.notifyDataSetChanged();
		// // }
		// // }, 1500);
		// }
		// });
	}

	/** decimal format for numbers */
	private DecimalFormat df = new DecimalFormat("###,###,##0");

	private boolean showCar(boolean next) {

		// switch (mode) {
		// case MODE_INVENTORY:
		// // list from inventory
		// cars = rpc.inventory;
		// break;
		// case MODE_SEARCH:
		// default:
		// // listing from a search (by default)
		// cars = rpc.search;
		// break;
		// }

		// check bounds and adjust if necessary (these checks always return
		// index to its original or correct value)
		if (next && ++index == cars.size())
			index--;
		else if (!next && --index < 0)
			index++;

		// // initialize button state; get the upper bound
		// int limit = cars.size();
		//
		// // index is [1, limit)
		// btnPrevious.setEnabled((index > 0) && (index < limit));
		//
		// // index is [0, limit - 1)
		// btnNext.setEnabled((index > -1) && (index < --limit));

		// set the car
		try {
			// use the service results
			setCar(cars.get(index));
			return true;
		} catch (IndexOutOfBoundsException e) {
			// the service has been reset; no data available
			index = -1;
			return false;
		}
	}

	private void setField(TextView field, String value) {
		// set visibility
		((View) field.getParent()).setVisibility(value == null || value.length() == 0 ? View.GONE
				: View.VISIBLE);

		// set value
		field.setText(value);

		// linkify is now in the layout xml
		// if (mask != 0)
		// Linkify.addLinks(field, mask);
	}

	// private boolean marked = false;

	private void updateBookmark() {
		// marked = bookmarks.contains(car.getKey());

		// set state of the button
		btnBookmark.setImageResource(bookmarks.contains(car.getKey()) ? R.drawable.ic_menu_stop
				: R.drawable.ic_menu_bookmark);
		// if (marked)
		// // bookmarked
		// btnBookmark.setImageResource(R.drawable.ic_menu_stop);
		// else
		// // not bookmarked
		// btnBookmark.setImageResource(R.drawable.ic_menu_bookmark);
	}
}