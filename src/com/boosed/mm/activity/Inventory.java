package com.boosed.mm.activity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.boosed.mm.R;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.CLLocale;
import com.boosed.mm.shared.db.CLState;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.enums.AdNetwork;
import com.boosed.mm.shared.db.enums.AdType;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseArrayAdapter;
import com.boosed.mm.ui.CheckedImage;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.ListItemPhoto;
import com.boosed.mm.ui.NumberRangeKeyListener;
import com.googlecode.objectify.Key;

/**
 * Activity for managing bookmarks, postings/ads, credits (maybe alerts?).
 * 
 * @author dsumera
 */
public class Inventory extends AbstractListActivity implements Controllable {

	/** bookmark mode */
	public static final int MODE_BMK = 0;

	/** posting mode */
	public static final int MODE_PST = 1;

	/** price alert mode */
	public static final int MODE_ALT = 2;

	/** dealer mode */
	public static final int MODE_DLR = 3;

	/** intent key for specifying dealer inventory */
	public static final String DEALER = "com.boosed.mm.inventory.Dealer";

	/** current mode of the activty */
	private int mode;

	/** the date format */
	private static final String FMT_DATE = " @ h:mm aa, M/d/yy";

	/** bookmarks */
	private final Set<Key<Car>> bookmarks;

	/** inventory */
	private final List<Tuple<Marker, Car>> inventory;

	/** inventory cursor */
	private String cursor = null;

	/** car adapter */
	private ArrayAdapter<Tuple<Marker, Car>> adapter;

	/** max price for marker */
	private Marker marker;

	/** car to repost */
	private long repost;

	/** key listener for price alert */
	private NumberRangeKeyListener listener;

	/** lookup dealer inventory (i.e., dealer id) */
	private String dealer = null;

	public Inventory() {
		super(R.string.warn_bookmarks, R.layout.title_inventory);
		bookmarks = new HashSet<Key<Car>>();
		inventory = new ArrayList<Tuple<Marker, Car>>();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// add the footer before setting the adapter
		mList.addFooterView(footer);

		// set & create adapter
		setListAdapter(adapter = new BaseArrayAdapter<Tuple<Marker, Car>, ListItemPhoto>(this,
				inventory, R.layout.list_item_photo) {

			private DecimalFormat df = new DecimalFormat("###,###,##0");

			@Override
			protected ListItemPhoto createView(View view) {
				return new ListItemPhoto(view);
			}

			@Override
			protected void render(Tuple<Marker, Car> item, final ListItemPhoto row) {
				// get car
				final Car car = item.b;

				// get image
				final ImageView image = row.getImage();

				dm.display(image, car.image + "=s160", new ImageCallback() {
					@Override
					public void onLoad(int height, int width) {
						//image.refreshDrawableState();
						image.setVisibility(View.VISIBLE);
					}
				});
				
//				// set photo
//				dm.loadImage(car.image + "=s160", image, new ImageCallback() {
//					@Override
//					public void onLoad(int height, int width) {
//						// refresh image
//						image.refreshDrawableState();
//						image.setVisibility(View.VISIBLE);
//					}
//				});
				
				// set title
				row.getTitle().setText(/*car.model.getName()*/car.toString());

				// set year & colors
				row.getContent1().setText(
						car.year + ", " + car.exterior.getName() + "/" + car.interior.getName());

				// // set miles & price
				// row.getContent2().setText(df.format(car.mileage) +
				// " miles, $" + df.format(car.price));

				// set checked
				CheckedImage check = row.getCheck();

				switch (/* dealerMode(mode) */mode) {
				case MODE_DLR:
				case MODE_BMK:
					// bookmarks
					// set clickable only if not the owner
					boolean isOwner = car.owner.getName().equals(rpc.accountId);
					if (isOwner)
						check.setClickable(false);
					else {
						check.setClickable(true);
						check.setChecked(bookmarks.contains(car.getKey()));
					}

					check.setTag(car);

					// set miles & price
					row.getContent2().setText(
							df.format(car.mileage) + " miles, $" + df.format(car.price));

					// don't show miles since this was loaded inventory (not
					// search)
					row.getEnd().setText(car.location);
					break;
				case MODE_PST:
					// inventory
					check.setClickable(false);

					// car ad is new or expired
					check.setChecked(car.time != null);

					// remove the associated check tag
					check.setTag(null);

					// set miles & price
					row.getContent2().setText(
							df.format(car.mileage) + " miles, $" + df.format(car.price));

					// show the ad type
					StringBuilder sb = new StringBuilder(car.adType.toString());
					// sb.append(car.adType.toString());
					if (car.time == null)
						// car is not currently searchable
						sb.append(car.isActive() ? ", expired" : ", not activated");
					else
						// show elapsed time
						sb.append(", up ").append(elapsed(car.time));
					// sb.append(car.time == null ? (car.isActive() ?
					// ", expired" : ", not activated")
					// : (", " +
					// elapsed(car.time)));//DateFormat.format(FMT_DATE, new
					// Date(car.time)));
					row.getEnd().setText(sb);
					break;
				case MODE_ALT:
					final Marker m = item.a;

					// price
					check.setClickable(false);
					// alert met
					check.setChecked(m.point >= m.price);
					check.setTag(m);

					// show price & point
					row.getContent2()
							.setText("$" + df.format(m.point) + ", $" + df.format(m.price));

					// point
					sb = new StringBuilder(df.format(m.delta));
					sb.append(DateFormat.format(FMT_DATE, new Date(m.time)));

					// set stats
					row.getEnd().setText(sb);
					break;
				default:
					break;
				}
			}

			// @Override
			// public void notifyDataSetChanged() {
			// super.notifyDataSetChanged();
			//
			// // reset visibility of shadow if list is empty
			// shadow.setVisibility(inventory.isEmpty() ? View.GONE :
			// View.VISIBLE);
			// }
		});

		// don't need footer so remove it now
		mList.removeFooterView(footer);

		// register list for context menus
		registerForContextMenu(mList);

		if (initialized) {
			// init marker
			marker = (Marker) savedInstanceState.get("marker");

			// init dealer
			dealer = (String) savedInstanceState.get(DEALER);

			// init repost
			repost = savedInstanceState.getLong("repost");
		} else
			dealer = getIntent().getStringExtra(DEALER);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save marker
		outState.putSerializable("marker", marker);

		// save dealer
		outState.putString(DEALER, dealer);

		// save repost
		outState.putLong("repost", repost);
	}

	// /** listviews backing dialogs */
	// private Map<Integer, ListView> lists = new HashMap<Integer, ListView>();

	/** suppress alert when user seeks to new price */
	private Boolean suppressPriceAlert = false;

	@Override
	protected Dialog onCreateDialog(int id) {
		// for all cases
		Builder builder = new Builder(this);

		switch (id) {
		case R.id.inventory:
			// title
			builder.setTitle(R.string.dlg_title_inventory);

			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// if (which == Dialog.BUTTON_NEGATIVE)
					// return;

					// set the category
					mode = which;
					settings.edit().putInt(RpcService.PREF_INVENTORY, mode).commit();

					// perform an update
					load();

					// dismiss
					dialog.dismiss();
				}
			};

			// add negative button to cancel out dialog
			// builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.setSingleChoiceItems(R.array.inventory, mode, ocl).create();
		case R.id.sort:
			// title
			builder.setTitle(R.string.dlg_title_sort);

			ocl = new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// if (which == Dialog.BUTTON_NEGATIVE)
					// return;

					// set sort
					settings.edit().putInt(RpcService.PREF_SORT_INV, sorts[which]).commit();

					// load results
					load();

					// dismiss
					dialog.dismiss();
				}
			};

			// attempt to find selected sort in settings
			int selected = settings.getInt(RpcService.PREF_SORT_INV, 0);
			for (int i = sorts.length; --i > -1;)
				if (sorts[i] == selected) {
					// sort at index i matches selected value, set the index
					selected = i;
					break;
				}

			// add negative button to cancel out dialog
			// builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.setSingleChoiceItems(R.array.sorts_inv, selected, ocl).create();
		case R.id.alert:
			// set title
			builder.setTitle(R.string.dlg_title_alert);

			View view = getLayoutInflater().inflate(R.layout.dlg_price_alt,
					(ViewGroup) findViewById(R.id.dlg_price_alt));
			alert = (SeekBar) view.findViewById(R.id.alert);
			hi = (TextView) view.findViewById(R.id.hi);
			target = (EditText) view.findViewById(R.id.target);
			builder.setView(view);

			// set text watcher
			TextWatcher tw = new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					synchronized (suppressPriceAlert) {
						// suppress changes on seekbar
						suppressPriceAlert = true;

						// if text is blank, set progress to zero
						alert.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not implemented
				}

				@Override
				public void afterTextChanged(Editable s) {
					// not implemented
				}
			};
			target.addTextChangedListener(tw);

			// set key listener
			target.setFilters(new InputFilter[] { listener = new NumberRangeKeyListener(
					marker.price) });
			// mText.setRawInputType(InputType.TYPE_CLASS_NUMBER);

			// set seekbar listener
			OnSeekBarChangeListener osbcl = new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// not implemented
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// not implemented
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					// target.setText(Integer.toString(progress));
					synchronized (suppressPriceAlert) {

						if (suppressPriceAlert)
							suppressPriceAlert = false;
						else {
							// int value = Integer.parseInt(s.toString());
							// alert.setProgress(value);
							String value = Integer.toString(progress);
							target.setText(value);
							target.setSelection(value.length());
						}
					}
				}
			};
			alert.setOnSeekBarChangeListener(osbcl);

			ocl = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						new MobTask<Void>(R.string.stat_price, true) {
							@Override
							public Void doit() throws RemoteServiceFailureException {
								// set the price alert value
								rpc.service.updateAlert(marker.car.getId(), alert.getProgress());
								return null;
							}

							@Override
							public void onSuccess(Void t) {
								if (mode == MODE_ALT)
									for (Tuple<Marker, Car> tuple : inventory)
										if (tuple.b.getKey().equals(marker.car)) {
											// change the prices

											// set new markerprice point
											tuple.a.point = alert.getProgress();

											// set new marker car price
											tuple.a.price = alert.getMax() + 1;

											// set new car price
											tuple.b.price = tuple.a.price;

											// update view
											adapter.notifyDataSetChanged();

											// exit
											break;
										}
							}
						}.execute();
						break;
					case Dialog.BUTTON_NEGATIVE:
					default:
						// close dialog
						break;
					}
				}
			};

			// set buttons
			builder.setPositiveButton(R.string.dlg_btn_set, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.ctx_repost:
			// set title
			builder.setTitle(R.string.dlg_title_cg);

			// set view
			view = getLayoutInflater().inflate(R.layout.dlg_post_cl,
					(ViewGroup) findViewById(R.id.dlg_post_cl));
			builder.setView(view);
			warn = (TextView) view.findViewById(R.id.warn);

			// locale adapter
			locale = (Spinner) view.findViewById(R.id.locale);
			final List<Key<CLLocale>> locales = new ArrayList<Key<CLLocale>>();
			final ArrayAdapter<Key<CLLocale>> la = new ArrayAdapter<Key<CLLocale>>(this,
					R.layout.spinner_item, locales) {
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					TextView tv = (TextView) super.getView(position, convertView, parent);
					tv.setText(locales.get(position).getName());
					return tv;
				}

				@Override
				public View getDropDownView(int position, View convertView, ViewGroup parent) {
					TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
					tv.setText(locales.get(position).getName());
					return tv;
				}
			};
			la.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			locale.setAdapter(la);
			OnItemSelectedListener oisl = new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, final int position,
						long id) {
					// set selection in preferences
					settings.edit().putInt(RpcService.PREF_CL_LOCALE, position).commit();
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// not implemented
				}
			};
			locale.setOnItemSelectedListener(oisl);

			// state adapter
			state = (Spinner) view.findViewById(R.id.state);
			final List<Key<CLState>> states = new ArrayList<Key<CLState>>();
			final ArrayAdapter<Key<CLState>> sa = new ArrayAdapter<Key<CLState>>(this,
					R.layout.spinner_item, states) {
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					TextView tv = (TextView) super.getView(position, convertView, parent);
					tv.setText(states.get(position).getName());
					return tv;
				}

				@Override
				public View getDropDownView(int position, View convertView, ViewGroup parent) {
					TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
					tv.setText(states.get(position).getName());
					return tv;
				}
			};
			sa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			state.setAdapter(sa);
			oisl = new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, final int position,
						long id) {
					// set selection in preferences
					settings.edit().putInt(RpcService.PREF_CL_STATE, position).commit();

					// retrieve the locales from the datastore
					new MobTask<List<Key<CLLocale>>>() {
						@Override
						protected void onPreExecute() {
							super.onPreExecute();

							// disable models spinner
							locale.setEnabled(false);
						}

						@Override
						public List<Key<CLLocale>> doit() throws RemoteServiceFailureException {
							return rpc.service.loadLocales(states.get(position));
						}

						@Override
						public void onSuccess(List<Key<CLLocale>> t) {
							// adjust shown models
							locales.clear();
							locales.addAll(t);

							if (initCl) {
								// locales loaded for first time, initialize the value
								locale.setSelection(settings.getInt(RpcService.PREF_CL_LOCALE, 0));
								initCl = false;
							}

							la.notifyDataSetChanged();
						}

						@Override
						protected void onPostExecute(List<Key<CLLocale>> result) {
							// enable locale spinner
							locale.setEnabled(true);

							// continue task
							super.onPostExecute(result);
						}
					}.execute();
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// not implemented
				}
			};
			state.setOnItemSelectedListener(oisl);

			// populate the states
			new MobTask<List<Key<CLState>>>() {
				@Override
				protected void onPreExecute() {
					super.onPreExecute();

					// disable makes spinner
					state.setEnabled(false);
				}

				@Override
				public List<Key<CLState>> doit() throws RemoteServiceFailureException {
					return rpc.service.loadStates();
				}

				@Override
				public void onSuccess(List<Key<CLState>> t) {
					// update makes
					states.clear();
					states.addAll(t);

					// initialize the selected state
					state.setSelection(settings.getInt(RpcService.PREF_CL_STATE, 0));

					sa.notifyDataSetChanged();
				}

				@Override
				protected void onPostExecute(List<Key<CLState>> result) {
					// enable makes spinner
					state.setEnabled(true);

					// continue task
					super.onPostExecute(result);
				}
			}.execute();

			ocl = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// Key<CLLocale> area = (Key<CLLocale>)
						// locale.getSelectedItem();
						new MobTask<Void>(R.string.stat_post, true) {
							@SuppressWarnings("unchecked")
							@Override
							public Void doit() throws RemoteServiceFailureException {
								// post to craigslist
								rpc.service
										.postCg(repost, (Key<CLLocale>) locale.getSelectedItem());
								return null;
							}

							@Override
							public void onSuccess(Void t) {
								toast("posted to clist, check email for further instructions");
							}
						}.execute();
						break;
					case Dialog.BUTTON_NEGATIVE:
					default:
						// close dialog
						break;
					}
				}
			};

			// set buttons
			builder.setPositiveButton(R.string.dlg_btn_post, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		default:
			return super.onCreateDialog(id);
		}
	}

	// @Override
	// protected Dialog onCreateDialog(final int id) {
	//
	// // for all cases
	// // AlertDialog.Builder builder = new AlertDialog.Builder(this);
	//
	// switch (id) {
	// default:
	// return super.onCreateDialog(id);
	// }
	// }

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		showCar(position);
	}

	/** choice items mapped to sort type indexes */
	private static final int[] sorts = new int[] { SortType.NONE.ordinal(),
			SortType.TIME.ordinal(), SortType.MODEL.ordinal(), SortType.MODEL_DESC.ordinal(),
			SortType.PRICE.ordinal(), SortType.PRICE_DESC.ordinal(), SortType.YEAR_DESC.ordinal(),
			SortType.YEAR.ordinal() };

	/** initialize the cg values */
	private boolean initCl = true;
	
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case R.id.inventory:
			// set correct inventory mode
			((AlertDialog) dialog).getListView().setItemChecked(
					settings.getInt(RpcService.PREF_INVENTORY, 0), true);
			break;
		case R.id.sort:
			// set correct sort mode

			// attempt to find selected sort in settings
			int selected = settings.getInt(RpcService.PREF_SORT_INV, 0);
			for (int i = sorts.length; --i > -1;)
				if (sorts[i] == selected) {
					// sort at index i matches selected value, set the index
					selected = i;
					break;
				}

			((AlertDialog) dialog).getListView().setItemChecked(selected, true);
			break;
		case R.id.alert:
			// set the markers
			alert.setMax(marker.price);
			alert.setProgress(marker.point);
			hi.setText("$" + Integer.toString(marker.price));
			String value = Integer.toString(marker.point);
			target.setText(value);
			target.setSelection(value.length());
			listener.setMax(marker.price);
			// target.setFilters(new InputFilter[] { new
			// NumberRangeKeyListener(marker.price) });
			break;
		case R.id.ctx_repost:
			// set state and locales
			state.setSelection(settings.getInt(RpcService.PREF_CL_STATE, 0));
			initCl = true;
//			locale.setSelection(settings.getInt(RpcService.PREF_CL_LOCALE, 0));
			break;
		default:
			super.onPrepareDialog(id, dialog);
			break;
		}
	}

	/** intent returned from onActivityResult */
	private Intent data = null;

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (/* dealerMode(requestCode) */requestCode) {
			case MODE_DLR:
				// check if current user is the dealer (this may not be
				// necessary)
				if (dealer == null || !dealer.equals(rpc.accountId))
					break;
			case MODE_PST:
				// returned from an edit, refresh the postings
				// (invoked before
				// onResume/onBind)
				// onControl(settings.getBoolean(RpcService.PREF_INVENTORY,
				// false) ? btnPosts : btnBookmarks);

				// if (settings.getBoolean(RpcService.PREF_INVENTORY, false))

				// came back from edit, update inventory
				if (data == null)
					load();
				else
					// set the intent data for later processing in onBind()
					this.data = data;

				// came back from photo edit and main image updated
				// toast("inventory size is: " + inventory.size());

				// // just set the new image url on the ad
				// inventory.get(data.getIntExtra(Photos.POSITION,
				// 0)).b.image = data
				// .getStringExtra(Photos.URL);
				//
				// // update view
				// adapter.notifyDataSetChanged();

				// setReplay(settings.getBoolean(RpcService.PREF_INVENTORY,
				// false) ? btnPosts
				// : btnBookmarks);
				// updateView();
				break;
			case MODE_BMK:
			case MODE_ALT:
				// do nothing
			default:
				break;
			}
			return;
		}
	}

	// @Override
	// protected void onSaveInstanceState(Bundle outState) {
	// super.onSaveInstanceState(outState);
	//
	// // save inventory
	// outState.putSerializable("inventory", (Serializable) inventory);
	// }

	private ImageButton btnSort;
	private RelativeLayout btnInventory;
	// private ImageView shadow;
	private TextView hi, warn;
	private EditText target;

	/** spinners for craigslist posting */
	private Spinner state, locale;

	/** price alert seekbar */
	private SeekBar alert;

	/** the footer */
	private View footer;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// // set the controls
		// View view = getLayoutInflater().inflate(R.layout.ctl_inventory,
		// null);
		// shadow = (ImageView) findViewById(R.id.list_shadow);
		//
		// LayoutParams lp = new LayoutParams(0, LayoutParams.FILL_PARENT, 1);
		// controls.addView(view, lp);
		// controls.setVisibility(View.VISIBLE);

		btnInventory = (RelativeLayout) findViewById(R.id.inventory);
		btnSort = find(ImageButton.class, R.id.sort);
		// btnTypes.setEnabled(false);
		// btnPosts = (ImageButton) view.findViewById(R.id.posts);

		// disable pp button for now
		// findViewById(R.id.paypal).setEnabled(false);

		// create the footer
		footer = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.list_footer, null);
		// btnMakes.setEnabled(false);
		// btnModels = (ImageButton) view.findViewById(R.id.models);
		// btnModels.setEnabled(false);
		// btnFilters = (ImageButton) view.findViewById(R.id.filters);
		// btnFilters.setEnabled(false);
	}

	@Override
	public void onConnect() {
		// load the user's bookmarks
		new MobTask<Void>() {
			@Override
			public Void doit() throws RemoteServiceFailureException {
				rpc.loadBookmarks();
				return null;
			}

			@Override
			public void onSuccess(Void t) {
				// update bookmarks
				rpc.syncBookmarks(bookmarks);

				// update the adapter
				adapter.notifyDataSetChanged();
			}
		}.execute();

		// end authentication dialog and run replays
		super.onConnect();
	}

	@Override
	public void onDisconnect() {
		// update the state of the adapter
		adapter.notifyDataSetChanged();

		super.onDisconnect();
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {

		// position of item
		// final int place = ((AdapterContextMenuInfo)
		// item.getMenuInfo()).position;

		// account fields
		// final Map<Serializable, FieldAccount> fields = new
		// HashMap<Serializable, FieldAccount>();
		final int id = item.getItemId();
		switch (id) {
		case R.id.ctx_edit_car:
			// start activity to perform edit
			Intent intent = new Intent(Inventory.this, Edit.class);

			// get car
			Car car = adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position).b;

			// add vehicle information
			intent.putExtra(Edit.CAR, car);

			// start activity for result to reload item post edit
			startActivityForResult(intent, mode);
			return true;
		case R.id.ctx_repost:
			// save repost id
			repost = adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position).b.id;

			// create dialog
			showDialog(id);
			return true;
		case R.id.ctx_edit_details:
		case R.id.ctx_edit_img:
			// edit image/details
			final int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
			final Car c = adapter.getItem(position).b;

			new MobTask<Tuple<Ad, Account>>(item) {
				@Override
				public Tuple<Ad, Account> doit() throws RemoteServiceFailureException {
					// load the ad w/ account & images, do not sanitize
					return rpc.service.loadAd(c.ad.getId(), false); // rpc.loadModel(adapter.getItem(position).model);
				}

				@Override
				public void onSuccess(Tuple<Ad, Account> t) {
					if (id == R.id.ctx_edit_details) {
						// start the edit details activity
						Intent intent = new Intent(Inventory.this, Details.class);

						// add ad information
						intent.putExtra(Details.AD, t.a);

						// add vehicle information
						intent.putExtra(Details.CAR, c);

						// start activity (no immediate changes)
						startActivity(intent);
					} else {
						// start activity to perform edit
						Intent intent = new Intent(Inventory.this, Photos.class);

						// add ad information
						intent.putExtra(Photos.AD, t.a);

						// add model
						intent.putExtra(Photos.MODEL, c.year + " " + c.model.getName());

						// add position
						intent.putExtra(Photos.POSITION, position);

						// start activity (may change the main image)
						startActivityForResult(intent, mode);
					}
				}
			}.execute();
			return true;
		case R.id.ctx_activate:
			// get car
			car = adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position).b;
			if (confirm(item, car.isActive() ? R.string.cfm_renew : R.string.cfm_activate))
				// activate this listing
				new MobTask<Long>(R.string.stat_activate, true, item) {

					private final int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;

					@Override
					public Long doit() throws RemoteServiceFailureException {
						return rpc.service.activate(adapter.getItem(position).b.id, true);
					}

					@Override
					public void onSuccess(Long t) {
						// set the listing's activation time
						adapter.getItem(position).b.time = t;
						// rpc.inventory.get(position).time = t;

						// update adapter
						adapter.notifyDataSetChanged();
					}
				}.execute();
			return true;
		case R.id.ctx_del_ad:
			// delete the currently selected vehicle
			if (confirm(item, R.string.cfm_del_ad))
				new MobTask<Void>(R.string.stat_delete, true, item) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						// Car car = adapter.getItem(position);
						// rpc.service.removeCar(car.id);
						rpc.removeAd(adapter
								.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position));
						return null;
					}

					@Override
					public void onFailure(int errResId) {
						sync(false);
						super.onFailure(errResId);
					}

					@Override
					public void onSuccess(Void t) {
						sync(true);
						// remove the car from list
						// rpc.inventory.remove(t);

						// set the empty string
						// setEmptyString(rpc.inventory.isEmpty() ?
						// R.string.warn_postings : R.string.warn_load);

						// update adapter
						// adapter.notifyDataSetChanged();
					}
				}.execute();
			return true;
		case R.id.ctx_del_alert:
			if (confirm(item, R.string.cfm_del_alert))
				// delete the alert
				new MobTask<Void>() {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						rpc.removeAlert(adapter.getItem(((AdapterContextMenuInfo) item
								.getMenuInfo()).position));
						return null;
					}

					@Override
					public void onFailure(int errResId) {
						sync(false);
						super.onFailure(errResId);
					}

					@Override
					public void onSuccess(Void t) {
						sync(true);
					}
				}.execute();
			return true;
		case R.id.ctx_inquire:
			// start activty for making an inquiry
			car = adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position).b;
			if (car.owner.getName().equals(rpc.accountId))
				// TODO: does this check need to be performed
				toast(getString(R.string.exception_owner));
			else {
				intent = new Intent(this, Composer.class);
				intent.putExtra(Composer.REFERENCE, car);
				startActivity(intent);
			}
			return true;
		case R.id.ctx_price:
			// load the marker
			new MobTask<Marker>(item) {

				/** selected car */
				final Car car = adapter
						.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position).b;

				@Override
				public Marker doit() throws RemoteServiceFailureException {
					return rpc.service.loadPrice(car.id);
				}

				@Override
				public void onSuccess(Marker t) {
					if (t == null) {
						marker = new Marker();
						marker.price = car.price;
						marker.point = 0;
						marker.car = car.getKey();
					} else {
						marker = t;

						// adjust for inactive marker
						if (!marker.active) {
							// treat like new marker
							marker.price = car.price;
							marker.point = 0;
							marker.car = car.getKey();
						}
					}

					// decrement price by 1
					marker.price--;

					// show the alert dialog
					showDialog(R.id.alert);
				}
			}.execute();
			return true;
		case R.id.ctx_upgrade:
			// upgrade the vehicle
			new MobTask<Integer>(R.string.stat_upgrade, true, item) {
				@Override
				public Integer doit() throws RemoteServiceFailureException {
					int position = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
					rpc.service.upgrade(adapter.getItem(position).b.id);
					return position;
				}

				@Override
				public void onSuccess(Integer t) {
					// adjust the adtype for the result set
					adapter.getItem(t).b.adType = AdType.STANDARD;

					// update adapter
					adapter.notifyDataSetChanged();
				}
			}.execute();
			return true;
		case R.id.ctx_bookmark:
		case R.id.ctx_remove:
			// set bookmark
			final boolean marked = id == R.id.ctx_remove;
			new MobTask<Void>(marked ? R.string.stat_remove : R.string.stat_bookmark, true, item) {
				@Override
				public Void doit() throws RemoteServiceFailureException {
					rpc.bookmark(adapter
							.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position).b.id,
							!marked);
					return null;
				}

				// @Override
				// public void onFailure(int errResId) {
				// switch (errResId) {
				// case R.string.exception_resource_limit:
				// toast("your limit of allotted bookmarks has been reached");
				// break;
				// default:
				// // continue with failure
				// super.onFailure(errResId);
				// }
				// }

				@Override
				public void onSuccess(Void t) {
					// update bookmarks
					rpc.syncBookmarks(bookmarks);

					// update the adapter
					adapter.notifyDataSetChanged();
				}
			}.execute();
			return true;
			// case R.id.ctx_remove:
			// // remove the bookmark
			// new MobTask<Void>("removing", true, item) {
			// @Override
			// public Void doit() throws RemoteServiceFailureException {
			// rpc.removeBookmark(adapter.getItem(((AdapterContextMenuInfo)
			// item.getMenuInfo()).position).id);
			// return null;
			// }
			//
			// @Override
			// public void onSuccess(Void t) {
			// // update bookmarks
			// rpc.syncBookmarks(bookmarks);
			//
			// // update the adapter
			// adapter.notifyDataSetChanged();
			// }
			// }.execute();
			// return true;
		case R.id.ctx_inquiries:
			// view inquiries
			intent = new Intent(this, Messages.class);

			// add vehicle information
			intent.putExtra(Messages.REFERENCE, adapter.getItem(((AdapterContextMenuInfo) item
					.getMenuInfo()).position).b.id);
			intent.putExtra(Messages.TYPE, 0);
			intent.putExtra(Messages.TITLE, R.layout.title_reference);
			startActivity(intent);
			return true;
		case R.id.ctx_view:
			// show car
			showCar(((AdapterContextMenuInfo) item.getMenuInfo()).position);
			return true;
			// case R.id.ctx_cancel:
		default:
			// return the default result
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		// get car
		Car car = adapter.getItem(((AdapterContextMenuInfo) menuInfo).position).b;

		// set title
		menu.setHeaderTitle(/*car.model.getName()*/car.toString());

		// selling or buying?
		// boolean selling = car.owner.getName().equals(rpc.accountId);
		switch (/* dealerMode(mode) */mode) {
		case MODE_DLR:
		case MODE_BMK:
			// bookmark
			getMenuInflater().inflate(R.menu.ctx_menu_buying, menu);

			// buying, initialize bookmark/remove
			boolean present = bookmarks.contains(car.getKey());
			menu.findItem(R.id.ctx_bookmark).setVisible(!present);
			menu.findItem(R.id.ctx_remove).setVisible(present);
			
			// cannot inquire against basic ads
			menu.findItem(R.id.ctx_inquire).setVisible(car.adType != AdType.BASIC);
			break;
		case MODE_PST:
			// posting
			getMenuInflater().inflate(R.menu.ctx_menu_selling, menu);

			// selling, initialize activate/delete
			boolean active = /* car.time != null */car.isActive();

			// the car is inactive (not searchable) due to being new or expired
			MenuItem item = menu.findItem(R.id.ctx_activate);
			item.setVisible(/* !active */car.time == null);
			item.setTitle(active ? R.string.ctx_renew : R.string.ctx_activate);

			// car has been activated
			menu.findItem(R.id.ctx_inquiries).setVisible(active && car.adType != AdType.BASIC);
			menu.findItem(R.id.ctx_upgrade).setVisible(active && car.adType == AdType.BASIC);
			menu.findItem(R.id.ctx_repost).setVisible(active /*&& !car.getNetwork(AdNetwork.CLIST)*/);
			
			if (warn != null)
				warn.setText(car.getNetwork(AdNetwork.CLIST) ? R.string.warn_cgb : R.string.warn_cga);
			break;
		case MODE_ALT:
			getMenuInflater().inflate(R.menu.ctx_menu_alert, menu);
			break;
		default:
			break;
		}
	}

	@Override
	public void onReplay(View replay) {
		onControl(replay);
	}

	@Override
	public void onBind() {
		// initialize mode
		mode = settings.getInt(RpcService.PREF_INVENTORY, MODE_BMK);

		// no dealer specified, change mode in preferences
		if (mode == MODE_DLR && dealer == null) {
			mode = MODE_BMK;
			settings.edit().putInt(RpcService.PREF_INVENTORY, mode).commit();
		}

		if (initialized) {
			// init title/button
			switch (mode) {
			case MODE_DLR:
				setTitle(R.string.app_dealer);
				// btnInventory.setImageResource(R.drawable.ic_menu_shopping);
				break;
			case MODE_BMK:
				setTitle(R.string.app_bookmarks);
				// btnInventory.setImageResource(R.drawable.ic_menu_bookmark);
				break;
			case MODE_PST:
				setTitle(R.string.app_postings);
				// btnInventory.setImageResource(R.drawable.ic_menu_pin);
				break;
			case MODE_ALT:
				setTitle(R.string.app_alerts);
				// btnInventory.setImageResource(R.drawable.ic_menu_tags);
				break;
			default:
				break;
			}

			// sync results
			sync(true);

			// update inventory according to possible edits
			// just set the new image url on the ad
			if (data != null) {
				// update inventory
				inventory.get(data.getIntExtra(Photos.POSITION, 0)).b.image = data
						.getStringExtra(Photos.URL);

				// update view
				adapter.notifyDataSetChanged();

				// set data to null
				data = null;
			}
		} else
			// only load if activity first started
			load();

		// already initialized the adapter, continue
		// rpc.syncInventory(inventory);
		// adapter.notifyDataSetChanged();

		// initialize the active view (inventory or bookmarks according to
		// preferences); always gets executed when resuming the activity

		// onControl(settings.getBoolean(RpcService.PREF_INVENTORY, false) ?
		// btnPosts : btnInventory);
		// onControl(btnInventory);

		// if (settings.getBoolean(RpcService.PREF_INVENTORY, false))
		// // retrieve inventory
		// onControl(btnPosts);
		// else
		// // retrieve bookmarks
		// onControl(btnBookmarks);
	}

	@Override
	public void onControl(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.add:
			// start the creation wizard (new)
			startActivityForResult(new Intent(Inventory.this, Edit.class), mode);
			break;
		case R.id.inventory:
			if (initialized)
				// show dialog for selecting inventory
				showDialog(id);
			else
				// initialize for first time (no dialog)
				load();
			break;
		case R.id.sort:
			// show dialog for selecting sort
			showDialog(id);
			break;
		case android.R.id.checkbox:
			// handle bookmarking
			final CheckedImage bookmark = (CheckedImage) view;
			final boolean marked = bookmark.isChecked();
			// if (bookmark.isChecked())
			// remove the bookmark
			new MobTask<Void>(marked ? R.string.stat_remove : R.string.stat_bookmark, true, view) {
				@Override
				public Void doit() throws RemoteServiceFailureException {
					rpc.bookmark(((Car) bookmark.getTag()).id, !marked);
					return null;
				}

				// public void onFailure(int errResId) {
				// // TODO: retain bookmark; is this necessary?
				// // bookmark.setChecked(true);
				//
				// // show login
				// super.onFailure(errResId);
				// }

				@Override
				public void onSuccess(Void t) {
					// update bookmarks
					rpc.syncBookmarks(bookmarks);

					// update the adapter
					adapter.notifyDataSetChanged();
				}
			}.execute();
			// else
			// // set this bookmark
			// new MobTask<Void>("bookmarking", true, view) {
			// @Override
			// public Void doit() throws RemoteServiceFailureException {
			// rpc.addBookmark(car.id);
			// return null;
			// }
			//
			// @Override
			// public void onFailure(int errResId) {
			// // TODO: disallow bookmark; is this necessary
			// // bookmark.setChecked(false);
			//
			// switch (errResId) {
			// case R.string.exception_resource_limit:
			// toast("your limit of allotted bookmarks has been reached");
			// break;
			// default:
			// // continue with failure
			// super.onFailure(errResId);
			// }
			// }
			//
			// @Override
			// public void onSuccess(Void t) {
			// // update bookmarks
			// rpc.syncBookmarks(bookmarks);
			//
			// // update the adapter
			// adapter.notifyDataSetChanged();
			// }
			// }.execute();
			break;
		default:
			break;
		}
	}

	/** fetching status */
	private boolean fetching = false;

	/** footer status */
	private boolean hasFooter = false;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
			int totalItemCount) {

		// not fetching, not empty, at end of list, rpc cursor not null
		if (!fetching && totalItemCount != 0
				&& ((firstVisibleItem + visibleItemCount) >= totalItemCount) && rpc != null
				&& cursor != null)

			// perform scroll
			new MobTask<String>() {
				@Override
				protected void onPreExecute() {
					super.onPreExecute();

					// set fetching on
					fetching = true;

					// show footer
					if (!hasFooter) {
						mList.addFooterView(footer);
						hasFooter = true;
					}

					// disable buttons
					btnInventory.setEnabled(false);
					btnSort.setEnabled(false);
				}

				@Override
				public String doit() throws RemoteServiceFailureException {
					return rpc.loadInventory(true, dealer);
				}

				@Override
				public void onFailure(int errResId) {
					// sync content
					sync(false);

					// continue with failure
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(String t) {
					// set cursor
					cursor = t;

					// remove footer if at end of content
					if (cursor == null) {
						mList.removeFooterView(footer);
						hasFooter = false;
					}

					// sync content
					sync(true);
				}

				@Override
				protected void onPostExecute(String result) {
					// set fetching off
					fetching = false;

					super.onPostExecute(result);
				}
			}.execute();
	}

	// @Override
	// public void onConnect() {
	// // connected
	// //super.onConnect();
	// //endStatus();
	//
	// // perform the online step now that we're authenticated
	// //init = false;
	// //onOnline();
	//
	//
	// if (!init) {
	// // refresh the view
	// updateView();
	//
	// // set initialized to true
	// //init = true;
	// }
	// }

	// @Override
	// public void onOnline() {
	// super.onOnline();
	//
	// if (!init) {
	// // refresh the view
	// updateView();
	//
	// // set initialized to true
	// init = true;
	// }
	//
	// // // if inventory is populated, just update the controls
	// // if (rpc.inventory.isEmpty()) {
	// // if (inventory)
	// // // retrieve inventory
	// // onControl(btnPosts);
	// // else
	// // // retrieve bookmarks
	// // onControl(btnBookmarks);
	// // } else {
	// // // update controls
	// // updateControls(inventory);
	// //
	// // // update list
	// // adapter.notifyDataSetChanged();
	// // }
	// }

	// /**
	// * Returns appropriate mode according to whether user is the dealer.
	// *
	// * @return
	// */
	// private int dealerMode(int mode) {
	// if (mode == MODE_DLR)
	// if (dealer.equals(rpc.accountId))
	// // user is the owner
	// return MODE_PST;
	// else
	// // user is not owner
	// return MODE_BMK;
	// else
	// return mode;
	// }

	/**
	 * Provide a string of elapsed time.
	 * 
	 * @param time
	 * @return
	 */
	private String elapsed(long time) {
		long elapsed = System.currentTimeMillis() - time;

		if (elapsed < 60000)
			// if under minute, show seconds
			return new BigDecimal(elapsed * .001).setScale(0, RoundingMode.HALF_UP) + " seconds";
		else if (elapsed < 3600000)
			// if under an hour, show minutes
			return new BigDecimal(elapsed * 1.66667e-5).setScale(2, RoundingMode.HALF_UP) + " minutes";
		else if (elapsed < 86400000)
			// if under day, show hours
			return new BigDecimal(elapsed * 2.77778e-7).setScale(2, RoundingMode.HALF_UP) + " hours";
		else
			// show days
			return new BigDecimal(elapsed * 1.15741e-8).setScale(2, RoundingMode.HALF_UP) + " days";
	}
	
	/**
	 * Perform a load of inventory items wrt type and sort order.
	 */
	private void load() {
		// update according to the mode (use inventory button as replay view)
		new MobTask<String>(btnInventory) {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				// update title/button
				switch (mode) {
				case MODE_DLR:
					setTitle(R.string.app_dealer);
					// btnInventory.setImageResource(R.drawable.ic_menu_shopping);
					break;
				case MODE_BMK:
					setTitle(R.string.app_bookmarks);
					// btnInventory.setImageResource(R.drawable.ic_menu_bookmark);
					break;
				case MODE_PST:
					setTitle(R.string.app_postings);
					// btnInventory.setImageResource(R.drawable.ic_menu_pin);
					break;
				case MODE_ALT:
					setTitle(R.string.app_alerts);
					// btnInventory.setImageResource(R.drawable.ic_menu_tags);
					break;
				default:
					break;
				}

				// disable buttons
				btnInventory.setEnabled(false);
				btnSort.setEnabled(false);

				// set the loading string
				setEmptyString(R.string.warn_load, true);

				// clear the messages
				inventory.clear();
				cursor = null;

				// update view
				adapter.notifyDataSetChanged();
			}

			@Override
			public String doit() throws RemoteServiceFailureException {
				return rpc.loadInventory(false, dealer);
			}

			@Override
			public void onFailure(int errResId) {
				// set to initialized (w/ failure)
				// initialized = false;

				// sync content
				sync(false);

				// continue with failure
				super.onFailure(errResId);
			}

			@Override
			public void onSuccess(String t) {
				// set to initialized
				initialized = true;

				// set cursor
				cursor = t;

				// sync content
				sync(true);
			}
		}.execute();
	}

	/**
	 * Show the vehicle at the given position.
	 * 
	 * @param position
	 */
	private void showCar(int position) {
		Intent intent = new Intent(this, Classified.class);
		intent.putExtra(Classified.CAR, inventory.get(position).b);
		intent.putExtra(Classified.MODE, Classified.MODE_INVENTORY);
		intent.putExtra(Classified.INDEX, position);
		startActivity(intent);
	}

	/**
	 * Sync the rpc data with this activity.
	 */
	private void sync(boolean success) {
		if (success)
			// sync inventory
			cursor = rpc.syncInventory(inventory);

		boolean empty = inventory.isEmpty();

		// update view
		if (empty)
			switch (mode) {
			case MODE_BMK:
				setEmptyString(R.string.warn_bookmarks, false);
				break;
			case MODE_DLR:
			case MODE_PST:
				setEmptyString(R.string.warn_listings, false);
				break;
			case MODE_ALT:
				setEmptyString(R.string.warn_alerts, false);
				break;
			default:
				break;
			}

		// update controls
		btnInventory.setEnabled(true);
		btnSort.setEnabled(!empty);

		// update view
		adapter.notifyDataSetChanged();

		// TODO update image changes from photos activity
	}
}