package com.boosed.mm.activity;

import java.util.concurrent.CountDownLatch;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.boosed.mm.R;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;

public class Notice extends AbstractActivity implements Controllable {

	public Notice() {
		super(R.layout.title_notice);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set content view
		//setContentView(/* view */R.layout.act_notice);
		setContentView(view = new WebView(this));
		
		// add app title mod common to all activities
		// getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
		// R.layout.title);

		// set title
		//setTitle(R.string.app_terms);

		view.getSettings().setJavaScriptEnabled(true);

		// do not cache images, such as the captcha image
		view.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

		view.setWebViewClient(new WebViewClient() {

			// private Dialog dialog;
			private CountDownLatch latch = new CountDownLatch(1);

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);

				// clear the cache for captcha image
				// view.clearCache(true);

				// Log.i("chat", url);

				// if no longer in signup, kill the activity
				// if (!url.contains("signup"))
				// finish();
				// else
				// dialog = ProgressDialog.show(Notice.this, "",
				// getText(R.string.stat_load));
				setStatus(getString(R.string.stat_load));

				new MobTask<Void>(R.string.stat_load, true) {

					@Override
					public Void doit() throws RemoteServiceFailureException {
						try {
							latch.await();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						return null;
					}

					@Override
					public void onSuccess(Void t) {
						// not implemented
					}
				}.execute();
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

				// dialog.dismiss();
				endStatus();

				// release latch
				latch.countDown();
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				// return super.shouldOverrideUrlLoading(view, url);
				return false;
			}
		});

		// final Activity activity = this;
		// webview.setWebChromeClient(new WebChromeClient() {
		// public void onProgressChanged(WebView view, int progress) {
		// // Activities and WebViews measure progress with different scales.
		// // The progress meter will automatically disappear when we reach 100%
		// activity.setProgress(progress * 1000);
		// }
		// });
		// webview.setWebViewClient(new WebViewClient() {
		// public void onReceivedError(WebView view, int errorCode, String
		// description, String failingUrl) {
		// Toast.makeText(activity, "Oh no! " + description,
		// Toast.LENGTH_SHORT).show();
		// }
		// });

		view.loadUrl(/* "http://m.mobimotos.com" */getString(R.string.url_terms));
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onBind() {
		// not implemented
	}

	private WebView view;

	//private FrameLayout frame;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// center frame
		//frame = find(FrameLayout.class, R.id.frame);

		// add the view
		//frame.addView(view = new WebView(this));
	}

	@Override
	public void onControl(View view) {
		finish();
	}
}