package com.boosed.mm.activity.task;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.boosed.mm.shared.exception.InvalidLocationException;

/**
 * Class which can be set against a <code>LocationManager</code> to receive
 * location updates. Calling <code>await</code> waits up until the specified
 * time to receive a <code>Location</code>. The retrieved <code>Location</code>
 * can be accessed by calling <code>getLocation</code>.
 * 
 * @author dsumera
 */
public class LocationTask implements LocationListener {

	private final CountDownLatch latch;

	private Location location = null;

	public LocationTask() {
		latch = new CountDownLatch(1);
	}

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;

		// got the location, now decrement the latch
		latch.countDown();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// not implemented
	}

	@Override
	public void onProviderEnabled(String provider) {
		// not implemented
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// not implemented
	}

	public Location getLocation() throws InvalidLocationException {
		if (location == null)
			throw new InvalidLocationException();

		return location;
	}

	/**
	 * Wait for task to receive a <code>Location</code> for the specified
	 * <code>time</code> in seconds.
	 * 
	 * @throws InvalidLocationException
	 */
	public void await(int time) throws InvalidLocationException {
		try {
			latch.await(time, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// could not find location
			throw new InvalidLocationException();
		}
	}
}