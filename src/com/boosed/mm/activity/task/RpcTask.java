package com.boosed.mm.activity.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.boosed.mm.i18n.ExceptionMessageFactory;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;

/**
 * <code>AsyncTask</code> designed to work with the exceptions thrown from
 * GWT-RPC.
 * 
 * @author dsumera
 * 
 * @param <T>
 *            the return type for the <code>AsyncCallback</code>
 */
public abstract class RpcTask<T> extends AsyncTask<Void, Void, T> {

	// whether the rpc operation succeeded or not
	private boolean success = false;

	// exception message factory for generating the propery exception code
	private static ExceptionMessageFactory emf = ExceptionMessageFactory.getInstance();

	// exception that was thrown
	private Exception exception;

	// modal dialog
	private ProgressDialog dialog;

	// model dialog message
	private CharSequence message = null;

	// context for displaying dialog
	private Context context;

	public RpcTask() {
		// default no-arg constructor
	}

	public RpcTask(Context context, CharSequence message) {
		this.context = context;
		this.message = message;
	}

	/**
	 * Attempt to dismiss the <code>Dialog</code> if it exists and is showing.
	 */
	public void dismissDialog() {
		if (dialog != null && dialog.isShowing())
			try {
				dialog.dismiss();
			} catch (IllegalArgumentException e) {
				// this happens when dialog is no longer attached to window
				// manager
			}
	}

	/**
	 * Change the displayed message for the dialog of this <code>RpcTask</code>.
	 * 
	 * @param message
	 */
	public void setMessage(CharSequence message) {
		dialog.setMessage(message);
	}

	/**
	 * Set the <code>Context</code> for this <code>RpcTask</code>. Useful for
	 * when the task may be executed in a different <code>Activity</code>.
	 * 
	 * @param context
	 */
	protected void setContext(Context context) {
		this.context = context;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		if (message != null)
			dialog = ProgressDialog.show(context, "", message);
	}

	@Override
	protected T doInBackground(Void... params) {
		try {
			T t = doit();
			success = true;
			return t;
		} catch (Exception e) {
			success = false;
			exception = e;
			return null;
		}
	}

	@Override
	protected void onPostExecute(T result) {
		super.onPostExecute(result);

		// if dialog is present, dismiss it
		dismissDialog();

		if (success)
			onSuccess(result);
		else {
			Log.w("mm", exception.getClass().toString());
			//exception.printStackTrace();
			//Log.w("mm", "the value of exception: " + emf.getMessage(exception));
			onFailure(emf.getMessage(exception));
		}
	}

	/**
	 * Work to be done on separate thread. Any thrown exceptions will be caught
	 * and handled by <code>onFailure</code>.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	abstract public T doit() throws RemoteServiceFailureException;

	/**
	 * Callback for successful execution of threaded process.
	 * 
	 * @param t
	 */
	abstract public void onSuccess(T t);

	/**
	 * Callback for exceptions that are thrown during <code>doit</code>.
	 * 
	 * @param errResId
	 */
	abstract public void onFailure(int errResId);
}