package com.boosed.mm.activity.task;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.boosed.mm.shared.exception.InvalidLocationException;

/**
 * Helper task for geocoding a provided <code>Location</code> within a specified
 * period of time.
 * 
 * @author dsumera
 */
public class GeoTask extends Thread {

	/** the context of the activity for which work is being performed */
	private final Context context;

	private final CountDownLatch latch;

	private final Location location;

	private Address address;

	public GeoTask(Context context, Location location) {
		this.context = context;
		this.location = location;
		latch = new CountDownLatch(1);
	}

	/**
	 * Invoking thread will be blocked until work is complete or
	 * <code>time</code> has elapsed.
	 * 
	 * @param time
	 * @throws InvalidLocationException
	 */
	public void await(int time) throws InvalidLocationException {
		try {
			// start the geocoding on another thread
			start();

			// causes calling thread to wait/block until specified time
			latch.await(time, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			throw new InvalidLocationException();
		}
	}

	@Override
	public void run() {
		try {
			// limit to a single result to limit latency
			List<Address> addresses = new Geocoder(context).getFromLocation(location.getLatitude(),
					location.getLongitude(), 1);

			// retain the first result
			if (addresses != null && !addresses.isEmpty())
				address = addresses.get(0);
		} catch (IOException e) {
			e.printStackTrace();
			// ignored
		} finally {
			latch.countDown();
		}

		// try {
		// List<Address> addresses;
		// while (addresses == null || addresses.isEmpty()) {
		// addresses = geo.getFromLocation(location.getLatitude(),
		// location.getLongitude(), 1);
		//
		// // wait until we can get an address
		// Thread.sleep(2000);
		// }
		//
		// return addresses.get(0);
		// } catch (Exception e) {
		// throw new InvalidLocationException();
		// }
	}

	public Address getAddress() throws InvalidLocationException {
		if (address == null)
			throw new InvalidLocationException();

		return address;
	}
	
	public Location getLocation() {
		return location;
	}
}