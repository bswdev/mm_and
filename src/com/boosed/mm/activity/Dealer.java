package com.boosed.mm.activity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

import com.boosed.mm.R;
import com.boosed.mm.activity.task.GeoTask;
import com.boosed.mm.activity.task.LocationTask;
import com.boosed.mm.activity.task.RpcTask;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseArrayAdapter;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ListItemDealer;

/**
 * Dealer interface, extends from <code>AbstractListActivity</code>.
 * 
 * @author dsumera
 */
public class Dealer extends AbstractListActivity implements Controllable {

	/** criteria for using location service */
	private final Criteria locationCritera;

	/** results of search */
	private final List<Account> results;

	/** result offset */
	private int offset = 0;

	/** car adapter */
	private ArrayAdapter<Account> adapter;

	/**
	 * Default no-arg constructor
	 */
	public Dealer() {
		// set the empty string for the list
		super(R.string.warn_dealer, R.layout.title_dealer);

		// init collections
		results = new ArrayList<Account>();

		// for geocoding
		locationCritera = new Criteria();
		locationCritera.setAccuracy(Criteria.ACCURACY_FINE);
		locationCritera.setAltitudeRequired(false);
		locationCritera.setBearingRequired(false);
		locationCritera.setCostAllowed(true);
		locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set activity title
		// setTitle(R.string.app_search);

		// add the footer before setting the adapter
		mList.addFooterView(footer);

		// set adapter
		setListAdapter(adapter = new BaseArrayAdapter<Account, ListItemDealer>(this, results,
				R.layout.list_item_dealer) {

			@Override
			protected ListItemDealer createView(View view) {
				// create view holder
				return new ListItemDealer(view);
			}

			@Override
			protected void render(Account item, final ListItemDealer row) {
				// // get image
				// final ImageView image = row.getImage();
				//
				// // set image
				// dm.loadImage(item.image + "=s160", image, new ImageCallback()
				// {
				// @Override
				// public void onLoad(int height, int width) {
				// image.refreshDrawableState();
				// image.setVisibility(View.VISIBLE);
				// }
				// });

				// set text
				row.getTitle().setText(item.name);

				// set year & colors
				StringBuilder sb = new StringBuilder();

				// address
				if (item.address != null)
					sb.append(item.address);

				// postal code
				if (item.postal != null) {
					if (sb.length() != 0)
						sb.append(", ");
					sb.append(item.postal);
				}

				// distance
				if (settings.getString(RpcService.PREF_POST_DLR, null) != null) {
					// only show miles if postal code was provided
					if (sb.length() != 0)
						sb.append(" ");
					sb.append("(").append(item.distance).append(" miles)");
				}

				if (sb.length() == 0)
					row.getAddress().setVisibility(View.GONE);
				else {
					TextView address = row.getAddress();
					address.setVisibility(View.VISIBLE);
					address.setText(sb.toString());
				}
			}
		});

		// don't need footer so remove it now
		mList.removeFooterView(footer);

		// register list for context menu
		registerForContextMenu(mList);

		// set divider
		mList.setDivider(getResources().getDrawable(android.R.drawable.divider_horizontal_bright));
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// show inventory when item is clicked
		showInventory(results.get(position).id);
	}

	/** suppress a location lookup when text changes in postal edit text */
	private boolean suppressLocationLookup = false;

	@Override
	protected Dialog onCreateDialog(final int id) {

		// for all cases
		Builder builder = new Builder(this);

		switch (id) {
		case R.id.geocode:
			// location/geocode warning
			builder.setTitle(R.string.dlg_title_geocode);
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage(getString(R.string.warn_rev_geo));

			// set the click listener
			OnClickListener ocl = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// set the location
						setLocation();
					case Dialog.BUTTON_NEGATIVE:
						// do nothing
					default:
						break;
					}
				}
			};
			builder.setPositiveButton(R.string.dlg_btn_ok, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.location:
			// postal dialog
			// set the view
			builder.setTitle(R.string.dlg_title_location);
			View view = getLayoutInflater().inflate(R.layout.dlg_crit_geo,
					(ViewGroup) findViewById(R.id.dlg_crit_geo));
			builder.setView(view);

			// the distance
			distance = (Spinner) view.findViewById(R.id.distance);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,
					getResources().getStringArray(R.array.distances));
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			distance.setAdapter(adapter);

			// the name of the city
			name = (TextView) view.findViewById(R.id.name);

			// the geocode button
			geocode = (ImageButton) view.findViewById(R.id.geocode);

			// listen to changes on textview
			postal = (EditText) view.findViewById(R.id.postal);

			postal.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(final CharSequence s, int start, int before, int count) {
					final String zip = s.toString();

					// length is 5 and not using same value
					if (zip.length() == 5) {
						// performed population of postal textview

						// suppress lookup for this instance
						if (suppressLocationLookup)
							suppressLocationLookup = false;
						else
							// perform a lookup
							new MobTask<com.boosed.mm.shared.db.Location>() {
								@Override
								protected void onPreExecute() {
									super.onPreExecute();
									name.setText(R.string.dlg_resolve);
								}

								@Override
								public com.boosed.mm.shared.db.Location doit()
										throws RemoteServiceFailureException {
									return rpc.service.loadLocation(zip);
								}

								public void onFailure(int errResId) {
									// mark unsuccessful
									postal.setTag(false);

									// set error on the name field
									name.setText(errResId);
								}

								@Override
								public void onSuccess(com.boosed.mm.shared.db.Location t) {
									// mark successful
									postal.setTag(true);

									// set name to location
									name.setText(t.toString());
								}
							}.execute();
					} else {
						// length is not 5

						// mark unsuccessful
						postal.setTag(false);

						name.setText(R.string.dlg_anywhere);
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not implemented
				}

				@Override
				public void afterTextChanged(final Editable s) {
					// not implemented
				}
			});

			ocl = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case Dialog.BUTTON_POSITIVE:
						// set the postal field only if valid & user confirms it
						if ((Boolean) postal.getTag()) {
							// successful lookup, save postal parameters to
							// preferences
							settings.edit().putString(RpcService.PREF_POST_DLR,
									postal.getText().toString()).commit();
							settings.edit().putString(RpcService.PREF_NAME_DLR,
									name.getText().toString()).commit();
							settings.edit().putInt(RpcService.PREF_DIST_DLR,
									distance.getSelectedItemPosition()).commit();

							// perform search
							search();

							// break out of switch
							break;
						} // else
						// positive falls through if lookup unsuccessful
					case Dialog.BUTTON_NEUTRAL:
						// clear postal parameters
						settings.edit().putString(RpcService.PREF_POST_DLR, null).commit();
						settings.edit().putString(RpcService.PREF_NAME_DLR, null).commit();
						settings.edit().putInt(RpcService.PREF_DIST_DLR, 0).commit();

						// perform search
						search();
						break;
					case Dialog.BUTTON_NEGATIVE:
					default:
						// close dialog
						break;
					}
				}
			};

			builder.setPositiveButton(R.string.dlg_btn_set, ocl);
			builder.setNeutralButton(R.string.dlg_btn_clear, ocl);
			builder.setNegativeButton(R.string.dlg_btn_cancel, ocl);

			return builder.create();
		case R.id.sort:
			// title
			builder.setTitle(R.string.dlg_title_sort);

			ocl = new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// if (which == Dialog.BUTTON_NEGATIVE)
					// return;

					// set sort
					settings.edit().putInt(RpcService.PREF_SORT_DLR, sorts[which]).commit();

					// load results
					search();

					// dismiss
					dialog.dismiss();
				}
			};

			// attempt to find selected sort in settings
			int selected = settings.getInt(RpcService.PREF_SORT_DLR, SortType.NAME.ordinal());
			for (int i = sorts.length; --i > -1;)
				if (sorts[i] == selected) {
					// sort at index i matches selected value, set the index
					selected = i;
					break;
				}

			// add negative button to cancel out dialog
			return builder.setSingleChoiceItems(R.array.sorts_dlr, selected, ocl).create();
		default:
			return super.onCreateDialog(id);
		}
	}

	/** choice items mapped to sort type indexes */
	private static final int[] sorts = new int[] { SortType.NAME.ordinal(),
			SortType.NAME_DESC.ordinal() };

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);

		switch (id) {
		case R.id.sort:
			// set correct sort mode

			// attempt to find selected sort in settings
			int selected = settings.getInt(RpcService.PREF_SORT_DLR, SortType.NAME.ordinal());
			for (int i = sorts.length; --i > -1;)
				if (sorts[i] == selected) {
					// sort at index i matches selected value, set the index
					selected = i;
					break;
				}

			((AlertDialog) dialog).getListView().setItemChecked(selected, true);
			break;
		case R.id.location:
			// postal dialog

			// code
			String value = settings.getString(RpcService.PREF_POST_DLR, null);
			if (value == null) {
				// no location
				postal.setText(value);
				postal.setTag(false);
			} else {
				// suppress lookup since setting text triggers the textwatcher
				suppressLocationLookup = true;
				postal.setText(value);
				postal.setTag(true);
				postal.setSelection(value.length());
			}

			// name
			value = settings.getString(RpcService.PREF_NAME_DLR, null);
			if (value == null)
				name.setText(R.string.dlg_anywhere);
			else
				name.setText(value);

			// set distance
			distance.setSelection(settings.getInt(RpcService.PREF_DIST_DLR, 0));

			// move to front?
			// dialog.getWindow().makeActive();
			break;
		default:
			break;
		}
	}
	
	/** textfields */
	private TextView name;
	private EditText postal;
	private Spinner distance;
	private ImageButton geocode;

	/** footer */
	private View footer;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// create the footer
		footer = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.list_footer, null);
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		final int id = item.getItemId();
		switch (id) {
		case R.id.ctx_inventory:
			// start activty for viewing inventory
			Account dealer = adapter
					.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position);

			showInventory(dealer.id);
			return true;
		case R.id.ctx_directions:
			// start activity to view map
			dealer = adapter.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position);
			try {
				String location = URLEncoder.encode(dealer.name + ", " + dealer.address + ", " + dealer.postal /*+ "&gl=us"*/, "UTF-8");
				String uri = "http://maps.google.com/maps?q=" + location; ///*dealer.latitude + ","
						//+ dealer.longitude*/ + "+(" + dealer.name + ")";
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			// String uri = "geo:0,0?q=" + car.latitude + "," +
			// car.longitude + " (location)";
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		// retrieve selected item
		Account dealer = adapter.getItem(((AdapterContextMenuInfo) menuInfo).position);

		// set menu title
		menu.setHeaderTitle(dealer.name);

		// initialize menu
		getMenuInflater().inflate(R.menu.ctx_menu_dealer, menu);

		// // only show directions if dealer has lat/lon specified
		// menu.findItem(R.id.ctx_directions).setVisible(dealer.latitude != 0);
	}

	@Override
	public void onReplay(View replay) {
		// replay a control
		onControl(replay);
	}

	@Override
	public void onBind() {
		// sync the results
		sync(true);
	}

	@Override
	public void onControl(View view) {
		// show associated dialog
		showDialog(view.getId());
	}

	/** track whether the scrollview is in a fetching state */
	private boolean fetching = false;

	/** footer status */
	private boolean hasFooter = false;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
			int totalItemCount) {
		if (rpc != null && offset != 0 & !fetching && totalItemCount != 0
				&& (firstVisibleItem + visibleItemCount) == totalItemCount)

			// scroll results
			new MobTask<Integer>() {
				@Override
				protected void onPreExecute() {
					super.onPreExecute();

					// set fetching on
					fetching = true;

					// show footer
					if (!hasFooter) {
						mList.addFooterView(footer);
						hasFooter = true;
					}
				}

				@Override
				public Integer doit() throws RemoteServiceFailureException {
					return rpc.loadDealers(true);
				}

				@Override
				public void onFailure(int errResId) {
					// sync content
					sync(false);

					// continue failure
					super.onFailure(errResId);
				}

				@Override
				public void onSuccess(Integer t) {
					// set offset
					offset = t;

					// remove footer if at end of content
					if (offset == 0) {
						mList.removeFooterView(footer);
						hasFooter = false;
					}

					// sync content
					sync(true);
				}

				@Override
				protected void onPostExecute(Integer result) {
					// set fetching variables and end status
					fetching = false;

					// proceed with failure/success
					super.onPostExecute(result);
				}
			}.execute();
	}

	/**
	 * Set the location using services.
	 */
	private void setLocation() {

		final String provider = lm.getBestProvider(locationCritera, true);

		// there is a provider & it is enabled
		if (provider != null && lm.isProviderEnabled(provider)) {

			new MobTask<Address>() {

				private LocationTask lt;

				private GeoTask gt;

				protected void onPreExecute() {
					super.onPreExecute();

					// set geocode button to disabled
					geocode.setEnabled(false);

					// set current location status to false in case user oks
					// dialog while task executes
					postal.setTag(false);

					name.setText(provider + getString(R.string.dlg_coord));
				}

				@Override
				public Address doit() throws RemoteServiceFailureException {
					lt = new LocationTask();
					lm.requestLocationUpdates(provider, 60000, 1, lt, Dealer.this.getMainLooper());

					// wait 15 seconds for coordinates to be found
					lt.await(15);

					gt = new GeoTask(Dealer.this, lt.getLocation());
					handler.post(new Runnable() {
						public void run() {
							// set geocoding message
							name.setText(R.string.dlg_geocode);
						}
					});

					// wait 15 seconds for address to be found
					gt.await(15);
					return gt.getAddress();
				}

				public void onFailure(int errResId) {
					// remove zip
					postal.setText("");

					// mark unsuccessful
					postal.setTag(false);

					// set name to error
					name.setText(errResId);
				}

				@Override
				public void onSuccess(final Address t) {
					// mark successful
					postal.setTag(true);

					// suppress lookup when populating the postal field
					suppressLocationLookup = true;
					postal.setText(t.getPostalCode());
					postal.setSelection(5);
					name.setText(t.getLocality() + ", " + t.getAdminArea());

					// suggest this address to database if it's not present
					new RpcTask<Void>() {
						@Override
						public Void doit() throws RemoteServiceFailureException {
							// submit these parameters to the server
							Location location = gt.getLocation();
							rpc.service.suggest(t.getPostalCode(), t.getLocality(), t
									.getAdminArea(), location.getLatitude(), location
									.getLongitude());
							return null;
						}

						@Override
						public void onSuccess(Void t) {
							// ignored
						}

						@Override
						public void onFailure(int errResId) {
							// ignored
						}
					}.execute();
				}

				protected void onPostExecute(Address result) {
					// remove the listener for location updates
					lm.removeUpdates(lt);

					// enable the button
					geocode.setEnabled(true);

					// continue with task
					super.onPostExecute(result);
				}
			}.execute();
		} else {
			// provider not enabled, prompt user to enable it
			Toast.makeText(this, R.string.warn_location, Toast.LENGTH_LONG).show();
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
	}

	/**
	 * Perform search.
	 */
	private void search() {
		new MobTask<Integer>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				// loading
				setEmptyString(R.string.warn_load, true);

				// clear results
				results.clear();
				offset = 0;

				// update view
				adapter.notifyDataSetChanged();
			}

			@Override
			public Integer doit() throws RemoteServiceFailureException {
				return rpc.loadDealers(false);
			}

			@Override
			public void onFailure(int errResId) {
				// set to init'd
				initialized = true;

				// sync content
				sync(false);

				// continue failure
				super.onFailure(errResId);
			}

			public void onSuccess(Integer t) {
				// set to init'd
				initialized = true;

				// set the offset
				offset = t;

				// sync content
				sync(true);
			}
		}.execute();
	}

	/**
	 * Show the vehicle at the given position.
	 * 
	 * @param position
	 */
	private void showInventory(String dealerId) {
		// commit preference
		settings.edit().putInt(RpcService.PREF_INVENTORY, Inventory.MODE_DLR).commit();
		// settings.edit().putString(RpcService.PREF_DEALER,
		// car.owner.getName()).commit();

		// create intent for inventory
		Intent intent = new Intent(this, Inventory.class);

		// set the dealer as the car owner
		intent.putExtra(Inventory.DEALER, dealerId);
		
		// start activity
		startActivity(intent);
	}

	/**
	 * Sync the rpc data with this activity.
	 */
	private void sync(boolean success) {
		if (success)
			// succeeded, sync results
			offset = rpc.syncDealers(results);

		// update the empty message
		if (results.isEmpty())
			setEmptyString(R.string.warn_dealer, false);

		// update view
		adapter.notifyDataSetChanged();
	}
}