package com.boosed.mm.activity;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.boosed.mm.R;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.util.DataUtil;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;

/**
 * Manage the user <code>Account</code>.
 * 
 * @author dsumera
 */
public class Manage extends AbstractActivity implements Controllable {

	/** intent key for account reference */
	public static final String ACCOUNT = "com.boosed.mm.manage.Account";

	public Manage() {
		super(R.layout.title_save);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set content
		setContentView(R.layout.act_manage);

		// set title
		setTitle(R.string.app_settings);

		// set the title icon
		setTitleIcon(R.drawable.ic_menu_gear_w);

		// set the account
		if (!initialized) {
			Account account = (Account) getIntent().getSerializableExtra(ACCOUNT);

			// initialize all the widgets
			name.setText(account.name);
			email.setText(account.email);
			phone.setText(account.phone);
			address.setText(account.address);
			postal.setText(account.postal);
			url.setText(account.url);

			// radio groups
			((RadioButton) dealer.findViewById(account.dealer ? R.id.dealer_yes : R.id.dealer_no)).setChecked(true);
			((RadioButton) show.findViewById(account.showEmail ? R.id.show_yes : R.id.show_no)).setChecked(true);

			// notifications
			notifyAlerts.setChecked(account.getNotify(MessageType.ALERTS));
			notifyMessages.setChecked(account.getNotify(MessageType.INQUIRIES));
			// ((RadioButton) notify.findViewById(account.notify ?
			// R.id.notify_yes : R.id.notify_no)).setChecked(true);
		}
		// else this will be set automatically from widgets freezing state
	}

	private TextView name, email, phone, address, postal, url;
	private RadioGroup dealer, show;
	/** represents both inquiries and responses */
	private CheckBox notifyAlerts, notifyMessages;

	@Override
	public void assignHandles() {
		super.assignHandles();

		name = (TextView) findViewById(R.id.name);
		email = (TextView) findViewById(R.id.email);
		phone = (TextView) findViewById(R.id.phone);
		address = (TextView) findViewById(R.id.address);
		postal = find(TextView.class, R.id.postal);
		url = (TextView) findViewById(R.id.url);

		dealer = (RadioGroup) findViewById(R.id.dealer);
		show = find(RadioGroup.class, R.id.show);
		// notify = find(RadioGroup.class, R.id.notify);

		// notifications
		notifyAlerts = find(CheckBox.class, R.id.alerts);
		notifyMessages = find(CheckBox.class, R.id.messages);
	}

	@Override
	public void onReplay(View replay) {
		// replay control
		onControl(replay);
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onBind() {
		// not implemented
	}

	@Override
	public void onControl(View view) {
		switch (view.getId()) {
		case (R.id.save):
			if (confirm(view, R.string.cfm_save) && validate())
				// save the changes
				new MobTask<Void>(R.string.stat_save, true, view) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						Map<FieldAccount, Serializable> fields = new HashMap<FieldAccount, Serializable>();

						// text fields
						fields.put(FieldAccount.NAME, name.getText().toString());
						fields.put(FieldAccount.EMAIL, email.getText().toString());
						fields.put(FieldAccount.PHONE, phone.getText().toString());
						fields.put(FieldAccount.ADDRESS, address.getText().toString());
						fields.put(FieldAccount.LOCATION, postal.getText().toString());
						fields.put(FieldAccount.URL, url.getText().toString());

						// options
						fields.put(FieldAccount.DEALER, dealer.getCheckedRadioButtonId() == R.id.dealer_yes);
						fields.put(FieldAccount.EML_SHOW, show.getCheckedRadioButtonId() == R.id.show_yes);
						// fields.put(FieldAccount.NOTIFY,
						// notify.getCheckedRadioButtonId() ==
						// android.R.id.button1);

						// handle lat/lon for supporting dealer search
						if ((Boolean) fields.get(FieldAccount.DEALER)) {
							try {
								List<Address> addresses = new Geocoder(Manage.this).getFromLocationName(
										fields.get(FieldAccount.ADDRESS) + " " + fields.get(FieldAccount.LOCATION), 1);
								if (addresses != null && !addresses.isEmpty()) {
									// set lat/lon from the first address
									Address address = addresses.get(0);
									//29.578978  Schertz, TX  -98.27784  
									fields.put(FieldAccount.LATITUDE, address.getLatitude());
									fields.put(FieldAccount.LONGITUDE, address.getLongitude());
								}
							} catch (IOException e) {
								// ignore, cannot lookup address
								e.printStackTrace();
							}
						}

						// notifications
						fields.put(FieldAccount.NOTIFY_ALT, notifyAlerts.isChecked());

						boolean notifyMessage = notifyMessages.isChecked();
						fields.put(FieldAccount.NOTIFY_INQ, notifyMessage);
						fields.put(FieldAccount.NOTIFY_RES, notifyMessage);

						// update account
						rpc.service.updateAccount(fields);
						return null;
					}

					public void onFailure(int errResId) {
						switch (errResId) {
						case R.string.exception_incomplete:

							break;
						case R.string.exception_invalid_location:
							postal.setError("unknown location, please set another");
							break;
						// TODO: handle all other errors before session timeout,
						// etc.
						default:
							// continue with failure
							super.onFailure(errResId);
						}
					}

					@Override
					public void onSuccess(Void t) {
						// close activity
						finish();
					}
				}.execute();
			break;
		case R.id.cancel:
			if (!confirm(view, R.string.cfm_exit)) {
				// not confirmed
				break;
			}
			// else finish
		default:
			// user canceled this operation
			finish();
			break;
		}
	}

	private boolean validate() {
		boolean valid = true;

		if (name.getText().toString().trim().length() == 0) {
			name.setError("name cannot be blank");
			valid = false;
		}

		if (!DataUtil.emailCheck(email.getText().toString().trim())) {
			email.setError("email format is incorrect");
			valid = false;
		}

		// if (email.getText().toString().)
		return valid;
	}
}