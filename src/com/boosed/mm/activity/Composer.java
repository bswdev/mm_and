package com.boosed.mm.activity;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map.Entry;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boosed.mm.R;
import com.boosed.mm.rpc.RpcService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.ui.BaseHandler;
import com.boosed.mm.ui.Controllable;
import com.boosed.mm.ui.ImageCallback;
import com.boosed.mm.ui.ListItemPhoto;

public class Composer extends AbstractActivity implements Controllable {

	/** the reference vehicle for the message */
	private Car reference = null;

	/** the original message */
	private Message message = null;

	/** the price format */
	private static final DecimalFormat df = new DecimalFormat("###,###,##0");

	/** the date format */
	private static final String FMT_DATE = " @ M/d/yy, h:mm aa";

	/** intent key for original message when replying */
	public static final String MESSAGE = "com.boosed.mm.composer.Message";

	/** intent key for reference (i.e., car) */
	public static final String REFERENCE = "com.boosed.mm.composer.Reference";

	/** whether message is an inquiry */
	private boolean isInquiry = false;

	public Composer() {
		super(R.layout.title_composer);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the layout (also assigns the handles)
		setContentView(R.layout.act_composer);

		// retrieve the intent
		Intent intent = getIntent();

		// set original message id if present
		setMessage((Message) intent.getSerializableExtra(MESSAGE));

		// set the car from the intent
		setReference((Car) intent.getSerializableExtra(REFERENCE));

		// set the title
		setTitle(isInquiry ? R.string.app_inquiry : R.string.app_reply);
	}

	private TextView original, subject;
	private EditText content;
	private ListItemPhoto header;

	@Override
	public void assignHandles() {
		super.assignHandles();

		// fields
		subject = find(TextView.class, R.id.subject);
		original = find(TextView.class, R.id.original);
		content = find(EditText.class, R.id.content);

		// header
		header = new ListItemPhoto(find(View.class, R.id.header));
		header.getCheck().setVisibility(View.GONE);
	}

	@Override
	public void onReplay(View replay) {
		// replay control
		onControl(replay);
	}

	@Override
	public Handler createHandler() {
		return new BaseHandler(this);
	}

	@Override
	public void onBind() {
		// what happens when we bind to service? nothing?
	}

	@Override
	public void onControl(View view) {
		switch (view.getId()) {
		case R.id.discard:
			if (confirm(view, R.string.cfm_dis_msg))
				finish();
			// discard message/end activity (ran in modal to show alert)
			// new MobTask<Void>("discarding", true) {
			// @Override
			// public Void doit() throws RemoteServiceFailureException {
			// return null;
			// }
			//
			// @Override
			// public void onSuccess(Void t) {
			// finish();
			// }
			// }.execute();
			break;
		case R.id.send:
			final String text = content.getText().toString().trim();
			if (text.length() == 0) {
				toast("no message entered");
				break;
			}
			
			if (confirm(view, isInquiry ? R.string.cfm_inqr : R.string.cfm_resp))
				// send inquiry
				new MobTask<Void>(R.string.stat_send, true, view) {
					@Override
					public Void doit() throws RemoteServiceFailureException {
						if (isInquiry)
							// no original message, this is an inquiry
							rpc.service.inquire(reference.id, text);
						else
							// message is being sent in response to original
							rpc.service.reply(message.id, text);
						return null;
					}

					public void onFailure(int errResId) {
						switch (errResId) {
						case R.string.exception_owner:
							// close this composer since this is the owner
							Toast.makeText(Composer.this, errResId, Toast.LENGTH_SHORT).show();
							finish();
							break;
						default:
							super.onFailure(errResId);
						}
					}

					@Override
					public void onSuccess(Void t) {
						// sending complete, close this activity
						finish();
					}
				}.execute();
			break;
		case R.id.header:
			// don't set index and mode to open activity in stand-alone config
			Intent intent = new Intent(this, Classified.class);
			intent.putExtra(Classified.CAR, reference);
			intent.putExtra(Classified.MODE, Classified.MODE_STANDALONE);
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	/**
	 * Create the thread of messages from the <code>Message</code> item.
	 * 
	 * @param message
	 * @return
	 */
	private Spanned getMessage(Message message) {
		SpannableStringBuilder ssb = new SpannableStringBuilder();

		for (Entry<Long, String> entry : message.messages.entrySet()) {
			// get content
			String content = entry.getValue();

			// set header according to content
			String header = (content.charAt(0) == Message.PRE_RCP ? message.recipientName
					: message.senderName)
					+ DateFormat.format(FMT_DATE, new Date(entry.getKey())) + ":\n";

			// set bold span on the header
			int start = ssb.length();
			ssb.append(header);
			ssb.setSpan(new StyleSpan(Typeface.BOLD), start, start + header.length(), 0);

			// add the header + content to builder
			ssb.append(content.substring(1)).append("\n\n");
		}

		return ssb;
	}

	/**
	 * Set the original <code>Message</code> if this is a response.
	 * 
	 * @param message
	 */
	private void setMessage(Message message) {
		this.message = message;

		if (message == null) {
			// inquiry
			isInquiry = true;

			// set subject header
			subject.setText(R.string.label_inquiry);

			// set hint
			content.setHint(R.string.hint_inquiry);

			// set visibility
			original.setVisibility(View.GONE);
		} else {
			// response
			isInquiry = false;

			// set subject header
			subject.setText(R.string.label_response);

			// set hint
			content.setHint(R.string.hint_response);

			try {
				// show the message with markup
				// String intro = "previous message:\n\n";
				// String intro = message.senderName
				// + (message.getType() == MessageType.RESPONSES ? " responded "
				// : " inquired ") + "("
				// + DateFormat.format(FMT_DATE, new Date(message.time)) + "):";
				// SpannableString ss = new SpannableString(intro + "\n" +
				// message.message);
				// message.senderName + " wrote:\n" + message.message)
				// bold the header
				// ss.setSpan(new StyleSpan(Typeface.BOLD), 0, intro.length(),
				// 0);
				// italicize the name
				// int len = date.length();
				// ss.setSpan(new StyleSpan(Typeface.ITALIC), len, len +
				// message.senderName.length(), 0);
				original.setText(/* ss */getMessage(message));
				// original.setText(DateFormat.format(FMT_DATE, new
				// Date(message.time)) + ", " + message.senderName
				// + " wrote:" + "\n" + message.message);
				original.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// private String getMessage(Message message) {
	// String rv = "";
	//
	// for (Entry<Long, String> entry : message.messages.entrySet()) {
	// String value = entry.getValue();
	// rv = (value.charAt(0) == Message.PRE_RCP ? message.recipientName :
	// message.senderName)
	// + DateFormat.format(FMT_DATE, new Date(entry.getKey())) + ":\n" +
	// value.substring(1)
	// + "\n\n" + rv;
	// }
	//
	// return rv;
	// }

	/**
	 * Set the instance of <code>Car</code> which the <code>Message</code> will
	 * reference.
	 * 
	 * @param reference
	 */
	private void setReference(Car reference) {
		this.reference = reference;

		// set image
		final ImageView image = header.getImage();
		
		dm.display(image, reference.image + "=s160", new ImageCallback() {
			@Override
			public void onLoad(int height, int width) {
				//image.refreshDrawableState();
				image.setVisibility(View.VISIBLE);
			}
		});
//		dm.loadImage(reference.image + "=s160", image, new ImageCallback() {
//			@Override
//			public void onLoad(int height, int width) {
//				image.refreshDrawableState();
//				image.setVisibility(View.VISIBLE);
//			}
//		});

		// set first row content
		// header.getContent1().setText(
		// reference.year + ", " + reference.exterior.getName() + "/" +
		// reference.interior.getName());

		if (isInquiry) {
			// set items as inquiry
			// set title
			header.getTitle().setText(/*reference.model.getName()*/reference.toString());

			// set first row content
			header.getContent1().setText(
					reference.year + ", " + reference.exterior.getName() + "/"
							+ reference.interior.getName());

			// set second row content
			header.getContent2().setText(
					df.format(reference.mileage) + " miles, $" + df.format(reference.price));

			// only show miles if postal code was provided
			String miles = settings.getString(RpcService.PREF_POST_SRCH, null) == null ? "" : (" ("
					+ reference.distance + " miles)");
			header.getEnd().setText(reference.location + miles);
		} else {
			// set items as response
			// set title
			String title = message.getReference();
			
			// truncate off up until colon
			title = title.substring(0, title.indexOf(":") + 2);
			
			// since car may have changed since last time, use latest reference string
			header.getTitle().setText(title + /*reference.model.getName()*/reference.toString());

			// set first row content
			header.getContent1().setText(
					reference.year + ", " + reference.exterior.getName() + "/"
							+ reference.interior.getName());

			// set second row content
			header.getContent2().setText(
					df.format(reference.mileage) + " miles, $" + df.format(reference.price)/*
																							 * message
																							 * .
																							 * identifier
																							 */);

			// only show miles if postal code was provided
			// header.getEnd().setText(
			// message.senderName + DateFormat.format(FMT_DATE, new
			// Date(message.time)));
			header.getEnd().setText(message.identifier);
		}
	}
}