package com.boosed.mm.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.boosed.mm.R;

public abstract class CheckListView<T> extends ListView implements OnItemClickListener {

	public ArrayAdapter<T> adapter;

	private List<T> items = new ArrayList<T>();

	private int limit;

	public CheckListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CheckListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CheckListView(Context context, List<T> items, int limit) {
		super(context);
		this.limit = limit;

		adapter = new BaseArrayAdapter<T, RowWrapper>(context, items, R.layout.list_item_checked) {
			@Override
			protected RowWrapper createView(View view) {
				return new RowWrapper(view);
			}

			protected void render(T item, RowWrapper row) {
				row.getTitle().setText(getName(item));
			}
		};
		setAdapter(adapter);
		setChoiceMode(CHOICE_MODE_MULTIPLE);
		setOnItemClickListener(this);
	}

	public void setItems(List<T> items) {
		// get matches
		List<Integer> checked = new ArrayList<Integer>();

		// scan through original items
		for (int pos = this.items.size(); --pos > -1;) {
			if (isItemChecked(pos)) {
				int index = items.indexOf(this.items.get(pos));
				if (index != -1)
					checked.add(index);
			}
		}

		// clear items
		this.items.clear();
		this.items.addAll(items);
		adapter.notifyDataSetChanged();

		// set checked items
		for (int pos : checked)
			setItemChecked(pos, true);
	}

	private int total = 0;

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (isItemChecked(position))
			total++;
		else
			total--;

		// uncheck last option if limit exceeded
		if (total > limit) {
			setItemChecked(position, false);
			total = limit;
		}
	}

	/**
	 * Store <code>ListView</code> views in tag for convenience.
	 */
	private class RowWrapper {
		View base;
		TextView title;

		public RowWrapper(View base) {
			this.base = base;
			// Button b = (Button) base.findViewById(R.id.button);
			// b.getBackground().setColorFilter(0xFFFF0000,
			// PorterDuff.Mode.MULTIPLY);
		}

		public TextView getTitle() {
			if (title == null)
				title = (TextView) base.findViewById(android.R.id.text1);

			return title;
		}
	}

	public abstract String getName(T item);
}