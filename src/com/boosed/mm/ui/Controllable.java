package com.boosed.mm.ui;

import android.view.View;

/**
 * Interface for <code>View</code>s which have clickable widgets.
 * 
 * @author dsumera
 */
public interface Controllable {

	/**
	 * Method which intercepts button clicks on a control panel.
	 * 
	 * @param view
	 */
	public void onControl(View view);
}
