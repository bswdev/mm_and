package com.boosed.mm.ui;

import android.view.View;
import android.widget.TextView;

import com.boosed.mm.R;

/**
 * Storing <code>ListView</code> rows in tags for convenience.
 * 
 * @author dsumera
 */
public class ListItemDealer {

	private View base;
	private TextView title, address;

	public ListItemDealer(View base) {
		this.base = base;
	}

	public void setClickable(boolean clickable) {
		base.setClickable(clickable);
	}
	
	public TextView getTitle() {
		if (title == null)
			title = (TextView) base.findViewById(android.R.id.text1);

		return title;
	}

	public TextView getAddress() {
		if (address == null)
			address = (TextView) base.findViewById(R.id.address);

		return address;
	}
}