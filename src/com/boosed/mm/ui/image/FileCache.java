package com.boosed.mm.ui.image;

import java.io.File;
import java.net.URLEncoder;

import android.content.Context;
import android.os.Environment;

public class FileCache {

	/**
	 * The root directory for the cache.
	 */
	private File cache;

	/** images directory */
	public static final String DIRECTORY = "com.boosed/mm";
	
	public FileCache(Context context) {
		// Find the dir to save cached images
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			cache = new File(Environment.getExternalStorageDirectory(), DIRECTORY);
		else
			cache = context.getCacheDir();

		// create the cache
		if (!cache.exists())
			cache.mkdirs();
	}

	public File getFile(String url) {
		// I identify images by hashcode. Not a perfect solution, good for the
		// demo.
		// String filename = String.valueOf(url.hashCode());

		// Another possible solution (thanks to grantland)
		// String filename = URLEncoder.encode(url);
		return new File(cache, URLEncoder.encode(url));
		// return f;

	}

	/**
	 * Clear out all the files.
	 */
	public void clear() {
		try {
			for (File file : cache.listFiles())
				file.delete();
		} catch (Exception e) {
			// user may have forcibly deleted this directory
		}
	}
}