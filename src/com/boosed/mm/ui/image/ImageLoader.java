package com.boosed.mm.ui.image;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;

import com.boosed.mm.activity.AbstractActivity;
import com.boosed.mm.ui.ImageCallback;

public class ImageLoader {

	private static final int MEM_SIZE = 500;

	private static final int POOL_SIZE = 2;
	
	private MemoryCache memoryCache;

	private FileCache fileCache;

	/** views to loading url, the views will be recycled independent of this map */
	private Map<ImageView, String> images;

	private ExecutorService executorService;

	private static ImageLoader loader;

	private String blank;

	/** image to show when loading */
	// private static final int stub = R.drawable.ic_menu_database;

	/**
	 * Private constructor for singleton.
	 * 
	 * @param context
	 */
	private ImageLoader(Context context) {
		memoryCache = new MemoryCache(MEM_SIZE);
		fileCache = new FileCache(context);
		executorService = Executors.newFixedThreadPool(POOL_SIZE);
		images = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
	}

	public static ImageLoader getInstance(Context context) {
		if (loader == null)
			loader = new ImageLoader(context);

		return loader;
	}

	public void display(ImageView image, String url, ImageCallback callback) {

		// store view
		images.put(image, url);

		// look for value in memory
		Bitmap bitmap = memoryCache.get(url);

		if (bitmap != null) {
			// set from memory
			image.setImageBitmap(bitmap);
			callback.onLoad(0, 0);

			// set image visible
			// image.setVisibility(View.VISIBLE);
		} else {
			// hide image
			// image.setVisibility(View.INVISIBLE);

			// queue photo to load
			// queuePhoto(image, url);
			// view.setImageResource(stub);
			executorService.submit(new Loader(image, url, callback));
		}
	}

	/**
	 * The default image to load.
	 * 
	 * @param url
	 */
	public void setBlank(String url) {
		blank = url;
		getBitmap(blank);
	}

	/**
	 * Decodes image and scales it to reduce memory consumption.
	 * 
	 * @param file
	 * @return
	 */
	private Bitmap decodeFile(File file) {
		try {
			// decode image size
			//Options options = new Options();
			// options.inJustDecodeBounds = true;
			return BitmapFactory.decodeStream(new FileInputStream(file));

			// // Find the correct scale value. It should be the power of 2.
			// final int REQUIRED_SIZE = 70;
			// int width_tmp = options.outWidth, height_tmp = options.outHeight;
			// int scale = 1;
			// while (true) {
			// if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 <
			// REQUIRED_SIZE)
			// break;
			// width_tmp /= 2;
			// height_tmp /= 2;
			// scale *= 2;
			// }
			//
			// // decode with inSampleSize
			// Options o2 = new Options();
			// o2.inSampleSize = scale;
			// return BitmapFactory.decodeStream(new FileInputStream(file),
			// null, o2);
		} catch (FileNotFoundException e) {
			// file was not found, fetch from web
			return null;
		}
	}

	/**
	 * Retrieve the bitmap from file, store to file from web if not available.
	 * 
	 * @param url
	 * @return
	 */
	private Bitmap getBitmap(String url) {
		// fetch the file
		File file = fileCache.getFile(url);

		// from SD cache
		Bitmap bm = decodeFile(file);
		if (bm != null)
			return bm;

		// from web
		try {
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			OutputStream os = new FileOutputStream(file);
			Utils.copyStream(conn.getInputStream(), os);
			bm = decodeFile(file);
			return bm;
		} catch (Exception ex) {
			ex.printStackTrace();

			// return the blank image
			return decodeFile(fileCache.getFile(blank));
		}
	}

	// private void queuePhoto(String url, ImageView image) {
	// // PhotoToLoad p = new PhotoToLoad(url, view);
	// executorService.submit(new Loader(image, url));
	// }

	// /**
	// * Check if the image is being reused or recycled.
	// *
	// * @param photo
	// * @return
	// */
	// private boolean invalidated(/*Photo photo*/ImageView image, String url) {
	// // return true if view recycled OR view has been repurposed
	// return !url.equals(views.get(image));
	// }

	// /**
	// * Url associated with an image view.
	// */
	// private class Photo {
	//
	// /** url */
	// public String url;
	//
	// /** image */
	// public ImageView image;
	//
	// public Photo(String u, ImageView i) {
	// url = u;
	// image = i;
	// }
	// }

	/**
	 * Thread to perform fetching of image if not available on file.
	 */
	private class Loader implements Runnable {

		private ImageView image;

		private String url;

		private ImageCallback callback;

		// /** image & url to load */
		// private Photo photo;

		public Loader(/* Photo photo */ImageView image, String url, ImageCallback callback) {
			// this.photo = photo;
			this.image = image;
			this.url = url;
			this.callback = callback;
		}

		@Override
		public void run() {
			// check if view is still valid
			if (!url.equals(images.get(image))) {
				callback.onLoad(0, 0);
				return;
			}

			// fetch the photo (from file or web) & store in memory
			Bitmap bmp = getBitmap(url);
			memoryCache.put(url, bmp);

			// check if view is still valid
			if (!url.equals(images.get(image))) {
				callback.onLoad(0, 0);
				return;
			}

			// render the photo on the owning activity UI thread
			((AbstractActivity) image.getContext())/*runOnUiThread*/.handler.post(new BitmapDisplayer(bmp, image, url,
					callback));
		}
	}

	// Used to display bitmap in the UI thread
	private class BitmapDisplayer implements Runnable {
		private Bitmap bitmap;
		// Photo photoToLoad;
		private ImageView image;
		private String url;
		private ImageCallback callback;

		public BitmapDisplayer(Bitmap b, /* Photo p */ImageView image, String url,
				ImageCallback callback) {
			bitmap = b;
			this.image = image;
			this.url = url;
			this.callback = callback;
			// photoToLoad = p;
		}

		public void run() {
			// check if image is still valid
			if (!url.equals(images.get(image))) {
				callback.onLoad(0, 0);
				return;
			}

			if (bitmap != null) {
				image.setImageBitmap(bitmap);
				callback.onLoad(0, 0);
				
				// set image visibility (adapter could be pointing to old reference)
				image.setVisibility(View.VISIBLE);
			}

			// else
			// photoToLoad.imageView.setImageResource(stub);
		}
	}

	/**
	 * Clear out the caches.
	 */
	public void clear() {
		memoryCache.clear();
		fileCache.clear();
	}
}