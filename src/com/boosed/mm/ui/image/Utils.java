package com.boosed.mm.ui.image;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.util.Log;

public class Utils {

	/** buffer size */
	private static final int SIZE = 1024;

	/**
	 * Write data from input stream to an output stream (i.e., file).
	 * 
	 * @param is
	 * @param os
	 */
	public static void copyStream(InputStream is, OutputStream os) {
		try {
			byte[] bytes = new byte[SIZE];
			int count = 0;
			while ((count = is.read(bytes, 0, SIZE)) != -1)
				// for (;;) {
				// int count = is.read(bytes, 0, buffer_size);
				// if (count == -1)
				// break;
				os.write(bytes, 0, count);
			// }
			os.close();
		} catch (IOException io) {
			io.printStackTrace();
			Log.e("cl", "problem copying stream data");
		}
	}
}