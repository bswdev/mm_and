package com.boosed.mm.ui.image;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import android.graphics.Bitmap;

public class MemoryCache {

	/** the images LRU cache */
	private Map<String, Bitmap> lru = new HashMap<String, Bitmap>();

	/**
	 * Cache of images in memory.
	 * 
	 * @param size
	 *            max size, anything over will be recycled wrt least recently
	 *            used.
	 */
	public MemoryCache(final int size) {
		lru = Collections.synchronizedMap(new LinkedHashMap<String, Bitmap>(20,
				0.75f, true) {
			/**
			 * This implementation will remove the eldest entry if current size
			 * is greater than specified max.
			 */
			@Override
			protected boolean removeEldestEntry(Entry<String, Bitmap> eldest) {
				return size() > size;
			}
		});
	}

	/**
	 * Retrieve an image.
	 * 
	 * @param id
	 * @return
	 */
	public Bitmap get(String id) {
		if (!lru.containsKey(id))
			return null;

		return lru.get(id);
	}

	/**
	 * Store an image.
	 * 
	 * @param id
	 * @param bitmap
	 */
	public void put(String id, Bitmap bitmap) {
		lru.put(id, bitmap);
	}

	/**
	 * Clear all items.
	 */
	public void clear() {
		lru.clear();
	}
}