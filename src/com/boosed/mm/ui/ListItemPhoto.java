package com.boosed.mm.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boosed.mm.R;

/**
 * Storing <code>ListView</code> rows in tags for convenience.
 * 
 * @author dsumera
 */
public class ListItemPhoto {

	private View base;
	private ImageView image;
	private CheckedImage check;
	private TextView title, content1, content2, end;

	public ListItemPhoto(View base) {
		this.base = base;
	}

	public void setClickable(boolean clickable) {
		base.setClickable(clickable);
	}
	
	public TextView getTitle() {
		if (title == null)
			title = (TextView) base.findViewById(android.R.id.text1);

		return title;
	}

	public TextView getContent1() {
		if (content1 == null)
			content1 = (TextView) base.findViewById(R.id.content1);

		return content1;
	}

	public TextView getContent2() {
		if (content2 == null)
			content2 = (TextView) base.findViewById(R.id.content2);

		return content2;
	}
	
	public TextView getEnd() {
		if (end == null)
			end = (TextView) base.findViewById(android.R.id.text2);

		return end;
	}

	public ImageView getImage() {
		if (image == null)
			image = (ImageView) base.findViewById(android.R.id.icon);

		return image;
	}

	public CheckedImage getCheck() {
		if (check == null)
			check = (CheckedImage) base.findViewById(android.R.id.checkbox);

		return check;
	}
}