package com.boosed.mm.ui;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.boosed.mm.R;

/**
 * Factory for creating an <code>ArrayAdapter</code> for <code>Spinner</code>
 * widgets.
 * 
 * @author dsumera
 */
public final class AdapterFactory {

	/**
	 * Generate an <code>ArrayAdapter</code> for items of type <code>T</code>.
	 * 
	 * @param <T>
	 * @param context
	 * @param items
	 * @param label
	 * @return
	 */
	public static final <T> ArrayAdapter<T> getAdapter(Context context, List<T> items, final AdapterLabel<T> label) {
		ArrayAdapter<T> adapter = new ArrayAdapter<T>(context, R.layout.spinner_item, items) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				TextView rv = (TextView) super.getView(position, convertView, parent);
				rv.setText(label.getName(getItem(position)));
				return rv;
			}

			@Override
			public View getDropDownView(int position, View convertView, ViewGroup parent) {
				TextView rv = (TextView) super.getDropDownView(position, convertView, parent);
				rv.setText(label.getName(getItem(position)));
				return rv;
			}
		};
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		return adapter;
	}

	/**
	 * Generate an <code>ArrayAdapter</code> for items of type <code>T</code>.
	 * 
	 * @param <T>
	 * @param context
	 * @param items
	 * @return
	 */
	public static final <T> ArrayAdapter<T> getAdapter(Context context, T[] items) {
		ArrayAdapter<T> adapter = new ArrayAdapter<T>(context, R.layout.spinner_item, items);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		return adapter;
	}

	/**
	 * Interface for labeling items for an <code>ArrayAdapter</code> created by
	 * this factory.
	 * 
	 * @param <T>
	 */
	public interface AdapterLabel<T> {
		public CharSequence getName(T item);
	}
}