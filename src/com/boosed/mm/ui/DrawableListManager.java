package com.boosed.mm.ui;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

public class DrawableListManager extends Handler {

	private static DrawableListManager manager;

	/** max cache size */
	private static final int MAX_SIZE = 500;

	/** the images LRU cache */
	private final Map<String, Bitmap> lru;

	/** image URL to views waiting for image data */
	private final Map<String, Set<ImageView>> views;

	/** image URL to callbacks */
	private final Map<String, Set<ImageCallback>> callbacks;

	/** TODO: this is only used now for testing; remove when finished */
	//private Context context;

	private String blankUrl = "";

	private DrawableListManager(/*Context context*/) {
		lru = Collections.synchronizedMap(new LinkedHashMap<String, Bitmap>(20, 0.75f, true) {
			/**
			 * This implementation will remove the eldest entry if current size
			 * is greater than specified max.
			 */
			@Override
			protected boolean removeEldestEntry(Entry<String, Bitmap> eldest) {
				return size() > MAX_SIZE;
			}
		});
		views = new HashMap<String, Set<ImageView>>();
		callbacks = new HashMap<String, Set<ImageCallback>>();
		//this.context = context;
	}

	public static DrawableListManager getInstance(/*Context context*/) {
		if (manager == null)
			manager = new DrawableListManager(/*context*/);

		return manager;
	}

	/**
	 * Useful for loading an image into the cache.
	 * 
	 * @param url
	 * @return
	 */
	public Bitmap loadImage(final String url) {
		if (lru.containsKey(url))
			// image is loading (returns null) or already loaded
			return lru.get(url);
		else {
			// mark placeholder to indicate image is loading
			lru.put(url, null);

			// perform the image load
			new Thread() {
				@Override
				public void run() {
					// this will call back via the handler
					fetchDrawable(url);
				}
			}.start();

			return null;
		}
	}

	/**
	 * Sets an image against an <code>ImageView</code> and automatically loads
	 * the image if there is no cache hit.
	 * 
	 * @param url
	 * @param view
	 * @return
	 */
	public Bitmap loadImage(final String url, ImageView view) {
		Bitmap b = loadImage(url);
		if (b == null)
			// no image, queue the view
			addView(url, view);
		else
			// set the image
			view.setImageBitmap(b);

		return b;
	}

	/**
	 * Sets an image against an <code>ImageView</code> and automatically loads
	 * the image if there is no cache hit. The <code>callback</code> is invoked
	 * to notify when the <code>ImageView</code> has been set.
	 * 
	 * @param url
	 * @param view
	 * @param callback
	 */
	public void loadImage(final String url, ImageView view, final ImageCallback callback) {
		Bitmap b = loadImage(url, view);
		if (b == null)
			// no image, queue the callback
			addCallback(url, callback);
		else
			// invoke the callback
			callback.onLoad(0, 0);//b.getHeight(), b.getWidth());
	}

	public void setBlank(String blankUrl) {
		loadImage(blankUrl);
		this.blankUrl = blankUrl;
	}

	/**
	 * Add a callback to invoke when the specified url/image has been fetched.
	 * 
	 * @param url
	 * @param callback
	 */
	private void addCallback(String url, ImageCallback callback) {
		Set<ImageCallback> values = callbacks.get(url);

		if (values == null) {
			// values does not exist, create it
			values = new HashSet<ImageCallback>();
			callbacks.put(url, values);
		}

		values.add(callback);
		// if (callbacks.containsKey(url))
		// // get existing set and add
		// callbacks.get(url).add(callback);
		// else {
		// // create a new set and add
		// Set<ImageCallback> values = new HashSet<ImageCallback>();
		// values.add(callback);
		// callbacks.put(url, values);
		// }
	}

	/**
	 * Add an <code>ImageView</code> to be set when specified url/image has been
	 * fetched.
	 * 
	 * @param url
	 * @param view
	 */
	private void addView(String url, ImageView view) {
		// set a loading image?
		// view.setImageResource(R.drawable.ic_menu_car);

		Set<ImageView> values = views.get(url);

		if (values == null) {
			// values does not exist, create it
			values = new HashSet<ImageView>();
			views.put(url, values);
		}

		values.add(view);
		// if (views.containsKey(url))
		// // get existing set and add
		// views.get(url).add(view);
		// else {
		// // create a new set and add
		// Set<ImageView> values = new HashSet<ImageView>();
		// values.add(view);
		// views.put(url, values);
		// }
	}

	@Override
	public void handleMessage(Message msg) {

		// retrieve data from message
		String url = msg.getData().getString("url");
		Bitmap image = (Bitmap) msg.obj;

		// remove views and set image (if present)
		Set<ImageView> iupdates = views.remove(url);
		if (iupdates != null)
			for (ImageView view : iupdates)
				// Log.i("chat", "VIEWSET: " + url + " " + view.hashCode());
				view.setImageBitmap(image);

		// Log.w("chat", "the image is: " + image);
		// remove callbacks and invoke (if present)
		Set<ImageCallback> cupdates = callbacks.remove(url);
		if (cupdates != null) {
			int h = image == null ? 0 : image.getHeight();
			int w = image == null ? 0 : image.getWidth();
			for (ImageCallback callback : cupdates)
				// Log.i("chat", "VIEWSET: " + url + " " + view.hashCode());
				callback.onLoad(h, w);
		}
		// ImageCallback callback = (ImageCallback)
		// msg.getData().get("callback");
		// if (callback != null)
		// callback.onLoad(image.getHeight(), image.getWidth());
		// // adjust the cache size
		// if (images.size() > MAX_SIZE) {
		// // Iterator<SoftReference<Drawable>> it = cache.values().iterator();
		// Iterator<Bitmap> it = images.values().iterator();
		// it.next();
		// it.remove();
		// }
	}

	private static final int IO_BUFFER_SIZE = 2048;

	private void fetchDrawable(String url/* , ImageCallback callback */) {
		HttpURLConnection conn = null;
		InputStream is = null;
		OutputStream os = null;
		Message msg = Message.obtain(this);
		// do not need to add time date to differentiate URL since all new
		// photos will have unique URLs (and default/blank URL will be sourced
		// directly in Car.java class)
		msg.getData().putString("url", url);
		// msg.getData().putSerializable("callback", callback);

		try {
			// Log.i("chat", "the drawable: " + url);
			conn = (HttpURLConnection) new URL(url).openConnection();
			conn.setConnectTimeout(0);
			// //conn.setReadTimeout(0);
			conn.setDoInput(true);
			conn.setInstanceFollowRedirects(true);
			// //conn.setChunkedStreamingMode(2048);
			conn.connect();
			// // int length = conn.getContentLength();
			// is = conn.getInputStream();
			// // is = (InputStream) conn.getContent();
			// // Drawable draw = Drawable.createFromStream((InputStream)
			// conn.getContent(), "src name");
			// ByteBuffer bb = ByteBuffer.allocate(1024);

			byte[] buffer = new byte[IO_BUFFER_SIZE];
			is = conn.getInputStream();
			// is = new URL(url).openStream();
			BufferedInputStream in = new BufferedInputStream(is, IO_BUFFER_SIZE);

			os = new ByteArrayOutputStream();
			BufferedOutputStream out = new BufferedOutputStream(os, IO_BUFFER_SIZE);
			int len;

			// write input stream to output stream
			while ((len = in.read(buffer)) != -1)
				out.write(buffer, 0, len);

			// all pending data written to target/output stream and stream is
			// also flushed
			out.flush();
			buffer = ((ByteArrayOutputStream) os).toByteArray();

			// Log.i("chat", "the drawable: " + url);
			// conn = (HttpURLConnection) new URL(url).openConnection();
			// //conn.setConnectTimeout(0);
			// //conn.setReadTimeout(0);
			// conn.setDoInput(true);
			// conn.setInstanceFollowRedirects(true);
			// //conn.setChunkedStreamingMode(2048);
			// conn.connect();
			// // int length = conn.getContentLength();
			// is = conn.getInputStream();
			//
			// // get image and set it on widget
			// Drawable draw;
			//
			// //Drawable draw = Drawable.createFromStream(is, "");
			// ImageView view = views.get(url);
			//
			// if (view != null)
			// draw = new BitmapDrawable(view.getResources(), is);
			// //draw = Drawable.createFromResourceStream(view.getResources(),
			// new TypedValue(), is, "");
			// else
			// draw = Drawable.createFromStream(is, "");

			// cache the image
			// Log.i("chat", "CACHING: " + url);

			msg.obj = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);

			// caching the image
			lru.put(url, (Bitmap) msg.obj);

			msg.sendToTarget();
		} catch (Exception e) {
			e.printStackTrace();

			// just use the default image
			msg.obj = lru.get(blankUrl);
			msg.sendToTarget();
		} finally {
			// close input stream
			if (conn != null)
				try {
					conn.disconnect();
				} catch (Exception e) {
				}

			// close output stream
			if (os != null)
				try {
					os.close();
				} catch (Exception e) {
				}
		}
	}

//	/**
//	 * Dummy method for working w/o internet connection.
//	 * 
//	 * @param url
//	 */
//	@SuppressWarnings("unused")
//	private void fetchDrawablee(String url/* , ImageCallback callback */) {
//		Message msg = Message.obtain(this);
//		msg.getData().putString("url", url);
//		// msg.getData().putSerializable("callback", callback);
//
//		try {
//
//			Thread.sleep(1000);
//			msg.obj = BitmapFactory.decodeResource(context.getResources(), R.drawable.w100);
//
//			// caching the image
//			lru.put(url, (Bitmap) msg.obj);
//
//			msg.sendToTarget();
//		} catch (Exception e) {
//			e.printStackTrace();
//
//			// just use the blank image
//			msg.obj = lru.get(blankUrl);
//			msg.sendToTarget();
//		}
//	}

	// public Bitmap getDrawable(Context context, String url) {
	// if (lru.containsKey(url))
	// return lru.get(url);
	//
	// HttpURLConnection conn = null;
	// InputStream is = null;
	// OutputStream os = null;
	//
	// try {
	// // // Log.i("chat", "the drawable: " + url);
	// // conn = (HttpURLConnection) new URL(url).openConnection();
	// // conn.setConnectTimeout(0);
	// // // //conn.setReadTimeout(0);
	// // conn.setDoInput(true);
	// // conn.setInstanceFollowRedirects(true);
	// // // //conn.setChunkedStreamingMode(2048);
	// // conn.connect();
	// // // // int length = conn.getContentLength();
	// // //is = conn.getInputStream();
	// // // // is = (InputStream) conn.getContent();
	// // // // Drawable draw = Drawable.createFromStream((InputStream)
	// // // conn.getContent(), "src name");
	// // // ByteBuffer bb = ByteBuffer.allocate(1024);
	// //
	// //
	// // is = conn.getInputStream();
	// // //is = new URL(url).openStream();
	// // BufferedInputStream in = new BufferedInputStream(is,
	// // IO_BUFFER_SIZE);
	// //
	// // os = new ByteArrayOutputStream();
	// // BufferedOutputStream out = new BufferedOutputStream(os,
	// // IO_BUFFER_SIZE);
	// // int len;
	// //
	// // byte[] buffer = new byte[IO_BUFFER_SIZE];
	// //
	// // // write input stream to output stream
	// // while ((len = in.read(buffer)) != -1)
	// // out.write(buffer, 0, len);
	// //
	// // // all pending data written to target/output stream and stream is
	// // also flushed
	// // out.flush();
	// // buffer = ((ByteArrayOutputStream) os).toByteArray();
	// // return BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
	//
	// Thread.sleep(3000);
	// return BitmapFactory.decodeResource(context.getResources(),
	// R.drawable.icon);
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// // just use the blank image
	// return null;
	// } finally {
	// // close input stream
	// if (conn != null)
	// try {
	// conn.disconnect();
	// } catch (Exception e) {
	// }
	//
	// // close output stream
	// if (os != null)
	// try {
	// os.close();
	// } catch (Exception e) {
	// }
	// }
	// }
}