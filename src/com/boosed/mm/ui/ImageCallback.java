package com.boosed.mm.ui;

import java.io.Serializable;

/**
 * Interface for notifying <code>ImageView</code>s their source has been
 * provided.
 * 
 * @author dsumera
 */
public interface ImageCallback extends Serializable {

	public void onLoad(int height, int width);
}
