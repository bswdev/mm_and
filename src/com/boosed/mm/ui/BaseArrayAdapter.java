package com.boosed.mm.ui;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Base class for creating <code>ArrayAdapter</code> for a <code>List</code> of
 * items.
 * 
 * @author admin
 * 
 * @param <T>
 * @param <R>
 */
public abstract class BaseArrayAdapter<T, R> extends ArrayAdapter<T> {

	private int listItemResourceId;

	public BaseArrayAdapter(Context context, List<T> items, int listItemResourceId) {
		super(context, 0, items);

		this.listItemResourceId = listItemResourceId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		T item = null;

		// check for array out of bounds
		try {
			item = getItem(position);
		} catch (IndexOutOfBoundsException oobe) {
			Log.w("mm", "base array adapter index out of bounds: " + position);
			// return view;
		} finally {
			if (view == null) {
				// this view has not been initalized yet
				view = ((LayoutInflater) getContext().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE))
						.inflate(listItemResourceId, parent, false);

				// create and set the new wrapper
				view.setTag(createView(view));
			}

			if (item == null)
				return view;
		}

		// retrieve item for rendering
		render(item, (R) view.getTag());

		// return the view
		return view;
	}

	/**
	 * Implement how the <code>View</code> will be instantiated. Usually returns
	 * some type of "row wrapper."
	 * 
	 * @param view
	 * @return
	 */
	abstract protected R createView(View view);

	/**
	 * Implement how the <code>View</code> will be rendered.
	 * 
	 * @param item
	 * @param view
	 *            the base <code>View</code>; can get <code>R</code> by using
	 *            <code>getTag</code>
	 */
	abstract protected void render(T item, R row);
}