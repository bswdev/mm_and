package com.boosed.mm.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Borrowed from <code>CheckedTextView</code>.
 * 
 * @author dsumera
 */
public class CheckedLayout extends LinearLayout implements Checkable {

	private boolean mChecked;
	// private int mCheckMarkResource;
	private Drawable mCheckMarkDrawable;
	private int mBasePaddingRight;
	@SuppressWarnings("unused")
	private int mPaddingRight;
	private int mCheckMarkWidth;

	private ImageView image;

	private static final int[] CHECKED_STATE_SET = { android.R.attr.state_checked };

	public CheckedLayout(Context context) {
		super(context);
		image = new ImageView(context);
	}

	public CheckedLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		image = new ImageView(context);
		//
		// TypedArray a = context.obtainStyledAttributes(attrs,
		// R.styleable.CheckedLayout);
		//
		// Drawable d =
		// a.getDrawable(R.styleable.CheckedLayout_android_checkMark);
		// if (d != null)
		// setCheckMarkDrawable(d);
		//
		// boolean checked =
		// a.getBoolean(R.styleable.CheckedLayout_android_checked, false);
		// setChecked(checked);
		//
		// a.recycle();
	}

	public void toggle() {
		setChecked(!mChecked);
	}

	public boolean isChecked() {
		return mChecked;
	}

	// private int index = 0;

	@Override
	public void addView(View child, android.view.ViewGroup.LayoutParams params) {
		super.addView(child, params);

		// index++;
		// if (index == 2) {
		// // add an imageview that can receive clicks
		// params.height = LayoutParams.WRAP_CONTENT;
		// params.width = LayoutParams.WRAP_CONTENT;
		// //params.gravity = Gravity.CENTER;
		// addView(image, params);
		// }
	}

	/**
	 * <p>
	 * Changes the checked state of this text view.
	 * </p>
	 * 
	 * @param checked
	 *            true to check the text, false to uncheck it
	 */
	public void setChecked(boolean checked) {
		// if (mChecked != checked) {
		mChecked = checked;
		refreshDrawableState();
	}

	// /**
	// * Set the checkmark to a given Drawable, identified by its resourece id.
	// * This will be drawn when {@link #isChecked()} is true.
	// *
	// * @param resid
	// * The Drawable to use for the checkmark.
	// */
	// public void setCheckMarkDrawable(int resid) {
	// if (resid != 0 && resid == mCheckMarkResource) {
	// return;
	// }
	//
	// mCheckMarkResource = resid;
	//
	// Drawable d = null;
	// if (mCheckMarkResource != 0)
	// d = getResources().getDrawable(mCheckMarkResource);
	//
	// setCheckMarkDrawable(d);
	// }

	/**
	 * Set the checkmark to a given Drawable. This will be drawn when
	 * {@link #isChecked()} is true.
	 * 
	 * @param d
	 *            The Drawable to use for the checkmark.
	 */
	public void setCheckMarkDrawable(Drawable d) {
		if (d != null) {
			if (mCheckMarkDrawable != null) {
				mCheckMarkDrawable.setCallback(null);
				unscheduleDrawable(mCheckMarkDrawable);
			}
			d.setCallback(this);
			d.setVisible(getVisibility() == VISIBLE, false);
			d.setState(CHECKED_STATE_SET);
			// setMinHeight(d.getIntrinsicHeight());

			mCheckMarkWidth = d.getIntrinsicWidth();
			mPaddingRight = mCheckMarkWidth + mBasePaddingRight;

			d.setState(getDrawableState());
			mCheckMarkDrawable = d;
			image.setImageDrawable(d);
		} else {
			mPaddingRight = mBasePaddingRight;
		}
		requestLayout();
	}

	// @Override
	// public void setPadding(int left, int top, int right, int bottom) {
	// super.setPadding(left, top, right, bottom);
	// mBasePaddingRight = mPaddingRight;
	// }

	// @Override
	// protected void onDraw(Canvas canvas) {
	// super.onDraw(canvas);
	//
	// final Drawable checkMarkDrawable = mCheckMarkDrawable;
	// if (checkMarkDrawable != null) {
	// // final int verticalGravity = /*getGravity() &*/
	// // Gravity.VERTICAL_GRAVITY_MASK;
	// final int height = checkMarkDrawable.getIntrinsicHeight();
	//
	// int y = (getHeight() - height) / 2;// 0;
	//
	// // switch (verticalGravity) {
	// // case Gravity.BOTTOM:
	// // y = getHeight() - height;
	// // break;
	// // case Gravity.CENTER_VERTICAL:
	// // y = (getHeight() - height) / 2;
	// // break;
	// // }
	//
	// int right = getWidth();
	// checkMarkDrawable.setBounds(right - mCheckMarkWidth - mBasePaddingRight,
	// y, right
	// - mBasePaddingRight, y + height);
	// image.draw(canvas);
	// }
	// }

	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);

		if (mChecked)
			return mergeDrawableStates(drawableState, CHECKED_STATE_SET);

		String state = "";
		for (int i : drawableState)
			state += i + " ";

		// Log.w("chat", "the states are: " + state);
		return drawableState;
	}

	@Override
	protected void drawableStateChanged() {
		super.drawableStateChanged();

		if (mCheckMarkDrawable != null) {
			int[] myDrawableState = getDrawableState();

			// Set the state of the Drawable
			mCheckMarkDrawable.setState(myDrawableState);

			// image.invalidate();
			invalidate();
		}
	}

	@Override
	public void setOnClickListener(OnClickListener l) {
		// super.setOnClickListener(l);
		// set the click listener on the image only
		// Log.w("chat", "setting the listener on the image");
		image.setOnClickListener(l);
	}
}