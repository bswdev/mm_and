package com.boosed.mm.ui;

import android.view.View;
import android.widget.TextView;

/**
 * Store <code>ListView</code> views in tag for convenience.
 */
public class CheckRowWrapper {

	private View base;
	private TextView title;

	public CheckRowWrapper(View base) {
		this.base = base;
		// Button b = (Button) base.findViewById(R.id.button);
		// b.getBackground().setColorFilter(0xFFFF0000,
		// PorterDuff.Mode.MULTIPLY);
	}

	public TextView getTitle() {
		if (title == null)
			title = (TextView) base.findViewById(android.R.id.text1);

		return title;
	}
}
