package com.boosed.mm.ui;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

/**
 * Base class for creating <code>ArrayAdapter</code> for a <code>List</code> of
 * items.
 * 
 * @author admin
 * 
 * @param <T>
 *            the type of data to be shown
 * @param <R>
 *            the layout for the row items
 */
public abstract class BaseSpinnerAdapter<T, R> extends ArrayAdapter<T> implements SpinnerAdapter {

	private int listItemResourceId;

	public BaseSpinnerAdapter(Context context, List<T> items, int listItemResourceId) {
		super(context, 0, items);

		this.listItemResourceId = listItemResourceId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		T item = null;

		// check for array out of bounds
		try {
			item = getItem(position);
		} catch (IndexOutOfBoundsException oobe) {
			Log.w("mm", "base spinner adapter out of bounds: " + position);
			// return view;
		} finally {
			if (view == null) {
				// this view has not been initalized yet
				view = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
						listItemResourceId, parent, false);

				// create and set the new wrapper
				view.setTag(createView(view));
			}

			if (item == null)
				return view;
		}

		// retrieve item for rendering
		render(item, (R) view.getTag());

		// return the view
		return view;
	}

	/**
	 * Implement how the <code>View</code> will be instantiated. Usually returns
	 * some type of "row wrapper."
	 * 
	 * @param view
	 * @return
	 */
	abstract protected R createView(View view);

	/**
	 * Implement how the <code>View</code> will be rendered.
	 * 
	 * @param item
	 * @param view
	 *            the base <code>View</code>; can get <code>R</code> by using
	 *            <code>getTag</code>
	 */
	abstract protected void render(T item, R row);

	abstract protected String getItemString(T item);

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView) super.getDropDownView(position, convertView, parent);
		view.setText(getItemString(getItem(position)));
		return view;
	}
}