package com.boosed.mm.ui;

import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.boosed.mm.R;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.ui.image.ImageLoader;

public class GalleryAdapter extends BaseArrayAdapter<Entry<String, Tuple<Boolean, String>>, ImageView> {

	private static ImageLoader dm;

	private List<Entry<String, Tuple<Boolean, String>>> images;

	// /** scale to provide appengine to resize photos */
	// private String scale;
	
	public GalleryAdapter(Context context, List<Entry<String, Tuple<Boolean, String>>> images) {
		super(context, images, R.layout.list_item_gallery);
		this.images = images;
		
		if (dm == null)
			dm = ImageLoader.getInstance(/*context*/getContext());
	}

	// @Override
	// public void clear() {
	// images.clear();
	// //notifyDataSetChanged();
	// }

	/**
	 * Set a new <code>List</code> of images to replace the existing set.
	 * 
	 * @param images
	 */
	public void setImages(Collection<Entry<String, Tuple<Boolean, String>>> images) {
		// this.images.clear();
		// clear the adapter of images
		clear();
		this.images.addAll(images);

		// for (Entry<String, Tuple<Boolean, String>> image : images)
		// add(image);

		// draw the gallery borders correctly
		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		notifyDataSetChanged();
		// }
		// }, 2000);
	}

	@Override
	protected ImageView createView(View view) {
//        view.setLayoutParams(new Gallery.LayoutParams(100, 60));
//        ((ImageView) view).setScaleType(ImageView.ScaleType.FIT_XY);
		return (ImageView) view;
	}

	@Override
	protected void render(Entry<String, Tuple<Boolean, String>> item, final ImageView row) {
		// set the loading image
		row.setImageResource(R.drawable.w100);

		dm.display(row, item.getValue().b + "=s160", new ImageCallback() {
			@Override
			public void onLoad(int height, int width) {
				// do nothing	
			}
		});
		
//		// load image
//		dm.loadImage(item.getValue().b + "=s160", row/*, new ImageCallback() {
//			@Override
//			public void onLoad(int height, int width) {
//				// row.invalidate();
//				// check when images count has been reached
//				// if (++count == images.size())
//				// perform last update to redraw gallery borders
//				// notifyDataSetChanged();
//			}
//		}*/);
	}
}