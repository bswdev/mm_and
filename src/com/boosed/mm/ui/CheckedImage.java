package com.boosed.mm.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.ImageView;

import com.boosed.mm.R;

/**
 * Borrowed from <code>CheckedTextView</code>.
 * 
 * @author dsumera
 */
public class CheckedImage extends ImageView implements Checkable {

	private static final int[] CHECKED_STATE_SET = { android.R.attr.state_checked };

	private Drawable checkmark;

	private boolean checked = false;

	public CheckedImage(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CheckedImage);

		// retrieve the checkmark drawable
		Drawable d = a.getDrawable(R.styleable.CheckedImage_android_checkMark);
		if (d != null)
			setCheckMarkDrawable(d);

		// retrieve the state
		setChecked(a.getBoolean(R.styleable.CheckedImage_android_checked, false));

		a.recycle();
	}

	public CheckedImage(Context context, AttributeSet attrs) {
		super(context, attrs);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CheckedImage);

		// retrieve the checkmark drawable
		Drawable d = a.getDrawable(R.styleable.CheckedImage_android_checkMark);
		if (d != null)
			setCheckMarkDrawable(d);

		// retrieve the state
		setChecked(a.getBoolean(R.styleable.CheckedImage_android_checked, false));

		a.recycle();
	}

	public CheckedImage(Context context) {
		super(context);
	}

	@Override
	public boolean isChecked() {
		return checked;
	}

	@Override
	public void setChecked(boolean checked) {
		if (this.checked != checked) {
			this.checked = checked;
			refreshDrawableState();
		}
	}

	@Override
	public void toggle() {
		setChecked(!checked);
	}

	/**
	 * Set the checkmark to a given Drawable. This will be drawn when
	 * {@link #isChecked()} is true.
	 * 
	 * @param d
	 *            The Drawable to use for the checkmark.
	 */
	public void setCheckMarkDrawable(Drawable d) {
		if (d != null) {
			if (checkmark != null) {
				checkmark.setCallback(null);
				unscheduleDrawable(checkmark);
			}

			// setMinHeight(d.getIntrinsicHeight());
			// mCheckMarkWidth = d.getIntrinsicWidth();
			// mPaddingRight = mCheckMarkWidth + mBasePaddingRight;
			d.setCallback(this);
			d.setVisible(getVisibility() == VISIBLE, false);
			// d.setState(CHECKED_STATE_SET);
			d.setState(getDrawableState());
			setImageDrawable(d);
			checkmark = d;
		}
		// else {
		// mPaddingRight = mBasePaddingRight;
		// }

		requestLayout();
	}

	// @Override
	// protected void drawableStateChanged() {
	// super.drawableStateChanged();
	//
	// if (checkmark != null) {
	// // set the state of the drawable
	// checkmark.setState(getDrawableState());
	//			
	// String state = "";
	// for (int i : getDrawableState())
	// state += i + " ";
	//			
	// Log.w("chat", "the state is " + state);
	//			
	// invalidate();
	// }
	// }

	@Override
	public int[] onCreateDrawableState(int extraSpace) {
		// remove the pressed state
//		int states[] = super.onCreateDrawableState(extraSpace + 1);
//		String s = "";
		
//		Arrays.sort(states);
//		int index = Arrays.binarySearch(states, android.R.attr.state_window_focused);
//		if (index > -1)
//			states[index] = 0;
		
		if (checked)
			return mergeDrawableStates(super.onCreateDrawableState(extraSpace + 1), CHECKED_STATE_SET);
		else
			return super.onCreateDrawableState(extraSpace + 1);
	}
}