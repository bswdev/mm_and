package com.boosed.mm.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boosed.mm.R;

/**
 * Storing <code>ListView</code> rows in tags for convenience.
 * 
 * @author dsumera
 */
public class ListItemMessage {

	View base;
	ImageView image;
	CheckedImage check;
	TextView subject, reference, sender, date;

	public ListItemMessage(View base) {
		this.base = base;
	}

	public TextView getSubject() {
		if (subject == null)
			subject = (TextView) base.findViewById(android.R.id.text1);

		return subject;
	}

	public TextView getReference() {
		if (reference == null)
			reference = (TextView) base.findViewById(android.R.id.text2);
		
		return reference;
	}
	
	public TextView getSender() {
		if (sender == null)
			sender = (TextView) base.findViewById(R.id.content1);

		return sender;
	}

	public TextView getDate() {
		if (date == null)
			date = (TextView) base.findViewById(R.id.date);
		
		return date;
	}
	
	public ImageView getImage() {
		if (image == null)
			image = (ImageView) base.findViewById(android.R.id.icon);

		return image;
	}

	public CheckedImage getCheck() {
		if (check == null)
			check = (CheckedImage) base.findViewById(android.R.id.checkbox);

		return check;
	}
}