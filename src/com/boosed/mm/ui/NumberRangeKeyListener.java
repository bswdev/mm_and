package com.boosed.mm.ui;

import android.text.InputType;
import android.text.Spanned;
import android.text.method.NumberKeyListener;

// XXX This doesn't allow for range limits when controlled by a
// soft input method!
public class NumberRangeKeyListener extends NumberKeyListener {

	private static final char[] DIGIT_CHARACTERS = new char[] { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9' };

	private int max;
	
	public NumberRangeKeyListener(int max) {
		this.max = max;
	}
	
	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart,
			int dend) {

		CharSequence filtered = super.filter(source, start, end, dest, dstart, dend);
		if (filtered == null) {
			filtered = source.subSequence(start, end);
		}

		String result = String.valueOf(dest.subSequence(0, dstart)) + filtered
				+ dest.subSequence(dend, dest.length());

		if ("".equals(result))
			return "";
		

		/*
		 * Ensure the user can't type in a value greater than the max allowed.
		 * We have to allow less than min as the user might want to delete some
		 * numbers and then type a new number.
		 */
		return Integer.parseInt(result) > max ? "" : filtered;
	}

	@Override
	protected char[] getAcceptedChars() {
		return DIGIT_CHARACTERS;
	}

	// XXX This doesn't allow for range limits when controlled by a
	// soft input method!
	public int getInputType() {
		return InputType.TYPE_CLASS_NUMBER;
	}
	
	public void setMax(int max) {
		this.max = max;
	}
}