package com.boosed.mm.ui;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.boosed.mm.activity.AbstractActivity;
import com.boosed.mm.rpc.RpcService;

public class BaseHandler extends Handler {

	/** the activity whose message queue this handler processes */
	private AbstractActivity activity;

	public BaseHandler(AbstractActivity activity) {
		this.activity = activity;
	}

	/**
	 * Messages that are not handled here have their <code>arg1</code> set to 1.
	 */
	@Override
	public void handleMessage(Message msg) {
		switch (msg.what) {
		case RpcService.MSG_RPC_CONNECT:
			activity.onConnect();
			break;
		case RpcService.MSG_RPC_DISCONNECT:
			activity.onDisconnect();
			break;
		case RpcService.MSG_RPC_ONLINE:
			activity.onOnline();
			break;
		case RpcService.MSG_RPC_OFFLINE:
			activity.onOffline();
			break;
		case RpcService.MSG_RPC_AUTOLOGIN:
			// // auto-login, get values from preferences
			// activity.new LoginTask().execute(RpcService.PREF_USER,
			// BaseActivity.settings.getString(RpcService.PREF_USER, ""),
			// RpcService.PREF_PASS,
			// BaseActivity.settings.getString(RpcService.PREF_PASS, ""));
			break;
		case RpcService.MSG_RPC_LOGINFAIL:
			// show that dialog
			// catch if user has switched activities too quickly
			try {
				activity.showDialog(AbstractActivity.DLG_LOGIN_FAILED_ID);
			} catch (Exception e) {
				Log.w("mm", "could not show failed login dialog, user is switching too quickly");
			}
			break;
		case RpcService.MSG_ACCTS:
			// catch if user has switched activities too quickly
			try {
				// activity.showDialog(AbstractActivity.DLG_ACCOUNTS);
				// pass account labels to start login process
				activity.login((CharSequence[]) msg.obj);
			} catch (Exception e) {
				Log.w("mm", "could not show the dialog login, user is switching too quickly");
			}
			// activity.showAccounts((Account[]) msg.obj);
			break;
		case RpcService.MSG_ACCTS_NONE:
			// invite user to create a new account
			activity.showDialog(AbstractActivity.DLG_ADD_ACCT);
			break;
		default:
			// no message was processed, forward to activity with status 1 for
			// further processing
			msg.arg1 = 1;
			break;
		}
	}
}
