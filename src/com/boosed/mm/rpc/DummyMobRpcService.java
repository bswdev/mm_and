package com.boosed.mm.rpc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.CLLocale;
import com.boosed.mm.shared.db.CLState;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.MobResult;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.State;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.exception.IncompleteException;
import com.boosed.mm.shared.exception.InvalidLocationException;
import com.boosed.mm.shared.exception.InvalidRecipientException;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.service.MobimotosRpcService;
import com.boosed.mm.shared.util.DataUtil;
import com.googlecode.objectify.Key;

public class DummyMobRpcService implements MobimotosRpcService {

	private Account account;

	private Ad ad;

	private Set<Key<Car>> bookmarks;

	private List<Car> cars;

	private List<Message> messages;
	
	private Make make;
	
	private Model model;

	public DummyMobRpcService() {
		// init variables
		account = new Account("1", "User1", "test@email.com");
		account.phone = "2109874657";
		account.address = "4675 Creek Rd.\nKansas City, MO";

		bookmarks = new HashSet<Key<Car>>();

		make = new Make("Ferrari");
		make.total = 1;
		//make.totals = new HashMap<Key<Type>, Integer>();
		make.totals.put(new Key<Type>(Type.class, "hatchback"), 1);
		
		model = new Model("Ferrari F-150");
		model.make = make.getKey();
		
		Car car = createCar(1);

		cars = new ArrayList<Car>();
		cars.add(car);
		cars.add(createCar(2));
		
		ad = new Ad();
		ad.id = 1L;
		ad.addImage("a", "a");
		ad.addImage("b", "b");
		ad.addImage("c", "c");
		ad.addImage("d", "d");
		ad.approveImage("a");
		ad.approveImage("b");
		ad.approveImage("c");
		ad.car = car.getKey();
		ad.setContent("this is a test string that should show ont the screen regardless of how many lines it may be and the only thing that prevents you from doing something else is that we do not have the time or patience to check this");
		ad.approveContent();
		
		messages = new ArrayList<Message>();
		// account, "this is a message from me!", car
		Message m = new Message(account.getKey(), car, "alert for your car!");
		m.id = 1L;
		messages.add(m);
		m = new Message(account.getKey(), car, "where does it go?");
		try {
			Thread.sleep(2000);
		} catch (Exception e) {

		}
		m.addMessage("I'm not really sure", account.getKey());
		m.id = 2L;
		messages.add(m);
		m = new Message(
				account,
				car,
				"this is a test string that should show ont the screen regardless of how many lines it may be and the only thing that prevents you from doing something else is that we do not have the time or patience to check this this is a test string that should show ont the screen regardless of how many lines it may be and the only thing that prevents you from doing something else is that we do not have the time or patience to check this this is a test string that should show ont the screen regardless of how many lines it may be and the only thing that prevents you from doing something else is that we do not have the time or patience to check this this is a test string that should show ont the screen regardless of how many lines it may be and the only thing that prevents you from doing something else is that we do not have the time or patience to check this");
		m.id = 3L;
		messages.add(m);
		// account.selling.add(car.getKey());
	}

	private Car createCar(long id) {
		Car car = new Car();
		car.id = id;
		// car.image = "a";
		car.make = make.getKey();
		car.model = model.getKey();
		car.conditions.add(ConditionType.CERTIFIED);
		car.drivetrain = new Drivetrain("RWD").getKey();
		car.engine = new Engine("diesel").getKey();
		car.transmission = new Transmission("automatic, 6spd").getKey();
		car.year = 2000;
		car.exterior = new Color("white").getKey();
		car.interior = new Color("black").getKey();
		car.mileage = 15467;
		car.price = 200000;
		car.location = "Lake Partolicca City, TX";
		car.doors = 2;
		car.owner = new Key<Account>(Account.class, "134");

		car.distance = 12;
		// car.trim = "";

		car.stock = "8576TYP";
		
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.add(Calendar.HOUR, -49);
		car.time = cal.getTimeInMillis();
		// car.vin = "WVYW26WDW5WWW88WWO";

		car.latitude = 36.7;
		car.longitude = -85.0;

		car.ad = new Key<Ad>(Ad.class, 1);
		return car;
	}
	
	@Override
	public Long activate(long carId, boolean activated) throws RemoteServiceFailureException {
		return System.currentTimeMillis();
	}

	// @Override
	// public void checkCreds() throws RemoteServiceFailureException {
	// // not implemented
	// }

	@Override
	public List<Key<Color>> createColor(String color) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public List<Key<Drivetrain>> createDrive(String drive) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public List<Key<Engine>> createEngine(String engine) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public void createLocation(String postal, String state, String city, double lat, double lon)
			throws RemoteServiceFailureException {
		// not implemented

	}

	@Override
	public List<Key<Make>> createMake(String make) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public List<Key<Transmission>> createTransmission(String trans) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public List<Key<Type>> createType(String type) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public String initialize() throws RemoteServiceFailureException {
		return "134";
	}

	@Override
	public void inquire(long carId, String message) throws RemoteServiceFailureException {
		// not implemented
	}

	@Override
	public Account loadAccount() throws RemoteServiceFailureException {
		return account;
	}

	@Override
	public String loadAction() throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public Ad loadAd(long adId) throws RemoteServiceFailureException {
		return ad;
	}

	@Override
	public Tuple<Ad, Account> loadAd(long adId, boolean sanitize) throws RemoteServiceFailureException {
		return new Tuple<Ad, Account>(ad, account);
	}

	@Override
	public Set<Key<Car>> loadBookmarks() throws RemoteServiceFailureException {
		return bookmarks;
	}

//	@Override
//	public Result<List<Car>> loadBuying(String cursor, int limit, SortType sort) throws RemoteServiceFailureException {
//		return new Result<List<Car>>(new ArrayList<Car>(), null);
//	}
//
//	@Override
//	public Result<List<Car>> loadBuying(String accountId, String cursor, int limit) throws RemoteServiceFailureException {
//		return loadBuying(cursor, limit, SortType.NONE);
//	}

	@Override
	public Car loadCar(long arg0) throws RemoteServiceFailureException {
		return cars.get(0);
	}

	@Override
	public MobResult loadCars(int offset, int limit, Key<Type> type, List<Key<Make>> makes, List<Key<Model>> models,
			PriceType price, MileageType mileage, ConditionType condition, SortType sort, String postal,
			int distanceIndex) throws RemoteServiceFailureException {

		return new MobResult(new ArrayList<Type>(), new ArrayList<Make>(), new ArrayList<Car>(cars), new ArrayList<Model>());
	}

	@Override
	public MobResult loadCars(int arg0, int arg1, List<Key<Type>> arg2, List<Key<Make>> arg3, List<Key<Model>> arg4,
			PriceType arg5, MileageType arg6, ConditionType arg7, String arg8, int arg9, SortType arg10)
			throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public Map<FieldCar, List<?>> loadEdits() throws RemoteServiceFailureException {
		// TODO: what edits do we provide here?
		return new HashMap<FieldCar, List<?>>();
	}

	@Override
	public Result<List<Tuple<Marker, Car>>> loadInventory(String accountId, String cursor, int limit, SortType sort,
			MarkerType type) throws RemoteServiceFailureException {
		return loadInventory(cursor, limit, sort, type);
	}
	
	@Override
	public Result<List<Tuple<Marker, Car>>> loadInventory(String cursor, int limit, SortType sort, MarkerType type)
			throws RemoteServiceFailureException {
		return new Result<List<Tuple<Marker, Car>>>(new ArrayList<Tuple<Marker, Car>>(), null);
	}
	
	@Override
	public Location loadLocation(String arg0) throws RemoteServiceFailureException {
		delay();
		if (arg0.equals("99999")) {
			Location location = new Location("78240", 0, 0);
			location.city = "San Antonio";
			location.state = new Key<State>(State.class, "TX");
			return location;
		} else
			throw new InvalidLocationException();
	}

	private void delay() {
		try {
			Thread.sleep(2000);
		} catch (Exception e) {

		}
	}

	@Override
	public Tuple<Long, Integer> loadCount() throws RemoteServiceFailureException {
		// not implemented
		return new Tuple<Long, Integer>(System.currentTimeMillis(), 5);
	}
	
//	@Override
//	public List<Location> loadLocations(String zip, int distance) throws RemoteServiceFailureException {
//		// not implemented
//		return null;
//	}

	@Override
	public List<Make> loadMakes(boolean filterEmpty) throws RemoteServiceFailureException {
		return Arrays.asList(make);
	}

	@Override
	public Result<List<Message>> loadMessages(String cursor, int limit, MessageType type, Long carId)
			throws RemoteServiceFailureException {
		return new Result<List<Message>>(messages, null);
	}

	@Override
	public Model loadModel(String model) throws RemoteServiceFailureException {
		return null;
	}

	@Override
	public List<Model> loadModels(String make) throws RemoteServiceFailureException {
		return Arrays.asList(model);
	}

	@Override
	public Result<List<Car>> loadPending(String cursor, int limit) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public Marker loadPrice(long carId) throws RemoteServiceFailureException {
		return null;
	}
	
//	@Override
//	public Result<List<Car>> loadSelling(String cursor, int limit, SortType sort) throws RemoteServiceFailureException {
//		return new Result<List<Car>>(cars, null);
//	}
//
//	@Override
//	public Result<List<Car>> loadSelling(String accountId, String cursor, int limit) throws RemoteServiceFailureException {
//		return loadSelling(cursor, limit, SortType.NONE);
//	}

	@Override
	public List<Type> loadTypes() throws RemoteServiceFailureException {
		// not implemented
		return new ArrayList<Type>();
	}

	@Override
	public void mark(long arg0, MessageState arg1, boolean arg2) throws RemoteServiceFailureException {
			for (Message message : messages)
				if (message.id == arg0)
					message.setState(arg1, arg2);
	}
	
	@Override
	public void removeCar(long carId) throws RemoteServiceFailureException {
		// not implemented
	}

	@Override
	public void removeMessages(List<Long> messageIds) throws RemoteServiceFailureException {
		for (Iterator<Message> it = messages.iterator(); it.hasNext();)
			if (messageIds.contains(it.next().id))
				it.remove();
	}

	@Override
	public Model renameModel(String modelId, String make, String model) throws RemoteServiceFailureException {
		// not implemented
		return null;
	}

	@Override
	public void reply(long messageId, String reply) throws RemoteServiceFailureException {
		// Message msg = get(original);
		Message original = messages.get((int) messageId - 1);
		original.setState(MessageState.REPLIED, true);
		if (original == null || original.sender == null)
			throw new InvalidRecipientException();

		// check respond-delete and manage (what about duplicate messages?)
		if (original.responseTo != null)
			messages.remove(original.responseTo.intValue() - 1);
		// drop(original.responseTo);

		// need to get account to ensure we get the latest name available
		Account account = new Account();
		account.id = "none";
		account.name = "thingy";
		//Message message = new Message(account, original);

		// set the message
		// message.message = reply + "\r\n\r\n" + original.senderName +
		// " wrote:"
		// + "\r\n" + original.message;

		// save message
		// save(message);
		// message
	}

	@Override
	public void removeAlert(long carId) throws RemoteServiceFailureException {
		// not implemented	
	}
	
	@Override
	public void addBookmark(long arg0) throws RemoteServiceFailureException {
		bookmarks.add(new Key<Car>(Car.class, arg0));
	}
	
	@Override
	public void removeBookmark(long arg0) throws RemoteServiceFailureException {
		bookmarks.remove(new Key<Car>(Car.class, arg0));
	}
	
	@Override
	public void suggest(String arg0, String arg1, String arg2, double arg3, double arg4) {
		// not implemented
	}
	
	@Override
	public void updateAccount(Map<FieldAccount, Serializable> fields) throws RemoteServiceFailureException {
		for (Entry<FieldAccount, Serializable> entry : fields.entrySet())
			switch (entry.getKey()) {
			case ADDRESS:
				account.address = DataUtil.getNullString((String) entry.getValue());
				break;
			case PHONE:
				account.phone = DataUtil.getNullString((String) entry.getValue());
				break;
			case DEALER:
				account.dealer = (Boolean) entry.getValue();
				break;
			case EMAIL:
				account.email = (String) entry.getValue();
				break;
			case EML_SHOW:
				account.showEmail = (Boolean) entry.getValue();
				break;
//			case NOTIFY:
//				account.notify = (Boolean) entry.getValue();
//				break;
			case LOCATION:
				String location = DataUtil.getNullString((String) entry.getValue());

				if (location == null)
					account.postal = null;
				else
					account.postal = location;//new Key<Location>(Location.class, location);
				break;
			case NAME:
				String name = DataUtil.getNullString((String) entry.getValue());
				if (name == null)
					throw new IncompleteException();
				else
					account.name = name;
				break;
			case URL:
				account.url = (String) entry.getValue();
				break;
			default:
				break;
			}
	}

	@Override
	public void updateAd(Map<Serializable, FieldAd> fields) throws RemoteServiceFailureException {
		// not implemented
	}

	@Override
	public void updateCar(Map<FieldCar, Serializable> fields) throws RemoteServiceFailureException {
		// not implemented
	}

	@Override
	public void updateModel(Map<FieldModel, Serializable> fields) throws RemoteServiceFailureException {
		// not implemented
	}

	@Override
	public void upgrade(long arg0) throws RemoteServiceFailureException {
		// not implemented
	}

	@Override
	public void updateAlert(long arg1, int arg2) throws RemoteServiceFailureException {
		// not implemented
	}

	@Override
	public List<Key<CLState>> loadStates() throws RemoteServiceFailureException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Key<CLLocale>> loadLocales(Key<CLState> state) throws RemoteServiceFailureException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void postCg(long carId, Key<CLLocale> locale) throws RemoteServiceFailureException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Account> loadDealers(int offset, int limit, String postal, int distance, SortType sort)
			throws RemoteServiceFailureException {
		// TODO Auto-generated method stub
		return null;
	}
}