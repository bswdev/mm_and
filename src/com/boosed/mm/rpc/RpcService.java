package com.boosed.mm.rpc;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ChoiceFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.boosed.mm.R;
import com.boosed.mm.activity.Inventory;
import com.boosed.mm.activity.Messages;
import com.boosed.mm.activity.task.RpcTask;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.HasKey;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.MobResult;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.exception.ResourceLimitException;
import com.boosed.mm.shared.exception.SessionTimeoutException;
import com.boosed.mm.shared.service.MobimotosRpcService;
import com.boosed.mm.shared.util.DataUtil;
import com.boosed.mm.sync.SyncAdapter;
import com.boosed.mm.ui.image.ImageLoader;
import com.gdevelop.gwt.syncrpc.SessionManager;
import com.gdevelop.gwt.syncrpc.SyncProxy;
import com.googlecode.objectify.Key;

/**
 * Background service that manages the state of the entire app.
 * 
 * @author dsumera
 */
public class RpcService extends Service {

	/* account manager */
	private AccountManager am;

	// /* drawable manager */
	// public DrawableManager dm = new DrawableManager();

	/** notification manager */
	private NotificationManager nm;

	/** message whats for handling un/registering */
	public static final int MSG_REGISTER_CLIENT = 100;
	public static final int MSG_UNREGISTER_CLIENT = 101;
	public static final int MSG_RPC_CONNECT = 102;
	public static final int MSG_RPC_DISCONNECT = 103;
	public static final int MSG_RPC_ONLINE = 104;
	public static final int MSG_RPC_OFFLINE = 105;
	public static final int MSG_RPC_AUTOLOGIN = 106;
	public static final int MSG_RPC_LOGINFAIL = 107;
	public static final int MSG_ACCTS = 108;
	public static final int MSG_ACCTS_NONE = 109;

	// /* action to sign in again */
	// public static final String ACTION_SIGNIN = "signin";

	/** preference keys */
	// public static final String PREF_DEALER = "dealer";
	public static final String PREF_INVENTORY = "inventory";
	public static final String PREF_MESSAGE = "message";
	public static final String PREF_FILTER = "filter";
	public static final String PREF_SORT_INV = "sort";
	public static final String PREF_PRICE = "price";
	public static final String PREF_MILE = "mileage";
	public static final String PREF_COND = "condition";
	public static final String PREF_POST_SRCH = "postal";
	public static final String PREF_NAME_SRCH = "name";
	public static final String PREF_DIST_SRCH = "distance";
	public static final String PREF_POST_DLR = "postal_d";
	public static final String PREF_NAME_DLR = "name_d";
	public static final String PREF_DIST_DLR = "distance_d";
	public static final String PREF_SORT_DLR = "sort_d";
	public static final String PREF_ACCT = "account";
	public static final String PREF_CL_STATE = "state";
	public static final String PREF_CL_LOCALE = "locale";

	/** notification ids */
	public static final int NOTE_MSG = 200;

	/* ui update message id */
	// public static final int MSG_UI_UPDATE = 300;

	/* ID for all operations besides ImType */
	public static final int GENERAL_OPS = 1000;

	// /* preference strings */
	// public static final String PREF_USER = "username";
	// public static final String PREF_PASS = "password";
	// public static final String PREF_LOGIN = "login";

	/** format for date */
	public static final String DATE_FORMAT = "M/d/yy h:mm AA";

	/** sync period (30 min) */
	private static final int SYNC_PERIOD = 1800000;

	/** host name */
	// private static String HOST;

	/** url of server */
	private static String URL;

	/** preferences */
	private SharedPreferences settings;

	/** list of clients attached to the service */
	private final Map<Integer, List<Messenger>> clients = new ConcurrentHashMap<Integer, List<Messenger>>();

	/*
	 * Handler of incoming messages from clients (i.e., registration, all else
	 * done through interface)
	 */
	private class IncomingHandler extends Handler {
		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				addClient(msg.replyTo, msg.arg1);
				break;
			case MSG_UNREGISTER_CLIENT:
				removeClient(msg.replyTo, msg.arg1);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	/* local messenger instance for handling messages to service */
	private Messenger messenger = new Messenger(new IncomingHandler());

	public class LocalBinder extends Binder {

		private IBinder binder;

		public LocalBinder(/* AbstractThreadedSyncAdapter adapter */IBinder binder) {
			this.binder = /* (Binder) adapter.getSyncAdapterBinder() */binder;
		}

		public RpcService getService() {
			return RpcService.this;
		}

		public Messenger getMessenger() {
			return messenger;
		}

		public IBinder getBinder() {
			return binder;
		}
	}

	/** local binder to return to binding contexts */
	private IBinder binder;// = new LocalBinder();

	/**
	 * online & connected status; immolated means service has been destroyed, do
	 * not sustain any existing connectivity
	 * 
	 * init is whether first load/search has been executed
	 */
	public boolean connected = false, online = false, immolated = false, init = false;

	// LIFECYCLE METHODS

	private final Object sSyncAdapterLock = new Object();

	private SyncAdapter sSyncAdapter = null;

	/** authority by which sync commands are issued */
	private static String authority;

	@Override
	public void onCreate() {
		super.onCreate();

		// prepare sync adapter
		synchronized (sSyncAdapterLock) {
			if (sSyncAdapter == null) {
				sSyncAdapter = new SyncAdapter(getApplicationContext(), true, this);
				binder = new LocalBinder(sSyncAdapter.getSyncAdapterBinder());
				authority = getString(R.string.sync_authority);
			}
		}

		// Log.i("chat", "service is being created");

		// initialize URL strings
		// HOST = getString(R.string.rpc_url);
		// URL = "https://" + HOST;
		URL = getString(R.string.rpc_url);

		// init account manager
		// am = (AccountManager) getSystemService(ACCOUNT_SERVICE);
		am = AccountManager.get(this);

		// init notification manager
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		// // init client map
		// for (int i = IM_TYPES_LEN; --i > -1;)
		// clients.put(i, new ArrayList<Messenger>());
		clients.put(GENERAL_OPS, new ArrayList<Messenger>());

		// init settings
		settings = PreferenceManager.getDefaultSharedPreferences(this);

		// check for online status
		pollOnline();
	}

	// INTERFACES

	// END INTERFACES

	// /**
	// * BaseActivity accounts for lifecycle of the service by stopping it if
	// not
	// * connected (regardless of bound status)
	// */
	// @Override
	// public void onStart(Intent intent, int startId) {
	// super.onStart(intent, startId);
	//
	// Log.i("chat", "the service is also started");
	// // TODO: handle switch over between 3G/WIFI/EDGE
	// }

	@Override
	public void onDestroy() {
		super.onDestroy();

		// stop the timer
		timer.cancel();

		// clear caches
		messages.clear();
		search.clear();
		inventory.clear();

		// logout
		if (connected)
			new Thread() {
				public void run() {
					logout();
				}
			}.start();

		// don't clear the clients, just each array therein
		for (List<Messenger> clientz : clients.values())
			clientz.clear();

		// // cancel the notifications for transient objects (mail is
		// persistent)
		// nm.cancel(ImType.MESSAGE.ordinal());
		// nm.cancel(ImType.REQUEST.ordinal());
		// nm.cancel(ImType.INVITE.ordinal());

		// message
		immolated = true;
		sSyncAdapter = null;
		binder = null;
		Log.i("mm", "the rpc service has stopped");

		// destroy all the images
		ImageLoader.getInstance(this).clear();
	}

	// END LIFECYCLE

	// CALLBACK METHODS

	@Override
	public IBinder onBind(Intent intent) {
		String action = intent.getAction();
		// Log.w("chat", "getting the binder... it is " + intent.getAction());

		// hacked to return the appropriate IBinder depending on intent
		if (action == null)
			// return local binder that wraps sync adapter binder
			return binder;
		else if (action.equals("android.content.SyncAdapter"))
			// return the wrapped sync adapter binder
			return ((LocalBinder) binder).getBinder();
		else
			return binder;
	}

	// END CALLBACK

	// PUBLIC METHODS

	// reset the polling for checking online status
	public void resetPoll() {
		// reset the period
		period = 0;

		// able to cancel task, start another one
		if (pollTask != null && pollTask.cancel())
			pollOnline();

		// if task is not cancelable, it is running and will call pollOnline if
		// connection unsuccessful
	}

	public void onConnect() {
		// this service has already expired, discontinue connection activities
		if (immolated) {
			Log.w("mm", "don't continue connection, we are stopped/immolated");

			// logout and return
			new Thread() {
				public void run() {
					logout();
				}
			}.start();

			return;
		}

		// broadcast connected (so activities can prepare)
		connected = true;
		broadcast(MSG_RPC_CONNECT);
	}

	public void onDisconnect() {
		// report disconnection
		connected = false;
		broadcast(MSG_RPC_DISCONNECT);

		// // reset all notifications (maybe except for MAIL types?)
		// resetNotes((1 << IM_TYPES_LEN) - 1);
	}

	/**
	 * This is already called inside of a thread.
	 */
	public void onOnline() {
		// in/itialize services
		if (service == null)
			service = SyncProxy.newProxyInstance(MobimotosRpcService.class, URL + "/mm/",
					getString(R.string.rpc_path), getString(R.string.rpc_policy_autobot));
		// service = new DummyMobRpcService();

		// set flag
		online = true;
		broadcast(MSG_RPC_ONLINE);
	}

	public void onOffline() {
		onDisconnect();

		online = false;
		broadcast(MSG_RPC_OFFLINE);

		// start a timer task until servers come back online
		pollOnline();
	}

	public void onUpdate(Account account) {
		try {
			Tuple<Long, Integer> update = service.loadCount();

			if (update.a != updateTime) {
				// process new update
				updateTime = update.a;
				updateCount = update.b;

				if (updateCount > 0)
					// there are unreads, notify user
					doNotify(updateCount);
			}
		} catch (SessionTimeoutException e) {
			// cancel sync task if not null
			if (syncTask != null)
				syncTask.cancel();

			// timed out, cancel syncs
			ContentResolver.cancelSync(account, authority);

			Log.w("mm", "syncing has been cancelled");
		} catch (RemoteServiceFailureException e) {
			// general catch
			e.printStackTrace();
		}
	}

	/**
	 * Resets the notifications and counts.
	 * 
	 * @param ops
	 */
	public void resetNotes(int ops) {
		// for (int i = IM_TYPES_LEN; --i > -1;)
		// if ((ops & (1 << i)) > 0) {
		// // cancel the notification
		// nm.cancel(i);
		//
		// // reset the count
		// notes[i] = 0;
		// }
	}

	// END PUBLIC

	// PRIVATE METHODS

	/**
	 * Bin clients according to their operations of interest.
	 */
	private void addClient(Messenger messenger, int interestOps) {
		// add to the general ops bin
		clients.get(GENERAL_OPS).add(messenger);

		// for (int i = IM_TYPES_LEN; --i > -1;)
		// if ((interestOps & (1 << i)) > 0)
		// clients.get(i).add(messenger);

		// inform the client of our current status
		android.os.Message msg = android.os.Message.obtain();

		// initial check for determining connectivity
		if (connected)
			// everything is running
			msg.what = MSG_RPC_CONNECT;
		else
			// indicate whether online or offline
			msg.what = online ? MSG_RPC_ONLINE : MSG_RPC_OFFLINE;

		try {
			messenger.send(msg);
		} catch (RemoteException e) {
			// failed send, remove the client
			removeClient(messenger, interestOps);
		}
		// if (online)
		// // online but not connected (could be disconnected but
		// // don't invoke this on initial bind)
		// msg.what = MSG_RPC_ONLINE;
		// else
		// // service is offline for the moment
		// onOffline();
		// }
	}

	private void removeClient(Messenger messenger, int interestOps) {
		// remove from general ops bin
		clients.get(GENERAL_OPS).remove(messenger);

		// for (int i = IM_TYPES_LEN; --i > -1;)
		// if ((interestOps & (1 << i)) > 0)
		// clients.get(i).remove(messenger);
	}

	/**
	 * Broadcast a message back to all relevant clients.
	 * 
	 * @param what
	 *            the <code>MSG_XXX</code> being sent to all activities.
	 */
	private void broadcast(int what) {
		List<Messenger> clientz;

		// should we use the notification service
		// boolean notify = false;

		// Log.i("chat", "broadcasting: " + what);
		// Message msg = Message.obtain(null, what, 0, 0);

		// "what" is not an ImType, send to all clients
		// if (what >= IM_TYPES_LEN)
		// broadcast to all clients
		// Set<Messenger> all = new HashSet<Messenger>();
		// for (Map.Entry<Integer, List<Messenger>> entry :
		// clients.entrySet())
		// all.addAll(entry.getValue());
		// clientz = Arrays.asList(all.toArray(new Messenger[0]));
		clientz = clients.get(GENERAL_OPS);
		// else {
		// // broadcast to clients of interest and send a notification
		// clientz = clients.get(what);
		// // set the object on the message
		// msg.obj = ims[0];
		// // notify if no listeners are available
		// notify = true;
		// }
		for (int i = clientz.size(); --i > -1;) {
			try {
				// get a new message for each client
				clientz.get(i).send(/* msg */android.os.Message.obtain(null, what, 0, 0));
				// notify = false;
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				clientz.remove(i);
			}
		}

		// send notification if no clients available to receive broadcast &
		// service not stopped
		// if (notify && !immolated)
		// doNotify(ims[0]);
	}

	// private int[] notes = new int[0];
	private MessageFormat format = null;

	// private ChoiceFormat choice = ;

	public void doNotify(int count) {
		
		// Log.i("chat", "sending a notification");
		Intent intent = new Intent(this, Messages.class);

		if (format == null) {
			// format is "<number> <choice format>"
			format = new MessageFormat("{0} {1}");
			format.setFormatByArgumentIndex(1, new ChoiceFormat(getString(R.string.note_format)));
		}

		// notification.number = missed;

		// The PendingIntent to launch our activity if the user selects this
		// notification
		// Log.i("rpc", "creating intent with index " + intent.hashCode());

		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pending = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// create the notification, set the icon, scrolling text and timestamp
		Notification notification = new Notification(R.drawable.ic_stat_message,
				getString(R.string.note_ticker), System.currentTimeMillis());
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.when = System.currentTimeMillis();
		notification.number = count;// = ++notes[/*
													// im.getType().ordinal()
													// */0];

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, getString(R.string.app_name), format
				.format(new Object[] { count, count }), pending);
		// notification.setLatestEventInfo(this, im.getType().toString() + " ("
		// + notification.number + ")",
		// "last received " + DateFormat.format(RpcService.DATE_FORMAT,
		// im.getTime()), pending);
		// Send the notification.
		// We use a string id because it is a unique number. We use it later
		// to cancel.
		nm.notify(15, notification);
	}

	/**
	 * Mark message as read & suppress the notifications.
	 */
	public void markRead(long messageId) throws RemoteServiceFailureException {
		service.mark(messageId, MessageState.READ, true);
		nm.cancel(15);
	}

	// RPC METHODS

	/** gwt rpc service */
	public MobimotosRpcService service;

	/** fetch/page limit for all retrievals */
	private static final int FETCH_LIMIT = 20;

	// query data
	/** master list of makes */
	private List<Make> masterMakes = new ArrayList<Make>();

	/** map of types to checked status */
	private Map<Type, Boolean> facetTypes = new TreeMap<Type, Boolean>();

	/** map of makes to checked status */
	private Map<Make, Boolean> facetMakes = new TreeMap<Make, Boolean>();

	/** map of models to checked status */
	private Map<Model, Boolean> facetModels = new TreeMap<Model, Boolean>();

	/** map of check arrays, facet (i.e., type, make, model) to checked state */
	private Map<Integer, boolean[]> checks = new HashMap<Integer, boolean[]>();

	// result data
	/** search result data */
	private List<Car> search = new ArrayList<Car>();
	/** offset for search items (multiple of FETCH_LIMIT) */
	private int searchOffset = 0;

	/** dealers */
	private List<com.boosed.mm.shared.db.Account> dealers = new ArrayList<com.boosed.mm.shared.db.Account>();
	private int dealerOffset = 0;

	/** inventory data */
	private List<Tuple<Marker, Car>> inventory = new ArrayList<Tuple<Marker, Car>>();
	/** cursor for inventory items */
	private String inventoryCursor = null;

	/** message data */
	private List<Message> messages = new ArrayList<Message>();
	/** cursor for message items */
	private String messageCursor = null;

	/** pending data uses search */
	private String pendingCursor = null;

	/** bookmarks */
	private Set<Key<Car>> bookmarks = new HashSet<Key<Car>>();

	// filter
	// public PriceType price = PriceType.ANY; // get from settings
	// public MileageType mileage = MileageType.ANY; // get from settings
	// public ConditionType condition = ConditionType.ANY; // get from settings

	// /** sort for search */
	// public SortType filter = SortType.values()[settings.getInt(PREF_FILTER,
	// 0)];

	// /** sort for inventory */
	// public SortType sort = SortType.values()[settings.getInt(PREF_SORT, 0)];

	// public String postal = null; // get from settings
	// public String postalName = null; // get from settings
	// public int distance = 9; // get from settings

	/** account id */
	public String accountId = null;

	/** update time */
	private long updateTime = 0;

	/** update count */
	private int updateCount = 0;

	/**
	 * Get the checkbox values associated with the given dialog (for selection).
	 * 
	 * @param id
	 */
	public boolean[] getChecks(int id) {
		boolean[] rv = null;
		int i = 0;

		switch (id) {
		case R.id.types:
			// create the boolean[]
			rv = new boolean[facetTypes.size()];
			for (Boolean b : facetTypes.values())
				rv[i++] = b.booleanValue();
			break;
		case R.id.makes:
			rv = new boolean[facetMakes.size()];
			for (Boolean b : facetMakes.values())
				rv[i++] = b.booleanValue();
			break;
		case R.id.models:
			rv = new boolean[facetModels.size()];
			for (Boolean b : facetModels.values())
				rv[i++] = b.booleanValue();
			break;
		default:
			break;
		}

		checks.put(id, rv);

		return rv;
	}

	/**
	 * Generate values used to populate multichoice dialogs.
	 * 
	 * @param id
	 * @return
	 */
	public CharSequence[] getChoices(int id) {
		String[] rv = null;
		int i = 0;

		switch (id) {
		case R.id.types:
			rv = new String[facetTypes.size() + 1];
			rv[i++] = "any";
			for (Type t : facetTypes.keySet())
				rv[i++] = t.getCount();
			return rv;
		case R.id.makes:
			rv = new String[facetMakes.size()];
			// retrieve the one selected type criteria (may support more in
			// future)
			Key<Type> type = getSingleSelectedKey(facetTypes);
			for (Make m : facetMakes.keySet())
				rv[i++] = m.getCount(type);
			return rv;
		case R.id.models:
			rv = new String[facetModels.size()];
			for (Model m : facetModels.keySet())
				rv[i++] = m.getCount();
			return rv;
		default:
			return null;
		}
	}

	/**
	 * Initialize queries with the latest values from the server.
	 */
	public void initQuery() throws RemoteServiceFailureException {
		// load the master make set
		masterMakes = service.loadMakes(true);

		// sets all the facets according to user selection
		getType();

		// // set makes query againt the master set
		// setQuery(makes, master);
		//
		// // set types query against the master set
		// List<Make> values = getSelected(makes);
		// setQuery(types, DataUtil.getTypes(values.isEmpty() ? master :
		// values));
	}

	/**
	 * Load all the bookmarked <code>Key&lt;Car&gt;</code>s.
	 * 
	 * @throws RemoteServiceFailureException
	 */
	public void loadBookmarks() throws RemoteServiceFailureException {
		if (bookmarks.isEmpty()) {
			// Log.w("chat", "loading bookmarks");
			bookmarks = service.loadBookmarks();

			// add some dummy variable that will never be removed
			bookmarks.add(new Key<Car>(Car.class, -1L));
		}
	}

	// /**
	// * Load <code>List</code> of <code>Car</code>s given the selection
	// criteria.
	// *
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// public void loadCars() throws RemoteServiceFailureException {
	// // Key<Type> type = getSingleSelected(types);
	// //
	// // // init make facet; qualify against type
	// // setQuery(makes, DataUtil.getMakes(type, master));
	// //
	// // // init types facet; qualify against selected makes
	// // List<Make> values = getSelected(makes);
	// // setQuery(types, DataUtil.getTypes(values.isEmpty() ? master :
	// // values));
	//
	// // get all the filters
	// PriceType price = PriceType.values()[settings.getInt(PREF_PRICE, 0)];
	// MileageType mileage = MileageType.values()[settings.getInt(PREF_MILE,
	// 0)];
	// ConditionType condition =
	// ConditionType.values()[settings.getInt(PREF_COND, 0)];
	// SortType filter = SortType.values()[settings.getInt(PREF_FILTER, 0)];
	// String postal = settings.getString(PREF_POST, null);
	// int distance = settings.getInt(PREF_DIST, 9);
	//
	// // get the result
	// MobResult result = service.loadCars(0, FETCH_LIMIT, getType(),
	// getSelectedKeys(facetMakes),
	// getSelectedKeys(facetModels), price, mileage, condition, filter, postal,
	// distance);
	//
	// // set model query facet
	// setQuery(facetModels, result.models);
	//
	// // update the cars
	// search.clear();
	// search.addAll(result.cars);
	//
	// // set the new offset
	// searchOffset = result.cars.isEmpty() ? -1 : 1;
	// }

	/**
	 * Whenever scrolling of a list is required to show more data elements for
	 * the search results.
	 * 
	 * @throws RemoteServiceFailureException
	 */
	public int loadCars(boolean scrolling) throws RemoteServiceFailureException {
		// user issued first search, set to true
		init = true;

		if (scrolling && searchOffset == 0)
			// all search results have been retrieved
			return searchOffset;
		else if (!scrolling) {
			// fresh load
			search.clear();
			searchOffset = 0;
		}

		// get all the filters
		PriceType price = PriceType.values()[settings.getInt(PREF_PRICE, 0)];
		MileageType mileage = MileageType.values()[settings.getInt(PREF_MILE, 0)];
		ConditionType condition = ConditionType.values()[settings.getInt(PREF_COND, 0)];
		SortType filter = SortType.values()[settings.getInt(PREF_FILTER, 0)];
		String postal = settings.getString(PREF_POST_SRCH, null);
		int distance = settings.getInt(PREF_DIST_SRCH, 0);

		// load the current page
		MobResult result = service.loadCars(searchOffset * FETCH_LIMIT, FETCH_LIMIT, getType(),
				getSelectedKeys(facetMakes), getSelectedKeys(facetModels), price, mileage,
				condition, filter, postal, distance);

		// update the search

		// if new load, update viewable models query
		if (!scrolling)
			// set model query facet
			setQuery(facetModels, result.models);

		// remove the existing matches
		result.cars.removeAll(search);

		// add result to existing result set
		search.addAll(search.size(), result.cars);

		// set the new offset (0 if empty)
		searchOffset += result.cars.isEmpty() ? -searchOffset : 1;

		// return offset
		return searchOffset;
	}

	/**
	 * Whenever scrolling of a list is required to show more data elements for
	 * the search results.
	 * 
	 * @throws RemoteServiceFailureException
	 */
	public int loadDealers(boolean scrolling) throws RemoteServiceFailureException {
		if (scrolling && dealerOffset == 0)
			// all dealer results have been retrieved
			return dealerOffset;
		else if (!scrolling) {
			// fresh load
			dealers.clear();
			dealerOffset = 0;
		}

		// get all the filters
		String postal = settings.getString(PREF_POST_DLR, null);
		int distance = settings.getInt(PREF_DIST_DLR, 0);
		final SortType sort = SortType.values()[settings.getInt(PREF_SORT_DLR, SortType.NAME
				.ordinal())];

		// load the current page
		List<com.boosed.mm.shared.db.Account> result = service.loadDealers(dealerOffset
				* FETCH_LIMIT, FETCH_LIMIT, postal, distance, sort);

		// remove the existing matches
		result.removeAll(dealers);

		// add result to existing result set
		dealers.addAll(dealers.size(), result);

		// set the new offset (0 if empty)
		dealerOffset += result.isEmpty() ? -dealerOffset : 1;

		// return offset
		return dealerOffset;
	}

	/**
	 * Load <code>List</code> of <code>Car</code>s user is buying or selling.
	 * 
	 * @return inventory cursor
	 * @throws RemoteServiceFailureException
	 */
	public String loadInventory(boolean scrolling, String dealer)
			throws RemoteServiceFailureException {
		if (scrolling && inventoryCursor == null)
			// reached the end
			return inventoryCursor;
		else if (!scrolling) {
			// fresh start
			inventory.clear();
			inventoryCursor = null;
		}

		// get the inventory according to the preference
		// result
		// Result<List<Tuple<Marker, Car>>> result;

		// init paremeters
		final SortType sort = SortType.values()[settings.getInt(PREF_SORT_INV, 0)];

		// determine whether to perform account or dealer fetch (if dealer not
		// specified, use bookmarks)
		int invPref = settings.getInt(PREF_INVENTORY, Inventory.MODE_BMK);

		// change inventory preference if no dealer specified
		// Log.w("mm", "the preference is " + invPref);

		// if invPref is 3, do a dealer lookup
		Result<List<Tuple<Marker, Car>>> result = invPref == Inventory.MODE_DLR ? service
				.loadInventory(dealer, inventoryCursor, FETCH_LIMIT, sort, MarkerType.STOCK)
				: service.loadInventory(inventoryCursor, FETCH_LIMIT, sort,
						MarkerType.values()[invPref]);

		// // message category is determined in the settings
		// switch (settings.getInt(PREF_INVENTORY, 0)) {
		// case 0:
		// result = /* service.loadBuying(inventoryCursor, FETCH_LIMIT, sort);
		// */service.loadInventory(inventoryCursor,
		// FETCH_LIMIT, sort, MarkerType.BOOKMARK);
		// break;
		// case 1:
		// result = service.loadInventory(inventoryCursor, FETCH_LIMIT, sort,
		// MarkerType.STOCK);
		// break;
		// case 2:
		// result = service.loadInventory(inventoryCursor, FETCH_LIMIT, sort,
		// MarkerType.PRICE);
		// default:
		// // do nothing
		// return inventoryCursor;
		// }

		// update the inventory

		// remove the matches
		result.result.removeAll(inventory);

		// add new results to inventory
		inventory.addAll(inventory.size(), result.result);

		// set the new offset
		inventoryCursor = result.cursor;// result.isEmpty() ? -1 : 1;

		// return cursor
		return inventoryCursor;
	}

	// /**
	// * Whenever scrolling of a list is required to show more data elements for
	// * the inventory results.
	// *
	// * @throws RemoteServiceFailureException
	// */
	// public void scrollInventory() throws RemoteServiceFailureException {
	// // all inventory has been retrieved
	// if (inventoryCursor == null)
	// return;
	//
	// // get the inventory according to the preference
	// Result<List<Car>> result = settings.getBoolean(PREF_INVENTORY, false) ?
	// service.loadSelling(inventoryCursor,
	// FETCH_LIMIT) : service.loadBuying(inventoryCursor, FETCH_LIMIT);
	//
	// // add results to current list
	// inventory.addAll(inventory.size(), result.result);
	//
	// // set the new offset
	// inventoryCursor = result.cursor;// result.isEmpty() ? -1 :
	// // (inventoryPage + 1);
	// }

	// public List<com.boosed.ml.shared.db.Message> loadMessages(Long carId)
	// throws RemoteServiceFailureException {
	// Result<List<com.boosed.ml.shared.db.Message>> result;
	//
	// switch (settings.getInt(PREF_MESSAGE, R.id.all)) {
	// case R.id.inquiries:
	// result = service.loadMessages(messageCursor, FETCH_LIMIT,
	// MessageType.INQUIRIES, carId);
	// break;
	// case R.id.responses:
	// result = service.loadMessages(messageCursor, FETCH_LIMIT,
	// MessageType.RESPONSES, carId);
	// break;
	// case R.id.all:
	// default:
	// result = service.loadMessages(messageCursor, FETCH_LIMIT,
	// MessageType.ALL, carId);
	// break;
	// }
	//
	// // set the cursor
	// messageCursor = result.cursor;
	//
	// // return the messages
	// return result.result;
	// }

	/**
	 * Load <code>List</code> of <code>Message</code>s.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	public String loadMessages(Long carId, boolean scrolling) throws RemoteServiceFailureException {
		// remove all message notifications
		nm.cancel(15);

		// get the inventory according to the preference
		// List<Message> result = settings.getBoolean(PREF_INVENTORY, false) ?
		// service.loadSelling(0,
		// CAR_LIMIT) : service.loadBuying(0, CAR_LIMIT);

		// the manipulation of the model must occur in the UI thread!
		if (scrolling && messageCursor == null)
			// no more results
			return messageCursor;
		else if (!scrolling) {
			// fresh start
			messages.clear();
			messageCursor = null;
		}

		// result
		Result<List<Message>> result;

		// message category is determined in the settings
		// switch (settings.getInt(PREF_MESSAGE, /*R.id.all*/3)) {
		// case R.id.all:
		// result = service.loadMessages(messageCursor, FETCH_LIMIT,
		// MessageType.ALL, carId);
		// break;
		// case R.id.inquiries:
		// result = service.loadMessages(messageCursor, FETCH_LIMIT,
		// MessageType.INQUIRIES, carId);
		// break;
		// case R.id.responses:
		// result = service.loadMessages(messageCursor, FETCH_LIMIT,
		// MessageType.RESPONSES, carId);
		// break;
		// default:
		// // do nothing
		// return messageCursor;
		// }
		if (carId == null)
			// load selected message type (no reference)
			result = service.loadMessages(messageCursor, FETCH_LIMIT, MessageType.values()[settings
					.getInt(PREF_MESSAGE, /*
										 * R.id.all
										 */3)], carId);
		else
			// load inquiries for reference
			result = service.loadMessages(messageCursor, FETCH_LIMIT, MessageType.INQUIRIES, carId);

		// update the messages

		// remove the matches
		result.result.removeAll(messages);

		// add new results to messages
		messages.addAll(messages.size(), result.result);

		// set the new offset
		// messagePage = result.isEmpty() ? -1 : 1;
		messageCursor = result.cursor;

		// return cursor
		return messageCursor;
	}

	/**
	 * Load <code>List</code> of <code>Message</code>s.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	public String loadPending(boolean scrolling) throws RemoteServiceFailureException {
		if (scrolling && pendingCursor == null)
			// no more results
			return pendingCursor;
		else if (!scrolling) {
			// fresh start
			search.clear();
			pendingCursor = null;
		}

		// result
		Result<List<Car>> result = service.loadPending(pendingCursor, FETCH_LIMIT);

		// update the results

		// remove the matches
		result.result.removeAll(search);

		// add new results to pending
		search.addAll(search.size(), result.result);

		// set the new offset
		pendingCursor = result.cursor;

		// return cursor
		return pendingCursor;
	}

	/**
	 * Add or remove a bookmark to/from <code>Account</code>.
	 * 
	 * @param carId
	 * @throws RemoteServiceFailureException
	 */
	public void bookmark(long carId, boolean enabled) throws RemoteServiceFailureException {
		if (enabled) {
			// attempt to add bookmark
			service.addBookmark(carId);

			// if successful, add to local copy of bookmarks
			bookmarks.add(new Key<Car>(Car.class, carId));
		} else {
			// remove bookmark
			service.removeBookmark(carId);

			// if successful, remove from local copy of bookmarks
			bookmarks.remove(new Key<Car>(Car.class, carId));
		}
	}

	/**
	 * Remove an <code>Ad</code> given the <code>tuple</code> from the
	 * <code>ListView</code>.
	 * 
	 * @param tuple
	 * @throws RemoteServiceFailureException
	 */
	public void removeAd(Tuple<Marker, Car> tuple) throws RemoteServiceFailureException {
		// remove car from server
		service.removeCar(tuple.b.id);

		// remove car from rpc data
		inventory.remove(tuple);
		search.remove(tuple.b);
	}

	/**
	 * Remove a price <code>Marker</code> given the <code>tuple</code> from the
	 * <code>ListView</code>.
	 * 
	 * @param tuple
	 * @throws RemoteServiceFailureException
	 */
	public void removeAlert(Tuple<Marker, Car> tuple) throws RemoteServiceFailureException {
		// remove alert from server
		service.removeAlert(tuple.b.id);

		// remove marker from rpc data
		inventory.remove(tuple);
	}

	public void removeMessages(Collection<Message> messages) throws RemoteServiceFailureException {
		// remove messages from server
		List<Long> messageIds = new ArrayList<Long>();
		for (Message message : messages)
			messageIds.add(message.id);
		service.removeMessages(messageIds);

		// remove messages from rpc data
		this.messages.removeAll(messages);

		// broadcast a message update
		// broadcast(MSG_MESSAGES);
	}

	// /**
	// * Whenever scrolling of a list is required to show more data elements for
	// * the inventory results.
	// *
	// * @throws RemoteServiceFailureException
	// */
	// public void scrollMessages(Long carId) throws
	// RemoteServiceFailureException {
	// // all inventory has been retrieved
	// if (messageCursor == null)
	// return;
	//
	// Result<List<com.boosed.ml.shared.db.Message>> result;
	//
	// // get the messages according to the preference
	// switch (settings.getInt(PREF_MESSAGE, R.id.all)) {
	// case R.id.all:
	// result = service.loadMessages(messageCursor, FETCH_LIMIT,
	// MessageType.ALL, carId);
	// break;
	// case R.id.inquiries:
	// result = service.loadMessages(messageCursor, FETCH_LIMIT,
	// MessageType.INQUIRIES, carId);
	// break;
	// case R.id.responses:
	// result = service.loadMessages(messageCursor, FETCH_LIMIT,
	// MessageType.RESPONSES, carId);
	// break;
	// default:
	// // do nothing
	// return;
	// }
	//
	// // add results to current list
	// messages.addAll(messages.size(), result.result);
	//
	// // set the new offset
	// messageCursor = result.cursor;
	// //messagePage = result.isEmpty() ? -1 : (messagePage + 1);
	// }

	// /**
	// * Remove a bookmark from <code>Account</code>.
	// *
	// * @param carId
	// * @throws RemoteServiceFailureException
	// */
	// public void removeBookmark(long carId) throws
	// RemoteServiceFailureException {
	// // remove bookmark
	// service.removeBookmark(carId);
	//
	// // if successful, remove from local copy of bookmarks
	// bookmarks.remove(new Key<Car>(Car.class, carId));
	// }

	/**
	 * Delete an image associated with a user's <code>Ad</code>.
	 * 
	 * @param adId
	 * @param key
	 * @throws RemoteServiceFailureException
	 */
	public void deleteImage(long adId, String key) throws RemoteServiceFailureException {
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();

		// ad id
		fields.put(adId, FieldAd.ID_AD);

		// blobkey to delete
		fields.put(key, FieldAd.IMG_DELETE);

		// update
		service.updateAd(fields);
	}

	/**
	 * Set the primary image against the user's <code>Ad</code>.
	 * 
	 * @param carId
	 * @param adId
	 * @param key
	 * @throws RemoteServiceFailureException
	 */
	public void setPrimary(long carId, long adId, String key) throws RemoteServiceFailureException {
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();

		// ad id
		fields.put(adId, FieldAd.ID_AD);

		// car id
		fields.put(carId, FieldAd.ID_CAR);

		// the blobkey image to set
		fields.put(key, FieldAd.IMG_CAR);

		// update
		service.updateAd(fields);
	}

	/**
	 * Submit an image.
	 * 
	 * @param url
	 * @param adId
	 * @param file
	 * @return the <code>Ad</code> with current image data
	 */
	public Ad uploadFile(String url, long adId, Uri file) {
		String boundary = "*****************************************";
		String newLine = "\r\n";
		int maxBufferSize = 4096;

		try {
			// set cookies through sync proxy
			SessionManager manager = SyncProxy.getDefaultSessionManager();
			HttpURLConnection con = manager.openConnection(new URL(url));
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Connection", "Keep-Alive");
			con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			DataOutputStream dos = new DataOutputStream(con.getOutputStream());

			// write the ad id
			dos.writeBytes("--" + boundary + newLine);
			dos
					.writeBytes("Content-Disposition: form-data; name=\"ad\"" + newLine + newLine
							+ adId);
			dos.writeBytes(newLine);

			// write the uri
			dos.writeBytes("--" + boundary + newLine);
			dos.writeBytes("Content-Disposition: form-data; " + "name=\"pic\"; filename=\"" + ""
					+ "\"" + newLine + newLine);
			InputStream fis = getContentResolver().openInputStream(file);
			int bytesAvailable = fis.available();
			int bufferSize = Math.min(bytesAvailable, maxBufferSize);
			byte[] buffer = new byte[bufferSize];
			int bytesRead = fis.read(buffer, 0, bufferSize);
			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fis.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fis.read(buffer, 0, bufferSize);
			}
			dos.writeBytes(newLine);
			dos.writeBytes("--" + boundary + "--" + newLine);
			fis.close();

			// flush the output
			dos.flush();

			// store the cookies from connection
			manager.getCookieManager().storeCookies(con);

			// read the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));

			// detect the fail string
			String line;
			while ((line = rd.readLine()) != null)
				if (line.contains("failed")) {
					// close streams
					dos.close();
					rd.close();

					// throw an exception for the photo
					throw new ResourceLimitException();
				}

			// ret.content += line + "\r\n";
			// // get headers
			// Map<String, List<String>> headers = con.getHeaderFields();
			// Set<Entry<String, List<String>>> hKeys = headers.entrySet();
			// for (Iterator<Entry<String, List<String>>> i = hKeys.iterator();
			// i
			// .hasNext();) {
			// Entry<String, List<String>> m = i.next();
			// Log.w("HEADER_KEY", m.getKey() + "");
			// ret.headers.put(m.getKey(), m.getValue().toString());
			// if (m.getKey().equals("set-cookie"))
			// ret.cookies.put(m.getKey(), m.getValue().toString());
			// }
			dos.close();
			rd.close();

			// retrieve ad with latest updates and return
			return service.loadAd(adId);
			// } catch (MalformedURLException me) {
			// Log.e("mm", "upload exception: " + me.toString());
			// return null;
			// } catch (IOException ie) {
			// Log.e("mm", "Exception: " + ie.toString());
			// return null;
			// } catch (Exception e) {
			// Log.e("chat", "Exception: " + e.toString());
			// e.printStackTrace();
			// return null;
			// }
		} catch (Exception e) {
			Log.e("mm", "upload exception: " + e.toString());
			return null;
		}
	}

	public void setChecks(int id) {
		int i = 0;

		// set the checks against the maps
		switch (id) {
		case R.id.types:
			boolean[] checkz = checks.get(id);
			for (Entry<Type, Boolean> entry : facetTypes.entrySet())
				facetTypes.put(entry.getKey(), checkz[i++]);
			break;
		case R.id.makes:
			checkz = checks.get(id);
			for (Entry<Make, Boolean> entry : facetMakes.entrySet())
				facetMakes.put(entry.getKey(), checkz[i++]);
			break;
		case R.id.models:
			checkz = checks.get(id);
			for (Entry<Model, Boolean> entry : facetModels.entrySet())
				facetModels.put(entry.getKey(), checkz[i++]);
			break;
		default:
			break;
		}
	}

	/**
	 * Sets the selectable values in the query facet.
	 * 
	 * @param <T>
	 * @param facet
	 * @param values
	 */
	public <T> void setQuery(Map<T, Boolean> facet, List<T> values) {
		// retain the common values
		facet.keySet().retainAll(values);

		// add ones that have not been set
		for (T value : values) {
			if (facet.containsKey(value))
				// re-add existing values (to update any totals)
				facet.put(value, facet.remove(value));
			else
				// add in new value unchecked
				facet.put(value, false);
		}
	}

	// END RPC METHODS

	/**
	 * Login with <code>Account</code> provided by the
	 * <code>AccountManager</code>.
	 * 
	 * @param account
	 */
	public void login(/* Account account */int index) {
		// get account
		Account account = accounts[index];

		// perform authentication
		am.getAuthToken(account, getString(R.string.token_gae), false, new GetAuthTokenCallback(
				account), null);
	}

	/**
	 * Log out from currently logged in <code>Account</code>.
	 * 
	 * @return
	 */
	public boolean logout() {
		if (SyncProxy.logout()) {
			accountId = null;
			onDisconnect();
			return true;
		}

		return false;
	}

	/** accounts available login */
	private Account[] accounts;

	// /** labels for accounts */
	// private CharSequence[] accountLabels;

	/**
	 * Retrieve all <code>Account</code>s to provide to the user for login.
	 * 
	 * @param handler
	 */
	public void showLogin(final Handler handler) {
		// retrieve all available google accounts
		am.getAccountsByTypeAndFeatures(getString(R.string.acct_google), new String[] { /*
																						 * getString
																						 * (
																						 * R
																						 * .
																						 * string
																						 * .
																						 * svc_gae
																						 * )
																						 */},
				new AccountManagerCallback<Account[]>() {

					@Override
					public void run(final AccountManagerFuture<Account[]> result) {
						try {
							// // start a thread to poll the result
							// while (!result.isDone() && !result.isCancelled())
							// {
							// // run until the result is done or
							// // cancelled
							// }

							Account[] accounts = result.getResult();
							// Log.w("chat", "the accounts is " + accounts +
							// ", length is "
							// + (accounts == null ? "none" : accounts.length));
							if (accounts == null || accounts.length == 0) {
								// no accounts have been returned, invite user
								// to create account
								// showLogin(handler);
								android.os.Message msg = handler.obtainMessage();
								msg.what = MSG_ACCTS_NONE;
								msg.obj = am;
								handler.dispatchMessage(msg);
							} else {
								// capture the accounts for authentication
								RpcService.this.accounts = accounts;
								CharSequence[] labels = new CharSequence[accounts.length];
								int i = 0;
								String name = "";
								for (Account account : accounts) {
									name = account.name;
									int dot = name.lastIndexOf('.');

									// if name has an ending '.', use string up
									// until
									// character
									labels[i++] = dot == -1 ? name : name.substring(0, dot);
								}

								// result retrieved, pass back to the handler
								android.os.Message message = handler.obtainMessage(MSG_ACCTS);
								message.obj = labels;
								// message.obj = accounts;
								message.sendToTarget();
							}
						} catch (Exception e) {
							e.printStackTrace();

							// recurse the call
							showLogin(handler);
						}
					};
				}, handler);
	}

	/**
	 * Synchronize the parameter <code>bookmarks</code> with the rpc data.
	 * 
	 * @param bookmarks
	 */
	public void syncBookmarks(Set<Key<Car>> bookmarks) {
		bookmarks.clear();
		bookmarks.addAll(this.bookmarks);
	}

	/**
	 * Synchronize the parameter <code>inventory</code> with the rpc data.
	 * 
	 * @param inventory
	 * @return cursor
	 */
	public String syncClassified(List<Car> inventory) {
		// clear passed in inventory
		inventory.clear();

		// add all the results
		inventory.addAll(DataUtil.getSecondTupleList(this.inventory));

		// return cursor
		return inventoryCursor;
	}

	/**
	 * Synchronize the parameter <code>inventory</code> with the rpc data.
	 * 
	 * @param inventory
	 * @return cursor
	 */
	public String syncInventory(List<Tuple<Marker, Car>> inventory) {
		inventory.clear();
		inventory.addAll(this.inventory);
		return inventoryCursor;
	}

	/**
	 * Synchronize the parameter <code>dealers</code> with the rpc data.
	 * 
	 * @param dealers
	 * @return offset
	 */
	public int syncDealers(List<com.boosed.mm.shared.db.Account> dealers) {
		dealers.clear();
		dealers.addAll(this.dealers);
		return dealerOffset;
	}

	/**
	 * Synchronize the parameter <code>search</code> with the rpc data.
	 * 
	 * @param search
	 * @return offset
	 */
	public int syncSearch(List<Car> search) {
		search.clear();
		search.addAll(this.search);
		return searchOffset;
	}

	/**
	 * Synchronize the parameter <code>messages</code> with the rpc data.
	 * 
	 * @param messages
	 * @return cursor
	 */
	public String syncMessages(List<Message> messages) {
		messages.clear();
		messages.addAll(this.messages);
		return messageCursor;
	}

	/**
	 * Return all selected items in the <code>Map</code>.
	 * 
	 * @param <T>
	 * @param map
	 * @return empty list if nothing is selected
	 */
	private <T extends HasKey<T>> List<T> getSelected(Map<T, Boolean> map) {
		List<T> rv = new ArrayList<T>();
		for (Entry<T, Boolean> entry : map.entrySet())
			if (entry.getValue())
				rv.add(entry.getKey());

		return rv;
	}

	/**
	 * Return <code>Key</code>s for all selected items in the <code>Map</code>.
	 * 
	 * @param <T>
	 * @param map
	 * @return empty list if nothing is selected
	 */
	private <T extends HasKey<T>> List<Key<T>> getSelectedKeys(Map<T, Boolean> map) {
		List<Key<T>> rv = new ArrayList<Key<T>>();
		for (Entry<T, Boolean> entry : map.entrySet())
			if (entry.getValue())
				rv.add(entry.getKey().getKey());
		return rv;
	}

	/**
	 * Return the first selected item <code>T</code> in the <code>Map</code>.
	 * 
	 * @param <T>
	 * @param map
	 * @return null if nothing is selected
	 */
	private <T extends HasKey<T>> Key<T> getSingleSelectedKey(Map<T, Boolean> map) {
		for (Entry<T, Boolean> entry : map.entrySet())
			if (entry.getValue())
				return entry.getKey().getKey();
		return null;
	}

	/**
	 * Retrieves the selected <code>Type</code> criteria, <code>null</code> if
	 * nothing is selected. The selection also modifies any relevant query
	 * facets.
	 * 
	 * @return
	 */
	public Key<Type> getType() {
		// get the currently selected type
		Key<Type> type = getSingleSelectedKey(facetTypes);

		// set "make" facet according to type
		setQuery(facetMakes, DataUtil.getMakes(type, masterMakes));

		// use all makes if none were selected
		// if (values.isEmpty())
		// values.addAll(masterMakes);

		// get the currently selected makes
		List<Make> values = getSelected(facetMakes);

		// set "types" facet according to selected makes
		setQuery(facetTypes, DataUtil.getTypes(values.isEmpty() ? masterMakes : values));

		return type;
	}

	// /**
	// * Initialize all the services
	// */
	// private void initServices() {
	//
	// }

	/* timer for reconnects */
	private Timer timer = new Timer();

	/** the current wait period between connectivity checks */
	private int period = 0;

	/* maximum inactive period for determining connectivity */
	private static final int PERIOD_MAX = 320000;

	private TimerTask pollTask, syncTask;

	/** scheduled task for reconnecting to service */
	private class PollTask extends TimerTask {
		@Override
		public void run() {
			if (testConnection()) {
				// connection successful
				period = 0;
				onOnline();
			} else {
				if (period == 0)
					// init to 5 seconds
					period = 5000;
				else if (period < PERIOD_MAX)
					// adjust timer (this is around 2m40s)
					period <<= 1;

				// schedule another poll task for execution
				pollOnline();
			}
		}
	}

	/**
	 * Creates a new <code>PollTask</code> and schedules for execution.
	 */
	private void pollOnline() {
		pollTask = new PollTask();
		timer.schedule(pollTask, period);
		Log.i("mm", "checking server; next poll in " + period + " milliseconds");
	}

	/**
	 * Schedule sync updates against this <code>Account</code>.
	 * 
	 * @param account
	 */
	private void sync(final Account account) {
		// permit this account to have sync'd updates
		ContentResolver.setIsSyncable(account, authority, 1);

		// remove old sync task
		if (syncTask != null)
			syncTask.cancel();

		// task to request sync update
		syncTask = new TimerTask() {
			@Override
			public void run() {
				ContentResolver.requestSync(account, authority, new Bundle());
			}
		};

		// sync updates
		timer.schedule(syncTask, 0, SYNC_PERIOD);
	}

	/**
	 * Check if the server is available.
	 * 
	 * @return whether the server is available
	 */
	private boolean testConnection() {
		try {
			// Socket s = new Socket();
			// InetAddress inetAddress = InetAddress.getByName(HOST);
			// SocketAddress socketAddress = new InetSocketAddress(inetAddress,
			// 80);
			// s.connect(socketAddress, 5000);
			// s.close();

			return true;
		} catch (Exception e) {
			e.printStackTrace();

			return false;
		}
	}

	/**
	 * Private callback class for authenticating an account against GAE.
	 * 
	 * @author admin
	 * 
	 */
	private class GetAuthTokenCallback implements AccountManagerCallback<Bundle> {

		// the account to authenticate against
		private Account account;

		public GetAuthTokenCallback(Account account) {
			this.account = account;
		}

		public void run(AccountManagerFuture<Bundle> result) {
			try {
				// check if future has returned an intent to be processed (e.g.,
				// grant creds)
				final Bundle bundle = result.getResult();
				Intent intent = (Intent) bundle.get(AccountManager.KEY_INTENT);
				if (intent != null) {
					// broadcast disconnected to current activity
					broadcast(MSG_RPC_DISCONNECT);

					Log.w("mm", Integer.toHexString(intent.getFlags()));
					// User input required, will this kill activity once it
					// returns?
					// Log.w("mm", "starting the intent " + intent.getFlags() +
					// " " + Intent.FLAG_ACTIVITY_NEW_TASK);
					// getApplication().startActivity(intent);

					// problem with HTC Evo, will this fix it? (starting
					// activity from service w/ no task context)
					startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
				} else
					// start task to login using auth token
					new RpcTask<Boolean>() {

						private String authToken;

						@Override
						public Boolean doit() throws RemoteServiceFailureException {
							// get the token
							authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);

							// attempt to login with the token
							String loginUrl = URL;
							String logoutUrl = URL;

							loginUrl += "/_ah/login?continue=" + loginUrl + "&auth=" + authToken;
							logoutUrl += "/_ah/logout?continue=" + logoutUrl + "&service=ah";
							// return
							// SyncProxy.login("boosedcrowds.appspot.com",
							// authToken);

							// set the login provider
							SyncProxy.setAndroidLogin(loginUrl, logoutUrl);

							// perform the login
							return SyncProxy.login();// login(authToken);
						}

						public void onFailure(int errResId) {
							Toast.makeText(RpcService.this, getText(errResId), Toast.LENGTH_SHORT)
									.show();

							// did not log in successfully
							// broadcast(MSG_RPC_LOGINFAIL);
							// onDisconnect();
						}

						@Override
						public void onSuccess(Boolean t) {
							if (t) {
								// initialize the user
								new RpcTask<String>(RpcService.this, null) {
									@Override
									public String doit() throws RemoteServiceFailureException {
										return service.initialize();
									}

									@Override
									public void onFailure(int errResId) {
										Toast.makeText(RpcService.this, /*
																		 * getText(
																		 * errResId
																		 * )
																		 */"failed",
												Toast.LENGTH_SHORT).show();

										// did not log in successfully
										// broadcast(MSG_RPC_LOGINFAIL);
										// onDisconnect();
									}

									@Override
									public void onSuccess(String t) {
										if (t == null)
											// did not initialize successfully,
											// token likely expired
											invalidateToken(authToken);
										else {
											// sync account
											sync(account);

											// set the account id for this user
											accountId = t;

											// reset init variables
											bookmarks.clear();

											// broadcast connectivity
											onConnect();
										}
									}
								}.execute();
							} else {
								// did not log in successfully
								broadcast(MSG_RPC_LOGINFAIL);
								onDisconnect();

								// try to swap out token
								invalidateToken(authToken);
							}
						}
					}.execute();
			} catch (OperationCanceledException e) {
				Log.w("mm", "login operation was canceled");
				e.printStackTrace();
			} catch (AuthenticatorException e) {
				Log.w("mm", "could not access authenticator");
				e.printStackTrace();
			} catch (IOException e) {
				Log.w("mm", "could not connect to service");
				e.printStackTrace();
			}
		}

		/**
		 * Method to invalidate current <code>authToken</code> and retrieve a
		 * new one.
		 * 
		 * @param authToken
		 */
		private void invalidateToken(String authToken) {
			// unsuccessful login, swap out token by invalidating current one
			am.invalidateAuthToken(getString(R.string.acct_google), authToken);

			// try again
			am.getAuthToken(account, getString(R.string.token_gae), false,
					new GetAuthTokenCallback(account), null);
		}
	}

	// END PRIVATE
}