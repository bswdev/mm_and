/*
 * Copyright (C) 2010 The Android Open Source Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.boosed.mm.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.boosed.mm.rpc.RpcService;

/**
 * SyncAdapter implementation for syncing sample SyncAdapter contacts to the
 * platform ContactOperations provider.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

	// private static final String TAG = "SyncAdapter";
	//
	// private final AccountManager mAccountManager;
	//
	// private final Context mContext;
	//
	// private Date mLastUpdated;

	private final RpcService rpc;

	public SyncAdapter(Context context, boolean autoInitialize, RpcService rpc) {
		super(context, autoInitialize);
		// mContext = context;
		// mAccountManager = AccountManager.get(context);
		this.rpc = rpc;
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider,
			SyncResult syncResult) {

		try {
			// messages, invoke THROUGH rpc so notices may be sent
			//Tuple<Long, Integer> rv = rpc.service.loadCount();
			//Log.w("chat", "performing a network sync, number of unread messages: " + rv.b);
			
			// call update on rpc to retrieve message status
			rpc.onUpdate(account);
		} catch (Exception e) {
			Log.w("mm", "could not perform sync/update " + e.getClass());
		}
		// List<User> users;
		// List<Status> statuses;
		// String authtoken = null;
		// try {
		// // use the account manager to request the credentials
		// authtoken =
		// mAccountManager
		// .blockingGetAuthToken(account, Constants.AUTHTOKEN_TYPE, true /*
		// notifyAuthFailure */);
		// // fetch updates from the sample service over the cloud
		// users = NetworkUtilities.fetchFriendUpdates(account, authtoken,
		// mLastUpdated);
		// // update the last synced date.
		// mLastUpdated = new Date();
		// // update platform contacts.
		// Log.d(TAG, "Calling contactManager's sync contacts");
		// ContactManager.syncContacts(mContext, account.name, users);
		// // fetch and update status messages for all the synced users.
		// statuses = NetworkUtilities.fetchFriendStatuses(account, authtoken);
		// ContactManager.insertStatuses(mContext, account.name, statuses);
		// } catch (final AuthenticatorException e) {
		// syncResult.stats.numParseExceptions++;
		// Log.e(TAG, "AuthenticatorException", e);
		// } catch (final OperationCanceledException e) {
		// Log.e(TAG, "OperationCanceledExcetpion", e);
		// } catch (final IOException e) {
		// Log.e(TAG, "IOException", e);
		// syncResult.stats.numIoExceptions++;
		// } catch (final AuthenticationException e) {
		// mAccountManager.invalidateAuthToken(Constants.ACCOUNT_TYPE,
		// authtoken);
		// syncResult.stats.numAuthExceptions++;
		// Log.e(TAG, "AuthenticationException", e);
		// } catch (final ParseException e) {
		// syncResult.stats.numParseExceptions++;
		// Log.e(TAG, "ParseException", e);
		// } catch (final JSONException e) {
		// syncResult.stats.numParseExceptions++;
		// Log.e(TAG, "JSONException", e);
		// }
	}
}