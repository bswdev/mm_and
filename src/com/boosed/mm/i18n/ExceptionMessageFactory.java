package com.boosed.mm.i18n;

import java.io.FileNotFoundException;

import android.util.Log;

import com.boosed.mm.R;
import com.boosed.mm.shared.exception.IncompleteException;
import com.boosed.mm.shared.exception.InvalidLocationException;
import com.boosed.mm.shared.exception.NotFoundException;
import com.boosed.mm.shared.exception.OwnerException;
import com.boosed.mm.shared.exception.PrimaryImageException;
import com.boosed.mm.shared.exception.ResourceLimitException;
import com.boosed.mm.shared.exception.SessionTimeoutException;
import com.boosed.mm.shared.exception.UnauthorizedException;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;

public class ExceptionMessageFactory {

	private static ExceptionMessageFactory factory;

	private ExceptionMessageFactory() {
		// private constructor
	}

	public static ExceptionMessageFactory getInstance() {
		if (factory == null)
			factory = new ExceptionMessageFactory();

		return factory;
	}

	public int getMessage(Throwable caught) {
		try {
			throw caught;
		} catch (SessionTimeoutException e) {
			// the user is not authenticated
			return R.string.exception_session_timeout;
		} catch (NotFoundException e) {
			// the entity was not found
			return R.string.exception_not_found;
		} catch (ResourceLimitException e) {
			// number of allowed resources was exceeded
			return R.string.exception_resource_limit;
		} catch (IncompleteException e) {
			// form was incomplete
			return R.string.exception_incomplete;
		} catch (InvalidLocationException e) {
			// location was not found
			return R.string.exception_invalid_location;
		} catch (OwnerException e) {
			// user attempting action against own items (e.g., bookmarking)
			return R.string.exception_owner;
		} catch (PrimaryImageException e) {
			// tried to delete the primary image
			return R.string.exception_primary_image;
		} catch (UnauthorizedException e) {
			// user not authorized to perform action;
			return R.string.exception_unauthorized;
		} catch (IncompatibleRemoteServiceException e) {
			// client not compatible with the server; cleanup and refresh the
			// browser
			return R.string.exception_rpc_incompatible;
		} catch (InvocationException e) {
			// the call didn't complete cleanly, update the client
			try {
				cause(e);
				return R.string.exception_rpc_invocation;
			} catch (IncompatibleRemoteServiceException irs) {
				// why is this thrown here? update client
				return R.string.exception_rpc_incompatible;
			} catch (FileNotFoundException fnf) {
				// update client
				return R.string.exception_rpc_incompatible;
				// } catch (SocketException s) {
			} catch (Throwable t) {
				// couldn't connect (includes SocketExceptin)
				return R.string.exception_rpc_invocation;
			}
		} catch (Throwable t) {
			// unknown error thrown in service layer; remote service failure
			return R.string.exception_rpc_failure;
		}
	}

	private void cause(Throwable t) throws Throwable {
		if (t.getCause() == null) {
			Log.w("mm", "invalid invocation: " + t.getClass().toString());
			throw t;
		} else
			cause(t.getCause());
	}
}