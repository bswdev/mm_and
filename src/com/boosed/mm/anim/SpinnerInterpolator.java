package com.boosed.mm.anim;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Interpolator;

public class SpinnerInterpolator implements Interpolator {

	public SpinnerInterpolator() {
	}

	public SpinnerInterpolator(Context context, AttributeSet attrs) {
	}

	@Override
	public float getInterpolation(float input) {
		// translate to 12 distinct states
//		if (input <= 0.25f) {
//			return input;
//		} else if (input <= 0.50f) {
//			return 0.5f - input;
//		} else if (input <= 0.75f) {
//			return 1.5f - input;
//		} else
//			return input;
			
		return ((int) (input * 8)) * 0.125f;
		// return ((int) Math.floor(input * 12)) * .08333f;
		// int parts = (int) Math.floor(input * 12);
		// return parts * 0.0833f;
	}
}